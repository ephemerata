/**
 *
 */
package kezvh.primitive;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author afflux
 *
 */
public class Primitive {
    private static LinkedList<BigInteger> pfacts = new LinkedList<BigInteger>();
    private static LinkedList<BigInteger> primes = new LinkedList<BigInteger>();

    private static enum Case {
        FACTORIAL, PRIMORIAL, EXPONENTIAL
    }

    private static final Case type = Case.PRIMORIAL;

    private static final BigInteger increment(final BigInteger i) {
        switch (Primitive.type) {
            case FACTORIAL:
                return i.add(BigInteger.ONE);
            case PRIMORIAL:
                return i.nextProbablePrime();
            case EXPONENTIAL:
                return i.add(i);
            default:
                throw new RuntimeException();
        }
    }

    static {
        final BigInteger max = new BigInteger("5000");

        for (BigInteger cpf = BigInteger.valueOf(2), i = BigInteger.valueOf(2); i.compareTo(max) < 0; i = Primitive.increment(i), cpf = cpf.multiply(i)) {
            Primitive.pfacts.addFirst(cpf);
            Primitive.primes.addFirst(i);
        }

        System.out.println("number of pfacts: " + Primitive.pfacts.size());
        System.out.println("bits in largest element: " + Primitive.primes.getFirst().toString(2).length());
        System.out.println("bits in largest pfact: " + Primitive.pfacts.getFirst().toString(2).length());
    }

    private static Map<Integer, Character> rad = new HashMap<Integer, Character>() {
        {
            final char[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?".toCharArray();
            for (int i = 0; i < chars[i]; i++)
                this.put(i, chars[i]);
        }
    };

    /**
     * @param args x
     */
    public static void main(final String... args) {
        Primitive.six();
    }

    /**
     *
     */
    public static void two() {
        //        final Random r = new Random(0);
        BigInteger foo = new BigInteger("1").nextProbablePrime(); // 686
        for (int i = 0; i < 100; i++) {
            System.out.println(Primitive.format(foo));
            foo = foo.nextProbablePrime(); // 686
        }
        System.out.println(Primitive.format(foo));
    }

    /**
     *
     */
    public static void zero() {
        final Random r = new Random();
        System.out.println(Primitive.format(new BigInteger(500, r).nextProbablePrime())); // .nextProbablePrime()
    }

    /**
     *
     */
    public static void pointFive() {
        //        final Random r = new Random();
        //        BigInteger baz = new BigInteger(70, r).nextProbablePrime();
        BigInteger baz = new BigInteger("1107336284477113445057");
        System.out.println(baz.bitLength());
        final LinkedList<BigInteger> dfs = Primitive.dfs(baz);
        if (dfs == null) {
            System.out.println("null...");
            return;
        }
        final Iterator<BigInteger> i = dfs.descendingIterator();
        i.next();
        while (i.hasNext()) {
            final BigInteger next = i.next();
            final BigInteger fact = baz.subtract(next);
            final int o = Primitive.pfacts.indexOf(fact);
            if (o == -1)
                throw new RuntimeException(fact + " is not a factorial...? next = " + next + ", baz = " + baz);
            System.out.println(baz + " - " + (Primitive.pfacts.size() - o + 1) + "! = " + next);
            baz = next;
        }
        System.out.println(dfs);
    }

    /**
     *
     */
    public static void three() {
        System.out.println(Primitive.format(Primitive.pfacts.get(100).nextProbablePrime()));

        System.out.println(Primitive.format(Primitive.pfacts.get(99).nextProbablePrime()));

        System.out.println(Primitive.format(Primitive.pfacts.get(98).nextProbablePrime()));
    }

    /**
     *
     */
    public static void one() {
        final Random r = new Random();
        //        final BigInteger foo = new BigInteger("59");
        final BigInteger bar = new BigInteger(80, r).nextProbablePrime();
        final BigInteger foo = new BigInteger(80, r).nextProbablePrime();
        //        final BigInteger bar = new BigInteger("509");
        final BigInteger baz = foo.multiply(bar);
        System.out.println(baz.toString(2).length());

        //        System.out.print(foo + ": ");
        //        System.out.println(Primitive.format(foo));
        //        System.out.print(bar + ": ");
        //        System.out.println(Primitive.format(bar));
        System.out.println(baz + ": ");
        System.out.println(Primitive.format(baz));
    }

    /**
     *
     */
    public static void four() {
        final Random r = new Random(System.nanoTime() + System.currentTimeMillis());

        int j = 0;
        int i = 0;
        for (i = 0; i < 100; i++) {
            final BigInteger baz = new BigInteger(70, r).nextProbablePrime();
            final List<BigInteger> dfs = Primitive.dfs(baz);
            if (dfs != null)
                j++;
        }

        System.out.println(j + " / " + i);
        //        System.out.println(baz);
        //        if (dfs != null)
        //            System.out.println(dfs);
        //        else
        //            System.out.println("not");
    }

    /**
     *
     */
    public static void five() {
        BigInteger bucket = BigInteger.valueOf(4);
        int bits = 2;
        int counts = 0, hits = 0;
        for (BigInteger i = BigInteger.valueOf(3); true; i = i.nextProbablePrime()) {
            if (i.compareTo(bucket) >= 0) {
                System.out.println(bits + ": " + hits + " / " + counts + " = " + (double) hits / counts);
                bucket = bucket.shiftLeft(1);
                hits = counts = 0;
                bits++;
            }
            counts++;
            if (Primitive.hasPropertyOfDifference(i))
                hits++;
        }
    }

    /**
     *
     */
    public static void six() {
        final Random r = new Random();
        for (int i = 100; i < 101; i++) {
            int hits = 0;
            for (int j = 0; j < 100; j++) {
                final BigInteger bi = new BigInteger(10 * i, r).nextProbablePrime();
                if (Primitive.hasPropertyOfDifference(bi))
                    hits++;
            }
            System.out.println(i * 10 + ": " + hits + " / " + 100);
        }
    }

    /**
     *
     */
    public static void seven() {
        final Random r = new Random();
        final BigInteger bi = new BigInteger(100, r).nextProbablePrime();
        final int[] partsb = Primitive.moogle2(bi);

        for (int i = 1; i < partsb.length; i++) {
            final int tmp = partsb[i];
            partsb[i] = 0;
            final BigInteger pp = Primitive.unmoogle(partsb);
            if (pp.isProbablePrime(100))
                System.out.println(i);
            partsb[i] = tmp;
        }

        System.out.println("done");
    }

    /**
     *
     */
    public static void eight() {
        final Random r = new Random();
        final BigInteger bi = new BigInteger(200, r).nextProbablePrime();
        final int[] partsb = Primitive.moogle2(bi);
        System.out.println(bi);
        System.out.println(Arrays.toString(partsb));
        final List<BigInteger> hmm = Primitive.reach(partsb);
        System.out.println(hmm);

    }

    /**
     *
     */
    public static void nine() {
        final Random r = new Random();
        final BigInteger bi1 = new BigInteger(501, r).nextProbablePrime();
        final BigInteger bi2 = new BigInteger(501, r).nextProbablePrime();
        final BigInteger bi = bi1.multiply(bi2);
        final BigInteger bi3 = Primitive.squareRoot(bi);
        System.out.println(bi);
        System.out.println(bi3.multiply(bi3));
        System.out.println(bi3.bitLength());

    }

    private static BigInteger squareRoot(final BigInteger bi) {
        final BigInteger two = BigInteger.valueOf(2);
        final BigInteger n = bi;
        BigInteger i = BigInteger.ONE;
        BigInteger g;
        do {
            g = i;
            final BigInteger f = n.divide(g);
            final BigInteger h = f.add(g);
            i = h.divide(two);
        } while (!g.subtract(i).equals(BigInteger.ZERO));
        return g;
    }

    private static List<BigInteger> reach(final int[] partsb) {

        for (int i = 0; i < partsb.length - 1; i++) {
            if (partsb[i] == 0)
                continue;
            final int tmp = partsb[i];
            while (partsb[i] > 0) {
                partsb[i]--;
                final BigInteger pp = Primitive.unmoogle(partsb);
                if (BigInteger.ONE.equals(pp))
                    return new LinkedList<BigInteger>() {
                        {
                            this.add(BigInteger.ONE);
                        }
                    };

                if (pp.isProbablePrime(100)) {
                    final List<BigInteger> maybe = Primitive.reach(partsb);
                    if (maybe != null) {
                        maybe.add(pp);
                        return maybe;
                    }
                }
            }
            partsb[i] = tmp;
        }
        return null;
    }

    private static int[] moogle2(final BigInteger bi) {
        BigInteger divisor = bi.add(BigInteger.ZERO);

        final int[] values = new int[Primitive.primes.size()];
        int j = 0;
        final Iterator<BigInteger> i = Primitive.primes.descendingIterator();
        while (i.hasNext()) {
            final BigInteger prime = i.next();
            final BigInteger[] divs = divisor.divideAndRemainder(prime);
            values[j] = divs[1].intValue();
            divisor = divs[0];
            j++;
        }

        int x = values.length - 1;
        while (values[x] == 0)
            x--;

        final int[] values2 = new int[x + 1];
        for (int y = 0; x >= 0; y++, x--)
            values2[y] = values[x];

        return values2;
    }

    private static BigInteger unmoogle(final int[] remainders) {
        BigInteger sum = BigInteger.valueOf(remainders[remainders.length - 1]);
        final Iterator<BigInteger> i = Primitive.pfacts.descendingIterator();
        for (int j = remainders.length - 2; j >= 0; j--) {
            final BigInteger pfact = i.next();
            sum = sum.add(pfact.multiply(BigInteger.valueOf(remainders[j])));
        }
        return sum;
    }

    private static boolean hasPropertyOfDifference(final BigInteger bi) {
        final Iterator<BigInteger> i = Primitive.pfacts.descendingIterator();
        while (i.hasNext()) {
            final BigInteger n = i.next();
            if (n.compareTo(bi) >= 0)
                return false;
            if (bi.subtract(n).isProbablePrime(100))
                return true;
        }
        throw new RuntimeException();
    }

    /**
     * @param bi
     * @return x
     */
    public static String format(final BigInteger bi) {
        return Primitive.format(bi, " ");
    }

    /**
     *
     */
    public static final BigInteger THREE = BigInteger.valueOf(3);

    private static LinkedList<BigInteger> dfs(final BigInteger b) {
        final Iterator<BigInteger> i = Primitive.pfacts.descendingIterator();
        while (i.hasNext()) {
            final BigInteger pfact = i.next();
            //        for (int i = 0; i < Primitive.pfacts.size(); i++) {
            //            final BigInteger pfact = Primitive.pfacts.get(Primitive.pfacts.size() - i - 1);
            if (b.compareTo(pfact) < 0)
                continue;

            final BigInteger next = b.subtract(pfact);
            if (next.equals(BigInteger.ONE))
                return new LinkedList<BigInteger>() {
                    {
                        this.add(BigInteger.ONE);
                        this.add(b);
                    }
                };
            if (next.isProbablePrime(100)) {
                final LinkedList<BigInteger> possible = Primitive.dfs(next);
                if (possible != null) {
                    possible.add(b);
                    return possible;
                }
            }
        }
        return null;
    }

    /**
     * @param bi
     * @param delim
     * @return dogs
     */
    public static String format(final BigInteger bi, final String delim) {
        BigInteger current = bi.abs();
        final StringBuilder sb = new StringBuilder();

        boolean hasvalues = false;
        int i = Primitive.pfacts.size() + 1;
        if (!"".equals(delim))
            System.out.println("original is " + bi.isProbablePrime(100));
        for (final BigInteger pfact: Primitive.pfacts) {
            i--;
            int x = 0;
            while (current.compareTo(pfact) >= 0) {
                x++;
                current = current.subtract(pfact);
            }
            if (x != 0)
                hasvalues = true;
            if (hasvalues) {
                if (!"".equals(delim)) {
                    final List<Integer> haszx = new LinkedList<Integer>();
                    final List<Integer> haszm = new LinkedList<Integer>();

                    for (int o = 1; o <= i + 1; o++)
                        if (current.subtract(Primitive.pfacts.get(Primitive.pfacts.size() - o)).isProbablePrime(100))
                            haszx.add(0, o);

                    BigInteger blargh = current.abs();
                    //                    System.out.println(Primitive.primes.get(Primitive.pfacts.size() - i).intValue());
                    //                    System.out.println(pfact + " " + Primitive.primes.get(Primitive.pfacts.size() - i));
                    for (int q = 0; q <= Primitive.primes.get(Primitive.pfacts.size() - i).intValue(); q++) {
                        if (blargh.isProbablePrime(100))
                            haszm.add(q);
                        if (!blargh.equals(bi) && !"".equals(delim) && !BigInteger.ONE.equals(blargh) && !BigInteger.ZERO.equals(blargh) && BigInteger.ZERO.equals(bi.remainder(blargh)))
                            System.out.println("= " + blargh + " * " + bi.divide(blargh));
                        blargh = blargh.add(pfact);
                    }
                    System.out.println(haszm);
                    System.out.println(haszx);
                    System.out.println("- " + x + " * p" + i + "# is " + current.isProbablePrime(100));
                }
                if (Primitive.rad.containsKey(x))
                    sb.append(Primitive.rad.get(x) + delim);
                else
                    sb.append(Primitive.format(BigInteger.valueOf(x), "") + delim);
            }

        }
        sb.append(Primitive.rad.get(current.intValue()));
        return sb.toString();
    }
}
