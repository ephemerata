/**
 *
 */
package kezvh.net.boggler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import net.kezvh.collections.AbstractIterator;

/**
 * @author afflux
 *
 */
public class Boggler {
    private static final class Coordinate<T extends Comparable<T>> implements Comparable<Coordinate<T>> {
        final T x;
        final T y;

        public Coordinate(final T x, final T y) {
            this.x = x;
            this.y = y;
        }

        /**
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         * @param o
         * @return x
         */
        @Override
        public int compareTo(final Coordinate<T> o) {
            final int a = this.x.compareTo(o.x);
            if (a != 0)
                return a;
            return this.y.compareTo(o.y);
        }

        /**
         * @see java.lang.Object#hashCode()
         * @return x
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (this.x == null ? 0 : this.x.hashCode());
            result = prime * result + (this.y == null ? 0 : this.y.hashCode());
            return result;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         * @param obj
         * @return x
         */
        @Override
        public boolean equals(final Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (this.getClass() != obj.getClass())
                return false;
            final Coordinate<?> other = (Coordinate<?>) obj;
            if (this.x == null) {
                if (other.x != null)
                    return false;
            } else if (!this.x.equals(other.x))
                return false;
            if (this.y == null) {
                if (other.y != null)
                    return false;
            } else if (!this.y.equals(other.y))
                return false;
            return true;
        }
    }

    private static final class BogglerArgs {
        private final File dictFile;
        private final BoggleBoard boggleBoard;
        private final int minimumWordSize;

        public BogglerArgs(final String... args) {
            this.dictFile = new File(args[0]);
            this.boggleBoard = new BoggleBoard(args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
            this.minimumWordSize = Integer.parseInt(args[4]);
        }

        public InputStream getDictionaryStream() throws IOException {
            return new FileInputStream(this.dictFile);
        }

        public BoggleBoard getBoard() {
            return this.boggleBoard;
        }

        public int getMinimumWordLength() {
            return this.minimumWordSize;
        }
    }

    private static final class BoggleBoard {
        private static final char[][] cubes = new char[][] { new char[] { 'a', 'a', 'c', 'i', 'o', 't' }, new char[] { 'd', 'e', 'n', 'o', 's', 'w' }, new char[] { 'a', 'b', 'i', 'l', 't', 'y' }, new char[] { 'd', 'k', 'n', 'o', 't', 'u' }, new char[] { 'a', 'b', 'j', 'm', 'o', 'q' }, new char[] { 'e', 'e', 'f', 'h', 'i', 'y' }, new char[] { 'a', 'c', 'd', 'e', 'm', 'p' }, new char[] { 'e', 'g', 'i', 'n', 't', 'v' }, new char[] { 'a', 'c', 'e', 'l', 's', 'r' }, new char[] { 'e', 'g', 'k', 'l', 'u', 'y' }, new char[] { 'a', 'd', 'e', 'n', 'v', 'z' }, new char[] { 'e', 'h', 'i', 'n', 'p', 's' }, new char[] { 'a', 'h', 'm', 'o', 'r', 's' }, new char[] { 'e', 'l', 'p', 's', 't', 'u' }, new char[] { 'b', 'f', 'i', 'o', 'r', 'x' }, new char[] { 'g', 'i', 'l', 'r', 'u', 'w' }, };
        private final char[][] chars;

        /**
         * @see java.lang.Object#toString()
         * @return asdf
         */
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            for (final char[] c: this.chars) {
                for (final char element: c)
                    sb.append(element);
                sb.append("\n");
            }
            return sb.toString();
        }

        public static BoggleBoard random() {
            final List<Integer> indexes = new LinkedList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)); // I have a range class, but i need to update it first
            final Random r = new Random();
            final StringBuffer sb = new StringBuffer(16);
            while (!indexes.isEmpty()) {
                final char[] cube = BoggleBoard.cubes[indexes.remove(r.nextInt(indexes.size()))];
                sb.append(cube[r.nextInt(6)]);
            }
            return new BoggleBoard(sb.toString(), 4, 4);
        }

        public BoggleBoard(final String values, final int width, final int height) {
            this.chars = new char[height][];
            for (int i = 0; i < height; i++) {
                this.chars[i] = new char[width];
                for (int j = 0; j < width; j++)
                    this.chars[i][j] = values.charAt(i * width + j);
            }
        }

        public char charAt(final int x, final int y) {
            return this.chars[y][x];
        }

        public int getWidth() {
            return this.chars[0].length;
        }

        public int getHeight() {
            return this.chars.length;
        }

        public Iterable<CharSequence> iterateAt(final int x, final int y, final int minimumWordLength, final BoggleTrie trie) {
            return new BoggleBoardSet(x, y, minimumWordLength, trie);
        }

        private final class BoggleBoardSet extends AbstractSet<CharSequence> {
            private final int x;
            private final int y;
            private final int minimumWordLength;
            private final BoggleTrie trie;

            /**
             * @param minimumWordLength
             * @param x
             * @param y
             * @param trie
             */
            public BoggleBoardSet(final int minimumWordLength, final int x, final int y, final BoggleTrie trie) {
                super();
                this.minimumWordLength = minimumWordLength;
                this.x = x;
                this.y = y;
                this.trie = trie;
            }

            /**
             * @see java.util.AbstractCollection#iterator()
             * @return x
             */
            @Override
            public Iterator<CharSequence> iterator() {
                return new BoggleBoardSetIterator();
            }

            /**
             * @see java.util.AbstractCollection#size()
             * @return size
             */
            @Override
            public int size() {
                throw new UnsupportedOperationException();
            }

            private final class BoggleBoardSetIterator extends AbstractIterator<CharSequence> {
                private final LinkedList<Coordinate<Integer>> currentSequence = new LinkedList<Coordinate<Integer>>();
                private final StringBuffer currentString = new StringBuffer();
                private final LinkedList<LinkedList<Coordinate<Integer>>> availableSteps = new LinkedList<LinkedList<Coordinate<Integer>>>();
                private final LinkedList<BoggleTrie> subDicts = new LinkedList<BoggleTrie>();

                {
                    final Coordinate<Integer> origin = new Coordinate<Integer>(BoggleBoardSet.this.x, BoggleBoardSet.this.y);
                    this.currentSequence.add(new Coordinate<Integer>(BoggleBoardSet.this.x, BoggleBoardSet.this.y));
                    this.availableSteps.add(this.findSteps(origin, this.currentSequence));
                    this.currentString.append(BoggleBoard.this.chars[BoggleBoardSet.this.y][BoggleBoardSet.this.x]);
                    this.subDicts.add(BoggleBoardSet.this.trie.subTrie(BoggleBoard.this.chars[BoggleBoardSet.this.y][BoggleBoardSet.this.x]));
                }

                private LinkedList<Coordinate<Integer>> findSteps(final Coordinate<Integer> location, final Collection<Coordinate<Integer>> occupied) {
                    final LinkedList<Coordinate<Integer>> steps = new LinkedList<Coordinate<Integer>>();
                    for (int dx = -1; dx <= 1; dx++)
                        for (int dy = -1; dy <= 1; dy++)
                            if (dx == 0 && dy == 0 || location.x + dx < 0 || location.x + dx >= BoggleBoard.this.getWidth() || location.y + dy < 0 || location.y + dy >= BoggleBoard.this.getHeight())
                                continue;
                            else {
                                final Coordinate<Integer> potentialStep = new Coordinate<Integer>(location.x + dx, location.y + dy);
                                if (!occupied.contains(potentialStep))
                                    steps.add(potentialStep);
                            }
                    return steps;
                }

                /**
                 * @see net.kezvh.collections.AbstractIterator#findNext()
                 * @return
                 * @throws NoSuchElementException
                 */
                @Override
                protected CharSequence findNext() throws NoSuchElementException {
                    while (!this.availableSteps.isEmpty())
                        if (this.availableSteps.getLast().isEmpty()) {
                            this.availableSteps.removeLast();
                            this.currentSequence.removeLast();
                            this.currentString.deleteCharAt(this.currentString.length() - 1);
                            this.subDicts.removeLast();
                        } else {
                            final Coordinate<Integer> nextStep = this.availableSteps.getLast().removeLast();
                            final char nextChar = BoggleBoard.this.chars[nextStep.y][nextStep.x];

                            if (this.subDicts.getLast().containsStartingWith(nextChar)) {
                                this.currentSequence.add(nextStep);
                                this.availableSteps.add(this.findSteps(nextStep, this.currentSequence));
                                this.currentString.append(nextChar);
                                this.subDicts.add(this.subDicts.getLast().subTrie(nextChar));
                                if (this.subDicts.getLast().isComplete() && this.currentString.length() >= BoggleBoardSet.this.minimumWordLength)
                                    return this.currentString.toString();
                            }
                            continue;

                        }
                    throw new NoSuchElementException();
                }
            }
        }
    }

    /**
     * TODO parameterize so you can specify you want the board to be random or read in as needed.
     *
     * @param args dictfile board with height maxwordlength
     * @throws Exception your mom
     */
    public static void main(final String... args) throws Exception {
        final BogglerArgs bogglerArgs = new BogglerArgs(args);

        //        final BoggleBoard board = BoggleBoard.random();
        final BoggleBoard board = bogglerArgs.boggleBoard;
        final LineNumberReader dictionaryReader = new LineNumberReader(new InputStreamReader(bogglerArgs.getDictionaryStream()));

        final ExecutorService executor = Executors.newFixedThreadPool(1);
        final FutureTask<BoggleTrie> ft = new FutureTask<BoggleTrie>(new Callable<BoggleTrie>() {
            /**
             * @see java.util.concurrent.Callable#call()
             * @return x
             * @throws Exception
             */
            @Override
            public BoggleTrie call() throws Exception {
                final long start = System.nanoTime();
                try {
                    return new BoggleTrie(dictionaryReader);
                } finally {
                    System.out.println("took " + (System.nanoTime() - start));
                }
            }
        });
        executor.execute(ft);

        System.out.println(board);
        System.in.read();

        final BoggleTrie dictionary = ft.get();

        final Collection<String> words = Boggler.findWords(board, dictionary, bogglerArgs.getMinimumWordLength());
        for (final String word: words)
            System.out.println(word);

        executor.shutdown();
    }

    private static Collection<String> findWords(final BoggleBoard boggleBoard, final BoggleTrie dictionary, final int minimumWordLength) {
        final Set<String> words = new TreeSet<String>();

        final long start = System.nanoTime();
        for (int y = 0; y < boggleBoard.getHeight(); y++)
            for (int x = 0; x < boggleBoard.getWidth(); x++)
                for (final CharSequence word: boggleBoard.iterateAt(minimumWordLength, x, y, dictionary))
                    words.add(word.toString());
        System.out.println("took " + (System.nanoTime() - start) + " ns");

        return words;
    }
}
