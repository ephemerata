/**
 *
 */
package kezvh.net.boggler;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author afflux
 *
 */
public final class BoggleTrie {
    private boolean complete = false;
    private final Map<Character, BoggleTrie> childrenByInitialChar = new TreeMap<Character, BoggleTrie>();

    private BoggleTrie() {
        // do nothing
    }

    /**
     * @param dictionary
     * @throws IOException stuff
     */
    public BoggleTrie(final LineNumberReader dictionary) throws IOException {
        this.complete = false;
        while (dictionary.ready())
            this.add(dictionary.readLine());
    }

    private void add(final CharSequence word) {
        if (word.length() == 0)
            this.complete = true;
        else {
            final char initial = word.charAt(0);
            if (!this.childrenByInitialChar.containsKey(initial))
                this.childrenByInitialChar.put(initial, new BoggleTrie());
            this.childrenByInitialChar.get(initial).add(word.subSequence(1, word.length()));
        }
    }

    /**
     * @param c FIXME COMMENT
     * @return FIXME COMMENT
     */
    public boolean containsStartingWith(final char c) {
        if (c == 'q' && this.childrenByInitialChar.containsKey('q'))
            return this.childrenByInitialChar.get('q').containsStartingWith('u');

        return this.childrenByInitialChar.containsKey(c);
    }

    /**
     * @param next FIXME COMMENT
     * @return FIXME COMMENT
     */
    public BoggleTrie subTrie(final char next) {
        if (next == 'q' && this.childrenByInitialChar.containsKey('q'))
            return this.childrenByInitialChar.get('q').subTrie('u');

        return this.childrenByInitialChar.get(next);
    }

    /**
     * @return FIXME COMMENT
     */
    public boolean isComplete() {
        return this.complete;
    }
}