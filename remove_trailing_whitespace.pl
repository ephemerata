#!/usr/bin/perl

use strict;
use warnings;

sub do_dir($);
sub do_file($);

if ($#ARGV >= 0) {
do_dir($ARGV[0]);
}

#do_dir('.');

exit(0);

sub do_dir($) {
  my ($dirname) = @_;
  my $dh;
	opendir $dh, $dirname;

  while (my $fname = readdir $dh) {

	  if ($fname =~ /\.$/ or $fname =~ /^\./) {
		  next;
    }

		$fname = "$dirname/$fname";
		if (-d $fname) {
      do_dir($fname);
		} elsif (-f $fname) {
      do_file($fname);
		}
	}

	closedir $dh;
}

sub do_file($) {
  my ($fname) = @_;
  print "$fname\n";
	my $fh;
	{
	  local $/;
		open $fh, $fname or do {
		  print "failed open for reading\n";
			return;
		};

		my $contents = <$fh>; #slurp!
		close $fh;
		#$contents =~ s/[ 	]+($)/\n/gm;
		$contents =~ s/\x0d\x0a/\n/gs;
		$contents =~ s/[ 	]+($)/$1/gm;
#    print "-----------------\n";
#		print $contents;
#    print "-----------------\n\n";
		open $fh, ">$fname" or do {
		  print "failed open for reading\n";
			return;
		};
		print $fh $contents;
		close $fh;
	}
}
