/**
 *
 */
package kezvh.symboljunky;

/**
 * @author afflux
 *
 * things like symmetry (rotational, axial, translational), intersections, line count, orientation of lines, style of lines, etc
 */
public final class SymbolPropertyType {
  private final int symbolPropertyTypeId;
  private final String symbolPropertyTypeName;
  private final SymbolPropertyType superType;

  /**
   * @param symbolPropertyTypeId
   * @param symbolPropertyTypeName
   */
  public SymbolPropertyType(int symbolPropertyTypeId, String symbolPropertyTypeName) {
    super();
    this.symbolPropertyTypeId = symbolPropertyTypeId;
    this.symbolPropertyTypeName = symbolPropertyTypeName;
    this.superType = null;
  }

  /**
   * @param symbolPropertyTypeId
   * @param symbolPropertyTypeName
   * @param superType
   */
  public SymbolPropertyType(int symbolPropertyTypeId, String symbolPropertyTypeName, SymbolPropertyType superType) {
    super();
    this.symbolPropertyTypeId = symbolPropertyTypeId;
    this.symbolPropertyTypeName = symbolPropertyTypeName;
    this.superType = superType;
  }

  /**
   * @return true if this type has a super type
   */
  public boolean hasSuperType() {
    return this.superType != null;
  }

  /**
   * @return the superType
   */
  public SymbolPropertyType getSuperType() {
    return this.superType;
  }

  /**
   * @return the symbolPropertyTypeId
   */
  public int getSymbolPropertyTypeId() {
    return this.symbolPropertyTypeId;
  }

  /**
   * @return the symbolPropertyTypeName
   */
  public String getSymbolPropertyTypeName() {
    return this.symbolPropertyTypeName;
  }
}
