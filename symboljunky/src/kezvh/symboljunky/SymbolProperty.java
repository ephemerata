/**
 *
 */
package kezvh.symboljunky;

/**
 * @author afflux
 *
 * if type is, say, rotational symmetry, need this to indicate something like "4"
 */
public interface SymbolProperty {
  /**
   *
   * @return FIXME
   */
  SymbolPropertyType getSymbolPropertyType();

  /**
   *
   * @return FIXME
   */
  String getSymbolPropertyDescription();
}
