/**
 *
 */
package kezvh.symboljunky;

import java.io.OutputStream;

/**
 * @author afflux
 *
 * things like ascii, unicode, image, ...
 */
public interface Representation {
  /**
   *
   * @return type of representation
   */
  RepresentationType getRepresentationType();

  /**
   *
   * @return a pointer to the actual representation (an image, a string, etc)
   */
  Object getRepresentation();

  /**
   *
   * @return a human readable verseion of the representation
   */
  String getRenderedRepresentation();

  /**
   *
   * @param os stream onto which the representation will be written
   */
  void printRenderedRepresentation(OutputStream os);
}
