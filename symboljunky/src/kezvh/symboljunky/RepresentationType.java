/**
 *
 */
package kezvh.symboljunky;

/**
 * @author afflux
 *
 */
public final class RepresentationType {
  private final long representationTypeId;
  private final String representationTypeName;

  /**
   * @param representationTypeId
   * @param representationTypeName
   */
  public RepresentationType(long representationTypeId, String representationTypeName) {
    super();
    this.representationTypeId = representationTypeId;
    this.representationTypeName = representationTypeName;
  }

  /**
   * @return the representationTypeId
   */
  public long getRepresentationTypeId() {
    return this.representationTypeId;
  }
  /**
   * @return the representationTypeName
   */
  public String getRepresentationTypeName() {
    return this.representationTypeName;
  }
}
