/**
 *
 */
package kezvh.symboljunky;

import java.io.Serializable;

/**
 * @author afflux
 *
 */
public final class Tag implements Serializable {
  private final String tag;

  /**
   *
   * @param tag
   */
  public Tag(String tag) {
    this.tag = tag;
  }

  /**
   *
   * @return the tag
   */
  String getTag() {
    return this.tag;
  }
}
