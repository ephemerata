/**
 *
 */
package kezvh.symboljunky;

import java.util.Set;

/**
 * @author afflux
 *
 */
public interface Symbol {
  /**
   *
   * @return id for this symbol
   */
  long getSymbolId();

  /**
   *
   * @return symbols marked as "related"
   */
  Set<Symbol> getRelated();

  /**
   *
   * @return tags for this symbol
   */
  Set<Tag> getTags();
}
