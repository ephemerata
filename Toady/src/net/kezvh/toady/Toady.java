/**
 *
 */
package net.kezvh.toady;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

/**
 * @author afflux
 *
 */
public class Toady {

    private static int dot;
    private static int mark;

    private interface ToadyAction {
        String toString();
    }

    private static final class AreaDeletedToadyAction implements ToadyAction {
        /**
         * @param dot
         * @param mark
         */
        public AreaDeletedToadyAction(final int dot, final int mark) {
            super();
            this._dot = dot;
            this._mark = mark;
        }

        private final int _dot;
        private final int _mark;

        /**
         * @see java.lang.Object#toString()
         * @return FIXME
         */
        @Override
        public String toString() {
            return "<ToadyAction>" + "<dot>" + this._dot + "</dot>" + "<mark>" + this._mark + "</mark>" + "</ToadyAction>";
        }
    }

    private static final class KeyPressedToadyAction implements ToadyAction {
        private final char c;
        private final int index;

        public KeyPressedToadyAction(final char c, final int index) {
            this.c = c;
            this.index = index;
        }

        /**
         * @see java.lang.Object#toString()
         * @return c
         */
        @Override
        public String toString() {
            return "<ToadyAction>" + "<key>" + Character.toString(this.c) + "</key>" + "<index>" + this.index + "</index>" + "</ToadyAction>";
        }
    }

    private static final List<ToadyAction> sequenceOfEdits = new ArrayList<ToadyAction>();

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final JFrame frame = new JFrame("Toady");

        final JMenuBar menuBar;
        final JMenu fileMenu, editMenu;
        final JMenu submenu;
        final JMenuItem saveMenuItem, openMenuItem, undoMenuItem;
        final JRadioButtonMenuItem rbMenuItem;
        final JCheckBoxMenuItem cbMenuItem;

        //Create the menu bar.
        menuBar = new JMenuBar();

        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_A);
        fileMenu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");
        menuBar.add(fileMenu);

        saveMenuItem = new JMenuItem("Save", KeyEvent.VK_S);
        saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.ALT_MASK));
        saveMenuItem.getAccessibleContext().setAccessibleDescription("Save");
        saveMenuItem.setAction(new Action() {
            /**
             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
             * @param e
             */
            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser chooser = new JFileChooser();

                // if you ahve a suggested file name to start with, use that

                final int response = chooser.showSaveDialog(frame);
                if (response == JFileChooser.APPROVE_OPTION) {// user clicked Save
                    final File file = chooser.getSelectedFile();
                    try {
                        final FileWriter fileWriter = new FileWriter(file);
                        for (final ToadyAction toadyAction: Toady.sequenceOfEdits)
                            fileWriter.write(toadyAction.toString() + "\n");
                        fileWriter.close();

                    } catch (final IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                } else {
                    // user cancelled...
                }
            }

            private boolean enabled = true;

            @Override
            public void addPropertyChangeListener(final PropertyChangeListener listener) {
                // TODO Auto-generated method stub

            }

            @Override
            public Object getValue(final String key) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public boolean isEnabled() {
                // TODO Auto-generated method stub
                return this.enabled;
            }

            @Override
            public void putValue(final String key, final Object value) {
                // TODO Auto-generated method stub

            }

            @Override
            public void removePropertyChangeListener(final PropertyChangeListener listener) {
                // TODO Auto-generated method stub

            }

            @Override
            public void setEnabled(final boolean b) {
                // TODO Auto-generated method stub
                this.enabled = true;
            }
        });
        fileMenu.add(saveMenuItem);

        openMenuItem = new JMenuItem("Open", KeyEvent.VK_O);
        openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.ALT_MASK));
        openMenuItem.getAccessibleContext().setAccessibleDescription("Open");
        fileMenu.add(openMenuItem);

        editMenu = new JMenu("Edit");
        editMenu.setMnemonic(KeyEvent.VK_E);
        editMenu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");
        menuBar.add(editMenu);

        undoMenuItem = new JMenuItem("Undo", KeyEvent.VK_Z);
        undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.ALT_MASK));
        undoMenuItem.getAccessibleContext().setAccessibleDescription("Undo");
        editMenu.add(undoMenuItem);

        frame.setJMenuBar(menuBar);

        final JTextArea textArea = new JTextArea(5, 20);
        textArea.setLineWrap(true);

        textArea.addCaretListener(new CaretListener() {
            /**
              * @see javax.swing.event.CaretListener#caretUpdate(javax.swing.event.CaretEvent)
              * @param e
              */
            @Override
            public void caretUpdate(final CaretEvent e) {
                // TODO Auto-generated method stub
                Toady.dot = e.getDot();
                Toady.mark = e.getMark();
            }
        });

        textArea.addKeyListener(new KeyListener() {
            /**
              * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
              * @param e
              */
            @Override
            public void keyPressed(final KeyEvent e) {
                // nooop
            }

            /**
                 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
                 * @param e
                 */
            @Override
            public void keyReleased(final KeyEvent e) {
                // nooop
            }

            /**
                 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
                 * @param e
                 */
            @Override
            public void keyTyped(final KeyEvent e) {
                if ((e.getModifiers() & (InputEvent.ALT_DOWN_MASK | InputEvent.CTRL_DOWN_MASK)) != 0)
                    return;

                final JTextArea component = (JTextArea) e.getComponent();
                if (Toady.dot != Toady.mark)
                    Toady.sequenceOfEdits.add(new AreaDeletedToadyAction(Toady.dot, Toady.mark));
                Toady.sequenceOfEdits.add(new KeyPressedToadyAction(e.getKeyChar(), Toady.dot));
            }
        });

        final JScrollPane scrollPane = new JScrollPane(textArea);

        frame.getContentPane().add(scrollPane);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        // TODO Auto-generated method stub
    }
}
