/**
 *
 */
package net.kezvh.toady;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import net.kezvh.collections.trees.ListRefTree;
import net.kezvh.collections.trees.Tree;
import net.kezvh.development.UnimplementedException;
import net.kezvh.ui.menu.SpringMenu;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * @author afflux
 *
 */
public class Toady2 {

    private static StringBuilder contents = new StringBuilder();
    private static final JTextArea textArea = new JTextArea(5, 20);
    private static long lastChangeTime = System.nanoTime();

    private static final ReentrantLock lock = new ReentrantLock();

    private static final Object MENU = new Object[] { "File", new Object[] { "New", new Object[] { KeyEvent.VK_N, NewActionListener.class }, "Open", new Object[] { KeyEvent.VK_O, OpenActionListener.class }, "Save", new Object[] { KeyEvent.VK_S, SaveActionListener.class, }, }, "Edit", new Object[] { "Undo", new Object[] { KeyEvent.VK_Z, UndoActionListener.class, }, "Replay", new Object[] { KeyEvent.VK_R, ReplayActionListener.class, }, "Reset Timer", new Object[] { KeyEvent.VK_U, ResetActionListener.class, }, }, };

    private static final Tree<EditToadyAction> sequenceOfEdits = new ListRefTree<EditToadyAction>(new EditToadyAction("", "", 0, 0));
    private static Tree<EditToadyAction> currentAction = Toady2.sequenceOfEdits;
    private static final JFrame frame = new JFrame("Toady");

    private static boolean saved = true;

    /**
     * @author afflux
     *
     */
    private static final class TodayTextMouseListener implements MouseListener {
        /**
          * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
          * @param e
          */
        @Override
        public void mouseClicked(final MouseEvent e) {
            Toady2.update();
        }

        /**
             * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
             * @param e
             */
        @Override
        public void mouseEntered(final MouseEvent e) {
            // noop
        }

        /**
             * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
             * @param e
             */
        @Override
        public void mouseExited(final MouseEvent e) {
            // noop

        }

        /**
             * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
             * @param e
             */
        @Override
        public void mousePressed(final MouseEvent e) {
            Toady2.update();
        }

        /**
             * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
             * @param e
             */
        @Override
        public void mouseReleased(final MouseEvent e) {
            Toady2.update();
        }
    }

    /**
     * @author afflux
     *
     */
    private static final class ToadyTextKeyListener implements KeyListener {
        /**
          * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
          * @param e
          */
        @Override
        public void keyPressed(final KeyEvent e) {
            Toady2.update();
        }

        /**
             * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
             * @param e
             */
        @Override
        public void keyReleased(final KeyEvent e) {
            Toady2.update();
        }

        /**
             * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
             * @param e
             */
        @Override
        public void keyTyped(final KeyEvent e) {
            Toady2.update();
        }
    }

    /**
     * @author afflux
     *
     */
    public static final class ReplayActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         * @param e
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            synchronized (Toady2.lock) {
                Toady2.lock.lock();
            }
            final StringBuilder currentContents = new StringBuilder();
            Toady2.textArea.setEditable(false);
            Toady2.textArea.setText("");

            final Iterator<EditToadyAction> iterator = Toady2.sequenceOfEdits.iterator();
            Toady2.timedReplay(iterator, currentContents);
        }
    }

    /**
     * @author afflux
     *
     */
    public static final class ResetActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         * @param e
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            Toady2.lastChangeTime = System.nanoTime();
        }
    }

    /**
     * @author afflux
     *
     */
    public static final class UndoActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         * @param e
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            Toady2.rewind();
            Toady2.lastChangeTime = System.nanoTime();
        }
    }

    private static void rewind() {
        if (Toady2.sequenceOfEdits.size() == 0)
            return;
        final EditToadyAction last = Toady2.sequenceOfEdits.remove(Toady2.sequenceOfEdits.size() - 1);
        System.out.println(last);
        Toady2.contents.replace(last.getStartPos(), last.getEndPos() + last.getEditContents().length(), last.getPreviousContents());
        Toady2.textArea.setText(Toady2.contents.toString());
    }

    /**
     * @author afflux
     *
     */
    public static final class OpenActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         * @param e
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            final JFileChooser chooser = new JFileChooser();

            // if you have a suggested file name to start with, use that

            final int response = chooser.showOpenDialog(Toady2.frame);
            if (response == JFileChooser.APPROVE_OPTION) {// user clicked Save
                final File file = chooser.getSelectedFile();
                try {
                    final LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(new FileInputStream(file)));

                    Toady2.sequenceOfEdits.clear();
                    for (String line = lineNumberReader.readLine(); line != null; line = lineNumberReader.readLine())
                        if (line.startsWith("<ToadyAction>"))
                            Toady2.sequenceOfEdits.add(new EditToadyAction(line));
                        else {
                            Toady2.contents = new StringBuilder(StringEscapeUtils.unescapeJava(line));
                            Toady2.textArea.setText(Toady2.contents.toString());
                        }

                } catch (final IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                if (!Toady2.contents.equals(Toady2.replay()))
                    throw new RuntimeException(Toady2.replay());

            } else {
                // user cancelled...
            }
        }
    }

    /**
     * @author afflux
     *
     */
    public static final class SaveActionListener implements ActionListener {
        /**
          * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
          * @param e
          */
        @Override
        public void actionPerformed(final ActionEvent e) {
            Toady2.save();
        }
    }

    /**
     * @author afflux
     *
     */
    public static final class NewActionListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         * @param e
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            if (!Toady2.saved && Toady2.promptToSave() && !Toady2.save())
                return;

            Toady2.contents.delete(0, Toady2.contents.length());
            Toady2.textArea.setText("");
            Toady2.sequenceOfEdits.clear();
        }
    }

    private static boolean promptToSave() {
        throw new UnimplementedException();
    }

    /**
     * @return true if the file is sucessfully saved
     */
    private static boolean save() {
        final JFileChooser chooser = new JFileChooser();

        // if you have a suggested file name to start with, use that

        final int response = chooser.showSaveDialog(Toady2.frame);
        if (response == JFileChooser.APPROVE_OPTION) {// user clicked Save
            final File file = chooser.getSelectedFile();
            try {
                final FileWriter fileWriter = new FileWriter(file);
                for (final EditToadyAction toadyAction: Toady2.sequenceOfEdits)
                    fileWriter.write(toadyAction.toString() + "\n");
                fileWriter.write(StringEscapeUtils.escapeJava(Toady2.contents.toString()));
                fileWriter.write("\n");
                fileWriter.close();

            } catch (final IOException e1) {
                e1.printStackTrace(); // TODO do something with this
                return false;
            }

            return true;
        } else
            return false;
    }

    private interface ToadyAction {
        String toString();
    }

    private static EditToadyAction diff(final String newString) {
        int startPos = 0;
        int endPos = 0;

        while (newString.length() != startPos && Toady2.contents.length() != startPos && Toady2.contents.charAt(startPos) == newString.charAt(startPos))
            startPos++;

        while (newString.length() != startPos + endPos && Toady2.contents.length() != startPos + endPos && Toady2.contents.charAt(Toady2.contents.length() - 1 - endPos) == newString.charAt(newString.length() - 1 - endPos))
            endPos++;

        if (Toady2.contents.length() == newString.length() && startPos + endPos == newString.length())
            return null; // no change

        if (newString.length() - endPos < 0)
            throw new RuntimeException("error; end pos = " + endPos + " and string is " + newString.length());

        if (Toady2.contents.length() - endPos < 0)
            throw new RuntimeException("end position precedes beginning of string: " + endPos + ", " + Toady2.contents.length());

        if (startPos < 0)
            throw new RuntimeException("??????");

        if (newString.equals(Toady2.contents) && startPos != endPos)
            throw new RuntimeException(newString + ": " + startPos + ", " + endPos);

        if (startPos > newString.length() - endPos)
            throw new RuntimeException(startPos + ", " + (newString.length() - endPos) + " : " + Toady2.contents + " - " + newString);

        final long duration;
        final long now = System.nanoTime();
        if (Toady2.lastChangeTime == -1)
            duration = 0;
        else
            duration = System.nanoTime() - Toady2.lastChangeTime;
        Toady2.lastChangeTime = now;
        return new EditToadyAction(newString.substring(startPos, newString.length() - endPos), Toady2.contents.substring(startPos, Toady2.contents.length() - endPos), startPos, Toady2.contents.length() - endPos, duration);
    }

    private static final class EditToadyAction implements ToadyAction {
        /**
         * @return the previousContents
         */
        public String getPreviousContents() {
            return this.previousContents;
        }

        /**
         * @return the duration
         */
        public long getDuration() {
            return this.duration;
        }

        /**
         * @return the editContents
         */
        public String getEditContents() {
            return this.editContents;
        }

        /**
         * @return the startPos
         */
        public int getStartPos() {
            return this.startPos;
        }

        /**
         * @param editContents
         * @param previousContents
         * @param startPos
         * @param duration
         */
        public EditToadyAction(final String editContents, final String previousContents, final int startPos, final long duration) {
            super();
            this.editContents = editContents;
            this.previousContents = previousContents;
            this.startPos = startPos;
            this.duration = duration;
        }

        private final Pattern p = Pattern.compile("<ToadyAction><start>(.*)</start><contents>(.*)</contents><previous>(.*)</previous><duration>(.*)</duration></ToadyAction>");

        public EditToadyAction(final String s) {
            final Matcher m = this.p.matcher(s);
            if (!m.matches())
                throw new RuntimeException();
            this.startPos = Integer.valueOf(m.group(1));
            this.editContents = StringEscapeUtils.unescapeJava(StringEscapeUtils.unescapeXml(m.group(2)));
            this.previousContents = StringEscapeUtils.unescapeJava(StringEscapeUtils.unescapeXml(m.group(3)));
            this.duration = Long.valueOf(m.group(4));

        }

        private final String editContents;
        private final String previousContents;
        private final int startPos;
        private final long duration;

        /**
         * @see java.lang.Object#toString()
         * @return x
         */
        @Override
        public String toString() {
            return "<ToadyAction><start>" + this.startPos + "</start><contents>" + StringEscapeUtils.escapeJava(StringEscapeUtils.escapeXml(this.editContents)) + "</contents>" + StringEscapeUtils.escapeJava(StringEscapeUtils.escapeXml(this.previousContents)) + "<previous><duration>" + this.duration + "</duration></ToadyAction>";
        }
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        System.setProperty("sun.awt.exception.handler", AwtHandler.class.getName());
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler());

        final JMenuBar menuBar = new SpringMenu(Toady2.MENU);

        Toady2.frame.setJMenuBar(menuBar);

        Toady2.textArea.setLineWrap(true);

        Toady2.textArea.addKeyListener(new ToadyTextKeyListener());

        Toady2.textArea.addMouseListener(new TodayTextMouseListener());

        final JScrollPane scrollPane = new JScrollPane(Toady2.textArea);

        Toady2.frame.getContentPane().add(scrollPane);

        Toady2.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Toady2.frame.pack();
        Toady2.frame.setVisible(true);
    }

    private static void timedReplay(final Iterator<EditToadyAction> iterator, final StringBuilder currentContents) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (iterator.hasNext())
                    synchronized (Toady2.sequenceOfEdits) {
                        final EditToadyAction editToadyAction = iterator.next();
                        try {
                            final long duration = editToadyAction.getDuration() / 10;
                            if (duration == 0)
                                Toady2.sequenceOfEdits.wait(0, 1);
                            else {
                                final long milis = duration / 1000000;
                                final int nanos = (int) (duration % 1000000);
                                Toady2.sequenceOfEdits.wait(milis, nanos);
                            }

                        } catch (final InterruptedException e1) {
                            // nothing should interrupt us
                        }
                        currentContents.replace(editToadyAction.getStartPos(), editToadyAction.getEndPos(), editToadyAction.getEditContents());
                        Toady2.textArea.setText(currentContents.toString());
                        Toady2.timedReplay(iterator, currentContents);
                    }
                else {
                    if (!Toady2.contents.toString().equals(currentContents.toString()))
                        throw new RuntimeException("NOT EQUAL\n'" + Toady2.contents + "'\nvs\n'" + currentContents + "'");

                    synchronized (Toady2.lock) {
                        Toady2.lock.unlock();
                    }

                    Toady2.textArea.setEditable(true);
                    Toady2.lastChangeTime = System.nanoTime();

                }

            }
        });
    }

    private static void update() {
        synchronized (Toady2.lock) {
            if (Toady2.lock.isHeldByCurrentThread())
                return; // just give up
        }

        final String newText = Toady2.textArea.getText();

        final EditToadyAction editToadyAction = Toady2.diff(newText);
        if (editToadyAction == null)
            return;

        Toady2.saved = false;

        Toady2.contents.delete(0, Toady2.contents.length());
        Toady2.contents.append(newText);

        Toady2.sequenceOfEdits.add(editToadyAction);

    }

    private static class AwtHandler {
        public void handle(final Throwable t) {
            JOptionPane.showMessageDialog(null, ExceptionUtils.getStackTrace(t), "Error", 1);
        }
    }

    private static class MyExceptionHandler implements Thread.UncaughtExceptionHandler {

        public void uncaughtException(final Thread t, final Throwable e) {
            if (SwingUtilities.isEventDispatchThread())
                this.showException(t, e);
            else
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        MyExceptionHandler.this.showException(t, e);
                    }
                });
        }

        private void showException(final Thread t, final Throwable e) {
            final String msg = String.format("Unexpected problem on thread %s: %s\n%s", t.getName(), e.getMessage(), ExceptionUtils.getStackTrace(e));

            this.logException(t, e);

            // note: in a real app, you should locate the currently focused frame
            // or dialog and use it as the parent. In this example, I'm just passing
            // a null owner, which means this dialog may get buried behind
            // some other screen.
            JOptionPane.showMessageDialog(null, msg);
        }

        private void logException(final Thread t, final Throwable e) {
            // todo: start a thread that sends an email, or write to a log file, or
            // send a JMS message...whatever
        }
    }

    private static String replay() {
        final StringBuilder sb = new StringBuilder();
        for (final EditToadyAction editToadyAction: Toady2.sequenceOfEdits)
            sb.replace(editToadyAction.getStartPos(), editToadyAction.getEndPos(), editToadyAction.getEditContents());
        return sb.toString();
    }
}
