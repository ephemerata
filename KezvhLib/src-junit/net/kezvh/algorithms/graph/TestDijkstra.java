package net.kezvh.algorithms.graph;

import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.heaps.Heap;
import net.kezvh.patterns.AbstractFactory;

/**
 * @author mjacob
 *
 */
public class TestDijkstra extends TestSingleSourceShortestPath {

	@Override
	protected SingleSourceShortestPath<Integer> getSingleSourceShortedPath(final DirectedGraphWithEdgeValues<Integer, Integer> graph, final Integer source, final AbstractFactory<Heap<Integer, Integer>> heapMaker) {
		return new Dijkstra<Integer>(graph, source, heapMaker);
	}

}
