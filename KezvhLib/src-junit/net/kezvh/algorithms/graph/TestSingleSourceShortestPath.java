package net.kezvh.algorithms.graph;

import java.util.Comparator;

import junit.framework.Assert;
import junit.framework.TestCase;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValuesBuilder;
import net.kezvh.collections.graphs.directed.GraphWithEdgeValuesTemplate;
import net.kezvh.collections.graphs.directed.HashDirectedGraphSetBuilder;
import net.kezvh.collections.heaps.Heap;
import net.kezvh.collections.heaps.PairingHeap;
import net.kezvh.patterns.AbstractFactory;

import org.junit.Test;

/**
 * @author mjacob
 */
public abstract class TestSingleSourceShortestPath extends TestCase {
	private final DirectedGraphWithEdgeValuesBuilder GRAPH_BUILDER = new HashDirectedGraphSetBuilder();
	private final AbstractFactory<Heap<Integer, Integer>> HEAP_MAKER = new AbstractFactory<Heap<Integer, Integer>>() {
		@Override
		public Heap<Integer, Integer> create() throws net.kezvh.patterns.AbstractFactory.CreationFailedException {
			return new PairingHeap<Integer, Integer>(new Comparator<Integer>() {
				public int compare(final Integer o1, final Integer o2) {
					if (o1.equals(-1)) {
						if (o2.equals(-1))
							return 0;
						return 1;
					} else if (o2.equals(-1))
						return -1;
					else
						return o1.compareTo(o2);
				}
			});
		}
	};

	/**
	 * @param graph graph
	 * @param source source node
	 * @param heapMaker something to make a heap
	 * @return all pairs shortest path object for queries
	 */
	protected abstract SingleSourceShortestPath<Integer> getSingleSourceShortedPath(final DirectedGraphWithEdgeValues<Integer, Integer> graph, final Integer source, final AbstractFactory<Heap<Integer, Integer>> heapMaker);

	/**
	 * @throws Exception might throw an exception
	 */
	@Test
	public void testCreate() throws Exception {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.GRAPH_BUILDER.buildGraph(GraphWithEdgeValuesTemplate.TRIVIAL);
		final SingleSourceShortestPath<Integer> it = this.getSingleSourceShortedPath(graph, 1, this.HEAP_MAKER);
		Assert.assertNotNull(it);
	}

	/**
	 * @throws Exception might throw an exception
	 */
	@Test
	public void testTrivial() throws Exception {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.GRAPH_BUILDER.buildGraph(GraphWithEdgeValuesTemplate.TRIVIAL);
		final SingleSourceShortestPath<Integer> it = this.getSingleSourceShortedPath(graph, 1, this.HEAP_MAKER);
		final Path<Integer> path = it.getShortestPath(1);
		Assert.assertNotNull(path);
		Assert.assertEquals(1, path.size());
		Assert.assertEquals(0, path.getLength());
	}

	/**
	 * @throws Exception might throw an exception
	 */
	@Test
	public void testSimple() throws Exception {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.GRAPH_BUILDER.buildGraph(GraphWithEdgeValuesTemplate.SIMPLE);
		final SingleSourceShortestPath<Integer> it = this.getSingleSourceShortedPath(graph, 1, this.HEAP_MAKER);

		Assert.assertTrue(it.connected(2));

		final Path<Integer> path = it.getShortestPath(1);
		Assert.assertNotNull(path);
		Assert.assertEquals(1, path.size());
		Assert.assertEquals(0, path.getLength());
		final Path<Integer> path2 = it.getShortestPath(2);
		Assert.assertNotNull(path2);
		Assert.assertEquals(path2.toString(), 2, path2.size());
		Assert.assertEquals(1, path2.getLength());
	}

	/**
	 * @throws Exception might throw an exception
	 */
	@Test
	public void testComplete() throws Exception {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.GRAPH_BUILDER.buildGraph(GraphWithEdgeValuesTemplate.COMPLETE);
		final SingleSourceShortestPath<Integer> it = this.getSingleSourceShortedPath(graph, 1, this.HEAP_MAKER);

		for (int i = 0; i < 16; i++) {
			Assert.assertTrue(it.connected(i));
			final Path<Integer> path = it.getShortestPath(i);
			Assert.assertNotNull(path);
			if (i == 1) {
				Assert.assertEquals(i + " " + path.size() + " " + path, 1, path.size());
				Assert.assertEquals(i + " " + path.size() + " " + path, 0, path.getLength());
			} else {
				Assert.assertEquals(i + " ", 2, path.size());
				Assert.assertEquals(i + " ", 1, path.getLength());
			}
		}
	}

	/**
	 * @throws Exception might throw an exception
	 */
	@Test
	public void testLinear() throws Exception {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.GRAPH_BUILDER.buildGraph(GraphWithEdgeValuesTemplate.LINEAR);
		final SingleSourceShortestPath<Integer> it = this.getSingleSourceShortedPath(graph, 0, this.HEAP_MAKER);

		for (int i = 0; i < 100; i++) {
			Assert.assertTrue("node " + i + " in graph + ", graph.contains(i));
			if (i != 99)
				Assert.assertTrue("edge " + i + " " + (i + 1) + " is in graph", graph.containsEdge(i, i + 1));
			Assert.assertTrue("node " + i + " not connected?", it.connected(i));
			final Path<Integer> path = it.getShortestPath(i);
			Assert.assertNotNull(path);
			Assert.assertEquals(i + " ", i + 1, path.size());
			Assert.assertEquals(i + " ", i, path.getLength());
		}
	}
}
