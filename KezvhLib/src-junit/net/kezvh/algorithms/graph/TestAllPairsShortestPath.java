package net.kezvh.algorithms.graph;

import junit.framework.TestCase;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.heaps.Heap;
import net.kezvh.patterns.AbstractFactory;

/**
 * @author mjacob
 *
 */
public abstract class TestAllPairsShortestPath extends TestCase {
	/**
	 * @param <T> node type
	 * @param graph graph
	 * @param heapMaker something to make a heap
	 * @return all pairs shortest path object for queries
	 */
	protected abstract <T> AllPairsShortestPath<T> getAllPairsShortedPath(final DirectedGraphWithEdgeValues<T, Integer> graph, final AbstractFactory<Heap<T, Integer>> heapMaker);
}
