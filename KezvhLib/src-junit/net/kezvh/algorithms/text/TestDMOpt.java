package net.kezvh.algorithms.text;

/**
 * @author mjacob
 *
 */
public class TestDMOpt extends TestDamerauLevenshteinDistance {

	@Override
	protected EditDistance getEd() {
		return EditDistance.DAMERAU_LEVENSHTEIN_OPTIMIZED1;
	}

}
