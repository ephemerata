/**
 *
 */
package net.kezvh.algorithms.text;

/**
 * @author afflux
 *
 */
public class TestBerghelRoach extends TestDamerauLevenshteinDistance {
	@Override
	protected EditDistance getEd() {
		return EditDistance.BERGHEL_ROACH;
	}
}
