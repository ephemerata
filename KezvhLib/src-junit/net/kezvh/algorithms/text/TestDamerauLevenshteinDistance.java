package net.kezvh.algorithms.text;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author mjacob
 *
 */
public abstract class TestDamerauLevenshteinDistance extends TestCase {
	/**
	 * @return stuff
	 */
	protected abstract EditDistance getEd();

	/**
	 *
	 */
	@Test
	public void test__() {
		Assert.assertEquals(0, (int) this.getEd().op("", ""));
	}

	/**
	 *
	 */
	@Test
	public void test_O_O() {
		Assert.assertEquals(0, (int) this.getEd().op("O", "O"));
	}

	/**
	 *
	 */
	@Test
	public void test_OT_OT() {
		Assert.assertEquals(0, (int) this.getEd().op("OT", "OT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OTO_OTO() {
		Assert.assertEquals(0, (int) this.getEd().op("OTO", "OTO"));
	}

	/**
	 *
	 */
	@Test
	public void test_TO_OT() {
		Assert.assertEquals(1, (int) this.getEd().op("TO", "OT"));
	}

	/**
	 *
	 */
	@Test
	public void test_O_OT() {
		Assert.assertEquals(1, (int) this.getEd().op("O", "OT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OO_OOT() {
		Assert.assertEquals(1, (int) this.getEd().op("OO", "OOT"));
	}

	/**
	 *
	 */
	@Test
	public void test_ball_balls() {
		Assert.assertEquals(1, (int) this.getEd().op("ball", "balls"));
	}

	/**
	 *
	 */
	@Test
	public void test_alls_balls() {
		Assert.assertEquals(1, (int) this.getEd().op("alls", "balls"));
	}

	/**
	 *
	 */
	@Test
	public void test_bals_balls() {
		Assert.assertEquals(1, (int) this.getEd().op("bals", "balls"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_ball() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "ball"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_alls() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "alls"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_bals() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "bals"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_calls() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "calls"));
	}

	/**
	 *
	 */
	@Test
	public void test_abcd_bcda() {
		Assert.assertEquals(2, (int) this.getEd().op("abcd", "bcda"));
	}

	/**
	 *
	 */
	@Test
	public void test_abcd_cdab() {
		Assert.assertEquals(4, (int) this.getEd().op("abcd", "cdab"));
	}

	/**
	 *
	 */
	@Test
	public void test_abcde_fghij() {
		Assert.assertEquals(5, (int) this.getEd().op("abcde", "fghij"));
	}

	/**
	 *
	 */
	@Test
	public void test_abcd_fghi() {
		Assert.assertEquals(4, (int) this.getEd().op("abcd", "fghi"));
	}

	/**
	 *
	 */
	@Test
	public void test_abc_fgh() {
		Assert.assertEquals(3, (int) this.getEd().op("abc", "fgh"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_ballk() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "ballk"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_ablls() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "ablls"));
	}

	/**
	 *
	 */
	@Test
	public void test_OST_OT() {
		Assert.assertEquals(1, (int) this.getEd().op("OST", "OT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OT_OST() {
		Assert.assertEquals(1, (int) this.getEd().op("OT", "OST"));
	}

	/**
	 *
	 */
	@Test
	public void test__O() {
		Assert.assertEquals(1, (int) this.getEd().op("", "O"));
	}

	/**
	 *
	 */
	@Test
	public void test_T_OS() {
		Assert.assertEquals(2, (int) this.getEd().op("T", "OS"));
	}

	/**
	 *
	 */
	@Test
	public void test_TO_OST() {
		Assert.assertEquals(3, (int) this.getEd().op("TO", "OST"));
	}

	/**
	 *
	 */
	@Test
	public void test_OOO_OOOT() {
		Assert.assertEquals(1, (int) this.getEd().op("OOO", "OOOT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OOO_OOOOT() {
		Assert.assertEquals(2, (int) this.getEd().op("OOO", "OOOOT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OO_OOOT() {
		Assert.assertEquals(2, (int) this.getEd().op("OO", "OOOT"));
	}

	/**
	 *
	 */
	@Test
	public void test_OOOT_OO() {
		Assert.assertEquals(2, (int) this.getEd().op("OOOT", "OO"));
	}

	/**
	 *
	 */
	@Test
	public void test_OT_TO() {
		Assert.assertEquals(1, (int) this.getEd().op("OT", "TO"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_abcde_acdef() {
		Assert.assertEquals(2, (int) this.getEd().op("abcde", "acdef"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_ADCROFT_ADDESSI() {
		Assert.assertEquals(5, (int) this.getEd().op("ADCROFT", "ADDESSI"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_BAIRD_BAISDEN() {
		Assert.assertEquals(3, (int) this.getEd().op("BAIRD", "BAISDEN"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_BOGGAN_BOGGS() {
		Assert.assertEquals(2, (int) this.getEd().op("BOGGAN", "BOGGS"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_CLAYTON_CLEARY() {
		Assert.assertEquals(5, (int) this.getEd().op("CLAYTON", "CLEARY"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_DYBAS_DYCKMAN() {
		Assert.assertEquals(4, (int) this.getEd().op("DYBAS", "DYCKMAN"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_EMINETH_EMMERT() {
		Assert.assertEquals(4, (int) this.getEd().op("EMINETH", "EMMERT"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_GALANTE_GALICKI() {
		Assert.assertEquals(4, (int) this.getEd().op("GALANTE", "GALICKI"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_HARDIN_HARDING() {
		Assert.assertEquals(1, (int) this.getEd().op("HARDIN", "HARDING"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_KEHOE_KEHR() {
		Assert.assertEquals(2, (int) this.getEd().op("KEHOE", "KEHR"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_LOWRY_LUBARSKY() {
		Assert.assertEquals(5, (int) this.getEd().op("LOWRY", "LUBARSKY"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_MAGALLAN_MAGANA() {
		Assert.assertEquals(3, (int) this.getEd().op("MAGALLAN", "MAGANA"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_MAYO_MAYS() {
		Assert.assertEquals(1, (int) this.getEd().op("MAYO", "MAYS"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_MOENY_MOFFETT() {
		Assert.assertEquals(4, (int) this.getEd().op("MOENY", "MOFFETT"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_PARE_PARENT() {
		Assert.assertEquals(2, (int) this.getEd().op("PARE", "PARENT"));
	}

	/**
	 * test distance of something which needs 1 delete & one add
	 */
	@Test
	public void test_RAMEY_RAMFREY() {
		Assert.assertEquals(2, (int) this.getEd().op("RAMEY", "RAMFREY"));
	}

	/**
	 *
	 */
	@Test
	public void test_ab_fg() {
		Assert.assertEquals(2, (int) this.getEd().op("ab", "fg"));
	}

	/**
	 *
	 */
	@Test
	public void test_balls_balsl() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "balsl"));
	}

	/**
	 *
	 */
	@Test
	public void test_bals_basl() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "balsl"));
	}

	/**
	 *
	 */
	@Test
	public void test_bls_bsl() {
		Assert.assertEquals(1, (int) this.getEd().op("balls", "balsl"));
	}

}
