package net.kezvh.algorithms.text;

/**
 * @author mjacob
 *
 */
public class TesTDM extends TestDamerauLevenshteinDistance {

	@Override
	protected EditDistance getEd() {
		return EditDistance.DAMERAU_LEVENSHTEIN_NAIVE;
	}

}
