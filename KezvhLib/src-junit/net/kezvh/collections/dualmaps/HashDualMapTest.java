/**
 *
 */
package net.kezvh.collections.dualmaps;

import net.kezvh.collections.dualmaps.DualMap;
import net.kezvh.collections.dualmaps.HashDualMap;


/**
 * @author afflux
 */
public class HashDualMapTest extends DualMapTest {

	/**
	 * @see net.kezvh.collections.dualmaps.DualMapTest#createMap2()
	 * @param <K1> COMMENT
	 * @param <K2> COMMENT
	 * @param <V> COMMENT
	 * @return COMMENT
	 */
	@Override
	protected <K1, K2, V> DualMap<K1, K2, V> createMap2() {
		return new HashDualMap<K1, K2, V>();
	}

}
