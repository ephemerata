/**
 *
 */
package net.kezvh.collections.dualmaps;

import java.util.Collection;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import net.kezvh.collections.dualmaps.DualMap;

import org.junit.Test;

/**
 * @author afflux
 */
public abstract class DualMapTest extends TestCase {
	/**
	 * @param <K1> COMMENT
	 * @param <K2> COMMENT
	 * @param <V> COMMENT
	 * @return COMMENT
	 */
	protected abstract <K1, K2, V> DualMap<K1, K2, V> createMap2();

	/**
	 *
	 */
	@Test
	public void testCreate() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		Assert.assertNotNull(it);
	}

	/**
	 *
	 */
	@Test
	public void testPut() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		final Integer previous = it.put(1, 2, 3);
		Assert.assertNull(previous);
	}

	/**
	 *
	 */
	@Test
	public void testReput() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		it.put(1, 2, 3);
		final int previous = it.put(1, 2, 4);
		Assert.assertEquals(3, previous);
		final int current = it.get(1, 2);
		Assert.assertEquals(4, current);
	}

	/**
	 *
	 */
	@Test
	public void testRemove() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		it.put(1, 2, 3);
		final int previous = it.remove(1, 2);
		Assert.assertEquals(3, previous);
	}

	/**
	 *
	 */
	@Test
	public void testSize() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				for (int k = 0; k < 4; k++) {
					it.put(i, j, k);
					Assert.assertEquals(i * 10 + j + 1, it.size());
				}

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				for (int k = 0; k < 4; k++) {
					it.remove(i, j);
					Assert.assertEquals(99 - (i * 10 + j), it.size());
				}
	}

	/**
	 *
	 */
	@Test
	public void test1WayMapSize() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				for (int k = 0; k < 4; k++)
					it.put(i, j, k);

		for (int i = 0; i < 10; i++) {
			final Map<Integer, Integer> map = it.entryMapOnKey1(i);
			Assert.assertEquals(10, map.size());
		}
	}

	/**
	 *
	 */
	@Test
	public void test1WayMapContents() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				for (int k = 0; k < 4; k++)
					it.put(i, j, k);

		for (int i = 0; i < 10; i++) {
			final Map<Integer, Integer> map = it.entryMapOnKey1(i);
			for (int j = 0; j < 10; j++) {
				final int three = map.get(j);
				Assert.assertEquals(3, three);
			}
		}
	}

	/**
	 *
	 */
	@Test
	public void test1WayMapModifications() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				for (int k = 0; k < 4; k++)
					it.put(i, j, k);

		for (int i = 0; i < 10; i++) {
			final Map<Integer, Integer> map = it.entryMapOnKey1(i);
			for (int j = 0; j < 10; j++)
				for (int k = 2; k >= 0; k--) {
					final int previous = map.put(j, k);
					Assert.assertEquals(k + 1, previous);
				}
		}
	}

	/**
	 *
	 */
	@Test
	public void testRemove0() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				it.put(i, j, 10 * i + j);

		final int value = it.remove(1, 1);
		Assert.assertEquals(11, value);
		Assert.assertEquals(99, it.size());
		Assert.assertEquals(9, it.get1(1).size());
		Assert.assertEquals(9, it.get2(1).size());
		Assert.assertEquals(10, it.get1(0).size());
		Assert.assertEquals(10, it.get2(0).size());
		Assert.assertEquals(10, it.get1(2).size());
		Assert.assertEquals(10, it.get2(2).size());
	}

	/**
	 *
	 */
	@Test
	public void testRemove1() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				it.put(i, j, 10 * i + j);

		Assert.assertEquals(100, it.size());
		final Collection<Map.Entry<Integer, Integer>> firstRow = it.remove1(1);
		for (int i = 0; i < 10; i++)
			if (i == 1)
				Assert.assertNull("" + i, it.get1(i));
			else
				Assert.assertEquals("" + i, 10, it.get1(i).size());
		Assert.assertEquals(10, firstRow.size());
		Assert.assertEquals(90, it.size());
		Assert.assertEquals(9, it.get2(1).size());
	}

	/**
	 *
	 */
	@Test
	public void testRemove2() {
		final DualMap<Integer, Integer, Integer> it = this.createMap2();
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				it.put(j, i, 10 * i + j);

		Assert.assertEquals(100, it.size());
		final Collection<Map.Entry<Integer, Integer>> firstRow = it.remove2(1);
		for (int i = 0; i < 10; i++)
			if (i == 1)
				Assert.assertNull("" + i, it.get2(i));
			else
				Assert.assertEquals("" + i, 10, it.get2(i).size());
		Assert.assertEquals(10, firstRow.size());
		Assert.assertEquals(90, it.size());
		Assert.assertEquals(9, it.get1(1).size());
	}
}
