/**
 *
 */
package net.kezvh.collections.DualMap;

import net.kezvh.collections.DualMap.DualMap;
import net.kezvh.collections.DualMap.HashDualMap;


/**
 * @author afflux
 */
public class HashDualMapTest extends DualMapTest {

	/**
	 * @see net.kezvh.collections.DualMap.DualMapTest#createMap2()
	 * @param <K1> COMMENT
	 * @param <K2> COMMENT
	 * @param <V> COMMENT
	 * @return COMMENT
	 */
	@Override
	protected <K1, K2, V> DualMap<K1, K2, V> createMap2() {
		return new HashDualMap<K1, K2, V>();
	}

}
