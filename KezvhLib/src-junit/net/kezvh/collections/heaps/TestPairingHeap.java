package net.kezvh.collections.heaps;

/**
 * @author mjacob
 *
 */
public class TestPairingHeap extends TestHeap {

	@Override
	protected <E, W extends Comparable<W>> Heap<E, W> getNewHeap() {
		return new PairingHeap<E, W>();
	}

}
