package net.kezvh.collections.heaps;

import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import net.kezvh.collections.Pair;

import org.junit.Test;

/**
 * @author mjacob
 *
 */
public abstract class TestHeap extends TestCase {
	/**
	 * @author mjacob
	 *
	 * @param <E> COMMENT
	 * @param <W> COMMENT
	 */
	protected interface HeapTemplate<E, W extends Comparable<W>> {
		/**
		 * @return COMMENT
		 */
		List<Pair<E, W>> getElementsToInsert();
	}

	private static final HeapTemplate<Integer, Integer> emptyHeap = new HeapTemplate<Integer, Integer>() {
		@Override
		public List<Pair<Integer, Integer>> getElementsToInsert() {
			return Collections.emptyList();
		}
	};

	private final <E, W extends Comparable<W>> Heap<E, W> buildHeap(final HeapTemplate<E, W> template) {
		final Heap<E, W> heap = this.getNewHeap();
		for (final Pair<E, W> pair : template.getElementsToInsert())
			heap.add(pair.get1(), pair.get2());
		return heap;
	}

	/**
	 * @param <E> COMMENT
	 * @param <W> COMMENT
	 * @return COMMENT
	 */
	protected abstract <E, W extends Comparable<W>> Heap<E, W> getNewHeap();

	/**
	 *
	 */
	@Test
	public void testCreate() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);
		Assert.assertNotNull(it);
	}

	/**
	 *
	 */
	@Test
	public void testInserts() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);

		for (int i = 0; i < 10; i++)
			Assert.assertNull(it.add(i, i));
	}

	/**
	 *
	 */
	@Test
	public void testRemoves() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);

		for (int i = 0; i < 10; i++)
			Assert.assertNull(it.add(i, i));

		for (int j = 0; j < 10; j++)
			Assert.assertEquals(j, (int) it.remove());
	}

	/**
	 *
	 */
	@Test
	public void testDecreaseKey() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);

		for (int i = 0; i < 10; i++)
			Assert.assertNull(it.add(i, i == 5 ? 10 : i));

		it.reweight(5, 5);

		for (int j = 0; j < 10; j++)
			Assert.assertEquals(j, (int) it.remove());
	}

	/**
	 *
	 */
	@Test
	public void testDecreaseComplex() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);

		for (int i = 0; i < 10; i++)
			Assert.assertNull(it.add(i, i));
		// 0 1 2 3 4 5 6 7 8 9

		for (int i = 1; i < 10; i += 2)
			it.reweight(i, i - 2);
		// 1 0 3 2 5 4 7 6 9 8

		Assert.assertEquals(1, (int) it.remove());
		// 0 3 2 5 4 7 6 9 8

		for (int i = 0; i < 10; i++)
			Assert.assertNull(it.add(i + 10, i + 10));
		// 0 3 2 5 4 7 6 9 8 10 11 12 13 14 15 16 17 18 19

		for (int i = 1; i < 10; i += 2)
			it.reweight(i + 10, i);
		// 0 (3, 11) 2 (5, 13) 4 (7, 15) 6 (9, 17) 8 19 10 12 14 16 18

		Assert.assertEquals(0, (int) it.remove());
		int a = it.remove();
		int b = it.remove();
		Assert.assertTrue((a == 3 || a == 11) && (b == 3 || b == 11) && (a != b));

		Assert.assertEquals(2, (int) it.remove());

		a = it.remove();
		b = it.remove();
		Assert.assertTrue((a == 5 || a == 13) && (b == 5 || b == 13) && (a != b));

		Assert.assertEquals(4, (int) it.remove());

		a = it.remove();
		b = it.remove();
		Assert.assertTrue((a == 7 || a == 15) && (b == 7 || b == 15) && (a != b));

		Assert.assertEquals(6, (int) it.remove());

		a = it.remove();
		b = it.remove();
		Assert.assertTrue((a == 9 || a == 17) && (b == 9 || b == 17) && (a != b));

		Assert.assertEquals(8, (int) it.remove());
		Assert.assertEquals(19, (int) it.remove());
		Assert.assertEquals(10, (int) it.remove());
		Assert.assertEquals(12, (int) it.remove());
		Assert.assertEquals(14, (int) it.remove());
		Assert.assertEquals(16, (int) it.remove());
		Assert.assertEquals(18, (int) it.remove());
	}

	/**
	 *
	 */
	@Test
	public void testDecreaseMerge() {
		final Heap<Integer, Integer> it = this.buildHeap(TestHeap.emptyHeap);
		for (int i = 0; i < 10; i += 2)
			Assert.assertNull(it.add(i, i));

		final Heap<Integer, Integer> it2 = this.buildHeap(TestHeap.emptyHeap);
		for (int i = 1; i < 10; i += 2)
			Assert.assertNull(it2.add(i, i));

		it.merge(it2);

		for (int j = 0; j < 10; j++)
			Assert.assertEquals(j, (int) it.remove());
	}
}
