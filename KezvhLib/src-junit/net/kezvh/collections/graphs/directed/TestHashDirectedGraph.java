package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 */
public class TestHashDirectedGraph extends TestDirectedGraphWithValues {
	@Override
	protected DirectedGraphWithValues<Integer, Integer, Integer> getGraphWithValues() {
		return new HashDirectedGraph<Integer, Integer, Integer>();
	}
}
