package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 */
public class HashDirectedGraphSetBuilder extends DirectedGraphWithEdgeValuesBuilder {
	@Override
	public DirectedGraphWithEdgeValues<Integer, Integer> create() throws net.kezvh.patterns.AbstractFactory.CreationFailedException {
		return new GenericDirectedGraphSet<Integer, Integer>(new HashDirectedGraph<Integer, Object, Integer>());
	}
}
