package net.kezvh.collections.graphs.directed;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author mjacob
 *
 */
public abstract class TestDirectedGraphWithValues extends TestCase {
	/**
	 * @return COMMENT
	 */
	protected abstract DirectedGraphWithValues<Integer, Integer, Integer> getGraphWithValues();

	/**
	 *
	 */
	@Test
	public void testCreate() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		Assert.assertNotNull(it);
	}

	/**
	 *
	 */
	@Test
	public void testInsert() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		Assert.assertNull(it.put(1, 2));
	}

	/**
	 *
	 */
	@Test
	public void testGet() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		it.put(1, 2);
		Assert.assertEquals(2, (int) it.get(1));
	}

	/**
	 *
	 */
	@Test
	public void testEdgeInsert() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		it.put(1, 2);
		it.put(2, 4);
		Assert.assertNull(it.addEdge(1, 2, 3));
	}

	/**
	 *
	 */
	@Test
	public void testEdgeGet() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		it.put(1, 2);
		it.put(2, 4);
		it.addEdge(1, 2, 3);
		Assert.assertEquals(3, (int) it.getEdgeValue(1, 2));
	}

	/**
	 *
	 */
	@Test
	public void testRemove() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		it.put(1, 2);
		it.remove(1);
		Assert.assertFalse(it.containsKey(1));
		Assert.assertFalse(it.containsValue(2));
		Assert.assertNull(it.get(1));
	}

	/**
	 *
	 */
	@Test
	public void testRemoveEdge() {
		final DirectedGraphWithValues<Integer, Integer, Integer> it = this.getGraphWithValues();
		it.put(1, 2);
		it.put(2, 4);
		it.addEdge(1, 2, 3);
		it.removeEdge(1, 2);
		Assert.assertFalse(it.containsEdge(1, 2));
		Assert.assertNull(it.getEdgeValue(1, 2));
	}

}
