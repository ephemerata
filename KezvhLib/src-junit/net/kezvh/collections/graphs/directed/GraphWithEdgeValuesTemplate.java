/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.kezvh.collections.CollectionsUtilities;
import net.kezvh.collections.dualmaps.DualMap;
import net.kezvh.collections.dualmaps.HashDualMap;
import net.kezvh.functional.Operations;

/**
 * @author mjacob
 */
public enum GraphWithEdgeValuesTemplate {
	/**
	 * nodes: []
	 * edges: []
	 */
	EMPTY(new ArrayList<Integer>(), new HashDualMap<Integer, Integer, Integer>()),
	/**
	 * nodes: [0..15]
	 * edges: all on [0..7], all on [8..15] (weight 1)
	 */
	DISCONECTED(CollectionsUtilities.range(0, 16, Operations.INTEGER_INCREMENTER), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++)
					this.put(i, j, 1);

			for (int i = 8; i < 16; i++)
				for (int j = 8; j < 16; j++)
					this.put(i, j, 1);
		}
	}),
	/**
	 * nodes: [0..15]
	 * edges: all (weight 1)
	 */
	COMPLETE(CollectionsUtilities.range(0, 16, Operations.INTEGER_INCREMENTER), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 16; i++)
				for (int j = 0; j < 16; j++)
					if (i != j)
						this.put(i, j, 1);
		}
	}),
	/**
	 * nodes: [0..99]
	 * edges: [i => (i + 1) % 100] (weight 1)
	 */
	CIRCULAR(CollectionsUtilities.range(0, 100, Operations.INTEGER_INCREMENTER), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 100; i++)
				this.put(i, (i + 1) % 100, 1);
		}
	}),
	/**
	 * nodes: [0..99]
	 * edges: [i => i + 1] (weight 1)
	 */
	LINEAR(CollectionsUtilities.range(0, 100, Operations.INTEGER_INCREMENTER), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 99; i++)
				this.put(i, i + 1, 1);
		}
	}),
	/**
	 * nodes: [1,2]
	 * edges: [1 => 2] (weight 1)
	 */
	SIMPLE(Arrays.asList(1, 2), new HashDualMap<Integer, Integer, Integer>() {
		{
			this.put(1, 2, 1);
		}
	}),
	/**
	 * nodes: [1]
	 * edges: []
	 */
	TRIVIAL(Collections.singletonList(1), new HashDualMap<Integer, Integer, Integer>()), ;
	private final List<Integer> nodes;
	private final HashDualMap<Integer, Integer, Integer> edges;

	GraphWithEdgeValuesTemplate(final List<Integer> nodes, final HashDualMap<Integer, Integer, Integer> edges) {
		this.nodes = nodes;
		this.edges = edges;
	}

	List<Integer> getNodes() {
		return this.nodes;
	}

	DualMap<Integer, Integer, Integer> getEdges() {
		return this.edges;
	}
}