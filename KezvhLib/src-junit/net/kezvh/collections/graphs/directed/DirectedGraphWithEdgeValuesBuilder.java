package net.kezvh.collections.graphs.directed;

import java.util.Map;

import net.kezvh.collections.Pair;
import net.kezvh.patterns.AbstractFactory;

/**
 * @author mjacob
 *
 */
public abstract class DirectedGraphWithEdgeValuesBuilder implements AbstractFactory<DirectedGraphWithEdgeValues<Integer, Integer>> {

	/**
	 * @param graphTemplate a graph template
	 * @return a graph from the template
	 * @throws CreationFailedException If creating the graph fails
	 */
	public DirectedGraphWithEdgeValues<Integer, Integer> buildGraph(final GraphWithEdgeValuesTemplate graphTemplate) throws CreationFailedException {
		final DirectedGraphWithEdgeValues<Integer, Integer> graph = this.create();

		graph.addAll(graphTemplate.getNodes());
		for (final Map.Entry<Pair<Integer, Integer>, Integer> edge : graphTemplate.getEdges().entrySet())
			graph.addEdge(edge.getKey().get1(), edge.getKey().get2(), edge.getValue());
		return graph;
	}
}
