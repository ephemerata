/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.kezvh.collections.IntegerRange;
import net.kezvh.collections.dualmaps.DualMap;
import net.kezvh.collections.dualmaps.HashDualMap;
import net.kezvh.collections.views.MapAsView;
import net.kezvh.functional.Operations;
import net.kezvh.functional.lambda.L1;

/**
 * @author mjacob
 *
 */
public enum GraphWithValuesTemplate {
	/**
	 *
	 */
	EMPTY_GRAPH(new HashMap<Integer, Integer>(), new HashDualMap<Integer, Integer, Integer>()),
	/**
	 *
	 */
	DISCONECTED_GRAPH(new IntegerRange(0, 16), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++)
					this.put(i, j, 1);

			for (int i = 8; i < 16; i++)
				for (int j = 8; j < 16; j++)
					this.put(i, j, 1);
		}
	}),
	/**
	 *
	 */
	COMPLETE_GRAPH(new IntegerRange(0, 16), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 16; i++)
				for (int j = 0; j < 16; j++)
					this.put(i, j, 1);
		}
	}),
	/**
	 *
	 */
	CIRCULAR_GRAPH(new IntegerRange(0, 100), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 100; i++)
				this.put(i, (i + 1) % 100, 1);
		}
	}),
	/**
	 *
	 */
	LINEAR_GRAPH(new IntegerRange(0, 100), new HashDualMap<Integer, Integer, Integer>() {
		{
			for (int i = 0; i < 99; i++)
				this.put(i, i + 1, 1);
		}
	}),
	/**
	 *
	 */
	SIMPLE_GRAPH(new IntegerRange(1, 2), new HashDualMap<Integer, Integer, Integer>() {
		{
			this.put(1, 2, 100);
		}
	}),
	/**
	 *
	 */
	TRIVIAL_GRAPH(Collections.singleton(1), new HashDualMap<Integer, Integer, Integer>()), ;
	private final Map<Integer, Integer> nodes;
	private final HashDualMap<Integer, Integer, Integer> edges;
	private final L1<Integer, Integer> identityOpOnIntegers = Operations.identity();

	GraphWithValuesTemplate(final Set<Integer> numbers, final HashDualMap<Integer, Integer, Integer> edges) {
		this.nodes = new MapAsView<Integer, Integer>(numbers, this.identityOpOnIntegers, this.identityOpOnIntegers);
		this.edges = edges;
	}

	GraphWithValuesTemplate(final Map<Integer, Integer> nodes, final HashDualMap<Integer, Integer, Integer> edges) {
		this.nodes = nodes;
		this.edges = edges;
	}

	Map<Integer, Integer> getNodes() {
		return this.nodes;
	}

	DualMap<Integer, Integer, Integer> getEdges() {
		return this.edges;
	}
}