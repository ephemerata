/**
 *
 */
package net.kezvh.collections.trees;

/**
 * @author afflux
 *
 */
public class TestListRefTree extends TestTree {

	/**
	 * @see net.kezvh.collections.trees.TestTree#getTree(java.lang.Integer)
	 */
	@Override
	protected Tree<Integer> getTree(final Integer rootvalue) {
		return new ListRefTree<Integer>(rootvalue);
	}

}
