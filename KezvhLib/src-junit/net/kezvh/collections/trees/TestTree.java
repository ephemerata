/**
 *
 */
package net.kezvh.collections.trees;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author afflux
 *
 */
public abstract class TestTree extends TestCase {
	/**
	 * @param rootvalue initial root value
	 * @return a tree
	 */
	protected abstract Tree<Integer> getTree(Integer rootvalue);

	/**
	 *
	 */
	@Test
	public void testSomething() {
		final Tree<Integer> tree = this.getTree(0);
		final Tree<Integer> child = tree.addChild(1);
		Assert.assertEquals((Integer) 1, child.value());
	}

	/**
	 *
	 */
	@Test
	public void testSomething2() {
		final Tree<Integer> tree = this.getTree(0);
		final Tree<Integer> child = tree.addChild(1);
		Assert.assertEquals(2, tree.size());
		Assert.assertEquals(1, child.size());
	}

	/**
	 *
	 */
	@Test
	public void testSomething3() {
		final Tree<Integer> tree = this.getTree(0);
		final Tree<Integer> child = tree.addChild(1);
		final Tree<Integer> child2 = child.addChild(2);
		Assert.assertEquals(3, tree.size());
		Assert.assertEquals(2, child.size());
		Assert.assertEquals(1, child2.size());
	}

	/**
	 *
	 */
	@Test
	public void testSomething4() {
		final Tree<Integer> tree = this.getTree(0);
		final Tree<Integer> child = tree.addChild(1);
		final Tree<Integer> child2 = child.addChild(2);
		final Tree<Integer> chlid3 = child2.addChild(3);
		Assert.assertEquals(4, tree.size());
		Assert.assertEquals(3, child.size());
		Assert.assertEquals(2, child2.size());
		Assert.assertEquals(1, chlid3.size());
	}
}
