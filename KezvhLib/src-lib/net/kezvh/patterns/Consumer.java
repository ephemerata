package net.kezvh.patterns;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * @author mjacob
 *
 * @param <E> type of the element
 */
public interface Consumer<E> {
	/**
	 * Inserts the specified element into this queue if it is possible to do
	 * so immediately without violating capacity restrictions, returning
	 * <tt>true</tt> upon success and throwing an
	 * <tt>IllegalStateException</tt> if no space is currently available.
	 * When using a capacity-restricted queue, it is generally preferable to
	 * use {@link #offer(Object) offer}.
	 *
	 * @param e the element to add
	 * @return <tt>true</tt> (as specified by {@link Collection#add})
	 * @throws IllegalStateException if the element cannot be added at this
	 *         time due to capacity restrictions
	 * @throws ClassCastException if the class of the specified element
	 *         prevents it from being added to this queue
	 * @throws NullPointerException if the specified element is null
	 * @throws IllegalArgumentException if some property of the specified
	 *         element prevents it from being added to this queue
	 */
	boolean add(E e);

	/**
	 * Inserts the specified element into this queue if it is possible to do
	 * so immediately without violating capacity restrictions, returning
	 * <tt>true</tt> upon success and <tt>false</tt> if no space is currently
	 * available.  When using a capacity-restricted queue, this method is
	 * generally preferable to {@link #add}, which can fail to insert an
	 * element only by throwing an exception.
	 *
	 * @param e the element to add
	 * @return <tt>true</tt> if the element was added to this queue, else
	 *         <tt>false</tt>
	 * @throws ClassCastException if the class of the specified element
	 *         prevents it from being added to this queue
	 * @throws NullPointerException if the specified element is null
	 * @throws IllegalArgumentException if some property of the specified
	 *         element prevents it from being added to this queue
	 */
	boolean offer(E e);

	/**
	 * Inserts the specified element into this queue, waiting if necessary
	 * for space to become available.
	 *
	 * @param e the element to add
	 * @throws InterruptedException if interrupted while waiting
	 * @throws ClassCastException if the class of the specified element
	 *         prevents it from being added to this queue
	 * @throws NullPointerException if the specified element is null
	 * @throws IllegalArgumentException if some property of the specified
	 *         element prevents it from being added to this queue
	 */
	void put(E e) throws InterruptedException;

	/**
	 * Inserts the specified element into this queue, waiting up to the
	 * specified wait time if necessary for space to become available.
	 *
	 * @param e the element to add
	 * @param timeout how long to wait before giving up, in units of
	 *        <tt>unit</tt>
	 * @param unit a <tt>TimeUnit</tt> determining how to interpret the
	 *        <tt>timeout</tt> parameter
	 * @return <tt>true</tt> if successful, or <tt>false</tt> if
	 *         the specified waiting time elapses before space is available
	 * @throws InterruptedException if interrupted while waiting
	 * @throws ClassCastException if the class of the specified element
	 *         prevents it from being added to this queue
	 * @throws NullPointerException if the specified element is null
	 * @throws IllegalArgumentException if some property of the specified
	 *         element prevents it from being added to this queue
	 */
	boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException;
}
