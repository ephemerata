package net.kezvh.patterns;

/**
 * @author mjacob
 * @param <T> FIXME comment
 */
public interface AbstractFactory<T> {
	/**
	 * @author mjacob
	 */
	class CreationFailedException extends RuntimeException {

		public CreationFailedException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public CreationFailedException(final String message, final Throwable cause) {
			super(message, cause);
			// TODO Auto-generated constructor stub
		}

		public CreationFailedException(final String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}

		public CreationFailedException(final Throwable cause) {
			super(cause);
			// TODO Auto-generated constructor stub
		}

	}

	/**
	 * @return a new T
	 * @throws CreationFailedException
	 */
	T create() throws CreationFailedException;
}
