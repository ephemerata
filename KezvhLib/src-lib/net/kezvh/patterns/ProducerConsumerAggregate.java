/**
 *
 */
package net.kezvh.patterns;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author afflux
 * @param <E> element type
 *
 */
public class ProducerConsumerAggregate<E> {
	private final Collection<ProducerThread> producerThreads = new LinkedList<ProducerThread>();
	private final Collection<ConsumerThread> consumerThreads = new LinkedList<ConsumerThread>();

	//	private final ConcurrentLinkedQueue<E> items = new ConcurrentLinkedQueue<E>();
	private final ArrayBlockingQueue<E> items;

	private final class ProducerThread extends Thread {
		private final Producer<E> producer;

		public ProducerThread(final Producer<E> producer) {
			this.producer = producer;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			while (!this.isInterrupted())
				try {
					final E item = this.producer.take();
					if (item != null)
						ProducerConsumerAggregate.this.items.put(item);
					else
						return;
				} catch (final InterruptedException e) {
					// fall through and return
				}
		}
	}

	private final class ConsumerThread extends Thread {
		private final Consumer<E> consumer;

		public ConsumerThread(final Consumer<E> consumer) {
			this.consumer = consumer;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			while (!this.isInterrupted())
				try {
					final E item = ProducerConsumerAggregate.this.items.take();
					if (item != null)
						this.consumer.put(item);
					else
						return;
				} catch (final InterruptedException e) {
					// fall through and return
				}
		}
	}

	/**
	 * @param producers COMMENT
	 * @param consumers COMMENT
	 */
	public ProducerConsumerAggregate(final Collection<Producer<E>> producers, final Collection<Consumer<E>> consumers) {
		this(producers, consumers, false);
	}

	/**
	 * @param producers COMMENT
	 * @param consumers COMMENT
	 * @param fair COMMENT
	 */
	public ProducerConsumerAggregate(final Collection<Producer<E>> producers, final Collection<Consumer<E>> consumers, final boolean fair) {
		this.items = new ArrayBlockingQueue<E>(producers.size(), fair);
		for (final Consumer<E> consumer : consumers)
			this.consumerThreads.add(new ConsumerThread(consumer));
		for (final Producer<E> producer : producers)
			this.producerThreads.add(new ProducerThread(producer));
	}

	/**
	 * start the engines
	 */
	public synchronized void start() {
		for (final ConsumerThread consumerThread : this.consumerThreads)
			consumerThread.start();
		for (final ProducerThread producerThread : this.producerThreads)
			producerThread.start();
	}

	/**
	 * stop the engines
	 * @return the items in the queue when things were stopped
	 */
	public synchronized List<E> interrupt() {
		for (final ConsumerThread consumerThread : this.consumerThreads)
			consumerThread.interrupt();
		for (final ProducerThread producerThread : this.producerThreads)
			producerThread.interrupt();
		final LinkedList<E> leftovers = new LinkedList<E>();
		this.items.drainTo(leftovers);
		return leftovers;
	}
}
