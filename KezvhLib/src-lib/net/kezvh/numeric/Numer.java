/**
 *
 */
package net.kezvh.numeric;

import java.math.BigInteger;
import java.util.Random;

/**
 * @author afflux
 */
public class Numer extends BigInteger {

	/**
	 * @param val x
	 */
	public Numer(final byte[] val) {
		super(val);
	}

	/**
	 * @param signum x
	 * @param magnitude x
	 */
	public Numer(final int signum, final byte[] magnitude) {
		super(signum, magnitude);
	}

	/**
	 * @param bitLength x
	 * @param certainty x
	 * @param rnd x
	 */
	public Numer(final int bitLength, final int certainty, final Random rnd) {
		super(bitLength, certainty, rnd);
	}

	/**
	 * @param numBits x
	 * @param rnd x
	 */
	public Numer(final int numBits, final Random rnd) {
		super(numBits, rnd);
	}

	/**
	 * @param val x
	 * @param radix x
	 */
	public Numer(final String val, final int radix) {
		super(val, radix);
	}

	/**
	 * @param val x
	 */
	public Numer(final String val) {
		super(val);
	}

}
