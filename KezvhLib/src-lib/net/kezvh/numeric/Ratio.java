package net.kezvh.numeric;

/**
 * @author mjacob
 * @param <N> number type
 */
public class Ratio<N extends Number & Comparable<N>> extends Number implements Comparable<Ratio<? extends N>> {
	/**
	 * maximum long value that can be represented perfectly in a a double
	 */
	public static final long MAX_DOUBLE_LONG = 0x1FFFFFFFFFFFFFl;

	/**
	 * maximum long value that can be represented perfectly in a a double
	 */
	public static final double MAX_LONG_DOUBLE = 0x1FFFFFFFFFFFFFl;

	/**
	 * minimum long value that can be represented perfectly in a double
	 */
	public static final long MIN_DOUBLE_LONG = -0x1FFFFFFFFFFFFFl;

	/**
	 * maximum long value that can be represented perfectly in a a double
	 */
	public static final double MIN_LONG_DOUBLE = -0x1FFFFFFFFFFFFFl;

	private final N firstValue;
	private final N secondValue;

	/**
	 * @param firstValue numerator
	 * @param secondValue denominator
	 */
	public Ratio(final N firstValue, final N secondValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}

	/**
	 * @return numerator
	 */
	public N getFirstValue() {
		return this.firstValue;
	}

	/**
	 * @return denominator
	 */
	public N getSecondValue() {
		return this.secondValue;
	}

	/**
	 * @return something roughly representing firstValue+secondValue
	 */
	public Number getDenominator() {
		return Double.valueOf(this.firstValue.doubleValue() + this.secondValue.doubleValue());
	}

	/**
	 * simple implementation which doesn't handle rounding/casting errors
	 */
	@Override
	public int compareTo(final Ratio<? extends N> o) {
		// TODO there are ways to make this behave better
		final long ad = this.firstValue.longValue() * o.secondValue.longValue();
		final long bc = this.secondValue.longValue() * o.firstValue.longValue();
		if (ad > bc)
			return 1;
		else if (ad < bc)
			return -1;
		else
			return 0;

	}

	@Override
	public double doubleValue() {
		return this.firstValue.doubleValue() / this.getDenominator().doubleValue();
	}

	@Override
	public float floatValue() {
		return this.firstValue.floatValue() / this.getDenominator().floatValue();
	}

	@Override
	public int intValue() {
		return this.firstValue.intValue() / this.getDenominator().intValue();
	}

	@Override
	public long longValue() {
		return this.firstValue.longValue() / this.getDenominator().longValue();
	}
}
