package net.kezvh.development.code;

import net.kezvh.lang.UtilityClass;

/**
 * @author mjacob
 *
 */
public final class NamingUtilities extends UtilityClass {
	/**
	 * @param sentence FIXME comment
	 * @return FIXME comment
	 */
	public static String sentenceToCamelCase(final String sentence) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sentence.length(); i++) {
			final char c = sentence.charAt(i);
			if (c == ' ') {
				i++;
				if (i < sentence.length()) {
					final char d = sentence.charAt(i);
					sb.append(Character.toUpperCase(d));
				}
			} else
				sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * @param camelCase FIXME comment
	 * @return FIXME comment
	 */
	public static String camelCaseToSentence(final String camelCase) {
		final StringBuilder sb = new StringBuilder(camelCase);

		for (int i = 1; i < sb.length(); i++)
			if (Character.isUpperCase(sb.charAt(i))) {
				sb.insert(i, ' ');
				i++;
			}

		return sb.toString();
	}

	/**
	 * @param camelCase FIXME comment
	 * @return FIXME comment
	 */
	public static String camelCaseToSTATIC(final String camelCase) {
		final StringBuilder sb = new StringBuilder(camelCase);

		if (sb.length() > 0)
			sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));

		for (int i = 1; i < sb.length(); i++)
			if (Character.isUpperCase(sb.charAt(i))) {
				sb.insert(i, '_');
				i++;
			} else
				sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));

		return sb.toString();
	}
}
