/**
 *
 */
package net.kezvh.development;

/**
 * @author afflux
 *
 */
public class UnimplementedException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 6365439414710647608L;

	/**
	 *
	 */
	public UnimplementedException() {
		super();
	}

	/**
	 * @param message message
	 */
	public UnimplementedException(final String message) {
		super(message);
	}

	/**
	 * @param cause cause
	 */
	public UnimplementedException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param message messa
	 * @param cause cas
	 */
	public UnimplementedException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
