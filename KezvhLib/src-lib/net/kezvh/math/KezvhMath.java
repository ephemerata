package net.kezvh.math;

import java.util.Iterator;

import net.kezvh.collections.fixed.IntegersIterator;
import net.kezvh.lang.UtilityClass;

/**
 * @author afflux
 */
public final class KezvhMath extends UtilityClass {
	// "binary gcd" algorithm. gotten from knuth via wikipedia. not tested.
	// OpI_II BINARY_GCD = new OpI_II() { // FixMe test. if it works, replace current GCD with it. if not, delete it.
	// public int op(final int a, final int b) {
	// int n1 = a < 0 ? -a : a;
	// int n2 = b < 0 ? -b : b;
	//
	// int shift;
	//
	// // GCD(0,x) := x
	// if (n1 == 0 || n2 == 0)
	// return n1 | n2;
	//
	// // Let shift := lg K, where K is the greatest power of 2 dividing both u and v.
	// for (shift = 0; ((n1 | n2) & 1) == 0; ++shift) {
	// n1 >>>= 1;
	// n2 >>>= 1;
	// }
	//
	// while ((n1 & 1) == 0)
	// n1 >>>= 1;
	//
	// // From here on, u is always odd.
	// do {
	// while ((n2 & 1) == 0)
	// // Loop X
	// n2 >>>= 1;
	//
	// // Now u and v are both odd, so diff(u, v) is even.
	// // Let u = min(u, v), v = diff(u, v)/2.
	// if (n1 <= n2)
	// n2 -= n1;
	//
	// else {
	// int diff = n1 - n2;
	// n1 = n2;
	// n2 = diff;
	// }
	// n2 >>>= 1;
	// } while (n2 != 0);
	//
	// return n1 << shift;
	// }
	// };

	// //////////////////////////////

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return FIXME comment
	 */
	public static int compare(final long a, final long b) {
		if (a < b)
			return -1;
		else if (b < a)
			return 1;
		return 0;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return FIXME comment
	 */
	public static int compare(final double a, final double b) {
		if (a < b)
			return -1;
		else if (b < a)
			return 1;
		return 0;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return FIXME comment
	 */
	public static int compare(final boolean a, final boolean b) {
		return a == b ? 0 : a ? 1 : -1;
	}

	// /////////////////////////////////////////

	/**
	 * @param largeOrderInt FIXME comment
	 * @param smallOrderInt FIXME comment
	 * @return FIXME comment
	 */
	public static long toLong(final int largeOrderInt, final int smallOrderInt) {
		return KezvhMath.toLongUnsigned(largeOrderInt) << 32 | KezvhMath.toLongUnsigned(smallOrderInt);
	}

	/**
	 * @param l FIXME comment
	 * @return FIXME comment
	 */
	public static int getLargeOrderInt(final long l) {
		return (int) (l >> 32);
	}

	/**
	 * @param l FIXME comment
	 * @return FIXME comment
	 */
	public static int getSmallOrderInt(final long l) {
		return (int) l;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static long toLongUnsigned(final int n) {
		return n & 0xFFFFFFFFL;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static long toLongUnsigned(final short n) {
		return n & 0xFFFFL;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static long toLongUnsigned(final byte n) {
		return n & 0xFFL;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static int toIntUnsigned(final short n) {
		return n & 0xFFFF;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static int toIntUnsigned(final byte n) {
		return n & 0xFF;
	}

	/**
	 * @param n FIXME comment
	 * @return FIXME comment
	 */
	public static short toShortUnsigned(final byte n) {
		return (short) (n & 0xFF);
	}

	// /////////////////////////////////////////////////

	/**
	 * @param startingAt FIXME comment
	 * @return FIXME comment
	 */
	public static Iterator<String> numberNames(final int startingAt) {
		return new Iterator<String>() {
			Iterator<Integer> i = new IntegersIterator(startingAt);

			public boolean hasNext() {
				return this.i.hasNext();
			}

			public String next() {
				return KezvhMath.toWord(this.i.next());
			}

			public void remove() {
				this.i.remove();
			}
		};
	}

	private static final String[] digitNames = new String[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

	private static final String[] teenNames = new String[] { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };

	private static final String[] tensNames = new String[] { "", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety", };

	private static final String[] powerNames = new String[] { "", " thousand", " million", " billion", " trillion", " quadrillion", " quintillion", " sextillion", " septillion", " octillion" };

	/**
	 * @param l FIXME comment
	 * @return FIXME comment
	 */
	public static String toWord(final long l) {
		final StringBuilder word = new StringBuilder();
		final boolean negative = l < 0;
		long value = l < 0 ? -l : l;

		if (value == 0)
			return "zero";

		int power = 0;
		while (value > 0) {
			final int hundreds = (int) (value % 1000);
			if (hundreds > 0) {
				word.insert(0, KezvhMath.powerNames[power]);
				word.insert(0, KezvhMath.hundredsWord(hundreds));
			}

			power++;
			value = value / 1000;
			if (value > 0 && hundreds > 0)
				word.insert(0, " ");
		}

		if (negative)
			word.insert(0, "negative ");

		return word.toString();
	}

	private static String hundredsWord(final int l) {
		final int ones = l % 10;
		final int tens = l / 10 % 10;
		final int hundreds = l / 100;

		final StringBuilder word = new StringBuilder();
		if (hundreds > 0) {
			word.append(KezvhMath.digitNames[hundreds]);
			word.append(" hundred");
		}

		if (tens > 0 || ones > 0) {
			if (hundreds > 0)
				word.append(" ");

			if (tens == 1) {
				word.append(KezvhMath.teenNames[ones]);
				return word.toString();
			}

			if (tens > 1)
				word.append(KezvhMath.tensNames[tens]);

			if (ones > 0) {
				if (tens > 1)
					word.append(" ");

				word.append(KezvhMath.digitNames[ones]);
			}
		}

		return word.toString();
	}
}
