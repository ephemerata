package net.kezvh.math;

import net.kezvh.lang.UtilityClass;

/**
 * @author afflux
 */
public final class ArrayMath extends UtilityClass {

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return a | b
	 */
	public static boolean[] or(final boolean[] a, final boolean[] b) {
		final boolean[] result = new boolean[a.length];
		for (int i = 0; i < a.length; i++)
			result[i] = a[i] | b[i];

		return result;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 */
	public static void or2(final boolean[] a, final boolean[] b) {
		for (int i = 0; i < a.length; i++)
			a[i] |= b[i];
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return a ^ b
	 */
	public static boolean[] xor(final boolean[] a, final boolean[] b) {
		final boolean[] result = new boolean[a.length];
		for (int i = 0; i < a.length; i++)
			result[i] = a[i] ^ b[i];

		return result;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 */
	public static void xor2(final boolean[] a, final boolean[] b) {
		for (int i = 0; i < a.length; i++)
			a[i] ^= b[i];
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return a & b
	 */
	public static boolean[] and(final boolean[] a, final boolean[] b) {
		final boolean[] result = new boolean[a.length];
		for (int i = 0; i < a.length; i++)
			result[i] = a[i] & b[i];

		return result;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 */
	public static void and2(final boolean[] a, final boolean[] b) {
		for (int i = 0; i < a.length; i++)
			a[i] &= b[i];
	}
}
