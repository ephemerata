/**
 *
 */
package net.kezvh.math.big;

import java.math.BigInteger;

/**
 * @author afflux
 */
public interface BigIntegerUtil {
	/**
	 * a useful prime
	 */
	final BigInteger TWO = BigInteger.valueOf(2);
	/**
	 * a useful prime
	 */
	final BigInteger THREE = BigInteger.valueOf(3);
	/**
	 * remember, capital "-" is "_".
	 */
	final BigInteger _ONE = BigInteger.valueOf(-1);

	/**
	 * @param a
	 * @return stuff
	 */
	BigInteger sqrt(BigInteger a);
}
