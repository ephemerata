/**
 *
 */
package net.kezvh.math.big;

import java.math.BigInteger;

/**
 * @author afflux
 */
public class BigIntegerUtilImpl implements BigIntegerUtil {
	/**
	 * see Metrica by Heron of Alexandria, book 1, chapter 8
	 *
	 * @see net.kezvh.math.big.BigIntegerUtil#sqrt(java.math.BigInteger)
	 * @param bi s
	 * @return yourMom
	 */
	@Override
	public BigInteger sqrt(final BigInteger bi) {
		final BigInteger n = bi;
		BigInteger i = BigInteger.ONE;
		BigInteger g;
		do {
			g = i;
			final BigInteger f = n.divide(g);
			final BigInteger h = f.add(g);
			i = h.divide(BigIntegerUtil.TWO);
		} while (!g.subtract(i).equals(BigInteger.ZERO));
		return g;
	}

}
