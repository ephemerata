package net.kezvh.math;

import net.kezvh.lang.UtilityClass;

/**
 * @author afflux
 */
public class Bitlogic extends UtilityClass {

	/**
	 * @param x FIXME comment
	 * @return x has an even count of bits
	 */
	public static boolean evenBitCount(final int x) { // 11 unitary ops
		int y = x;
		y ^= y >> 1;
		y ^= y >> 2;
		y ^= y >> 4;
		y ^= y >> 8;
		y ^= y >> 16;

		return (y & 1) == 0;
	}

	/**
	 * @param x FIXME comment
	 * @return x has an even count of bits
	 */
	public static boolean evenBitCount2(final int x) {
		return Integer.bitCount(x) % 2 == 0;
	}
}
