package net.kezvh.math.algebra;

/**
 * @author afflux
 * @param <T> FIXME comment
 */
public interface Magma<T extends Magma<T>> {
	/**
	 * @param a
	 * @param b
	 * @return sum
	 */
	T add(T a, T b);
}
