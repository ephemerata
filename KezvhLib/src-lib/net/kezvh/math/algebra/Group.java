package net.kezvh.math.algebra;

/**
 * @author afflux
 * @param <T> FIXME comment
 */
public interface Group<T extends Group<T>> extends SemiGroup<Group<T>> {
	/**
	 * @param element
	 * @return FIXME comment
	 */
	T inverse(T element);
}
