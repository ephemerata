package net.kezvh.math.algebra.fields;

/**
 * @author afflux
 * @param <T>  FIXME comment
 */
public interface GaloisField2<T extends GaloisField2<T>> {
	/**
	 * @param a
	 * @param b
	 * @return a + b
	 */
	T add(T a, T b);

	/**
	 * @param a
	 * @param b
	 * @return a - b
	 */
	T subtract(T a, T b);

	/**
	 * @param a
	 * @param b
	 * @return a * b
	 */
	T multiply(T a, T b);

	/**
	 * @param a
	 * @param b
	 * @return a / b
	 */
	T divide(T a, T b);

	/**
	 * @param a
	 * @return a ^ -1
	 */
	T inverse(T a);
}
