package net.kezvh.math.algebra.fields;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author afflux
 */
public class G2_N { // exponentiation is designated w/ ` in this file because ^
	// is relevantly xor & fixed

	private static final class QR {
		long q;
		long r;

		/**
		 */
		QR() {
			//
		}

		QR(final long q, final long r) {
			this.q = q;
			this.r = r;
		}
	}

	/**
	 * @param <T> FIXME comment
	 * @param junction FIXME comment
	 * @param iterable FIXME comment
	 * @return x
	 */
	public static <T> String join(final String junction, final Collection<T> iterable) {
		final StringBuilder joined = new StringBuilder();
		final Iterator<T> i = iterable.iterator();

		if (i.hasNext())
			joined.append(i.next());

		while (i.hasNext())
			joined.append(junction).append(i.next());

		return joined.toString();
	}

	private final long highBit;
	private final long mask;
	private final int n;

	private final long p;

	private final long q; // p = x^n + q

	/**
	 * @param n FIXME comment
	 * @param q FIXME comment
	 */
	public G2_N(final int n, final long q) {
		this.n = n;
		this.q = q;
		this.mask = (1 << n) - 1;

		if (n == 64)
			this.p = q;
		else
			this.p = 1 << n ^ q;

		this.highBit = 1 << n - 1; // useful for certain tests
		this.verify();
	}

	/**
	 * note: a + b = a | b + a & b a ^ b = a | b - a & b so the result of this add is a + b - ((a & b) << 1) (more or less)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return sum
	 */
	public long add(final long a, final long b) {
		return (a ^ b) & this.mask;
	}

	private String alsjf(final int m) {
		if (m == 0)
			return "1";
		else if (m == 1)
			return "x";
		else
			return "x`" + m;
	}

	/**
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return a / b
	 */
	public long divide(final long a, final long b) { // O(2^n)
		return this.multiply(a, this.inverse(b)); // my god loves cheaters
	}

	/**
	 * @return n
	 */
	public int getN() {
		return this.n;
	}

	/**
	 * @return q
	 */
	public long getQ() {
		return this.q;
	}

	/**
	 * @param a FIXME comment
	 * @return b st a * b = 1
	 */
	public long inverse(final long a) {
		if (a == 0)
			throw new ArithmeticException("divide by zero");

		if (this.n == 64)
			throw new RuntimeException("sorry, gus, inverse not implemented for n = 64");

		final long p_ = this.q ^ 1 << this.n;

		final QR qr1 = new QR(0, p_);
		final QR qr2 = new QR(0, a);
		long aux1 = 0;
		long aux2 = 1;

		boolean one = false;

		while ((one ? qr1.r : qr2.r) > 1) {
			one = !one;
			if (one) {
				this.qr(qr1.r, qr2.r, qr1);
				aux1 = this.multiply(qr1.q, aux2) ^ aux1;
			} else {
				this.qr(qr2.r, qr1.r, qr2);
				aux2 = this.multiply(qr2.q, aux1) ^ aux2;
			}
		}
		return one ? aux1 : aux2;
	}

	/**
	 * a x b = a * sum(bits in b) = a * (bn * x^n + ... + b0) (where bn to b0 are bits in b) = xor(powers of x * a for bits in b) a x x`n = (a << n) ^ ((a >> (64 - n)) x p) so a * b = xor(a << n : bn) ^ xor((a >> (64 - n)) x p : bn) = xor(a << n : bn) ^ xor(a >> (64 - n) : bn) x p
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return a * b
	 */
	public long multiply(final long a, final long b) { // O(n)
		long d = a;
		long e = b;
		long c = 0;

		while (d != 0) {
			if ((d & 1) != 0)
				c ^= e;

			if ((e & this.highBit) == 0)
				e <<= 1;
			else
				e = e << 1 ^ this.p;
			d >>= 1;
		}

		return c;
	}

	/**
	 * @param b FIXME comment
	 * @return a * 2
	 */
	public long multiplyByX(final long b) {
		final long a = b;
		if ((a & this.highBit) == 0)
			return a << 1;
		return a << 1 ^ this.p;
	}

	private void qr(final long numerator, final long denominator, final QR out_qr) {
		int hb_n = 64 - Long.numberOfLeadingZeros(numerator);
		final int hb_d = 64 - Long.numberOfLeadingZeros(denominator);
		if (hb_n < hb_d) {
			out_qr.q = 0;
			out_qr.r = numerator;
		} else {
			long bar = denominator << hb_n - hb_d;
			out_qr.q = 1;
			long foo = numerator ^ bar;
			while (--hb_n >= hb_d) {
				out_qr.q <<= 1;
				bar >>= 1;
				if ((foo & 1 << hb_n - 1) != 0) {
					foo ^= bar;
					out_qr.q ^= 1;
				}
			}

			out_qr.r = foo;
		}
	}

	/**
	 * @param a FIXME comment
	 * @return string
	 */
	public String toPolyString(final long a) {
		final List<String> l = new ArrayList<String>();
		for (int i = this.n - 1; i >= 0; i--)
			if ((a & 1 << i) != 0)
				l.add(this.alsjf(i));
		return G2_N.join(" + ", l);
	}

	/**
	 * @param a FIXME comment
	 * @return string
	 */
	public String toString(final long a) {
		return this.toPolyString(a) + " % " + this.alsjf(this.n) + " + " + this.toPolyString(this.q);
	}

	private void verify() {
		long l = 1;

		for (int i = 0; i < (1 << this.n) - 2; i++) {
			l = this.multiply(l, 2);
			if (l == 0)
				throw new RuntimeException("really bad, x`" + i + " is 0");
			if (l == 1)
				throw new RuntimeException("x isn't a generator");
		}
		if (this.multiply(l, 2) != 1)
			throw new RuntimeException("x`(n - 1) != 1? " + this.multiply(l, 2));
	}
}
