package net.kezvh.math.algebra.fields;

/**
 * @author afflux
 */
public class NimberPowerGen {
	/**
	 * @param args FIXME comment
	 */
	public static void main(final String... args) {
		for (byte i = 1; i < 64; i++) {
			System.out.print("new long[] {");
			for (byte j = 1; j < 64; j++)
				System.out.print(String.format("0x%016Xl,", NimberPowerGen.nimPowMul(j, i)));
			System.out.println("},");
		}
	}

	private static long mulArgh(final int i, final int j) {
		final int iL = NimberPowerGen.nimLevel(i);
		final int jL = NimberPowerGen.nimLevel(j);

		final int im = i - iL;
		final int jm = j - jL;

		// (1 << i) * (1 << j) = (1 << im * 1 << iL) * (1 << jm * 1 << jL)
		// == (1 << im * 1 << jm) * (1 << iL) * (1 << jL)

		// 8 * 4096 = 2 * 4 * 16 * 256 = 2 * 16 * 4 * 256 = .. all fermat powers,
		// not reducible... ?????

		long a, b;
		if (jm == 0 && iL != jL) {
			a = NimberPowerGen.nimPowMul(im, jL);
			b = 1 << iL;
		} else if (im == 0 && iL != jL) {
			a = NimberPowerGen.nimPowMul(iL, jm);
			b = 1 << jL;
		} else if (jm == iL || im == jL) {
			a = NimberPowerGen.nimPowMul(jm, iL);
			b = NimberPowerGen.nimPowMul(im, jL);
		} else if (Long.bitCount(im) == 1 && Long.bitCount(jm) == 1 && Long.bitCount(iL) == 1 && Long.bitCount(jL) == 1)
			// iL will be largest
			return NimberPowerGen.nimPowMul(iL, jL + im + jm);
		else if (iL > jm + im + jL)
			return NimberPowerGen.nimPowMul(iL, jL + im + jm);
		else {
			a = NimberPowerGen.nimPowMul(im, jm);
			b = NimberPowerGen.nimPowMul(iL, jL);
		}

		return NimberPowerGen.nimMul(a, b);
	}

	private static int nimLevel(final int i) {
		return (int) Long.highestOneBit(i);
	}

	// private static int nimLevel(long l) {
	// if (l >= 0x100000000l)
	// return 32;
	// else if (l >= 0x10000)
	// return 16;
	// else if (l >= 0x100)
	// return 8;
	// else if (l >= 0x10)
	// return 4;
	// else if (l >= 0x4)
	// return 2;
	// else if (l >= 0x2)
	// return 1;
	// else
	// return 0;
	// }

	private static long nimMul(final long a, final long b) {
		long c = 0;
		for (int i = 0; i < 64; i++)
			for (int j = 0; j < 64; j++)
				if ((a >>> i & 1) != 0 && (b >>> j & 1) != 0)
					c ^= NimberPowerGen.nimPowMul(i, j);
		return c;
	}

	private static long nimPowMul(final int i, final int j) {
		if (j > i) // mirror cases
			return NimberPowerGen.nimPowMul(j, i);

		final long a = 1l << i;
		final long b = 1l << j;
		if (i == 0)
			return b;
		if (j == 0)
			return a;
		if (Long.bitCount(i) == 1) {
			if (Long.bitCount(j) == 1) {
				if (i == j)
					return a | a >>> 1;
				return a * b;
			}
			// the nim prod of a fermat power and any lesser number is the
			// normal product
			return a * b;
		}

		return NimberPowerGen.mulArgh(i, j);
	}
}
