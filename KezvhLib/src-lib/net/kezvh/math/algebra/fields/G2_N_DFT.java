package net.kezvh.math.algebra.fields;

/**
 * @author afflux
 */
public class G2_N_DFT {
	private final G2_N g;

	/**
	 * @param g2_n FIXME commment
	 */
	public G2_N_DFT(final G2_N g2_n) {
		this.g = g2_n;
	}

	/**
	 * @param vars FIXME commment
	 * @return dft
	 */
	public long[] dft(final long[] vars) {
		// 2 == primitive (2`n-1)th root of unity
		// so vars.length must divide

		// currently assuming vars.length = g.(2`n - 1)
		final long[] dft = new long[vars.length];

		long root = 2;
		for (int i = 0; i < vars.length; i++) {
			long localroot = root;

			for (final long element : vars) {
				dft[i] ^= this.g.multiply(element, localroot);
				localroot = this.g.multiply(localroot, root);
			}
			root = this.g.multiplyByX(root);
		}

		return dft;
	}

	/**
	 * @param vars FIXME commment
	 * @return idft
	 */
	public long[] idft(final long[] vars) {
		final long[] idft = this.dft(vars);
		final int r = idft.length - 1;
		final int s = r >> 1;
		for (int i = 0; i < s; i++)
			this.swap(idft, i, r - i - 1);
		return idft;
	}

	/**
	 * @param vars
	 * @param i
	 * @param j
	 */
	private void swap(final long[] vars, final int i, final int j) {
		final long tmp = vars[j];
		vars[j] = vars[i];
		vars[i] = tmp;
	}
}
