package net.kezvh.math.algebra.fields;

import net.kezvh.development.UnimplementedException;
import net.kezvh.math.algebra.FieldOnLong;

/**
 * @author afflux
 */
public class Nimber implements FieldOnLong {

	// [0,1]
	// [0.0,0.1,1.0,1.1]
	// [00.00,00.01,00.10,00.11,01.00,01.01, ...
	// [0000.0000,0000.0001, ....

	// private static boolean isPowerOfTwo(long value) {
	// return (value & -value) == value;
	// }

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#add(long, long)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return x
	 */
	public long add(final long a, final long b) {
		return a ^ b;
	}

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#subtract(long, long)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return x
	 */
	public long subtract(final long a, final long b) {
		return a ^ b;
	}

	// private long bitmul(int i, final int j) {
	// if (Nimber.isPowerOfTwo(i) && Nimber.isPowerOfTwo(j))
	// if (i == j)
	// return i &= i << 1;
	// else
	// return i & j;
	// else
	// return -1;
	// }

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#multiply(long, long)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return x
	 */
	// a * b = a1.a2 * b1.b2 = (a1.00 * b1.00) + (a1.00 * 00.b2) + (00.a1 * b1.00) + (00.a2 * 00.b2)
	public long multiply(final long a, final long b) {
		final int a1 = (int) (a >> 32);
		final int a2 = (int) a;
		final int b1 = (int) (b >> 32);
		final int b2 = (int) b;

		return this.m(a1, b1, true, true) ^ this.m(a1, b2, true, false) ^ this.m(a2, b1, false, true) ^ this.m(a2, b2, false, false);
	}

	private long m(final int a, final int b, final boolean x, final boolean y) {
		return -1;
	}

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#divide(long, long)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return x
	 */
	public long divide(final long a, final long b) {
		throw new UnimplementedException();//FIXME impelement
	}

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#exp(long, long)
	 * @param a FIXME comment
	 * @param b FIXME comment
	 * @return x
	 */
	public long exp(final long a, final long b) {
		throw new UnimplementedException();//FIXME impelement
	}

	/**
	 * @return x
	 */
	public int getN() {
		throw new UnimplementedException();//FIXME impelement
	}

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#inverse(long)
	 * @param a FIXME comment
	 * @return x
	 */
	public long inverse(final long a) {
		throw new UnimplementedException();//FIXME impelement
	}

	/**
	 * @see net.kezvh.math.algebra.FieldOnLong#log(long, long)
	 * @param a FIXME comment
	 * @param c FIXME comment
	 * @return x
	 */
	public long log(final long a, final long c) {
		throw new UnimplementedException();//FIXME impelement
	}
}
