package net.kezvh.math.algebra;

/**
 * @author afflux
 */
public interface FieldOnLong {
	/**
	 * @param a
	 * @param b
	 * @return "sum"
	 */
	long add(long a, long b);

	/**
	 * @param a
	 * @param b
	 * @return "difference"
	 */
	long subtract(long a, long b);

	/**
	 * @param a
	 * @param b
	 * @return x
	 */
	long multiply(long a, long b);

	/**
	 * @param a
	 * @param b
	 * @return x
	 */
	long divide(long a, long b);

	/**
	 * @param a
	 * @return x
	 */
	long inverse(long a);

	/**
	 * @param a
	 * @param b
	 * @return x
	 */
	long exp(long a, long b);

	/**
	 * @param a
	 * @param c
	 * @return x
	 */
	long log(long a, long c);
}