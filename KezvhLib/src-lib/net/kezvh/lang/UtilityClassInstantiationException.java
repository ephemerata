package net.kezvh.lang;

/**
 * @author afflux
 */
public class UtilityClassInstantiationException extends RuntimeException {
	/**
	   *
	   */
	private static final long serialVersionUID = -5420453442854914979L;

	/**
	   * @param message FIXME document
	   */
	public UtilityClassInstantiationException(final String message) {
		super(message);
	}

	/**
	 * @param clazz FIXME document
	 */
	public UtilityClassInstantiationException(final Class<?> clazz) {
		super("cannot instantiate utility class " + clazz);
	}
}
