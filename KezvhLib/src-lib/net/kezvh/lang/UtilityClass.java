package net.kezvh.lang;

/**
 * @author afflux
 *
 * marker class for utility classes. has the added benefit of keeping someone from instantiating a utility class without any code on that utility's behalf.
 */
public abstract class UtilityClass {
	/**
	 *
	 */
	protected UtilityClass() {
		throw new UtilityClassInstantiationException("cannot instantiate purely static class");
	}
}
