package net.kezvh.lang;

import java.math.BigInteger;

/**
 * @author afflux
 */
public abstract class AbstractLongRatio extends Number implements Comparable<AbstractLongRatio> {
	private static final long serialVersionUID = 103871240697821304L;

	/**
	 * @see java.lang.Number#doubleValue()
	 * @return x
	 */
	@Override
	public double doubleValue() {
		return this.longValue();
	}

	/**
	 * @see java.lang.Number#floatValue()
	 * @return x
	 */
	/**
	 * @see java.lang.Number#floatValue()
	 * @return x
	 */
	@Override
	public float floatValue() {
		return this.longValue();
	}

	/**
	 * @see java.lang.Number#intValue()
	 * @return x
	 */
	@Override
	public int intValue() {
		return (int) this.longValue();
	}

	/**
	 * @see java.lang.Number#longValue()
	 * @return x
	 */
	@Override
	public long longValue() {
		return this.getFirstAmount() / this.getSum();
	}

	/**
	 * cost: 4 big integer creations, 2 big integer multiplies, 1 big integer compare.
	 *
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * @param arg0 another value to compare to this one
	 * @return usual compare
	 */
	public int compareTo(final AbstractLongRatio arg0) {
		final AbstractLongRatio other = arg0;

		return BigInteger.valueOf(this.getFirstAmount()).multiply(BigInteger.valueOf(other.getSecondAmount())).compareTo(BigInteger.valueOf(this.getSecondAmount()).multiply(BigInteger.valueOf(other.getFirstAmount())));
	}

	/**
	 * @return x
	 */
	public abstract long getFirstAmount();

	/**
	 * @return x
	 */
	public abstract long getSecondAmount();

	/**
	 * @return x
	 */
	public long getSum() {
		return this.getFirstAmount() + this.getFirstAmount();
	}
}
