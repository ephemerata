/**
 *
 */
package net.kezvh.lang;

/**
 * @author afflux
 */
public class SimpleRatio extends AbstractLongRatio {
	private static final long serialVersionUID = 1L;
	private final long firstAmount;
	private final long secondAmount;

	/**
	 * @param firstAmount FIXME comment
	 * @param secondAmount FIXME comment
	 */
	public SimpleRatio(final long firstAmount, final long secondAmount) {
		this.firstAmount = firstAmount;
		this.secondAmount = secondAmount;
	}

	/**
	 * @see net.kezvh.lang.AbstractLongRatio#getFirstAmount()
	 * @return x
	 */
	@Override
	public long getFirstAmount() {
		return this.firstAmount;
	}

	/**
	 * @see net.kezvh.lang.AbstractLongRatio#getSecondAmount()
	 * @return x
	 */
	@Override
	public long getSecondAmount() {
		return this.secondAmount;
	}

	/**
	 * @param firstAmount FIXME comment
	 * @param secondAmount FIXME comment
	 * @return x
	 */
	public static SimpleRatio valueOf(final long firstAmount, final long secondAmount) {
		return new SimpleRatio(firstAmount, secondAmount);
	}
}
