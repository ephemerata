package net.kezvh.lang;

/**
 * @author mjacob
 *
 */
public class AlgorithmException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 8168781158516126603L;

	/**
	 *
	 */
	public AlgorithmException() {
		//
	}

	/**
	 * @param message FIXME document
	 */
	public AlgorithmException(final String message) {
		super(message);
	}

	/**
	 * @param message FIXME document
	 * @param cause FIXME document
	 */
	public AlgorithmException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause FIXME document
	 */
	public AlgorithmException(final Throwable cause) {
		super(cause);
	}
}
