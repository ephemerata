package net.kezvh.lang.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import net.kezvh.lang.UtilityClass;

/**
 * @author mjacob
 *
 */
public final class ReflectionUtilities extends UtilityClass {
	/**
	 * Map to facilitate looking up a boxed type's class given a primitive types' class.
	 * I feel like java must have this info built into it somewhere, but damned if I know.
	 */
	public static final Map<Class<?>, Class<?>> BOXED_TYPE_BY_PRIMITIVE_TYPE = Collections.unmodifiableMap(new HashMap<Class<?>, Class<?>>() {
		{
			this.put(int.class, Integer.class);
			this.put(long.class, Long.class);
			this.put(short.class, Short.class);
			this.put(byte.class, Byte.class);
			this.put(char.class, Character.class);
			this.put(float.class, Float.class);
			this.put(double.class, Double.class);
			this.put(boolean.class, Boolean.class);
		}
	});
	/**
	 * Map to facilitate looking up a primitive type's class given a boxed types' class.
	 * I feel like java must have this info built into it somewhere, but damned if I know.
	 */
	public static final Map<Class<?>, Class<?>> PRIMITIVE_TYPE_BY_BOXED_TYPE = Collections.unmodifiableMap(new HashMap<Class<?>, Class<?>>() {
		{
			this.put(Integer.class, int.class);
			this.put(Long.class, long.class);
			this.put(Short.class, short.class);
			this.put(Byte.class, byte.class);
			this.put(Character.class, char.class);
			this.put(Float.class, float.class);
			this.put(Double.class, double.class);
			this.put(Boolean.class, boolean.class);
		}
	});

	/**
	 * @param c as
	 * @return df
	 */
	public static final Map<String, List<Method>> getMethodsByName(final Class<?> c) {
		final Map<String, List<Method>> methodsByName = new HashMap<String, List<Method>>();
		for (final Method method : c.getMethods()) {
			List<Method> methods;
			if (methodsByName.containsKey(method.getName()))
				methods = methodsByName.get(method.getName());
			else {
				methods = new LinkedList<Method>();
				methodsByName.put(method.getName(), methods);
			}
			methods.add(method);
		}
		return Collections.unmodifiableMap(methodsByName);
	}

	/**
	 * @param propertyName a
	 * @return b
	 */
	public static final String getMutatorName(final String propertyName) {
		return "set" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
	}

	/**
	 * @param propertyName a
	 * @return b
	 */
	public static final String getAccessorName(final String propertyName) {
		return "get" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
	}

	/**
	 * @param propertyName a
	 * @return b
	 */
	public static final String getBooleanAccessorName(final String propertyName) {
		return "is" + Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
	}

	/**
	 * @author mjacob
	 *
	 */
	public static class InvocationFailedException extends Exception {

		/**
		 *
		 */
		public InvocationFailedException() {
			super();
		}

		/**
		 * @param message d
		 * @param cause c
		 */
		public InvocationFailedException(final String message, final Throwable cause) {
			super(message, cause);
		}

		/**
		 * @param message b
		 */
		public InvocationFailedException(final String message) {
			super(message);
		}

		/**
		 * @param cause a
		 */
		public InvocationFailedException(final Throwable cause) {
			super(cause);
		}
	}

	/**
	 * @param o asdf
	 * @param properties asfd
	 * @throws NoSuchMethodException asdf
	 * @throws InvocationFailedException e
	 */
	public static final void applyProperties(final Object o, final Properties properties) throws NoSuchMethodException, InvocationFailedException {
		final Map<String, List<Method>> methodsByName = ReflectionUtilities.getMethodsByName(o.getClass());
		for (final Entry<?, ?> entry : properties.entrySet()) {
			final List<Method> methods = methodsByName.get(ReflectionUtilities.getMutatorName(entry.getKey().toString()));
			if (methods == null)
				throw new NoSuchMethodException("could not find applicable mutator for " + entry + " for object in class " + o.getClass());
			Method method = null;
			for (final Method possibleMethod : methods) {
				if (possibleMethod.getParameterTypes().length != 1)
					continue;
				if (method != null)
					throw new NoSuchMethodException("too many potential could not find applicable mutators for " + entry + " for object in class " + o.getClass());
				method = possibleMethod;
			}
			if (method == null)
				throw new NoSuchMethodException("could not find applicable mutator for " + entry + " for object in class " + o.getClass());
			try {
				ReflectionUtilities.convertAndInvoke(o, method, entry.getValue());
			} catch (final IllegalArgumentException e) {
				throw new InvocationFailedException(e);
			} catch (final IllegalAccessException e) {
				throw new InvocationFailedException(e);
			} catch (final InvocationTargetException e) {
				throw new InvocationFailedException(e);
			}
		}
	}

	/**
	 * invoke expetedType.valueOf(originalValue)
	 *
	 * @param expectedType needed type
	 * @param originalValue current value
	 * @return value
	 * @throws NoSuchMethodException a
	 * @throws SecurityException b
	 * @throws InvocationTargetException c
	 * @throws IllegalAccessException d
	 * @throws IllegalArgumentException e
	 */
	public static final Object valueAs(final Class<?> expectedType, final Object originalValue) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		if (expectedType.isAssignableFrom(originalValue.getClass()))
			return originalValue;

		Method valueOfMethod;
		// TODO we should really look for methods which can be applied to the class of the value,
		// TODO not just those which match it exatctly. but as is, it works fine for what we use it for.
		if (expectedType.isPrimitive()) {
			final Class<?> boxedType = ReflectionUtilities.BOXED_TYPE_BY_PRIMITIVE_TYPE.get(expectedType);
			valueOfMethod = boxedType.getMethod("valueOf", originalValue.getClass());

		} else
			valueOfMethod = expectedType.getMethod("valueOf", originalValue.getClass());

		return valueOfMethod.invoke(null, originalValue);
	}

	/**
	 * Map the values to those expected by the method, then invoke on the object
	 * The mapping does done by finding a static "valueOf" method of the expected type which can take the given values type as an argument.
	 * This pattern is most useful for java's built in types, but can be followed for your own.
	 *
	 * @param object an object on which to invoke a method
	 * @param method method from the object
	 * @param values none of these can be null
	 * @return whatever the result of invoking the method on the object with the mapped values would be.
	 * @throws IllegalArgumentException raised by reflective invocation
	 * @throws IllegalAccessException raised by reflective invocation
	 * @throws InvocationTargetException raised by reflective invocation
	 * @throws NullPointerException if any of the values is null
	 */
	public static Object convertAndInvoke(final Object object, final Method method, final Object... values) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		final Object[] args = new Object[values.length];
		final Class<?>[] parameterTypes = method.getParameterTypes();

		for (int i = 0; i < args.length; i++)
			try {
				args[i] = ReflectionUtilities.valueAs(parameterTypes[i], values[i]);

			} catch (final SecurityException e) {
				throw new IllegalArgumentException("cannot find valueOf method to convert type " + parameterTypes[i] + " to " + values[i].getClass(), e);

			} catch (final NoSuchMethodException e) {
				throw new IllegalArgumentException("cannot find valueOf method to convert type " + parameterTypes[i] + " to " + values[i].getClass(), e);
			}
		return method.invoke(object, args);
	}

	/**
	 * @param c FIXME comment
	 * @param fieldName FIXME comment
	 * @return x FIXME comment
	 * @throws NoSuchFieldException FIXME comment
	 * @throws IllegalAccessException FIXME comment
	 */
	public static int getInt(final Class<?> c, final String fieldName) throws NoSuchFieldException, IllegalAccessException {
		final Field f = c.getField(fieldName);
		return ((Integer) f.get(null)).intValue();
	}

	/**
	 * @param o FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static boolean hasExecutableDeclaredMethod(final Object o, final String methodName, final Class<?>[] parameters) {
		return ReflectionUtilities.hasExecutableDeclaredMethod(o.getClass(), methodName, parameters);
	}

	/**
	 * @param clazz FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static boolean hasExecutableDeclaredMethod(final Class<?> clazz, final String methodName, final Class<?>[] parameters) {
		final Method m = ReflectionUtilities.getDeclaredMethod(clazz, methodName, parameters);
		return m != null;
	}

	/**
	 * @param className FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static Method getMethod(final String className, final String methodName, final Class<?>[] parameters) {
		try {
			final Class<?> clazz = Class.forName(className);
			return ReflectionUtilities.getMethod(clazz, methodName, parameters);
		} catch (final ClassNotFoundException e) {
			return null;
		}
	}

	/**
	 * @param o FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static Method getMethod(final Object o, final String methodName, final Class<?>[] parameters) {
		return ReflectionUtilities.getMethod(o.getClass(), methodName, parameters);
	}

	/**
	 * @param clazz FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static Method getMethod(final Class<?> clazz, final String methodName, final Class<?>[] parameters) {
		try {
			return clazz.getMethod(methodName, parameters);

		} catch (final NoSuchMethodException e) {
			return null;
		}
	}

	/**
	 * @param o FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static Method getDeclaredMethod(final Object o, final String methodName, final Class<?>[] parameters) {
		return ReflectionUtilities.getDeclaredMethod(o.getClass(), methodName, parameters);
	}

	/**
	 * @param clazz FIXME comment
	 * @param methodName FIXME comment
	 * @param parameters FIXME comment
	 * @return x
	 */
	public static Method getDeclaredMethod(final Class<?> clazz, final String methodName, final Class<?>[] parameters) {
		try {
			return clazz.getDeclaredMethod(methodName, parameters);

		} catch (final NoSuchMethodException e) {
			return null;
		}
	}
}
