package net.kezvh.lang.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.kezvh.patterns.AbstractFactory;

/**
 * @author mjacob
 *
 * @param <T> FIXME comment
 */
public class CloneFactory<T> implements AbstractFactory<T> {
	private final Cloneable object;
	private final Method cloneMethod;

	/**
	 * @param clazz FIXME comment
	 * @return FIXME comment
	 */
	public static boolean isClonable(final Class<?> clazz) {
		return Cloneable.class.isAssignableFrom(clazz) && ReflectionUtilities.hasExecutableDeclaredMethod(clazz, "clone", null);
	}

	/**
	 * @param o FIXME comment
	 * @return FIXME comment
	 */
	public static boolean isClonable(final Object o) {
		return o instanceof Cloneable && ReflectionUtilities.hasExecutableDeclaredMethod(o, "clone", null);
	}

	/**
	 * @param object FIXME comment
	 */
	public CloneFactory(final Cloneable object) {
		this.object = object;

		this.cloneMethod = ReflectionUtilities.getDeclaredMethod(object, "clone", null);

		if (this.cloneMethod == null)
			throw new IllegalArgumentException("object does not have a clone method.");
	}

	/**
	 * @return FIXME comment
	 */
	@SuppressWarnings("unchecked")
	public T create() throws CreationFailedException {
		try {
			return (T) this.cloneMethod.invoke(this.object, (Object[]) null);
		} catch (final IllegalArgumentException e) {
			throw new CreationFailedException(e);
		} catch (final IllegalAccessException e) {
			throw new CreationFailedException(e);
		} catch (final InvocationTargetException e) {
			throw new CreationFailedException(e);
		}
	}
}
