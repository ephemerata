package net.kezvh.functional;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import net.kezvh.collections.dualmaps.DualMap;
import net.kezvh.functional.lambda.L0;
import net.kezvh.functional.lambda.L1;
import net.kezvh.functional.lambda.L2;
import net.kezvh.functional.lambda.Predicate2;
import net.kezvh.lang.UtilityClass;

/**
 * @author afflux
 */
public final class Operations extends UtilityClass {
	/**
	 *
	 */
	public static final Predicate2<?, ?> EQUALS = new Predicate2<Object, Object>() {
		/**
		 * @see net.kezvh.functional.lambda.L2#op(java.lang.Object,
		 *      java.lang.Object)
		 * @param param1 param1
		 * @param param2 param2
		 * @return x
		 */
		@Override
		public Boolean op(final Object param1, final Object param2) {
			return (param1 == null) ? param2 == null : param1.equals(param2);
		}
	};

	/**
	 * @param <K> key type
	 * @param <V> value type
	 * @return function which returns the value of a map entry
	 */
	public static <K, V> L1<V, Map.Entry<K, V>> getValue() {
		return new L1<V, Entry<K, V>>() {
			@Override
			public V op(final Entry<K, V> arg0) {
				return arg0.getValue();
			}
		};
	}

	/**
	 * @param <K> key type
	 * @param <V> value type
	 * @return function which returns the value of a map entry
	 */
	public static <K, V> L1<K, Map.Entry<K, V>> getKey() {
		return new L1<K, Entry<K, V>>() {
			@Override
			public K op(final Entry<K, V> arg0) {
				return arg0.getKey();
			}
		};
	}

	/**
	 * @param <T> COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	public static final <T> Predicate2<T, T> equals() {
		return (Predicate2<T, T>) Operations.EQUALS;
	}

	/**
	 * @param <T> COMMENT
	 * @param t COMMENT
	 * @return COMMENT
	 */
	public static final <T> L0<T> constant0(final T t) {
		return new L0<T>() {
			@Override
			public T op() {
				return t;
			}
		};
	}

	/**
	 * @param <S> COMMENT
	 * @param <T> COMMENT
	 * @param s COMMENT
	 * @return COMMENT
	 */
	public static final <S, T> L1<S, T> constant1(final S s) {
		return new L1<S, T>() {
			public S op(final T param) {
				return s;
			}
		};
	}

	/**
	 * @param <S> COMMENT
	 * @param <T> COMMENT
	 * @param <U> COMMENT
	 * @param s COMMENT
	 * @return COMMENT
	 */
	public static final <S, T, U> L2<S, T, U> constant2(final S s) {
		return new L2<S, T, U>() {
			public S op(final T param, final U param2) {
				return s;
			}
		};
	}

	/**
	 * @param <S> COMMENT
	 * @param <T> COMMENT
	 * @param <U> COMMENT
	 * @param a COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static final <S, T, U> L1<S, U> compose(final L1<S, T> a, final L1<T, U> b) {
		return new L1<S, U>() {
			public S op(final U param) {
				return a.op(b.op(param));
			}
		};
	}

	/**
	 * @param <S> COMMENT
	 * @param <T> COMMENT
	 * @param map COMMENT
	 * @param defaultValue COMMENT
	 * @return COMMENT
	 */
	public static final <S, T> L1<S, T> forMap(final Map<T, S> map, final S defaultValue) {
		return new L1<S, T>() {
			public S op(final T param) {
				if (map.containsKey(param))
					return map.get(param);
				return defaultValue;
			}
		};
	}

	/**
	 * @param <D> COMMENT
	 * @param <K1> COMMENT
	 * @param <K2> COMMENT
	 * @param map COMMENT
	 * @param defaultValue COMMENT
	 * @return COMMENT
	 */
	public static final <D, K1, K2> L2<D, K1, K2> forMap2(final DualMap<K1, K2, D> map, final D defaultValue) {
		return new L2<D, K1, K2>() {
			public D op(final K1 param1, final K2 param2) {
				return map.get(param1, param2);
			}
		};
	}

	/**
	 * @param <T> type
	 * @return map which returns its argument
	 */
	public static final <T> L1<T, T> identity() {
		return new L1<T, T>() {
			public T op(final T param) {
				return param;
			}
		};
	}

	/**
	 * returns the size of a collection
	 */
	public static final L1<Integer, Collection<?>> COLLECTION_SIZE = new L1<Integer, Collection<?>>() {
		public Integer op(final Collection<?> value) {
			return value.size();
		}
	};
	/**
	 * returns the number of elements in the iterator (by iterating them, silly)
	 */
	public static final L1<Integer, ? super Iterator<?>> ITERATOR_COUNT = new L1<Integer, Iterator<?>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param arg0 some argument
		 * @return number of elements
		 */
		@Override
		public Integer op(final Iterator<?> arg0) {
			int i;
			for (i = 0; arg0.hasNext(); i++)
				arg0.next();
			return i;
		}
	};
	/**
	 *
	 */
	public static final L1<?, ?> UNSUPPORTED = new L1<?, ?>() {
		public Object op(final Object param) {
			throw new UnsupportedOperationException();
		}
	};
	/**
	 *
	 */
	public static final L2<Integer, ? super Integer, ? super Integer> SUMI = new L2<Integer, Integer, Integer>() {
		/**
		 * @see net.kezvh.functional.lambda.L2#op(java.lang.Object,
		 *      java.lang.Object)
		 * @param arg0 some argument
		 * @param arg1 some argument
		 * @return the sum of the arguments
		 */
		@Override
		public Integer op(final Integer arg0, final Integer arg1) {
			return Integer.valueOf(arg0.intValue() + arg1.intValue());
		}
	};
	/**
	 *
	 */
	public static final L1<Integer, Integer> INTEGER_INCREMENTER = new L1<Integer, Integer>() {
		public Integer op(final Integer param) {
			return param + 1;
		}
	};
}
