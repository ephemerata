/**
 *
 */
package net.kezvh.functional.morphisms;

import java.util.List;

import net.kezvh.functional.lambda.L2;

/**
 * currently just a stub to remind me of the motivation of doing it this way
 *
 * The whole point is you can break up a large inductive operation over multiple threads
 * to increase processing effeciency (that is, if the operation is associative).
 *
 * @author afflux
 * @param <S> FIXME comment
 * @param <T> FIXME comment
 */
public class ThreadedListCatamorphism<S, T> implements ListCatamorphism<S, T> {
	private final int maxChunkSize;
	private final int maxThreads;

	/**
	 * @param maxChunkSize FIXME comment
	 * @param maxThreads FIXME comment
	 */
	public ThreadedListCatamorphism(final int maxChunkSize, final int maxThreads) {
		this.maxChunkSize = maxChunkSize;
		this.maxThreads = maxThreads;
	}

	@Override
	public <U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> op, final V initial, final U... values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <U extends T> T fold(final L2<T, ? super T, ? super T> op, final U... values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> op, final V initial, final List<U> values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <U extends T> T fold(final L2<T, ? super T, ? super T> op, final List<U> values) {
		// TODO Auto-generated method stub
		return null;
	}
}
