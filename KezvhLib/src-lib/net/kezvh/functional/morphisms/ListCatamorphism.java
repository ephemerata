package net.kezvh.functional.morphisms;

import java.util.List;

import net.kezvh.functional.lambda.L2;

/**
 * @author afflux
 * @param <S> a
 * @param <T> b
 */
public interface ListCatamorphism<S, T> {
	/**
	 * @param <U>
	 * @param <V>
	 * @param op
	 * @param initial
	 * @param values
	 * @return the result of the catamorphism
	 */
	<U extends T, V extends S> S fold(L2<S, ? super S, ? super T> op, V initial, U... values);

	/**
	 * @param <U>
	 * @param values
	 * @param op
	 * @return fold
	 */
	<U extends T> T fold(L2<T, ? super T, ? super T> op, U... values);

	/**
	 * @param <U>
	 * @param <V>
	 * @param initial
	 * @param values
	 * @param op
	 * @return fold
	 */
	<U extends T, V extends S> S fold(L2<S, ? super S, ? super T> op, V initial, List<U> values);

	/**
	 * @param <U>
	 * @param values
	 * @param op
	 * @return fold
	 */
	<U extends T> T fold(L2<T, ? super T, ? super T> op, List<U> values);
}
