package net.kezvh.functional.morphisms;

import java.util.Iterator;
import java.util.List;

import net.kezvh.functional.lambda.L2;

/**
 * @author afflux
 * @param <S> COMMENT
 * @param <T> COMMENT
 */
public class SimpleListCatamorphism<S, T> implements ListCatamorphism<S, T> {
	/**
	 * @param values COMMENT
	 * @param map COMMENT
	 * @return stuff
	 */
	@Override
	public <U extends T> T fold(final L2<T, ? super T, ? super T> map, final List<U> values) {
		final Iterator<? extends T> i = values.iterator();
		T result = i.next();
		while (i.hasNext())
			result = map.op(result, i.next());
		return result;
	}

	/**
	 * @param initial COMMENT
	 * @param values COMMENT
	 * @param map COMMENT
	 * @return stuff
	 */
	@Override
	public <U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> map, final V initial, final List<U> values) {
		S result = initial;
		for (final T t : values)
			result = map.op(initial, t);
		return result;
	}

	/**
	 * @param initial COMMENT
	 * @param values COMMENT
	 * @param op COMMENT
	 * @return stuff
	 */
	@Override
	public <U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> op, final V initial, final U... values) {
		S result = initial;
		for (final T t : values)
			result = op.op(initial, t);
		return result;
	}

	/**
	 * @param values COMMENT
	 * @param map COMMENT
	 * @return stuff
	 */
	@Override
	public <U extends T> T fold(final L2<T, ? super T, ? super T> map, final U... values) {
		T result = values[0];
		for (int i = 1; i < values.length; i++)
			result = map.op(result, values[i]);
		return result;
	}
}
