package net.kezvh.functional.morphisms;

import java.util.Iterator;
import java.util.List;

import net.kezvh.functional.lambda.L2;
import net.kezvh.lang.UtilityClass;

/**
 * @author mjacob
 *
 */
public class StaticMorphisms extends UtilityClass {
	/**
	 * @param <S> COMMENT
	 * @param <T>  COMMENT
	 * @param <U> COMMENT
	 * @param <V> COMMENT
	 * @param op COMMENT
	 * @param initial COMMENT
	 * @param values COMMENT
	 * @return the result of the catamorphism
	 */
	public static <S, T, U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> op, final V initial, final U... values) {
		S result = initial;
		for (final T t : values)
			result = op.op(initial, t);
		return result;
	}

	/**
	 * @param <S>  COMMENT
	 * @param <T>  COMMENT
	 * @param <U> COMMENT
	 * @param values COMMENT
	 * @param op COMMENT
	 * @return fold
	 */
	public static <S, T, U extends T> T fold(final L2<T, ? super T, ? super T> op, final U... values) {
		T result = values[0];
		for (int i = 1; i < values.length; i++)
			result = op.op(result, values[i]);
		return result;
	}

	/**
	 * @param <S>  COMMENT
	 * @param <T>  COMMENT
	 * @param <U> COMMENT
	 * @param <V> COMMENT
	 * @param initial COMMENT
	 * @param values COMMENT
	 * @param op COMMENT
	 * @return fold
	 */
	public static <S, T, U extends T, V extends S> S fold(final L2<S, ? super S, ? super T> op, final V initial, final List<U> values) {
		S result = initial;
		for (final T t : values)
			result = op.op(initial, t);
		return result;
	}

	/**
	 * @param <S>  COMMENT
	 * @param <T>  COMMENT
	 * @param <U> COMMENT
	 * @param values COMMENT
	 * @param op COMMENT
	 * @return fold
	 */
	public static <S, T, U extends T> T fold(final L2<T, ? super T, ? super T> op, final List<U> values) {
		final Iterator<? extends T> i = values.iterator();
		T result = i.next();
		while (i.hasNext())
			result = op.op(result, i.next());
		return result;
	}
}
