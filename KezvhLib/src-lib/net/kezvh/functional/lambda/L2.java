package net.kezvh.functional.lambda;

/**
 * @author afflux
 * @param <R> return type
 * @param <P1> first parameter type
 * @param <P2> second parameter type
 */
public interface L2<R, P1, P2> {
	/**
	 * @param param1
	 * @param param2
	 * @return stuff
	 */
	R op(P1 param1, P2 param2);
}