package net.kezvh.functional.lambda;

import java.util.Collection;

/**
 * @author afflux
 * @param <R> return type
 * @param <P1> collection one parameter
 * @param <C1> collection one type
 * @param <P2> collection two parameter
 * @param <C2> collection two type
 */
public interface LC2<R, P1, C1 extends Collection<? extends P1>, P2, C2 extends Collection<? extends P2>> extends L2<R, C1, C2> {
	// inherited
}