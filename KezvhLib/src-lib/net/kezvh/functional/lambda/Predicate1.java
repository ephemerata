package net.kezvh.functional.lambda;

/**
 * @author mjacob
 *
 * @param <T> type on which to predicate
 */
public interface Predicate1<T> extends L1<Boolean, T> {
	//
}
