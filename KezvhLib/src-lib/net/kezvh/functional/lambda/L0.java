package net.kezvh.functional.lambda;

/**
 * @author afflux
 * @param <R> return type
 */
public interface L0<R> {
	/**
	 * @return stuff
	 */
	R op();
}