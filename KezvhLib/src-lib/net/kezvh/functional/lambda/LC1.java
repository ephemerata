package net.kezvh.functional.lambda;

import java.util.Collection;

/**
 * @author afflux
 * @param <R> return value
 * @param <P> parameter value
 * @param <C> collection type
 */
public interface LC1<R, P, C extends Collection<? extends P>> extends L1<R, C> {
	// inherited
}