package net.kezvh.functional.lambda;


/**
 * @author afflux
 * @param <R> return type
 * @param <P> parameter type
 */
public interface L1<R, P> {
	/**
	 * @param param
	 * @return stuff
	 */
	R op(P param);
}