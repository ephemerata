package net.kezvh.functional.lambda;

/**
 * @author afflux
 * @param <R> return type
 * @param <P> parameter type
 */
public interface LN<R, P> {
	/**
	 * @param params
	 * @return stuff
	 */
	R op(P... params);
}