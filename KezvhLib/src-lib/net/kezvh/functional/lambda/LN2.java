package net.kezvh.functional.lambda;

/**
 * @author mjacob
 *
 * @param <R> FIXME comment
 * @param <P1> FIXME comment
 * @param <P2> FIXME comment
 */
public interface LN2<R, P1, P2> {
	/**
	 * @param params1  FIXME comment
	 * @param params2  FIXME comment
	 * @return stuff FIXME comment
	 */
	R op(P1[] params1, P2[] params2);
}
