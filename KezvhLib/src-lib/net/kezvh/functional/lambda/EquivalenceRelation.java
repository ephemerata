/**
 *
 */
package net.kezvh.functional.lambda;

/**
 * @author afflux
 * @param <V> x
 */
public interface EquivalenceRelation<V> extends Relation<Boolean, V> {
	/**
	 *
	 */
	EquivalenceRelation<Object> EQUALS = new EquivalenceRelation<Object>() {
		/**
		 * @see net.kezvh.functional.lambda.L2#op(java.lang.Object, java.lang.Object)
		 * @param param1 sd
		 * @param param2 asdf
		 * @return x
		 */
		@Override
		public Boolean op(final Object param1, final Object param2) {
			return param1 == null ? param2 == null : param1.equals(param2);
		}
	};
}
