package net.kezvh.functional.lambda;

/**
 * @author afflux
 * @param <R> return type
 * @param <P1> first paramteter type
 * @param <P2> second paramterer type
 * @param <P3> third paramter type
 */
public interface L3<R, P1, P2, P3> {
	/**
	 * @param param1 some argument
	 * @param param2 some argument
	 * @param param3 some argument
	 * @return stuff
	 */
	R op(P1 param1, P2 param2, P3 param3);
}