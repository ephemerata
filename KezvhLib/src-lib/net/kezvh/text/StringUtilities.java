package net.kezvh.text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author afflux
 */
public class StringUtilities { // Contains methods that could/should be in gerryCore class StringUtilities

	/**
	 * @param stringBuilder
	 * @param list
	 */
	/**
	 * @param <T> FIXME comment
	 * @param stringBuilder FIXME comment
	 * @param list FIXME comment
	 */
	public static <T> void appendIndentedStringRepresentationOfCollection(final StringBuilder stringBuilder, final List<T> list) {
		for (final Iterator<T> i = list.iterator(); i.hasNext();) {
			final Object object = i.next();

			final String indentedObjectString = StringUtilities.prependAllLinesWithTabs(object.toString());
			stringBuilder.append(indentedObjectString);
		}

	}

	/**
	 * @param charSequence FIXME comment
	 * @return x
	 */
	public static String prependAllLinesWithTabs(final CharSequence charSequence) {
		return StringUtilities.prependAllLinesWithLinePrefix(charSequence, "\t");
	}

	/**
	 * @param charSequence FIXME comment
	 * @param linePrefix FIXME comment
	 * @return x
	 */
	public static String prependAllLinesWithLinePrefix(final CharSequence charSequence, final String linePrefix) {
		final Pattern pattern = Pattern.compile("^", Pattern.MULTILINE);
		final Matcher matcher = pattern.matcher(charSequence);

		return matcher.replaceAll(linePrefix);
	}

	/**
	 * @param n FIXME comment
	 * @param s FIXME comment
	 * @return x
	 */
	public static String nCopies(final int n, final Object s) {
		final StringBuilder string = new StringBuilder();
		for (int i = 0; i < n; i++)
			string.append(s);
		return string.toString();
	}

	/**
	 * @param regexp FIXME comment
	 * @param string FIXME comment
	 * @return x
	 */
	public static String[] getAllGroups(final String regexp, final String string) {
		return StringUtilities.getAllGroups(Pattern.compile(regexp), string);
	}

	/**
	 * @param pattern FIXME comment
	 * @param string FIXME comment
	 * @return x
	 */
	public static String[] getAllGroups(final Pattern pattern, final String string) {
		final Matcher m = pattern.matcher(string);
		final List<String> strings = new ArrayList<String>();

		while (m.find())
			for (int i = 1; i <= m.groupCount(); i++)
				strings.add(m.group(i));

		return strings.toArray(new String[strings.size()]);
	}

}
