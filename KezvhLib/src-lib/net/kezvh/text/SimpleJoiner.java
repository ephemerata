/**
 *
 */
package net.kezvh.text;

import java.util.List;

import net.kezvh.collections.KezvhArrays;
import net.kezvh.collections.views.ListSlice;
import net.kezvh.collections.wrappers.ArrayAsList;
import net.kezvh.functional.lambda.L2;
import net.kezvh.functional.morphisms.SimpleListCatamorphism;

/**
 * @author mjacob
 *
 * @param <T> FIXME comment
 */
public final class SimpleJoiner<T> implements Joiner<T> {
	private final String junction;

	private final L2<StringBuilder, StringBuilder, T> join = new L2<StringBuilder, StringBuilder, T>() {
		@Override
		public StringBuilder op(final StringBuilder param1, final T param2) {
			return param1.append(SimpleJoiner.this.junction).append(param2);
		}
	};

	private final SimpleListCatamorphism<StringBuilder, T> cat = new SimpleListCatamorphism<StringBuilder, T>();

	/**
	 * @param junction FIXME comment
	 */
	public SimpleJoiner(final String junction) {
		super();
		this.junction = junction;
	}

	@Override
	public String join(final T... values) {
		if (values.length == 0)
			return "";
		return this.cat.fold(this.join, new StringBuilder(values[0].toString()), new ArrayAsList<T>(values, 1)).toString();
	}

	@Override
	public String join(final List<T> values) {
		if (values.isEmpty())
			return "";
		return this.cat.fold(this.join, new StringBuilder(values.get(0).toString()), new ListSlice<T>(values, KezvhArrays.range(1, values.size()))).toString();
	}
}