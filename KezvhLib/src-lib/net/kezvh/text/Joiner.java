/**
 *
 */
package net.kezvh.text;

import java.util.List;

/**
 * @author mjacob
 *
 * @param <T> FIXME comment
 */
public interface Joiner<T> {
	/**
	 * @param values
	 * @return FIXME comment
	 */
	String join(T... values);

	/**
	 * @param values
	 * @return FIXME comment
	 */
	String join(List<T> values);
}