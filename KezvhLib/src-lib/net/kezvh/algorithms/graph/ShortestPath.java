package net.kezvh.algorithms.graph;

/**
 * @author mjacob
 *
 * @param <T> node type
 */
public interface ShortestPath<T> {
	/**
	 * @param dest
	 * @return shortest path between the two points
	 */
	Path<T> getShortestPath();

	/**
	 * @param dest
	 * @return true if the source is connected to the destination;
	 */
	boolean connected();

	/**
	 * @return the source for this sssp instance
	 */
	T getSource();

	/**
	 * @return the source for this sssp instance
	 */
	T getDestination();
}
