package net.kezvh.algorithms.graph;

/**
 * @author mjacob
 * @param <T> node type
 */
public interface SingleSourceShortestPath<T> {
	/**
	 * @param dest
	 * @return shortest path between the two points
	 */
	Path<T> getShortestPath(T dest);

	/**
	 * @param dest
	 * @return true if the source is connected to the destination;
	 */
	boolean connected(T dest);

	/**
	 * @return the source for this sssp instance
	 */
	T getSource();
}
