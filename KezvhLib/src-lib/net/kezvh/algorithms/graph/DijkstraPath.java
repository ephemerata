package net.kezvh.algorithms.graph;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.heaps.Heap;
import net.kezvh.collections.heaps.PairingHeap;
import net.kezvh.lang.AlgorithmException;
import net.kezvh.patterns.AbstractFactory;
import net.kezvh.patterns.AbstractFactory.CreationFailedException;

/**
 * @author mjacob
 * @param <T> node type
 */
public class DijkstraPath<T> implements ShortestPath<T> {
	final T source;
	final T dest;
	private final Map<T, Integer> dist = new HashMap<T, Integer>();
	private final Map<T, T> previous = new HashMap<T, T>();

	private ShortestPath shortestPath;
	private boolean connected = false;

	/**
	 * infinity to use in comparators
	 */
	public static final int INFINITY = -1;

	/**
	 * 1 function Dijkstra(Graph, source): 2 for each vertex v in Graph: //
	 * Initializations 3 dist[v] := infinity // Unknown distance function from
	 * source to v 4 previous[v] := undefined // Previous node in optimal path
	 * from source 5 dist[source] := 0 // Distance from source to source 6 Q :=
	 * the set of all nodes in Graph // All nodes in the graph are unoptimized -
	 * thus are in Q 7 while Q is not empty: // The main loop 8 u := node in Q
	 * with smallest dist[] 9 remove u from Q 10 for each neighbor v of u: //
	 * where v has not yet been removed from Q. 11 alt := dist[u] +
	 * dist_between(u, v) 12 if alt < dist[v] // Relax (u,v) 13 dist[v] := alt
	 * 14 previous[v] := u 15 return previous[]
	 */

	private final class ShortestPath extends AbstractList<T> implements Path<T> {
		private final T[] nodes;

		@SuppressWarnings("unchecked")
		public ShortestPath() {
			super();

			final List<T> previa = new LinkedList<T>();
			T p = DijkstraPath.this.dest;
			while (p != null) {
				previa.add(0, p);
				p = DijkstraPath.this.previous.get(p);
			}

			this.nodes = previa.toArray((T[]) Array.newInstance(DijkstraPath.this.dest.getClass(), previa.size()));

			if (!this.nodes[0].equals(DijkstraPath.this.source))
				throw new AlgorithmException();
			if (!DijkstraPath.this.dest.equals(DijkstraPath.this.source) && this.nodes.length == 1)
				throw new AlgorithmException();
		}

		@Override
		public T get(final int index) {
			return this.nodes[index];
		}

		@Override
		public int size() {
			return this.nodes.length;
		}

		@Override
		public int getLength() {
			return DijkstraPath.this.dist.get(DijkstraPath.this.dest);
		}
	}

	/**
	 * @param graph graph of nodes
	 * @param source source node
	 * @param dest destination node
	 */
	public DijkstraPath(final DirectedGraphWithEdgeValues<T, Integer> graph, final T source, final T dest) {
		this(graph, source, dest, new AbstractFactory<Heap<T, Integer>>() {
			@Override
			public Heap<T, Integer> create() throws CreationFailedException {
				return new PairingHeap<T, Integer>(new Comparator<Integer>() {
					@Override
					public int compare(final Integer o1, final Integer o2) {
						if (DijkstraPath.INFINITY == o1)
							return DijkstraPath.INFINITY == o2 ? 0 : 1;
						else if (DijkstraPath.INFINITY == o2)
							return -1;
						else
							return o1.compareTo(o2);
					}
				});
			}
		});
	}

	/**
	 * @param graph graph of nodes
	 * @param source source node
	 * @param dest destination node
	 * @param heapMaker needs to create a heap w/ a priority which ranks -1 as
	 *            INFINITY
	 */
	public DijkstraPath(final DirectedGraphWithEdgeValues<T, Integer> graph, final T source, final T dest, final AbstractFactory<Heap<T, Integer>> heapMaker) {
		super();
		this.source = source;
		this.dest = dest;

		final Heap<T, Integer> Q;
		try {
			Q = heapMaker.create();
		} catch (final CreationFailedException e) {
			throw new RuntimeException();
		}

		final Comparator<? super Integer> comparator = Q.comparator();

		for (final T v : graph)
			if (!source.equals(v)) {
				this.dist.put(v, Dijkstra.INFINITY);
				Q.add(v, Dijkstra.INFINITY);
			} else {
				this.dist.put(v, 0);
				Q.add(v, 0);
			}

		while (!Q.isEmpty()) {
			final T u = Q.remove();
			if (this.dist.get(u) == DijkstraPath.INFINITY) {
				this.connected = false;
				break;
			}

			if (u.equals(dest)) {
				this.connected = true;
				break;
			}
			for (final T v : graph.adjacentFrom(u)) {
				final int alt = ((int) this.dist.get(u)) == Dijkstra.INFINITY ? Dijkstra.INFINITY : this.dist.get(u) + graph.getEdgeValue(u, v);

				if (comparator.compare(alt, this.dist.get(v)) < 0) {
					if (Q.containsKey(v))
						Q.reweight(v, alt);
					this.dist.put(v, alt);
					this.previous.put(v, u);
				}
			}
		}
	}

	@Override
	public Path<T> getShortestPath() {
		if (this.shortestPath == null && this.connected)
			this.shortestPath = new ShortestPath();
		return this.shortestPath;
	}

	public boolean connected() {
		return this.connected;
	}

	/**
	 * @see net.kezvh.algorithms.graph.SingleSourceShortestPath#getSource()
	 * @return the source of the sssp
	 */
	@Override
	public T getSource() {
		return this.source;
	}

	@Override
	public T getDestination() {
		return this.dest;
	}
}
