/**
 *
 */
package net.kezvh.algorithms.graph;


/**
 * @author afflux
 * @param <T> Node type
 */
public interface AllPairsShortestPath<T> {
	/**
	 * @param source
	 * @param dest
	 * @return shortest path between the two points
	 */
	Path<T> getShortestPath(T source, T dest);
}
