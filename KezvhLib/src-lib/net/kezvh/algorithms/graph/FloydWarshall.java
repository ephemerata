/**
 *
 */
package net.kezvh.algorithms.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import net.kezvh.collections.IntegerRange;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.views.ListSlice;

/**
 * @author afflux
 * @param <T> Node type
 */
public class FloydWarshall<T> implements AllPairsShortestPath<T> {
	private final DirectedGraphWithEdgeValues<T, Integer> graph;

	private static final class NodeList<T> extends LinkedList<T> implements Path<T> {
		private int length;

		public NodeList() {
			this.length = 0;
		}

		public NodeList(final T a, final T b, final int initialLength) {
			this.add(a, 0);
			this.add(b, initialLength);
		}

		@Deprecated
		@Override
		public boolean add(final T e) {
			throw new UnsupportedOperationException();
		}

		@Deprecated
		@Override
		public boolean addAll(final Collection<? extends T> c) {
			throw new UnsupportedOperationException();
		}

		@Deprecated
		@Override
		public T set(final int index, final T element) {
			throw new UnsupportedOperationException();
		}

		@Deprecated
		@Override
		public boolean addAll(final int index, final Collection<? extends T> c) {
			throw new UnsupportedOperationException();
		}

		@Deprecated
		@Override
		public void add(final int index, final T element) {
			throw new UnsupportedOperationException();
		}

		public void add(final T node, final int addedLength) {
			super.add(node);
			this.length += addedLength;
		}

		public void addAll(final NodeList<T> b, final NodeList<T> c) {
			super.addAll(b);
			super.addAll(new ListSlice<T>(c, new IntegerRange(1, c.size())));
			this.length += b.getLength() + c.getLength();
		}

		public int getLength() {
			return this.length;
		}
	}

	private final Map<T, Map<T, NodeList<T>>> shortestPaths;

	/**
	 * @param graph a directed graph
	 */
	public FloydWarshall(final DirectedGraphWithEdgeValues<T, Integer> graph) {
		this.graph = graph;
		this.shortestPaths = new HashMap<T, Map<T, NodeList<T>>>(graph.size());
		for (final T t : graph) {
			this.shortestPaths.put(t, new HashMap<T, NodeList<T>>());
			for (final T s : graph)
				if (this.graph.containsEdge(t, s)) {
					final NodeList<T> array = new NodeList<T>(t, s, this.graph.getEdgeValue(t, s));
					this.set(t, s, array);
				} else {
					final NodeList<T> empty = new NodeList<T>();
					this.set(t, s, empty);
				}
		}

		for (final T k : graph)
			for (final T j : graph)
				for (final T i : graph)
					this.set(i, j, this.min(this.get(i, j), this.get(i, k), this.get(k, j)));
	}

	private NodeList<T> get(final T a, final T b) {
		return this.shortestPaths.get(a).get(b);
	}

	private void set(final T a, final T b, final NodeList<T> value) {
		this.shortestPaths.get(a).put(b, value);
	}

	private final NodeList<T> min(final NodeList<T> a, final NodeList<T> b, final NodeList<T> c) {
		if (b.getLength() != 0 && c.getLength() != 0)
			if (a.getLength() == 0) {
				a.addAll(b, c);
				return a;
			} else if (b.getLength() + c.getLength() - 1 < a.getLength()) {
				a.clear();
				a.addAll(b, c);
				return a;
			} else
				return a;
		return a;
	}

	/**
	 * @see net.kezvh.algorithms.graph.AllPairsShortestPath#getShortestPath(java.lang.Object,
	 *      java.lang.Object)
	 * @param source source node
	 * @param dest destniation node
	 * @return shortest path
	 */
	public Path<T> getShortestPath(final T source, final T dest) {
		return this.shortestPaths.get(source).get(dest);
	}
}
