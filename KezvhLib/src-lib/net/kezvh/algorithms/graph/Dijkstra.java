package net.kezvh.algorithms.graph;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.heaps.Heap;
import net.kezvh.collections.heaps.PairingHeap;
import net.kezvh.lang.AlgorithmException;
import net.kezvh.patterns.AbstractFactory;
import net.kezvh.patterns.AbstractFactory.CreationFailedException;

/**
 * @author mjacob
 * @param <T> node type
 */
public class Dijkstra<T> implements SingleSourceShortestPath<T> {
	final T source;
	private final Map<T, Integer> dist = new HashMap<T, Integer>();
	private final Map<T, T> previous = new HashMap<T, T>();

	private final Map<T, ShortestPath> shortestPaths = new HashMap<T, ShortestPath>();

	/**
	 * infinity to use in comparators
	 */
	public static final int INFINITY = -1;

	/**
	 * 1 function Dijkstra(Graph, source): 2 for each vertex v in Graph: //
	 * Initializations 3 dist[v] := infinity // Unknown distance function from
	 * source to v 4 previous[v] := undefined // Previous node in optimal path
	 * from source 5 dist[source] := 0 // Distance from source to source 6 Q :=
	 * the set of all nodes in Graph // All nodes in the graph are unoptimized -
	 * thus are in Q 7 while Q is not empty: // The main loop 8 u := node in Q
	 * with smallest dist[] 9 remove u from Q 10 for each neighbor v of u: //
	 * where v has not yet been removed from Q. 11 alt := dist[u] +
	 * dist_between(u, v) 12 if alt < dist[v] // Relax (u,v) 13 dist[v] := alt
	 * 14 previous[v] := u 15 return previous[]
	 */

	private final class ShortestPath extends AbstractList<T> implements Path<T> {
		private final T dest;
		private final T[] nodes;

		@SuppressWarnings("unchecked")
		public ShortestPath(final T dest) {
			super();
			this.dest = dest;

			final List<T> previa = new LinkedList<T>();
			T p = dest;
			while (p != null) {
				previa.add(0, p);
				p = Dijkstra.this.previous.get(p);
			}

			this.nodes = previa.toArray((T[]) Array.newInstance(dest.getClass(), previa.size()));

			if (this.nodes[0] != Dijkstra.this.source)
				throw new AlgorithmException();
			if (dest != Dijkstra.this.source && this.nodes.length == 1)
				throw new AlgorithmException();
		}

		@Override
		public T get(final int index) {
			return this.nodes[index];
		}

		@Override
		public int size() {
			return this.nodes.length;
		}

		@Override
		public int getLength() {
			return Dijkstra.this.dist.get(this.dest);
		}
	}

	/**
	 * @param graph graph of nodes
	 * @param source source node
	 */
	public Dijkstra(final DirectedGraphWithEdgeValues<T, Integer> graph, final T source) {
		this(graph, source, new AbstractFactory<Heap<T, Integer>>() {
			@Override
			public Heap<T, Integer> create() throws CreationFailedException {
				return new PairingHeap<T, Integer>(new Comparator<Integer>() {
					@Override
					public int compare(final Integer o1, final Integer o2) {
						if (DijkstraPath.INFINITY == o1)
							return DijkstraPath.INFINITY == o2 ? 0 : 1;
						else if (DijkstraPath.INFINITY == o2)
							return -1;
						else
							return o1.compareTo(o2);
					}
				});
			}
		});
	}

	/**
	 * @param graph COMMENT
	 * @param source COMMENT
	 * @param heapMaker needs to create a heap w/ a priority which ranks -1 as
	 *            INFINITY
	 */
	public Dijkstra(final DirectedGraphWithEdgeValues<T, Integer> graph, final T source, final AbstractFactory<Heap<T, Integer>> heapMaker) {
		super();
		this.source = source;

		final Heap<T, Integer> Q;
		try {
			Q = heapMaker.create();
		} catch (final CreationFailedException e) {
			throw new RuntimeException();
		}

		final Comparator<? super Integer> comparator = Q.comparator();

		for (final T v : graph)
			if (!source.equals(v)) {
				this.dist.put(v, Dijkstra.INFINITY);
				Q.add(v, Dijkstra.INFINITY);
			} else {
				this.dist.put(v, 0);
				Q.add(v, 0);
			}

		while (!Q.isEmpty()) {
			final T u = Q.remove();
			if (this.dist.get(u) == DijkstraPath.INFINITY)
				break;

			for (final T v : graph.adjacentFrom(u)) {
				final int alt = ((int) this.dist.get(u)) == Dijkstra.INFINITY ? Dijkstra.INFINITY : this.dist.get(u) + graph.getEdgeValue(u, v);
				if (comparator.compare(alt, this.dist.get(v)) < 0) {
					if (Q.containsKey(v))
						Q.reweight(v, alt);
					this.dist.put(v, alt);
					this.previous.put(v, u);
				}
			}
		}
	}

	@Override
	public Path<T> getShortestPath(final T dest) {
		if (!this.dist.containsKey(dest))
			throw new IllegalArgumentException("Node " + dest + " is not in the graph");
		if (this.dist.get(dest) == Dijkstra.INFINITY)
			return null;

		if (this.shortestPaths.containsKey(dest))
			return this.shortestPaths.get(dest);

		final ShortestPath shortestPath = new ShortestPath(dest);
		this.shortestPaths.put(dest, shortestPath);
		return shortestPath;
	}

	/**
	 * @return all nodes connected to the source node
	 */
	public Collection<T> getAllConnected() {
		return this.previous.keySet(); // i think this should be good
	}

	/**
	 * @param dest dest
	 * @return the length of the path to dest
	 */
	public int getDistanceTo(final T dest) {
		final Path<T> path = this.getShortestPath(dest);
		if (path == null)
			return Dijkstra.INFINITY;
		return path.getLength();
	}

	public boolean connected(final T dest) {
		if (!this.dist.containsKey(dest))
			throw new IllegalArgumentException("Node " + dest + " is not in the graph");
		return this.dist.get(dest) != Dijkstra.INFINITY;
	}

	/**
	 * @see net.kezvh.algorithms.graph.SingleSourceShortestPath#getSource()
	 * @return the source of the sssp
	 */
	@Override
	public T getSource() {
		return this.source;
	}
}
