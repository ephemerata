package net.kezvh.algorithms.graph;

import java.util.List;

/**
 * @author mjacob
 *
 * @param <E> graph node
 */
public interface Path<E> extends List<E> {
	/**
	 * @return the length of the path
	 */
	int getLength();
}
