package net.kezvh.algorithms.text;

import net.kezvh.functional.lambda.L2;

/**
 *
 * @author mjacob
 *
 */
public enum EditDistance implements L2<Integer, String, String> {
	/**
	 *
	 */
	LEVENSHTEIN_NAIVE {
		@Override
		public Integer op(final String str1, final String str2) {
			final int[][] d = new int[str1.length() + 1][];
			for (int i = 0; i <= str1.length(); i++) {
				d[i] = new int[str2.length() + 1];
				d[i][0] = i;
			}
			for (int j = 1; j <= str2.length(); j++)
				d[0][j] = j;

			for (int i = 1; i <= str1.length(); i++)
				for (int j = 1; j <= str2.length(); j++) {
					final char c1 = str1.charAt(i - 1);
					final char c2 = str2.charAt(j - 1);
					d[i][j] = this.min(d[i - 1][j] + 1, // deletion
					d[i][j - 1] + 1, // insertion
					d[i - 1][j - 1] + (c1 == c2 ? 0 : 1));// substitution
				}
			return d[str1.length()][str2.length()];
		}

		private int min(final int a, final int b, final int c) {
			if (a < b && a < c)
				return a;
			else if (b < c)
				return b;
			return c;
		}
	},
	/**
	 *
	 */
	DAMERAU_LEVENSHTEIN_NAIVE {
		/**
		 * see http://en.wikipedia.org/wiki/Damerau-Levenshtein_distance
		 *
		 * int DamerauLevenshteinDistance(char str1[1..lenStr1], char str2[1..lenStr2])
		 *    // d is a table with lenStr1+1 rows and lenStr2+1 columns
		 *    declare int d[0..lenStr1, 0..lenStr2]
		 *    // i and j are used to iterate over str1 and str2
		 *    declare int i, j, cost
		 *
		 *    for i from 0 to lenStr1
		 *        d[i, 0] := i
		 *    for j from 1 to lenStr2
		 *        d[0, j] := j
		 *
		 *    for i from 1 to lenStr1
		 *        for j from 1 to lenStr2
		 *            if str1[i] = str2[j] then cost := 0
		 *                                 else cost := 1
		 *            d[i, j] := minimum(
		 *                                 d[i-1, j  ] + 1,     // deletion
		 *                                 d[i  , j-1] + 1,     // insertion
		 *                                 d[i-1, j-1] + cost   // substitution
		 *                             )
		 *            if(i > 1 and j > 1 and str1[i] = str2[j-1] and str1[i-1] = str2[j]) then
		 *                d[i, j] := minimum(
		 *                                 d[i, j],
		 *                                 d[i-2, j-2] + cost   // transposition
		 *                              )
		 *
		 * return d[lenStr1, lenStr2]
		 */
		@Override
		public Integer op(final String str1, final String str2) {
			final int[][] d = new int[str1.length() + 1][];
			for (int i = 0; i <= str1.length(); i++) {
				d[i] = new int[str2.length() + 1];
				d[i][0] = i;
			}
			for (int j = 1; j <= str2.length(); j++)
				d[0][j] = j;

			for (int i = 1; i <= str1.length(); i++)
				for (int j = 1; j <= str2.length(); j++) {
					final char c1 = str1.charAt(i - 1);
					final char c2 = str2.charAt(j - 1);
					d[i][j] = this.min(d[i - 1][j] + 1, // deletion
					d[i][j - 1] + 1, // insertion
					d[i - 1][j - 1] + (c1 == c2 ? 0 : 1));// substitution

					if (i > 1 && j > 1) {
						final char c1p = str1.charAt(i - 2);
						final char c2p = str2.charAt(j - 2);

						if (c1 == c2p && c1p == c2)
							d[i][j] = this.min(d[i][j], d[i - 2][j - 2] + (c1 == c2 ? 0 : 1)); // parentheses, parentheses
					}
				}
			return d[str1.length()][str2.length()];
		}

		private int min(final int a, final int b) {
			return (a < b) ? a : b;
		}

		private int min(final int a, final int b, final int c) {
			if (a < b && a < c)
				return a;
			else if (b < c)
				return b;
			return c;
		}
	},
	/**
	 *
	 */
	DAMERAU_LEVENSHTEIN_OPTIMIZED1 {
		private final int WORD_LENGTH = 23;// FIXME set this to whatever the maximum word size is

		private final int[][] buffer = new int[3][];
		{
			for (int i = 0; i < 3; i++)
				this.buffer[i] = new int[this.WORD_LENGTH];
		}

		@Override
		public Integer op(final String string1, final String string2) {
			final int length1 = string1.length();
			final int length2 = string2.length();
			if (length1 == 0 || length2 == 0)
				return length1 + length2;
			if (length1 == 1)
				if (string2.indexOf(string1.charAt(0)) >= 0)
					return length2 - 1;
				else
					return length2;
			if (length2 == 1)
				if (string1.indexOf(string2.charAt(0)) >= 0)
					return length1 - 1;
				else
					return length1;

			final char[] chars1 = string1.toCharArray();
			final char[] chars2 = string2.toCharArray();

			char char1, char2, char1p, char2p;
			int i, ip, ipp, pipp;

			int a, b, c, d, e, f, cost;
			int av = 0, bv, cv;

			/**
			 *     j
			 *   0 1 2 3 4 5
			 * i 1
			 *   2
			 *   3
			 */

			/**
			 *  c b
			 *  a X
			 *
			 *  a = d[i-1, j  ] + 1,     // deletion
			 *  b = d[i  , j-1] + 1,     // insertion
			 *    c = d[i-1, j-1] + cost   // substitution
			 */

			// initialize first element
			char1 = chars1[0];
			if (char1 == chars2[0])
				av = this.buffer[0][0] = 0;
			else
				av = this.buffer[0][0] = 1;

			// initialize first row
			for (int j = 1; j < length2; j++) {
				// "i = 0"
				char2 = chars2[j];
				a = av + 1;// deletion d[0,j] = j
				b = j + 1;// insertion
				c = j + (char1 == char2 ? 0 : 1);// substitution   d[0, j-1] = jp

				av = this.buffer[0][j] = (a < b && a < c) ? a : (b < c) ? b : c;

			}

			int x = 1; // position in string 1

			/**
			 * four pointers for looping through 3 values
			 */
			for (i = 1, ip = 0, ipp = 2, pipp = 2; x < length1; x++, ipp = ip, ip = i, i = pipp, pipp = ipp) {
				/**
				 *         j
				 *         |
				 *         v
				 *     d
				 *       c b
				 * i-->  a X
				 *
				 *  a = d[i-1, j  ] + 1,     // deletion
				 *  b = d[i  , j-1] + 1,     // insertion
				 *    c = d[i-1, j-1] + cost   // substitution
				 *  d = d[i-2, j-2] + cost   // transposition
				 */

				// initialize first column (j = 0)
				char1p = char1; // previous char initialized as char1 before first loop
				char1 = chars1[x]; // char we're ut
				char2 = chars2[0]; // start of word
				a = i + 1;// deletion // cost deletion (point a)
				bv = this.buffer[ip][0]; // store value @ b for next loop
				b = bv + 1;// insertion ... ip is the row we're in....
				c = x + (char1 == char2 ? 0 : 1);// substitution
				av = this.buffer[i][0] = (a < b && a < c) ? a : (b < c) ? b : c;

				// the rest
				for (int j = 1, jp = 0, jpp = -1; j < length2; jpp = jp, jp = j, j++) {
					char2p = char2; // increment position in second word
					char2 = chars2[j]; // increment position in second word
					cost = (char1 == char2 ? 0 : 1);
					cv = bv;
					bv = this.buffer[ip][j];
					a = av + 1;
					b = bv + 1;
					c = cv + (char1 == char2 ? 0 : 1);
					if (char1 == char2p && char1p == char2) { // we can transpose
						d = (jpp == -1 ? (x - 1) : this.buffer[ipp][jpp]) + cost;
						e = (a < b) ? a : b;
						f = (c < d) ? c : d;
						av = this.buffer[i][j] = (e < f ? e : f);
					} else
						av = this.buffer[i][j] = (a < b && a < c) ? a : (b < c) ? b : c;
				}
			}
			return av;
		}
	},
	/**
	 * @author afflux
	 *
	 * see http://www.berghel.net/publications/asm/asm.php
	 *
	 * note: a{x} means x'th character of a (ocr of pdf of scanned text as html == crap)
	 * also note: by convention, the second index in FKP ranges from -1 to something. these men deserve to be shot.
	 *
	 *   n := len(a)
	 *   m := len(b)
	 *   k := n-m
	 *   p := k
	 *   repeat
	 *       inc := p;
	 *       for temp_p := 0 to p-1
	 *           if |(n-m) - inc| <= temp_p
	 *               f((n-m)-inc,temp_p);
	 *           endif
	 *           if |(n-m) + inc| <= temp_p
	 *               f((n-m)+inc,temp_p);
	 *           endif
	 *           inc := inc - 1;
	 *       endfor
	 *       f(n-m,p);
	 *       p := p + 1;
	 *   until FKP[(n-m)+ZERO_K,p-1] = m;
	 *   s := p-1;
	 *
	 *   procedure f(k, p)
	 *       begin
	 *          t := FKP[k+ZERO_K,p-1] + 1;
	 *           t2 := t;
	 *           if a{t}a{t+1} = b{k+t+1}b{k+t} then
	 *              t2 := t+1;
	 *           t := max(t, FKP[k-1+ZERO_K,p-1],
	 *               FKP[k+1+ZERO_K,p-1] + 1,t2);
	 *           while a{t+1} = b{t+1+k} and t < min(m,n-k) do
	 *               t := t+1;
	 *           FKP[k+ZERO_K,p] := t;
	 *       end
	 */
	BERGHEL_ROACH {
		private final int MAX_WORD_LENGTH = 24;
		private final int MAX_P = this.MAX_WORD_LENGTH; // maximum distance
		private final int MAX_K = this.MAX_WORD_LENGTH; // maximum difference in length
		private final int ZERO_K = this.MAX_K >> 1;

		private final int[][] FKP = new int[this.MAX_K + 1][];
		{
			for (int i = 0; i <= this.MAX_K; i++)
				this.FKP[i] = new int[this.MAX_P];

		}

		private void init() {
			for (int i = 0; i < this.FKP.length; i++)
				for (int j = 0; j < this.FKP[i].length; j++)
					this.FKP[i][j] = Integer.MIN_VALUE;
			this.FKP[this.ZERO_K][0] = -1;
			for (int i = 1; i <= this.ZERO_K; i++) {
				this.FKP[this.ZERO_K - i][i] = i - 1;
				this.FKP[this.ZERO_K + i][i] = -1;
			}
		}

		@Override
		public Integer op(final String param1, final String param2) {
			final int m = param1.length();
			final int n = param2.length();
			if (n < m)
				return this.op(param2, param1);

			if (m == 0 || n == 0)
				return n + m;
			if (m == 1)
				if (param2.indexOf(param1.charAt(0)) >= 0)
					return n - 1;
				else
					return n;
			if (n == 1)
				if (param1.indexOf(param2.charAt(0)) >= 0)
					return m - 1;
				else
					return m;
			if (param1.equals(param2))
				return 0;

			final char[] a = param1.toCharArray();
			final char[] b = param2.toCharArray();
			final int k = n - m;
			int p = k;

			this.init();
			EditDistance.print(this.FKP);
			do {
				int inc = p;
				for (int tmp_p = 0; tmp_p < p; tmp_p++) {
					if (Math.abs(n - m - inc) <= tmp_p)
						this.f(n - m - inc, tmp_p, a, b);
					else if (Math.abs(n - m + inc) <= tmp_p)
						this.f(n - m + inc, tmp_p, a, b);
					inc--;
				}
				this.f(n - m, p, a, b);
				p++;
			} while (this.FKP[(n - m) + this.ZERO_K][p] != m);
			return p - 1;
		}

		private void f(final int k, final int p, final char[] a, final char[] b) {

			int t = this.FKP[k + this.ZERO_K][p] + 1;
			int t2 = t;
			final int m = a.length;
			final int n = b.length;

			if (t < a.length - 1 && a[t] == b[k + t + 1] && a[t + 1] == b[k + t])
				t2 = t + 1;

			final int A = this.FKP[k - 1 + this.ZERO_K][p];
			final int B = this.FKP[k + 1 + this.ZERO_K][p] + 1;
			t = (A > B && A > t2) ? A : (B > t2) ? B : t2;
			while (t < m && t < (n - k) && a[t] == b[t + k])
				t++;
			System.out.print(this.FKP[k + this.ZERO_K][p + 1] + ", ");
			this.FKP[k + this.ZERO_K][p + 1] = t;
			System.out.println(k + this.ZERO_K + ", " + (p + 1) + ", " + t);
			EditDistance.print(this.FKP);
		}
	},
	/**
	 * @author afflux
	 *
	 * hamming distance + length distance (starting @ 0)
	 */
	HAMMING_PLUS {
		@Override
		public Integer op(final String param1, final String param2) {

			int d = 0;
			if (param1.length() > param2.length()) {
				d += param1.length() - param2.length();
				for (int i = 0; i < param2.length(); i++)
					if (param1.charAt(i) != param2.charAt(i))
						d++;
			} else {
				d += param2.length() - param1.length();
				for (int i = 0; i < param1.length(); i++)
					if (param1.charAt(i) != param2.charAt(i))
						d++;
			}
			return d;
		}
	},
	/**
	 * @author afflux
	 *
	 * length distance + minimal hamming distance over possible offsets
	 */
	HAMMING_OFFSET {
		@Override
		public Integer op(final String param1, final String param2) {

			final int diff = param1.length() - param2.length();
			int d = 0;
			if (d > 0) {
				d += diff;
				int min = param2.length();
				for (int j = 0; j < diff; j++) {
					int current = 0;
					for (int i = 0; i < param2.length(); i++)
						if (param1.charAt(i + j) != param2.charAt(i))
							current++;
					if (current < min)
						min = current;
				}
				d += min;
			} else {
				d += diff;
				int min = param1.length();
				for (int j = 0; j < -diff; j++) {
					int current = 0;
					for (int i = 0; i < param1.length(); i++)
						if (param1.charAt(i) != param2.charAt(i + j))
							current++;
					if (current < min)
						min = current;
				}
				d += min;
			}
			return d;
		}
	};
	private static final void print(final int[][] array) {
		for (final int[] element : array) {
			for (final int element2 : element)
				System.out.print(element2 + ", ");
			System.out.println();
		}
		System.out.println("----------------------");
	}
}
