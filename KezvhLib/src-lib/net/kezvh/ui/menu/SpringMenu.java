/**
 *
 */
package net.kezvh.ui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * @author afflux
 *
 */
public class SpringMenu extends JMenuBar {
	/**
	 * menu object structure:
	 * [ "menuName", new Object["menuItemName", [KeyEvent.VK_X, MenuItemActionListener.class, ...], ...], ... ]
	 *
	 *
	 * @param menu complicated
	 */
	public SpringMenu(final Object menu) {
		JMenu currentMenu = null;
		JMenuItem currentMenuItem = null;

		for (final Object o : (Object[]) menu)
			if (o instanceof String) {
				if (currentMenu != null) {
					if (currentMenuItem != null) {
						currentMenu.add(currentMenuItem);
						System.out.println("1: adding " + currentMenuItem.getText() + " to " + currentMenu.getText());

					}
					this.add(currentMenu);
				}
				currentMenu = new JMenu((String) o);
				System.out.println("creating menu " + o);
				currentMenu.getAccessibleContext().setAccessibleDescription((String) o);
				currentMenuItem = null;
			} else
				// it's an array of menus
				for (final Object p : (Object[]) o)
					if (p instanceof String) {
						if (currentMenuItem != null) {
							if (currentMenu == null)
								throw new IllegalArgumentException("Invalid menu object");

							currentMenu.add(currentMenuItem);
							System.out.println("2: adding " + currentMenuItem.getText() + " to " + currentMenu.getText());
						}
						currentMenuItem = new JMenuItem((String) p);
						currentMenuItem.getAccessibleContext().setAccessibleDescription((String) p);
					} else
						// it's an array of menu item properties
						for (final Object q : (Object[]) p)
							if (q instanceof Class) {
								if (currentMenuItem == null)
									throw new IllegalArgumentException("Invalid menu object");

								try {
									currentMenuItem.addActionListener((ActionListener) ((Class<?>) q).newInstance());
								} catch (final InstantiationException e) {
									throw new IllegalArgumentException("could not create action listener", e);
								} catch (final IllegalAccessException e) {
									throw new IllegalArgumentException("could not create action listener", e);
								}
							} else if (q instanceof Integer) { // Assume it's a key
								if (currentMenuItem == null)
									throw new IllegalArgumentException("Invalid menu object");

								currentMenuItem.setMnemonic((Integer) q);
								currentMenuItem.setAccelerator(KeyStroke.getKeyStroke((Integer) q, ActionEvent.META_MASK));
							} else
								throw new IllegalArgumentException("Don't know what you're trying to do");

		if (currentMenu != null && currentMenuItem != null) {
			currentMenu.add(currentMenuItem);
			System.out.println("3: adding " + currentMenuItem.getText() + " to " + currentMenu.getText());

			this.add(currentMenu);
		}
	}
}
