package net.kezvh.collections;

import java.util.Set;

/**
 * A set which can contain more than 1 copy of any given item.
 *
 * @author afflux
 * @param <T> FIXME comment
 */
public interface Multiset<T> extends Set<T> {
	/**
	 * @param element
	 * @return x
	 */
	int getMembership(T element);

	/**
	 * @param element
	 * @param membership
	 * @return x
	 */
	int setMembership(T element, int membership);

	/**
	 * @param element
	 * @return x
	 */
	boolean removeAllMembers(T element);
}
