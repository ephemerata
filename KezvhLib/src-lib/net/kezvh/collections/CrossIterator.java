package net.kezvh.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author mjacob
 *
 * takes two iterables, and iterates over pairs of their elements, in dictionary order
 *
 * @param <S> first type
 * @param <T> second type
 */
public class CrossIterator<S, T> extends AbstractIterator<Pair<S, T>> {
	private final Iterator<S> sIterator;
	private S currentS;
	private final Iterable<T> tIterable;
	private Iterator<T> tIterator;

	/**
	 * @param sIterable s iterable
	 * @param tIterable t iterable
	 */
	public CrossIterator(final Iterable<S> sIterable, final Iterable<T> tIterable) {
		super();
		this.sIterator = sIterable.iterator();
		this.tIterable = tIterable;
		this.tIterator = this.tIterable.iterator();
		if (this.sIterator.hasNext())
			this.currentS = this.sIterator.next();
	}

	@Override
	protected Pair<S, T> findNext() throws NoSuchElementException {
		if (!this.tIterator.hasNext()) {
			this.tIterator = this.tIterable.iterator();
			this.currentS = this.sIterator.next(); // if no more s elements, this will throw NSEE
		}
		return new UnmodifiablePair<S, T>(this.currentS, this.tIterator.next()); // throws NSEE if t has nothing in it
	}
}
