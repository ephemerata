/**
 */
package net.kezvh.collections;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import net.kezvh.collections.wrappers.WrappedMap;

/**
 * @author afflux
 * @param <K> FIXME add comment
 * @param <V> FIXME add comment
 */
public class PublishMap<K, V> extends WrappedMap<K, V> {
	/**
	 * @author afflux
	 * @param <K> FIXME add comment
	 * @param <V> FIXME add comment
	 */
	public interface MapSubscriber<K, V> {
		/**
		 * @param map
		 * @param key
		 * @param value
		 * @param oldValue
		 */
		void wasPut(Map<K, V> map, K key, V value, V oldValue);

		/**
		 * @param map
		 * @param wasPut
		 */
		void wasPutAll(Map<K, V> map, Map<? extends K, ? extends V> wasPut);

		/**
		 * @param map
		 * @param key
		 * @param oldValue
		 */
		void wasRemoved(Map<K, V> map, Object key, V oldValue);
	}

	private final Collection<MapSubscriber<K, V>> mapSubscribers = new HashSet<MapSubscriber<K, V>>();

	/**
	 * @param base FIXME add comment
	 */
	public PublishMap(final Map<K, V> base) {
		super(base);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param mapSubscriber FIXME add comment
	 * @return x
	 */
	public boolean addMapSubscriber(final MapSubscriber<K, V> mapSubscriber) {
		return this.mapSubscribers.add(mapSubscriber);
	}

	/**
	 * @param mapSubscriber FIXME add comment
	 * @return x
	 */
	public boolean removeMapSubscriber(final MapSubscriber<K, V> mapSubscriber) {
		return this.mapSubscribers.remove(mapSubscriber);
	}

	/**
	 * @see net.kezvh.collections.wrappers.WrappedMap#put(java.lang.Object, java.lang.Object)
	 * @param arg0 FIXME add comment
	 * @param arg1 FIXME add comment
	 * @return x
	 */
	@Override
	public V put(final K arg0, final V arg1) {
		final V old = super.put(arg0, arg1);
		for (final MapSubscriber<K, V> mapSubscriber : this.mapSubscribers)
			mapSubscriber.wasPut(this, arg0, arg1, old);
		return old;
	}

	/**
	 * @see net.kezvh.collections.wrappers.WrappedMap#putAll(java.util.Map)
	 * @param arg0 FIXME add comment
	 */
	@Override
	public void putAll(final Map<? extends K, ? extends V> arg0) {
		super.putAll(arg0);
		for (final MapSubscriber<K, V> mapSubscriber : this.mapSubscribers)
			mapSubscriber.wasPutAll(this, arg0);
	}

	/**
	 * @see net.kezvh.collections.wrappers.WrappedMap#remove(java.lang.Object)
	 * @param arg0 FIXME add comment
	 * @return yourMom
	 */
	@Override
	public V remove(final Object arg0) {
		final V old = super.remove(arg0);
		for (final MapSubscriber<K, V> mapSubscriber : this.mapSubscribers)
			mapSubscriber.wasRemoved(this, arg0, old);
		return old;
	}
}
