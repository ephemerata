package net.kezvh.collections;

/**
 * @author mjacob
 *
 */
public class IntegerRange extends Range<Integer> {
	/**
	 * @param first COMMENT
	 * @param last COMMENT
	 */
	public IntegerRange(final int first, final int last) {
		this(first, last, false, true);
	}

	/**
	 * @param first COMMENT
	 * @param last COMMENT
	 * @param firstOpen COMMENT
	 * @param lastOpen COMMENT
	 */
	public IntegerRange(final int first, final int last, final boolean firstOpen, final boolean lastOpen) {
		super(first, last, firstOpen, lastOpen, Navigator.INTEGER_NAVIGATOR);
	}

	/**
	 * @see net.kezvh.collections.Range#size()
	 * @return COMMENT
	 */
	@Override
	public int size() {
		return this.last() - this.first();
	}
}
