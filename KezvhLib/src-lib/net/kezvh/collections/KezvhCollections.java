package net.kezvh.collections;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.kezvh.lang.UtilityClass;

/**
 * @author afflux
 *
 * library class
 */
public final class KezvhCollections extends UtilityClass {

	/**
	 * @param <T> collection type
	 * @param collection a collection
	 * @return the collection where as a list, where the ith element is the ith
	 *         element from the iterator.. useful for faking some things, but in
	 *         general really, really slow
	 */
	public static <T> List<T> asList(final Collection<T> collection) {
		return new AbstractList<T>() {
			/**
			 * @see java.util.AbstractList#iterator()
			 * @return the collection's iterator
			 */
			@Override
			public Iterator<T> iterator() {
				return collection.iterator();
			}

			/**
			 * this is horribly inefficient
			 */
			@Override
			public T get(final int index) {
				if (index < 0 || index > this.size())
					throw new IndexOutOfBoundsException();
				final Iterator<T> it = collection.iterator();
				for (int i = 0; i < index; i++)
					it.next();
				return it.next();
			}

			@Override
			public int size() {
				return collection.size();
			}

		};
	}
}
