package net.kezvh.collections;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;

import net.kezvh.collections.wrappers.CollectionAsSet;

import com.google.common.collect.AbstractMapEntry;

/**
 * @author mjacob
 *
 * @param <K> key type
 * @param <V> value type
 */
public class SortedArrayMap<K, V> extends AbstractMap<K, V> implements SortedMap<K, V> {
	@SuppressWarnings("unchecked")
	private static final <V> V[] create(final Class<?> clazz, final int length) {
		return (V[]) Array.newInstance(clazz, length);
	}

	private K[] keys;
	private V[] values;

	private final Comparator<? super K> comparator;

	/**
	 * construct a new comparator using built in comparisons
	 */
	public SortedArrayMap() {
		this(new ComparableComparator<K>());
	}

	@Override
	public int size() {
		if (this.keys == null)
			return 0;
		return this.keys.length;
	}

	/**
	 * @param comparator a comparator
	 */
	public SortedArrayMap(final Comparator<? super K> comparator) {
		this.comparator = comparator;
	}

	/**
	 * @param key COMMENT
	 * @return x
	 */
	public int keyIndex(final K key) {
		if (this.keys == null)
			return -1;
		return Arrays.binarySearch(this.keys, key, this.comparator);
	}

	@Override
	public V put(final K key, final V value) {
		if (this.keys == null) {
			this.keys = SortedArrayMap.create(key.getClass(), 1);
			this.values = SortedArrayMap.create(value.getClass(), 1);
			this.keys[0] = key;
			this.values[0] = value;
			return null;
		}

		final int index = this.keyIndex(key);
		if (index >= 0)
			try {
				return this.values[index];
			} finally {
				this.values[index] = value;
			}

		final int insertionPoint = -(index + 1);
		this.insert(key, value, insertionPoint);
		return null;
	}

	private void insert(final K key, final V value, final int insertionPoint) {
		this.keys = KezvhArrays.insert(this.keys, insertionPoint, key);
		this.values = KezvhArrays.insert(this.values, insertionPoint, value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean containsKey(final Object key) {
		return this.keyIndex((K) key) >= 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(final Object key) {
		final int index = this.keyIndex((K) key);
		if (index >= 0)
			return this.values[index];
		return null;
	}

	@Override
	public Set<K> keySet() {
		if (this.keys == null)
			return Collections.emptySet();
		return new CollectionAsSet<K, List<K>>(Arrays.asList(this.keys));
	}

	@Override
	public Collection<V> values() {
		if (this.keys == null)
			return Collections.emptySet();
		return Arrays.asList(this.values);
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return new AbstractSet<Entry<K, V>>() {

			@Override
			public Iterator<java.util.Map.Entry<K, V>> iterator() {
				return new AbstractIterator<Entry<K, V>>() {
					int index = 0;

					@Override
					protected java.util.Map.Entry<K, V> findNext() throws NoSuchElementException {
						if (this.index >= SortedArrayMap.this.size())
							throw new NoSuchElementException();
						try {
							return new AbstractMapEntry<K, V>() {
								private final int myIndex = index;

								@Override
								public K getKey() {
									return SortedArrayMap.this.keys[this.myIndex];
								}

								@Override
								public V getValue() {
									return SortedArrayMap.this.values[this.myIndex];
								}

								@Override
								public V setValue(final V value) {
									try {
										return SortedArrayMap.this.values[this.myIndex];
									} finally {
										SortedArrayMap.this.values[this.myIndex] = value;
									}
								}
							};
						} finally {
							this.index++;
						}
					}

					/**
					 * TODO this needs some checking to make sure it is used correctly
					 */
					@Override
					public void remove() {
						SortedArrayMap.this.remove(SortedArrayMap.this.keys[this.index - 1]);
					}
				};
			}

			@Override
			public int size() {
				return SortedArrayMap.this.size();
			}

			@Override
			public boolean remove(final Object o) {
				return SortedArrayMap.this.remove(((Map.Entry<?, ?>) o).getKey()) != null;
			}

			@Override
			public boolean add(final java.util.Map.Entry<K, V> e) {
				return SortedArrayMap.this.put(e.getKey(), e.getValue()) != null;
			}
		};
	}

	@Override
	public Comparator<? super K> comparator() {
		return this.comparator;
	}

	@Override
	public K firstKey() {
		if (this.keys.length > 0)
			return this.keys[0];
		return null;
	}

	@Override
	public SortedMap<K, V> headMap(final K toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public K lastKey() {
		if (this.keys.length > 0)
			return this.keys[this.keys.length - 1];
		return null;
	}

	@Override
	public SortedMap<K, V> subMap(final K fromKey, final K toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap<K, V> tailMap(final K fromKey) {
		throw new UnsupportedOperationException();
	}

}
