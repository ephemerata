package net.kezvh.collections.DualMap;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.kezvh.collections.Pair;
import net.kezvh.collections.views.ConditionalSetView;
import net.kezvh.collections.views.SetView;
import net.kezvh.functional.lambda.L1;
import net.kezvh.functional.lambda.Predicate1;

/**
 * @author mjacob
 * @param <K1> key 1 type
 * @param <K2> key 2 type
 * @param <V> value type
 */
public abstract class AbstractDualMap<K1, K2, V> extends AbstractMap<Pair<K1, K2>, V> implements DualMap<K1, K2, V> {
	private static final class AbstractCouple<K1, K2> implements Pair<K1, K2> {
		private final K1 key1;
		private final K2 key2;

		public AbstractCouple(final K1 key1, final K2 key2) {
			super();
			this.key1 = key1;
			this.key2 = key2;
		}

		@Override
		public K1 get1() {
			return this.key1;
		}

		@Override
		public K2 get2() {
			return this.key2;
		}
	}

	@Override
	public boolean containsKey(final K1 key1, final K2 key2) {
		return this.containsKey(new AbstractCouple<K1, K2>(key1, key2));
	}

	private final class K1Filter implements Predicate1<Map.Entry<Pair<K1, K2>, V>> {
		private final K1 key1;

		public K1Filter(final K1 key1) {
			this.key1 = key1;
		}

		public Boolean op(final Map.Entry<Pair<K1, K2>, V> entry) {
			return this.key1.equals(entry.getKey().get1());
		}
	}

	private final class K2Filter implements Predicate1<Map.Entry<Pair<K1, K2>, V>> {
		private final K2 key2;

		public K2Filter(final K2 key2) {
			this.key2 = key2;
		}

		public Boolean op(final Map.Entry<Pair<K1, K2>, V> entry) {
			return this.key2.equals(entry.getKey().get2());
		}
	}

	private final L1<DualMap.Entry<K1, K2, V>, Map.Entry<Pair<K1, K2>, V>> ENTRY_CONVERTER = new L1<DualMap.Entry<K1, K2, V>, Map.Entry<Pair<K1, K2>, V>>() {
		@Override
		public DualMap.Entry<K1, K2, V> op(final Map.Entry<Pair<K1, K2>, V> param) {
			return new DualMap.Entry<K1, K2, V>() {
				@Override
				public Pair<K1, K2> getKey() {
					return param.getKey();
				}

				@Override
				public K1 getKey1() {
					return param.getKey().get1();
				}

				@Override
				public K2 getKey2() {
					return param.getKey().get2();
				}

				@Override
				public V getValue() {
					return param.getValue();
				}

				public V setValue(final V value) {
					return param.setValue(value);
				}

				@Override
				public int hashCode() {
					return param.hashCode();
				}

				@Override
				public boolean equals(final Object obj) {
					if (obj == null)
						return false;
					if (obj == this)
						return true;
					if (this.getClass() != obj.getClass())
						return false;

					final DualMap.Entry<?, ?, ?> other = (DualMap.Entry<?, ?, ?>) obj;
					return this.getKey1().equals(other.getKey1()) && this.getKey2().equals(other.getKey2());
				}
			};
		}
	};

	@Override
	public Set<DualMap.Entry<K1, K2, V>> entrySetOnKey1(final K1 key1) {
		final Set<Map.Entry<Pair<K1, K2>, V>> filtered = new ConditionalSetView<Map.Entry<Pair<K1, K2>, V>>(new K1Filter(key1), this.entrySet());
		return new SetView<Map.Entry<Pair<K1, K2>, V>, DualMap.Entry<K1, K2, V>>(filtered, this.ENTRY_CONVERTER);
	}

	@Override
	public Set<DualMap.Entry<K1, K2, V>> entrySetOnKey2(final K2 key2) {
		final Set<Map.Entry<Pair<K1, K2>, V>> filtered = new ConditionalSetView<Map.Entry<Pair<K1, K2>, V>>(new K2Filter(key2), this.entrySet());
		return new SetView<Map.Entry<Pair<K1, K2>, V>, DualMap.Entry<K1, K2, V>>(filtered, this.ENTRY_CONVERTER);
	}

	@Override
	public V get(final K1 key1, final K2 key2) {
		for (final Map.Entry<Pair<K1, K2>, V> entry : this.entrySet())
			if (key1.equals(entry.getKey().get1()) && key2.equals(entry.getKey().get2()))
				return entry.getValue();
		return null;
	}

	private final L1<K2, DualMap.Entry<K1, K2, V>> K2VIEW = new L1<K2, DualMap.Entry<K1, K2, V>>() {
		@Override
		public K2 op(final net.kezvh.collections.DualMap.DualMap.Entry<K1, K2, V> param) {
			return param.getKey2();
		}
	};

	private final L1<K1, DualMap.Entry<K1, K2, V>> K1VIEW = new L1<K1, DualMap.Entry<K1, K2, V>>() {
		@Override
		public K1 op(final net.kezvh.collections.DualMap.DualMap.Entry<K1, K2, V> param) {
			return param.getKey1();
		}
	};

	private final L1<V, DualMap.Entry<K1, K2, V>> VVIEW = new L1<V, DualMap.Entry<K1, K2, V>>() {
		@Override
		public V op(final net.kezvh.collections.DualMap.DualMap.Entry<K1, K2, V> param) {
			return param.getValue();
		}
	};

	@Override
	public Set<K2> keySetOnKey1(final K1 key1) {
		return new SetView<DualMap.Entry<K1, K2, V>, K2>(this.entrySetOnKey1(key1), this.K2VIEW);
	}

	@Override
	public Set<K1> keySetOnKey2(final K2 key2) {
		return new SetView<DualMap.Entry<K1, K2, V>, K1>(this.entrySetOnKey2(key2), this.K1VIEW);
	}

	@Override
	public V put(final K1 key1, final K2 key2, final V value) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.AbstractMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(final net.kezvh.collections.Pair<K1, K2> key, final V value) {
		return this.put(key.get1(), key.get2(), value);
	}

	@Override
	public void putAll(final DualMap<? extends K1, ? extends K2, ? extends V> m) {
		for (final Map.Entry<Pair<K1, K2>, V> entry : this.entrySet())
			this.put(entry.getKey(), entry.getValue());
	}

	@Override
	public V remove(final K1 key1, final K2 key2) {
		for (final Iterator<Map.Entry<Pair<K1, K2>, V>> i = this.entrySet().iterator(); i.hasNext();) {
			final Map.Entry<Pair<K1, K2>, V> entry = i.next();
			if (key1.equals(entry.getKey().get1()) && key2.equals(entry.getKey().get2())) {
				i.remove();
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	public Collection<V> valuesOnKey1(final K1 key1) {
		return new SetView<DualMap.Entry<K1, K2, V>, V>(this.entrySetOnKey1(key1), this.VVIEW);
	}

	@Override
	public Collection<V> valuesOnKey2(final K2 key2) {
		return new SetView<DualMap.Entry<K1, K2, V>, V>(this.entrySetOnKey2(key2), this.VVIEW);
	}
}