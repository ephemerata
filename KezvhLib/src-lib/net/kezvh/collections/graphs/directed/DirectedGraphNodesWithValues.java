package net.kezvh.collections.graphs.directed;

import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 * @param <EDGE> COMMENT
 */
public interface DirectedGraphNodesWithValues<Node, NodeValue, EDGE extends EdgeWithNodeValues<Node, NodeValue>> extends Map<Node, NodeValue> {

	//	/**
	//	 * @return a set of nodes in the graph
	//	 */
	//	@Override
	//	DirectedGraphNodesWithoutValues<Node, EDGE> keySet();

	/**
	 * @param source a graph noe
	 * @return a map of adjacent nodes backed by the graph
	 * @throws NoSuchElementException if the node is not in the graph
	 */
	Map<Node, NodeValue> outEdgeDestinations(Node source);

	/**
	 * @param destination a graph node
	 * @return a map of adjacent nodes backed by the graph
	 * @throws NoSuchElementException if the node is not in the graph
	 */
	Map<Node, NodeValue> inEdgeSources(Node destination);

}
