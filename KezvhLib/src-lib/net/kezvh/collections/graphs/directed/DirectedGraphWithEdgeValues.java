package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 */
public interface DirectedGraphWithEdgeValues<Node, EdgeValue> extends DirectedGraphNodesWithoutValues<Node, EdgeWithEdgeValue<Node, EdgeValue>>, DirectedGraphEdgesWithValues<Node, EdgeValue, EdgeWithEdgeValue<Node, EdgeValue>> {
	// union
}
