package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 */
public interface Edge<Node> {
	/**
	 * @return COMMENT
	 */
	Node getSourceNode();

	/**
	 * @return COMMENT
	 */
	Node getDestinationNode();
}
