package net.kezvh.collections.graphs.directed;

import java.util.NoSuchElementException;
import java.util.Set;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EDGE> COMMENT
 */
public interface DirectedGraphEdges<Node, EDGE extends Edge<Node>> {
	/**
	 * @param source a graph node
	 * @return a set of adjacent objects backed by the graph.
	 * @throws NoSuchElementException if the node is not in the graph
	 */
	Set<EDGE> outEdges(Node source);

	/**
	 * @param destination a graph node
	 * @return a set of adjacent objects backed by the graph.
	 * @throws NoSuchElementException if the node is not in the graph
	 */
	Set<EDGE> inEdges(Node destination);

	/**
	 * @param source
	 * @return set of nodes with an edge from the specified source
	 */
	Set<Node> adjacentFrom(Node source);

	/**
	 * @param destination
	 * @return set of nodes with an edge to the specified destination
	 */
	Set<Node> adjacentTo(Node destination);

	/**
	 * @param source a graph node
	 * @param destination a graph node
	 * @return true if the edge is in the graph
	 * @throws NoSuchElementException if either element is not in the graph
	 */
	boolean containsEdge(Node source, Node destination);

	/**
	 * @param edge edge which may or may not be in the graph
	 * @return true if the edge is in the graph
	 * @throws NoSuchElementException if the edge is not in the graph
	 */
	boolean containsEdge(EDGE edge);

	/**
	 * @return the edges of the graph
	 */
	Set<EDGE> edges();

	/**
	 * @param destination a graph node
	 * @return the number of nodes adjacent nodes.
	 * @throws NoSuchElementException if the node is not in the graph
	 */
	int inDegree(Node destination);

	/**
	 * @param source a graph node
	 * @return the number of nodes adjacent nodes. -1 if the node is not in the
	 *         graph.
	 */
	int outDegree(Node source);

	/**
	 * @return the count of edges in the graph
	 */
	int edgeCount();
}
