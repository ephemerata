package net.kezvh.collections.graphs.directed;

import java.util.Set;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EDGE> COMMENT
 */
public interface DirectedGraphNodesWithoutValues<Node, EDGE extends Edge<Node>> extends Set<Node>, DirectedGraphEdges<Node, EDGE> {
	//
}
