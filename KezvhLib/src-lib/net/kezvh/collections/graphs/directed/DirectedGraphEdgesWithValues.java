package net.kezvh.collections.graphs.directed;

import java.util.NoSuchElementException;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 * @param <EDGE> COMMENT
 */
public interface DirectedGraphEdgesWithValues<Node, EdgeValue, EDGE extends EdgeWithEdgeValue<Node, EdgeValue>> extends DirectedGraphEdges<Node, EDGE> {
	/**
	 * @param source a graph node
	 * @param destination a graph node
	 * @return the value associated with the edge
	 * @throws NoSuchElementException if either element is not in the graph
	 */
	EdgeValue getEdgeValue(Node source, Node destination);

	/**
	 * @param source a graph node
	 * @param destination a graph node
	 * @param edgeValue value associated with the edge
	 * @return true if there is an edge
	 * @throws NoSuchElementException if either element is not in the graph
	 */
	EdgeValue addEdge(Node source, Node destination, EdgeValue edgeValue);

	/**
	 * @param source a graph node
	 * @param destination a graph node
	 * @return true if the edge was in the graph
	 * @throws NoSuchElementException if either element is not in the graph
	 */
	EdgeValue removeEdge(Node source, Node destination);

	/**
	 * @param edge edge to remove
	 * @return value of the removed edge
	 */
	EdgeValue removeEdge(EDGE edge);
}
