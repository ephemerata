package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 */
public interface DirectedGraph<Node> extends DirectedGraphEdgesWithoutValues<Node, Edge<Node>>, DirectedGraphNodesWithoutValues<Node, Edge<Node>> {
	// union
}
