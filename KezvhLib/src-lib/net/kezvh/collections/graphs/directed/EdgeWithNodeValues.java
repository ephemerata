package net.kezvh.collections.graphs.directed;

import java.util.Map.Entry;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 */
public interface EdgeWithNodeValues<Node, NodeValue> extends Edge<Node> {
	/**
	 * @return source node of the edge
	 */
	Entry<Node, NodeValue> getSource();

	/**
	 * @return destination node of the edge
	 */
	Entry<Node, NodeValue> getDestination();
}
