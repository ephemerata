/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

import net.kezvh.collections.views.SetView;
import net.kezvh.functional.Operations;
import net.kezvh.functional.lambda.L1;

import com.google.common.collect.AbstractMapEntry;

/**
 * @author afflux
 * @param <Node> node type
 * @param <NodeValue> node value type
 * @param <EdgeValue> edge value type
 */
class WrappedKeySet<Node, NodeValue, EdgeValue> extends AbstractSet<Node> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {
	private final DirectedGraphWithValues<Node, NodeValue, EdgeValue> wrappedGraph;

	/**
	 * @param wrappedGraph wrapped graph
	 */
	public WrappedKeySet(final DirectedGraphWithValues<Node, NodeValue, EdgeValue> wrappedGraph) {
		this.wrappedGraph = wrappedGraph;
	}

	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		return this.wrappedGraph.containsEdge(source, destination);
	}

	@Override
	public int edgeCount() {
		return this.wrappedGraph.edgeCount();
	}

	@Override
	public int inDegree(final Node destination) {
		return this.wrappedGraph.inDegree(destination);
	}

	@Override
	public int outDegree(final Node source) {
		return this.wrappedGraph.outDegree(source);
	}

	@Override
	public Set<Node> adjacentFrom(final Node source) {
		return this.wrappedGraph.adjacentFrom(source);
	}

	@Override
	public Set<Node> adjacentTo(final Node destination) {
		return this.wrappedGraph.adjacentTo(destination);
	}

	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		return this.wrappedGraph.addEdge(source, destination, edgeValue);
	}

	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.wrappedGraph.getEdgeValue(source, destination);
	}

	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		return this.wrappedGraph.removeEdge(source, destination);
	}

	@Override
	public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.wrappedGraph.removeEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	@Override
	public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.wrappedGraph.containsEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	private final L1<EdgeWithEdgeValue<Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>> getSetEdges = new L1<EdgeWithEdgeValue<Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>>() {
		@Override
		public EdgeWithEdgeValue<Node, EdgeValue> op(final EdgeWithValues<Node, NodeValue, EdgeValue> param) {
			return new EdgeWithEdgeValue<Node, EdgeValue>() {
				@Override
				public Node getDestinationNode() {
					return param.getDestinationNode();
				}

				@Override
				public Node getSourceNode() {
					return param.getSourceNode();
				}

				@Override
				public EdgeValue getValue() {
					return param.getValue();
				}

				public EdgeValue setValue(final EdgeValue newValue) {
					return param.setValue(newValue);
				}
			};
		}
	};

	private final L1<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>> inverseSetEdges = new L1<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>() {
		@Override
		public EdgeWithValues<Node, NodeValue, EdgeValue> op(final EdgeWithEdgeValue<Node, EdgeValue> param) {
			return new EdgeWithValues<Node, NodeValue, EdgeValue>() {
				@Override
				public java.util.Map.Entry<Node, NodeValue> getDestination() {
					return new AbstractMapEntry<Node, NodeValue>() {
						@Override
						public Node getKey() {
							return getDestinationNode();
						}

						@Override
						public NodeValue getValue() {
							return WrappedKeySet.this.wrappedGraph.get(this.getKey());
						}
					};
				}

				@Override
				public Node getDestinationNode() {
					return param.getDestinationNode();
				}

				@Override
				public java.util.Map.Entry<Node, NodeValue> getSource() {
					return new AbstractMapEntry<Node, NodeValue>() {
						@Override
						public Node getKey() {
							return getSourceNode();
						}

						@Override
						public NodeValue getValue() {
							return WrappedKeySet.this.wrappedGraph.get(this.getKey());
						}
					};
				}

				@Override
				public Node getSourceNode() {
					return param.getSourceNode();
				}

				@Override
				public EdgeValue getValue() {
					return param.getValue();
				}

				public EdgeValue setValue(final EdgeValue newValue) {
					return param.setValue(newValue);
				}
			};
		}
	};

	private final Set<EdgeWithEdgeValue<Node, EdgeValue>> edgesView = new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(this.wrappedGraph.edges(), this.getSetEdges, this.inverseSetEdges);

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
		return this.edgesView;
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
		return new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(this.wrappedGraph.inEdges(destination), this.getSetEdges, this.inverseSetEdges);
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
		return new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(this.wrappedGraph.outEdges(source), this.getSetEdges, this.inverseSetEdges);
	}

	/**
	 * @see java.util.Set#clear()
	 */
	@Override
	public void clear() {
		this.wrappedGraph.clear();
	}

	@Override
	public boolean contains(final Object o) {
		return this.wrappedGraph.containsKey(o);
	}

	@Override
	public boolean isEmpty() {
		return this.wrappedGraph.isEmpty();
	}

	private final L1<Node, Entry<Node, NodeValue>> getKey = Operations.getKey();
	private final Set<Node> entrySet = new SetView<Entry<Node, NodeValue>, Node>(this.wrappedGraph.entrySet(), this.getKey);

	@Override
	public Iterator<Node> iterator() {
		return this.entrySet.iterator();
	}

	@Override
	public boolean remove(final Object o) {
		return this.wrappedGraph.remove(o) != null;
	}

	@Override
	public int size() {
		return this.wrappedGraph.size();
	}

	@Override
	public Object[] toArray() {
		return this.entrySet.toArray();
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		return this.entrySet.toArray(a);
	}
}