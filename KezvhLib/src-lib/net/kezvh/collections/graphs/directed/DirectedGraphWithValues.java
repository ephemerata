package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 * @param <EdgeValue> COMMENT
 */
public interface DirectedGraphWithValues<Node, NodeValue, EdgeValue> extends DirectedGraphNodesWithValues<Node, NodeValue, EdgeWithValues<Node, NodeValue, EdgeValue>>, DirectedGraphEdgesWithValues<Node, EdgeValue, EdgeWithValues<Node, NodeValue, EdgeValue>> {
	@Override
	public DirectedGraphWithEdgeValues<Node, EdgeValue> keySet();
}
