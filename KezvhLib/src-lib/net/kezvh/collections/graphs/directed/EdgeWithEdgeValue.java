package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 */
public interface EdgeWithEdgeValue<Node, EdgeValue> extends Edge<Node> {
	/**
	 * @return value associated with the edge
	 */
	EdgeValue getValue();

	/**
	 * @param newValue
	 * @return old value associated with the edge
	 */
	EdgeValue setValue(EdgeValue newValue);
}