package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 * @param <EdgeValue> COMMENT
 */
public interface EdgeWithValues<Node, NodeValue, EdgeValue> extends EdgeWithEdgeValue<Node, EdgeValue>, EdgeWithNodeValues<Node, NodeValue> {
	// union
}
