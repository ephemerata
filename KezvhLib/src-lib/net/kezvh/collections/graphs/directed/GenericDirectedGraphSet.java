package net.kezvh.collections.graphs.directed;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import net.kezvh.collections.views.MapAsView;
import net.kezvh.collections.wrappers.CollectionAsSet;
import net.kezvh.functional.lambda.L1;

/**
 * @author mjacob
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 */
public class GenericDirectedGraphSet<Node, EdgeValue> extends AbstractSet<Node> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.innerGraph == null) ? 0 : this.innerGraph.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final GenericDirectedGraphSet<?, ?> other = (GenericDirectedGraphSet<?, ?>) obj;
		if (this.innerGraph == null) {
			if (other.innerGraph != null)
				return false;
		} else if (!this.innerGraph.equals(other.innerGraph))
			return false;
		return true;
	}

	private static final Object PRESENT = new Object();
	private final DirectedGraphWithValues<Node, Object, EdgeValue> innerGraph;

	/**
	 * @param innerGraph a graph on which to build the set
	 */
	public GenericDirectedGraphSet(final DirectedGraphWithValues<Node, Object, EdgeValue> innerGraph) {
		super();
		this.innerGraph = innerGraph;
	}

	private final L1<Object, Node> presentMapper = new L1<Object, Node>() {
		public Object op(final Node param) {
			return GenericDirectedGraphSet.PRESENT;
		}
	};

	@Override
	public boolean add(final Node e) {
		return this.innerGraph.put(e, GenericDirectedGraphSet.PRESENT) != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(final Collection<? extends Node> c) {
		this.innerGraph.putAll(new MapAsView<Node, Object>(new CollectionAsSet<Node, Collection<Node>>((Collection<Node>) c), this.presentMapper));
		return !c.isEmpty();
	}

	@Override
	public void clear() {
		this.innerGraph.clear();
	}

	@Override
	public boolean contains(final Object o) {
		return this.innerGraph.containsKey(o);
	}

	@Override
	public boolean isEmpty() {
		return this.innerGraph.isEmpty();
	}

	@Override
	public Iterator<Node> iterator() {
		return this.innerGraph.keySet().iterator();
	}

	@Override
	public boolean remove(final Object o) {
		return this.innerGraph.remove(o) != null;
	}

	@Override
	public int size() {
		return this.innerGraph.size();
	}

	@Override
	public Object[] toArray() {
		return this.innerGraph.keySet().toArray();
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		return this.innerGraph.keySet().toArray(a);
	}

	@Override
	public Set<Node> adjacentFrom(final Node source) {
		return this.innerGraph.adjacentFrom(source);
	}

	@Override
	public Set<Node> adjacentTo(final Node destination) {
		return this.innerGraph.adjacentTo(destination);
	}

	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		return this.innerGraph.containsEdge(source, destination);
	}

	@Override
	public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.innerGraph.keySet().contains(edge);
	}

	@Override
	public int edgeCount() {
		return this.innerGraph.edgeCount();
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
		return this.innerGraph.keySet().edges();
	}

	@Override
	public int inDegree(final Node destination) {
		return this.innerGraph.inDegree(destination);
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
		return this.innerGraph.keySet().inEdges(destination);
	}

	@Override
	public int outDegree(final Node source) {
		return this.innerGraph.outDegree(source);
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
		return this.innerGraph.keySet().outEdges(source);
	}

	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		return this.innerGraph.addEdge(source, destination, edgeValue);
	}

	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.innerGraph.getEdgeValue(source, destination);
	}

	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		return this.innerGraph.removeEdge(source, destination);
	}

	@Override
	public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.innerGraph.keySet().removeEdge(edge);
	}

}
