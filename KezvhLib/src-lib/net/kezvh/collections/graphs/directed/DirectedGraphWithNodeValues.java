package net.kezvh.collections.graphs.directed;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 */
public interface DirectedGraphWithNodeValues<Node, NodeValue> extends DirectedGraphNodesWithValues<Node, NodeValue, EdgeWithNodeValues<Node, NodeValue>> {
	@Override
	public DirectedGraph<Node> keySet();
}
