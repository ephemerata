/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import net.kezvh.collections.Pair;
import net.kezvh.collections.dualmaps.DualMap;
import net.kezvh.collections.dualmaps.HashDualMap;
import net.kezvh.collections.views.MapAsView;
import net.kezvh.collections.views.SetView;
import net.kezvh.collections.wrappers.WrappedSet;
import net.kezvh.functional.lambda.L1;

import com.google.common.collect.AbstractMapEntry;

/**
 * Nodes are tracked by a HashMap. Edges are tracked by a HashMap2. This gives
 * O(1) performance for adding or querying anything.
 *
 * @author afflux
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 * @param <EdgeValue> COMMENT
 */
public class HashDirectedGraph<Node, NodeValue, EdgeValue> extends HashMap<Node, NodeValue> implements DirectedGraphWithValues<Node, NodeValue, EdgeValue> {
	private final L1<NodeValue, Node> getValue = new L1<NodeValue, Node>() {
		public NodeValue op(final Node param) {
			return HashDirectedGraph.this.get(param);
		}
	};

	private final DualMap<Node, Node, EdgeValue> edges = new HashDualMap<Node, Node, EdgeValue>();
	private final L1<EdgeWithValues<Node, NodeValue, EdgeValue>, Map.Entry<Pair<Node, Node>, EdgeValue>> getEdges = new L1<EdgeWithValues<Node, NodeValue, EdgeValue>, Map.Entry<Pair<Node, Node>, EdgeValue>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public EdgeWithValues<Node, NodeValue, EdgeValue> op(final Map.Entry<Pair<Node, Node>, EdgeValue> param) {
			return new EdgeWithValues<Node, NodeValue, EdgeValue>() {
				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getDestination()
				 * @return COMMENT
				 */
				@Override
				public java.util.Map.Entry<Node, NodeValue> getDestination() {
					return new Map.Entry<Node, NodeValue>() {
						/**
						 * @see java.util.Map.Entry#getKey()
						 * @return COMMENT
						 */
						@Override
						public Node getKey() {
							return param.getKey().get2();
						}

						/**
						 * @see java.util.Map.Entry#getValue()
						 * @return COMMENT
						 */
						@Override
						public NodeValue getValue() {
							return HashDirectedGraph.this.get(this.getKey());
						}

						public NodeValue setValue(final NodeValue arg0) {
							return HashDirectedGraph.this.put(this.getKey(), arg0);
						}
					};
				}

				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getSource()
				 * @return COMMENT
				 */
				@Override
				public java.util.Map.Entry<Node, NodeValue> getSource() {
					return new Map.Entry<Node, NodeValue>() {
						/**
						 * @see java.util.Map.Entry#getKey()
						 * @return COMMENT
						 */
						@Override
						public Node getKey() {
							return param.getKey().get1();
						}

						/**
						 * @see java.util.Map.Entry#getValue()
						 * @return COMMENT
						 */
						@Override
						public NodeValue getValue() {
							return HashDirectedGraph.this.get(this.getKey());
						}

						public NodeValue setValue(final NodeValue arg0) {
							return HashDirectedGraph.this.put(this.getKey(), arg0);
						}
					};
				}

				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getValue()
				 * @return COMMENT
				 */
				@Override
				public EdgeValue getValue() {
					return param.getValue();
				}

				public EdgeValue setValue(final EdgeValue newValue) {
					return param.setValue(newValue);
				}

				@Override
				public Node getDestinationNode() {
					return param.getKey().get2();
				}

				@Override
				public Node getSourceNode() {
					return param.getKey().get1();
				}
			};
		}
	};

	private final L1<EdgeWithValues<Node, NodeValue, EdgeValue>, DualMap.Entry<Node, Node, EdgeValue>> getEdges2 = new L1<EdgeWithValues<Node, NodeValue, EdgeValue>, DualMap.Entry<Node, Node, EdgeValue>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public EdgeWithValues<Node, NodeValue, EdgeValue> op(final DualMap.Entry<Node, Node, EdgeValue> param) {
			return new EdgeWithValues<Node, NodeValue, EdgeValue>() {
				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getDestination()
				 * @return COMMENT
				 */
				@Override
				public java.util.Map.Entry<Node, NodeValue> getDestination() {
					return new Map.Entry<Node, NodeValue>() {
						/**
						 * @see java.util.Map.Entry#getKey()
						 * @return COMMENT
						 */
						@Override
						public Node getKey() {
							return param.getKey().get2();
						}

						/**
						 * @see java.util.Map.Entry#getValue()
						 * @return COMMENT
						 */
						@Override
						public NodeValue getValue() {
							return HashDirectedGraph.this.get(this.getKey());
						}

						public NodeValue setValue(final NodeValue arg0) {
							return HashDirectedGraph.this.put(this.getKey(), arg0);
						}
					};
				}

				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getSource()
				 * @return COMMENT
				 */
				@Override
				public java.util.Map.Entry<Node, NodeValue> getSource() {
					return new Map.Entry<Node, NodeValue>() {
						/**
						 * @see java.util.Map.Entry#getKey()
						 * @return COMMENT
						 */
						@Override
						public Node getKey() {
							return param.getKey().get1();
						}

						/**
						 * @see java.util.Map.Entry#getValue()
						 * @return COMMENT
						 */
						@Override
						public NodeValue getValue() {
							return HashDirectedGraph.this.get(this.getKey());
						}

						public NodeValue setValue(final NodeValue arg0) {
							return HashDirectedGraph.this.put(this.getKey(), arg0);
						}
					};
				}

				/**
				 * @see net.kezvh.collections.graphs.directed.EdgeWithValues#getValue()
				 * @return COMMENT
				 */
				@Override
				public EdgeValue getValue() {
					return param.getValue();
				}

				public EdgeValue setValue(final EdgeValue newValue) {
					return param.setValue(newValue);
				}

				@Override
				public Node getDestinationNode() {
					return param.getKey().get2();
				}

				@Override
				public Node getSourceNode() {
					return param.getKey().get1();
				}
			};
		}
	};

	private final Set<EdgeWithValues<Node, NodeValue, EdgeValue>> edgesSet = new SetView<Map.Entry<Pair<Node, Node>, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>>(this.edges.entrySet(), this.getEdges) {
		/**
		 * @see java.util.AbstractCollection#remove(java.lang.Object)
		 * @param o COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(final Object o) {
			final EdgeWithValues<Node, NodeValue, EdgeValue> other = (EdgeWithValues<Node, NodeValue, EdgeValue>) o;
			return HashDirectedGraph.this.edges.remove(other.getSource().getKey(), other.getDestination().getKey()) != null;
		}
	};

	private void assertContains(final Object... nodes) {
		for (final Object node : nodes)
			if (!this.containsKey(node))
				throw new NoSuchElementException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#addEdge(java.lang.Object,
	 *      java.lang.Object, java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @param edgeValue COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		this.assertContains(source, destination);

		return this.edges.put(source, destination, edgeValue);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#containsEdge(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		this.assertContains(source, destination);

		return this.edges.containsKey(source, destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#containsEdge(net.kezvh.collections.graphs.directed.Edge)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final EdgeWithValues<Node, NodeValue, EdgeValue> edge) {
		return this.containsEdge(edge.getSource().getKey(), edge.getDestination().getKey());
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#edgeCount()
	 * @return COMMENT
	 */
	@Override
	public int edgeCount() {
		return this.edges.size();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#edges()
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> edges() {
		return this.edgesSet;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#getEdgeValue(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.edges.get(source, destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#inDegree(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public int inDegree(final Node destination) {
		return this.edges.entrySetOnKey2(destination).size();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#inEdgeSources(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<Node, NodeValue> inEdgeSources(final Node destination) {
		return new MapAsView<Node, NodeValue>(this.edges.keySetOnKey2(destination), this.getValue);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#inEdges(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> inEdges(final Node destination) {
		return new SetView<DualMap.Entry<Node, Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>>(this.edges.entrySetOnKey2(destination), this.getEdges2);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#outDegree(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public int outDegree(final Node source) {
		return this.edges.entrySetOnKey1(source).size();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#outEdgeDestinations(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<Node, NodeValue> outEdgeDestinations(final Node source) {
		return new MapAsView<Node, NodeValue>(this.edges.keySetOnKey1(source), this.getValue);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#outEdges(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> outEdges(final Node source) {
		return new SetView<DualMap.Entry<Node, Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>>(this.edges.entrySetOnKey1(source), this.getEdges2);

	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#removeEdge(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		return this.edges.remove(source, destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#removeEdge(net.kezvh.collections.graphs.directed.EdgeWithEdgeValue)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final EdgeWithValues<Node, NodeValue, EdgeValue> edge) {
		return this.edges.remove(edge.getSourceNode(), edge.getDestinationNode());
	}

	@SuppressWarnings("unchecked")
	@Override
	public NodeValue remove(final Object key) {
		this.edges.remove1((Node) key);
		this.edges.remove2((Node) key);
		return super.remove(key);
	}

	/**
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		this.edges.clear();
		super.clear();
	}

	@Override
	public DirectedGraphWithEdgeValues<Node, EdgeValue> keySet() {
		return this.keyset;
	}

	private final KeySet keyset = new KeySet();

	private final class KeySet extends WrappedSet<Node, Set<Node>> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {

		public KeySet() {
			super(HashDirectedGraph.super.keySet());
		}

		@Override
		public boolean containsEdge(final Node source, final Node destination) {
			return HashDirectedGraph.this.containsEdge(source, destination);
		}

		@Override
		public int edgeCount() {
			return HashDirectedGraph.this.edgeCount();
		}

		@Override
		public int inDegree(final Node destination) {
			return HashDirectedGraph.this.inDegree(destination);
		}

		@Override
		public int outDegree(final Node source) {
			return HashDirectedGraph.this.outDegree(source);
		}

		@Override
		public Set<Node> adjacentFrom(final Node source) {
			return HashDirectedGraph.this.adjacentFrom(source);
		}

		@Override
		public Set<Node> adjacentTo(final Node destination) {
			return HashDirectedGraph.this.adjacentTo(destination);
		}

		@Override
		public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
			return HashDirectedGraph.this.addEdge(source, destination, edgeValue);
		}

		@Override
		public EdgeValue getEdgeValue(final Node source, final Node destination) {
			return HashDirectedGraph.this.getEdgeValue(source, destination);
		}

		@Override
		public EdgeValue removeEdge(final Node source, final Node destination) {
			return HashDirectedGraph.this.removeEdge(source, destination);
		}

		@Override
		public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
			return HashDirectedGraph.this.removeEdge(edge.getSourceNode(), edge.getDestinationNode());
		}

		@Override
		public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
			return HashDirectedGraph.this.containsEdge(edge.getSourceNode(), edge.getDestinationNode());
		}

		private final L1<EdgeWithEdgeValue<Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>> getSetEdges = new L1<EdgeWithEdgeValue<Node, EdgeValue>, EdgeWithValues<Node, NodeValue, EdgeValue>>() {
			@Override
			public EdgeWithEdgeValue<Node, EdgeValue> op(final EdgeWithValues<Node, NodeValue, EdgeValue> param) {
				return new EdgeWithEdgeValue<Node, EdgeValue>() {
					@Override
					public Node getDestinationNode() {
						return param.getDestinationNode();
					}

					@Override
					public Node getSourceNode() {
						return param.getSourceNode();
					}

					@Override
					public EdgeValue getValue() {
						return param.getValue();
					}

					public EdgeValue setValue(final EdgeValue newValue) {
						return param.setValue(newValue);
					}
				};
			}
		};

		private final L1<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>> inverseSetEdges = new L1<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>() {
			@Override
			public EdgeWithValues<Node, NodeValue, EdgeValue> op(final EdgeWithEdgeValue<Node, EdgeValue> param) {
				return new EdgeWithValues<Node, NodeValue, EdgeValue>() {
					@Override
					public java.util.Map.Entry<Node, NodeValue> getDestination() {
						return new AbstractMapEntry<Node, NodeValue>() {
							@Override
							public Node getKey() {
								return getDestinationNode();
							}

							@Override
							public NodeValue getValue() {
								return HashDirectedGraph.this.get(this.getKey());
							}
						};
					}

					@Override
					public Node getDestinationNode() {
						return param.getDestinationNode();
					}

					@Override
					public java.util.Map.Entry<Node, NodeValue> getSource() {
						return new AbstractMapEntry<Node, NodeValue>() {
							@Override
							public Node getKey() {
								return getSourceNode();
							}

							@Override
							public NodeValue getValue() {
								return HashDirectedGraph.this.get(this.getKey());
							}
						};
					}

					@Override
					public Node getSourceNode() {
						return param.getSourceNode();
					}

					@Override
					public EdgeValue getValue() {
						return param.getValue();
					}

					public EdgeValue setValue(final EdgeValue newValue) {
						return param.setValue(newValue);
					}
				};
			}
		};

		private final Set<EdgeWithEdgeValue<Node, EdgeValue>> edgesView = new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(HashDirectedGraph.this.edges(), this.getSetEdges, this.inverseSetEdges);

		@Override
		public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
			return this.edgesView;
		}

		@Override
		public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
			return new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(HashDirectedGraph.this.inEdges(destination), this.getSetEdges, this.inverseSetEdges);
		}

		@Override
		public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
			return new SetView<EdgeWithValues<Node, NodeValue, EdgeValue>, EdgeWithEdgeValue<Node, EdgeValue>>(HashDirectedGraph.this.outEdges(source), this.getSetEdges, this.inverseSetEdges);
		}
	}

	@Override
	public Set<Node> adjacentFrom(final Node source) {
		if (this.containsKey(source)) {
			if (this.edges.containsKey1(source))
				return this.edges.keySetOnKey1(source);
			return Collections.emptySet();
		}
		return null;
	}

	@Override
	public Set<Node> adjacentTo(final Node destination) {
		if (this.containsKey(destination)) {
			if (this.edges.containsKey2(destination))
				return this.edges.keySetOnKey2(destination);
			return Collections.emptySet();
		}
		return null;
	}
}
