/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.kezvh.patterns.AbstractFactory;

/**
 * @author afflux
 *
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 * @param <MAP> COMMENT
 */
public class MapMapGraph<Node, EdgeValue, MAP extends Map<Node, EdgeValue>> extends AbstractSet<Node> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {
	private final Map<Node, MAP> map;
	private final AbstractFactory<MAP> mapFactory;

	/**
	 * as is, the map will become the core of this object, and changes to the map will affect this object. we should fix that, but we're worried about (stupid) effeciency now
	 *
	 * @param map COMMENT
	 * @param mapFactory COMMENT
	 */
	public MapMapGraph(final Map<Node, MAP> map, final AbstractFactory<MAP> mapFactory) {
		this.map = map;
		this.mapFactory = mapFactory;
	}

	/**
	 * @see java.util.Set#add(java.lang.Object)
	 * @param o COMMENT
	 * @return COMMENT COMMENT
	 */
	@Override
	public boolean add(final Node o) {
		if (this.map.containsKey(o))
			return true;

		this.map.put(o, this.mapFactory.create());
		return false;
	}

	/**
	 * @see java.util.Set#clear()
	 */
	@Override
	public void clear() {
		this.map.clear();
	}

	/**
	 * @see java.util.Set#contains(java.lang.Object)
	 * @param o COMMENT
	 * @return COMMENT COMMENT
	 */
	@Override
	public boolean contains(final Object o) {
		return this.map.containsKey(o);
	}

	/**
	 * @see java.util.Set#isEmpty()
	 * @return COMMENT COMMENT
	 */
	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	/**
	 * @see java.util.Set#iterator()
	 * @return COMMENT COMMENT
	 */
	@Override
	public Iterator<Node> iterator() {
		return this.map.keySet().iterator();
	}

	/**
	 * @see java.util.Set#remove(java.lang.Object)
	 * @param o COMMENT
	 * @return COMMENT COMMENT
	 */
	@Override
	public boolean remove(final Object o) {
		return this.map.remove(o) != null;
	}

	/**
	 * @see java.util.Set#size()
	 * @return COMMENT COMMENT
	 */
	@Override
	public int size() {
		return this.map.size();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#adjacentFrom(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<Node> adjacentFrom(final Node source) {
		return this.map.get(source).keySet();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#adjacentTo(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<Node> adjacentTo(final Node destination) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#containsEdge(java.lang.Object, java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		if (this.map.containsKey(source))
			return this.map.get(source).containsKey(destination);
		return false;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#containsEdge(net.kezvh.collections.graphs.directed.Edge)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.containsEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#edgeCount()
	 * @return COMMENT
	 */
	@Override
	public int edgeCount() {
		int count = 0;
		for (final Map<Node, EdgeValue> edges : this.map.values())
			count += edges.size();
		return count;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#edges()
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#inDegree(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public int inDegree(final Node destination) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#inEdges(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#outDegree(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public int outDegree(final Node source) {
		return this.map.get(source).size();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#outEdges(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#addEdge(java.lang.Object, java.lang.Object, java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @param edgeValue COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		return this.map.get(source).put(destination, edgeValue);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#getEdgeValue(java.lang.Object, java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.map.get(source).get(destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#removeEdge(java.lang.Object, java.lang.Object)
	 * @param source COMMENT COMMENT
	 * @param destination COMMENT COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		return this.map.get(source).remove(destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#removeEdge(net.kezvh.collections.graphs.directed.EdgeWithEdgeValue)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.removeEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	@Override
	public boolean equals(final Object o) {
		if (!super.equals(o))
			return false;

		if (!(o instanceof MapMapGraph))
			return false;
		if (o == this)
			return true;

		final MapMapGraph<?, ?, ?> other = (MapMapGraph<?, ?, ?>) o;
		return this.map.equals(other.map);
	}
}
