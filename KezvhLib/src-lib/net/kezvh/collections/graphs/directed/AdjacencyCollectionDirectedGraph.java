/**
 *
 */
package net.kezvh.collections.graphs.directed;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.kezvh.collections.views.MapAsView;
import net.kezvh.functional.lambda.L1;
import net.kezvh.patterns.AbstractFactory;

/**
 * @author afflux
 * @param <Node> COMMENT
 * @param <NodeValue> COMMENT
 * @param <EdgeValue> COMMENT
 * @param <M> COMMENT
 */
public class AdjacencyCollectionDirectedGraph<Node, NodeValue, EdgeValue, M extends Map<Node, EdgeValue>> extends AbstractMap<Node, NodeValue> implements DirectedGraphWithValues<Node, NodeValue, EdgeValue> {
	/**
	 * @param collectionMaker something that makes a collection
	 */
	public AdjacencyCollectionDirectedGraph(final AbstractFactory<M> collectionMaker) {
		super();
		this.collectionMaker = collectionMaker;
	}

	private static final class Entry<NodeValue, M> {

		private NodeValue value;
		private final M destinations;

		/**
		 * @return the value
		 */
		public NodeValue getValue() {
			return this.value;
		}

		/**
		 * @param value the value to set
		 * @return the old value
		 */
		public NodeValue setValue(final NodeValue value) {
			try {
				return this.value;
			} finally {
				this.value = value;
			}
		}

		/**
		 * @return the m
		 */
		public M getDestinations() {
			return this.destinations;
		}

		public Entry(final NodeValue value, final M m) {
			super();
			this.value = value;
			this.destinations = m;
		}
	}

	private final AbstractFactory<M> collectionMaker;
	private final Map<Node, Entry<NodeValue, M>> adjacencyListsByNode = new HashMap<Node, Entry<NodeValue, M>>();

	DirectedGraphWithEdgeValues<Node, EdgeValue> keySet = new WrappedKeySet<Node, NodeValue, EdgeValue>(this);

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphWithValues#keySet()
	 * @return COMMENT
	 */
	@Override
	public DirectedGraphWithEdgeValues<Node, EdgeValue> keySet() {
		return this.keySet;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphNodesWithValues#inEdgeSources(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<Node, NodeValue> inEdgeSources(final Node destination) {
		// TODO Auto-generated method stub
		return null;
	}

	private final L1<NodeValue, Node> getNodeValue = new L1<NodeValue, Node>() {
		public NodeValue op(final Node param) {
			return AdjacencyCollectionDirectedGraph.this.adjacencyListsByNode.get(param).getValue();
		}
	};

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphNodesWithValues#outEdgeDestinations(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<Node, NodeValue> outEdgeDestinations(final Node source) {
		final Entry<NodeValue, M> entry = this.adjacencyListsByNode.get(source);
		return new MapAsView<Node, NodeValue>(entry.getDestinations().keySet(), this.getNodeValue) {
			@Override
			public NodeValue put(final Node key, final NodeValue value) {
				return AdjacencyCollectionDirectedGraph.this.adjacencyListsByNode.get(key).setValue(value);
			}
		};
	}

	/**
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		this.adjacencyListsByNode.clear();
	}

	/**
	 * @see java.util.Map#containsKey(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsKey(final Object key) {
		return this.adjacencyListsByNode.containsKey(key);
	}

	/**
	 * @see java.util.Map#containsValue(java.lang.Object)
	 * @param value COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsValue(final Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see java.util.Map#entrySet()
	 * @return COMMENT
	 */
	@Override
	public Set<Map.Entry<Node, NodeValue>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see java.util.Map#get(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@Override
	public NodeValue get(final Object key) {
		return this.adjacencyListsByNode.get(key).getValue();
	}

	/**
	 * @see java.util.Map#isEmpty()
	 * @return COMMENT
	 */
	@Override
	public boolean isEmpty() {
		return this.adjacencyListsByNode.isEmpty();
	}

	/**
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 * @param key COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	@Override
	public NodeValue put(final Node key, final NodeValue value) {
		if (this.adjacencyListsByNode.containsKey(key))
			return this.adjacencyListsByNode.get(key).setValue(value);

		this.adjacencyListsByNode.put(key, new Entry<NodeValue, M>(value, this.collectionMaker.create()));
		return null;
	}

	/**
	 * @see java.util.Map#remove(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@Override
	public NodeValue remove(final Object key) {
		if (this.adjacencyListsByNode.containsKey(key))
			return this.adjacencyListsByNode.remove(key).getValue();
		return null;
	}

	/**
	 * @see java.util.Map#size()
	 * @return COMMENT
	 */
	@Override
	public int size() {
		return this.adjacencyListsByNode.size();
	}

	/**
	 * @see java.util.Map#values()
	 * @return COMMENT
	 */
	@Override
	public Collection<NodeValue> values() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#addEdge(java.lang.Object,
	 *      java.lang.Object, java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @param edgeValue COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		return this.adjacencyListsByNode.get(source).getDestinations().put(destination, edgeValue);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#getEdgeValue(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.adjacencyListsByNode.get(source).getDestinations().get(destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#removeEdge(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		return this.adjacencyListsByNode.get(source).getDestinations().remove(destination);
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdgesWithValues#removeEdge(net.kezvh.collections.graphs.directed.EdgeWithEdgeValue)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public EdgeValue removeEdge(final EdgeWithValues<Node, NodeValue, EdgeValue> edge) {
		return this.removeEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#adjacentFrom(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<Node> adjacentFrom(final Node source) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#adjacentTo(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<Node> adjacentTo(final Node destination) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#containsEdge(java.lang.Object,
	 *      java.lang.Object)
	 * @param source COMMENT
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#containsEdge(net.kezvh.collections.graphs.directed.Edge)
	 * @param edge COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsEdge(final EdgeWithValues<Node, NodeValue, EdgeValue> edge) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#edgeCount()
	 * @return COMMENT
	 */
	@Override
	public int edgeCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#edges()
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> edges() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#inDegree(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public int inDegree(final Node destination) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#inEdges(java.lang.Object)
	 * @param destination COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> inEdges(final Node destination) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#outDegree(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public int outDegree(final Node source) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @see net.kezvh.collections.graphs.directed.DirectedGraphEdges#outEdges(java.lang.Object)
	 * @param source COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<EdgeWithValues<Node, NodeValue, EdgeValue>> outEdges(final Node source) {
		// TODO Auto-generated method stub
		return null;
	}

}
