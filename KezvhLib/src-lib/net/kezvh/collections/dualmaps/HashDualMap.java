/**
 *
 */
package net.kezvh.collections.dualmaps;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import net.kezvh.collections.AbstractIterator;
import net.kezvh.collections.MapEntryImpl;
import net.kezvh.collections.Pair;
import net.kezvh.collections.views.SetView;
import net.kezvh.functional.lambda.L1;

/**
 * Sometimes you want a map with 2 keys. While one could just use java's maps
 * but with something like Couple<K1, K2> as the key, there's an additional
 * opperation I often want to do that you just can't do with something like
 * that. In particular, this operation is incredibly useful if you use this
 * collection to map the edges of a graph, and you want to iterate through all
 * the edges into or out of a particular node. That operation is to slice the
 * map by 1 key or the other. This class grepped a lot of its guts out of java's
 * hashmap implementation (is that an ip issue?). However, each entry in the
 * table also maintains connections to other entries which share it's first or
 * second key. This way, you've got a linked list of entries for each key along
 * each dimension. Time complexity: get(k1, k2) this is operationally identical
 * to getting on a hashmap, so O(1) put(k1, k2, v) hashtable put + 2 linked list
 * head inserts, so also O(1) amortized remove(k1, k2) hashtable remove + 2
 * doubly linked list remove, so O(1) get(k1) || get(k2) returns a list of
 * elements backed by the map in O(1) time remove(k1) || remove(k2) O(n), where
 * n is the number of elements in get(k1) or get(k2) respectively.
 *
 * @author afflux
 * @param <K1> COMMENT
 * @param <K2> COMMENT
 * @param <V> COMMENT
 */
public class HashDualMap<K1, K2, V> implements DualMap<K1, K2, V> {
	/**
	 * The default initial capacity - MUST be a power of two.
	 */
	static final int DEFAULT_INITIAL_CAPACITY = 16;

	/**
	 * The maximum capacity, used if a higher value is implicitly specified by
	 * either of the constructors with arguments. MUST be a power of two <=
	 * 1<<30.
	 */
	static final int MAXIMUM_CAPACITY = 1 << 30;

	/**
	 * The load factor used when none specified in constructor.
	 **/
	static final float DEFAULT_LOAD_FACTOR = 0.75f;

	private static final <K1, K2, V> Entry<K1, K2, V>[] createEntries(final Entry<K1, K2, V>[] other, final int capacity) {
		return HashDualMap.createEntries(other.getClass().getComponentType(), capacity);
	}

	@SuppressWarnings("unchecked")
	private static final <K1, K2, V> Entry<K1, K2, V>[] createEntries(final Class<?> clazz, final int capacity) {
		return (Entry<K1, K2, V>[]) Array.newInstance(clazz, capacity);
	}

	private static final <K1, K2> int hash(final K1 k1, final K2 k2) {
		int h;

		if (k1 == null)
			if (k2 == null)
				h = 0;
			else
				h = k2.hashCode();
		else if (k2 == null || k1.equals(k2))
			h = k1.hashCode();
		else
			h = k1.hashCode() ^ k2.hashCode();

		h ^= (h >>> 20) ^ (h >>> 12);
		return h ^ (h >>> 7) ^ (h >>> 4);
	}

	private static final <K1, K2> int hash(final Pair<K1, K2> entry) {
		return HashDualMap.hash(entry.get1(), entry.get2());
	}

	private static final int indexFor(final int hash, final int length) {
		return hash & (length - 1);
	}

	/**
	 * TODO make this a libary method
	 *
	 * @param <T> COMMENT
	 * @param a COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	private static final <T> boolean objectsEqual(final T a, final T b) {
		if (a == null)
			return b == null;
		return a.equals(b);
	}

	/**
	 * so the complicated thing is this resides in 3 lists: 1 is the list of
	 * collisions 1 is the list of all entries matching key 1 1 is the list of
	 * all entries matching key 2
	 *
	 * @author afflux
	 * @param <K1>
	 * @param <K2>
	 * @param <V>
	 */
	private static final class Entry<K1, K2, V> implements DualMap.Entry<K1, K2, V>, Pair<K1, K2> {
		private final K1 k1;
		private final K2 k2;
		private V value;
		private final int hash;
		private Entry<K1, K2, V> nextCollision;
		private Entry<K1, K2, V> next1; // next in loop w/ fixed k1
		private Entry<K1, K2, V> next2; // next in loop w/ fixed k2
		private Entry<K1, K2, V> prev1; // prev in loop w/ fixed k1
		private Entry<K1, K2, V> prev2; // prev in loop w/ fixed k2

		@Override
		public String toString() {
			return "(" + this.k1 + ", " + this.k2 + ") => " + this.value;
		}

		public String specialString() {
			return "next1: " + this.next1 + " next2: " + this.next2 + " prev1: " + this.prev1 + " prev2: " + this.prev2;
		}

		public Entry(final int hash, final K1 k1, final K2 k2, final V v, final Entry<K1, K2, V> nextCollision) {
			this.k1 = k1;
			this.k2 = k2;
			this.value = v;
			this.nextCollision = nextCollision;
			this.hash = hash;
		}

		/**
		 * i kind of love this function
		 *
		 * @param bimap COMMENT
		 */
		public void removeSelf(final HashDualMap<K1, K2, V> bimap) {
			if (this.next1 != null)
				this.next1.setPrev1(this.prev1);
			if (this.next2 != null)
				this.next2.setPrev2(this.prev2);
			if (this.prev1 != null)
				this.prev1.setNext1(this.next1);
			if (this.prev2 != null)
				this.prev2.setNext2(this.next2);
			bimap.k1Heads.get(this.k1).removing(this);
			if (bimap.k1Heads.get(this.k1).isEmpty())
				bimap.k1Heads.remove(this.k1);
			bimap.k2Heads.get(this.k2).removing(this);
			if (bimap.k2Heads.get(this.k2).isEmpty())
				bimap.k2Heads.remove(this.k2);
		}

		/**
		 * @return the next1
		 */
		public Entry<K1, K2, V> getNext1() {
			return this.next1;
		}

		/**
		 * @param next1 the next1 to set
		 * @return COMMENT
		 */
		public Entry<K1, K2, V> setNext1(final Entry<K1, K2, V> next1) {
			try {
				return this.next1;
			} finally {
				this.next1 = next1;
			}
		}

		/**
		 * @return the next2
		 */
		public Entry<K1, K2, V> getNext2() {
			return this.next2;
		}

		/**
		 * @param next2 the next2 to set
		 * @return COMMENT
		 */
		public Entry<K1, K2, V> setNext2(final Entry<K1, K2, V> next2) {
			try {
				return this.next2;
			} finally {
				this.next2 = next2;
			}
		}

		/**
		 * @return the prev1
		 */
		public Entry<K1, K2, V> getPrev1() {
			return this.prev1;
		}

		/**
		 * @param prev1 the prev1 to set
		 * @return COMMENT
		 */
		public Entry<K1, K2, V> setPrev1(final Entry<K1, K2, V> prev1) {
			try {
				return this.prev1;
			} finally {
				this.prev1 = prev1;
			}
		}

		/**
		 * @return the prev2
		 */
		public Entry<K1, K2, V> getPrev2() {
			return this.prev2;
		}

		/**
		 * @param prev2 the prev2 to set
		 * @return COMMENT
		 */
		public Entry<K1, K2, V> setPrev2(final Entry<K1, K2, V> prev2) {
			try {
				return this.prev2;
			} finally {
				this.prev2 = prev2;
			}
		}

		public boolean matchesKeys(final K1 key1, final K2 key2) {
			return HashDualMap.objectsEqual(this.k1, key1) && HashDualMap.objectsEqual(this.k2, key2);
		}

		/**
		 * @return the nextCollision
		 */
		public Entry<K1, K2, V> getNextCollision() {
			return this.nextCollision;
		}

		/**
		 * @param nextCollision the nextCollision to set
		 */
		public void setNextCollision(final Entry<K1, K2, V> nextCollision) {
			this.nextCollision = nextCollision;
		}

		/**
		 * @see java.lang.Object#hashCode()
		 * @return COMMENT
		 */
		@Override
		public int hashCode() {
			return this.hash;
		}

		/**
		 * @see java.lang.Object#equals(java.lang.Object)
		 * @param obj COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (this.getClass() != obj.getClass())
				return false;
			final Entry<?, ?, ?> other = (Entry<?, ?, ?>) obj;
			if (this.k1 == null) {
				if (other.k1 != null)
					return false;
			} else if (!this.k1.equals(other.k1))
				return false;
			if (this.k2 == null) {
				if (other.k2 != null)
					return false;
			} else if (!this.k2.equals(other.k2))
				return false;
			return true;
		}

		/**
		 * @see net.kezvh.collections.dualmaps.DualMap.Entry#getKey1()
		 * @return COMMENT
		 */
		@Override
		public K1 getKey1() {
			return this.k1;
		}

		/**
		 * @see net.kezvh.collections.dualmaps.DualMap.Entry#getKey2()
		 * @return COMMENT
		 */
		@Override
		public K2 getKey2() {
			return this.k2;
		}

		/**
		 * @see net.kezvh.collections.dualmaps.DualMap.Entry#getValue()
		 * @return COMMENT
		 */
		@Override
		public V getValue() {
			return this.value;
		}

		/**
		 * @see net.kezvh.collections.dualmaps.DualMap.Entry#setValue(java.lang.Object)
		 * @param value COMMENT
		 * @return COMMENT
		 */
		@Override
		public V setValue(final V value) {
			try {
				return this.value;
			} finally {
				this.value = value;
			}
		}

		/**
		 * @see java.util.Map.Entry#getKey()
		 * @return COMMENT
		 */
		@Override
		public Pair<K1, K2> getKey() {
			return this;
		}

		/**
		 * @see net.kezvh.collections.Pair#get1()
		 * @return COMMENT
		 */
		@Override
		public K1 get1() {
			return this.k1;
		}

		/**
		 * @see net.kezvh.collections.Pair#get2()
		 * @return COMMENT
		 */
		@Override
		public K2 get2() {
			return this.k2;
		}
	}

	private abstract class HeadSet extends AbstractSet<Entry<K1, K2, V>> {
		private int headsetSize;

		private Entry<K1, K2, V> head;

		private final class HeadSetIterator extends AbstractIterator<Entry<K1, K2, V>> {
			private Entry<K1, K2, V> current = HeadSet.this.head;

			/**
			 * @see net.kezvh.collections.AbstractIterator#findNext()
			 * @return COMMENT
			 * @throws NoSuchElementException COMMENT
			 */
			@Override
			protected Entry<K1, K2, V> findNext() throws NoSuchElementException {
				if (this.current == null)
					throw new NoSuchElementException();
				try {
					return this.current;
				} finally {
					this.current = HeadSet.this.getNext(this.current);
				}
			}

			/**
			 * @see net.kezvh.collections.AbstractIterator#remove()
			 */
			@Override
			public void remove() {
				HashDualMap.this.remove(HeadSet.this.getPrev(this.current));
			}
		}

		public final void insertNewHead(final Entry<K1, K2, V> newHead) {
			this.setNext(newHead, this.head);
			if (this.head != null)
				this.setPrev(this.head, newHead);
			this.head = newHead;
			this.headsetSize++;
		}

		public final void removing(final Entry<K1, K2, V> removedEntry) {
			if (this.head == removedEntry)
				this.head = this.getNext(removedEntry);
			this.headsetSize--;
		}

		protected abstract Entry<K1, K2, V> getNext(Entry<K1, K2, V> entry);

		protected abstract Entry<K1, K2, V> getPrev(Entry<K1, K2, V> entry);

		protected abstract Entry<K1, K2, V> setNext(Entry<K1, K2, V> entry, Entry<K1, K2, V> next);

		protected abstract Entry<K1, K2, V> setPrev(Entry<K1, K2, V> entry, Entry<K1, K2, V> prev);

		/**
		 * @see java.util.AbstractCollection#iterator()
		 * @return COMMENT
		 */
		@Override
		public Iterator<Entry<K1, K2, V>> iterator() {
			return new HeadSetIterator();
		}

		/**
		 * @see java.util.AbstractCollection#size()
		 * @return COMMENT
		 */
		@Override
		public int size() {
			return this.headsetSize;
		}

		/**
		 * @see java.util.AbstractCollection#add(java.lang.Object)
		 * @param o COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean add(final Entry<K1, K2, V> o) {
			return HashDualMap.this.put(o) != null;
		}

		/**
		 * @see java.util.AbstractCollection#addAll(java.util.Collection)
		 * @param c COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean addAll(final Collection<? extends Entry<K1, K2, V>> c) {
			return HashDualMap.this.putAll(c);
		}

		/**
		 * @see java.util.AbstractCollection#clear()
		 */
		@Override
		public void clear() {
			for (final Entry<K1, K2, V> entry : this)
				HashDualMap.this.remove(entry);
		}

		/**
		 * @see java.util.AbstractCollection#contains(java.lang.Object)
		 * @param o COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public boolean contains(final Object o) {
			if (!(o instanceof DualMap.Entry))
				return false;

			final DualMap.Entry<K1, K2, V> other = (DualMap.Entry<K1, K2, V>) o;
			return HashDualMap.this.containsKey(other.getKey1(), other.getKey2());
		}

		/**
		 * @see java.util.AbstractCollection#containsAll(java.util.Collection)
		 * @param c COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean containsAll(final Collection<?> c) {
			for (final Object entry : c)
				if (!this.contains(entry))
					return false;
			return true;
		}

		/**
		 * @see java.util.AbstractCollection#isEmpty()
		 * @return COMMENT
		 */
		@Override
		public boolean isEmpty() {
			return this.head == null;
		}

		/**
		 * @see java.util.AbstractCollection#remove(java.lang.Object)
		 * @param o COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean remove(final Object o) {
			return HashDualMap.this.remove(o) != null;
		}

		/**
		 * @see java.util.AbstractCollection#retainAll(java.util.Collection)
		 * @param c COMMENT
		 * @return COMMENT
		 */
		@Override
		public boolean retainAll(final Collection<?> c) {
			boolean modified = false;
			for (final Entry<K1, K2, V> entry : this)
				if (!c.contains(entry))
					modified |= HashDualMap.this.remove(entry) != null;

			return modified;
		}

		/**
		 * @see java.util.AbstractCollection#toArray()
		 * @return TOOD COMMENT
		 */
		@Override
		public Object[] toArray() {
			final Entry<K1, K2, V>[] array = HashDualMap.createEntries(this.head.getClass(), HashDualMap.this.size);
			return this.toArray(array);
		}

		@SuppressWarnings("unchecked")
		@Override
		public <T extends Object> T[] toArray(final T[] a) {
			final T[] b = a.length >= HashDualMap.this.size ? a : (T[]) HashDualMap.createEntries(a.getClass().getComponentType(), HashDualMap.this.size);
			int i = 0;
			for (final Entry<K1, K2, V> entry : this)
				a[i++] = (T) entry;
			return b;
		}
	}

	private final class HeadSet1 extends HeadSet {
		@Override
		protected Entry<K1, K2, V> getPrev(final Entry<K1, K2, V> arg0) {
			return arg0.getPrev1();
		}

		@Override
		protected Entry<K1, K2, V> getNext(final Entry<K1, K2, V> arg0) {
			return arg0.getNext1();
		}

		@Override
		protected Entry<K1, K2, V> setNext(final Entry<K1, K2, V> entry, final Entry<K1, K2, V> next) {
			return entry.setNext1(next);
		}

		@Override
		protected Entry<K1, K2, V> setPrev(final Entry<K1, K2, V> entry, final Entry<K1, K2, V> prev) {
			return entry.setPrev1(prev);
		}
	}

	private final class HeadSet2 extends HeadSet {
		@Override
		protected Entry<K1, K2, V> getPrev(final Entry<K1, K2, V> arg0) {
			return arg0.getPrev2();
		}

		@Override
		protected Entry<K1, K2, V> getNext(final Entry<K1, K2, V> arg0) {
			return arg0.getNext2();
		}

		@Override
		protected Entry<K1, K2, V> setNext(final Entry<K1, K2, V> entry, final Entry<K1, K2, V> next) {
			return entry.setNext2(next);
		}

		@Override
		protected Entry<K1, K2, V> setPrev(final Entry<K1, K2, V> entry, final Entry<K1, K2, V> prev) {
			return entry.setPrev2(prev);
		}
	}

	private final Map<K1, HeadSet1> k1Heads = new HashMap<K1, HeadSet1>();
	private final Map<K2, HeadSet2> k2Heads = new HashMap<K2, HeadSet2>();
	private Entry<K1, K2, V>[] entries;
	/**
	 * The number of key-value mappings contained in this identity hash map.
	 */
	private transient int size;

	/**
	 * The next size value at which to resize (capacity * load factor).
	 *
	 * @serial
	 */
	private int threshold;

	/**
	 * The load factor for the hash table.
	 *
	 * @serial
	 */
	private final float loadFactor;

	/**
	 * The number of times this HashMap has been structurally modified
	 * Structural modifications are those that change the number of mappings in
	 * the HashMap or otherwise modify its internal structure (e.g., rehash).
	 * This field is used to make iterators on Collection-views of the HashMap
	 * fail-fast. (See ConcurrentModificationException).
	 */
	private transient volatile int modCount;

	/**
	 * TODO refactor this so it uses AbstractIterator
	 *
	 * @author afflux
	 * @param <E>
	 */
	private abstract class HashIterator<E> implements Iterator<E> {
		Entry<K1, K2, V> next; // next entry to return
		int expectedModCount; // For fast-fail
		int index; // current slot
		Entry<K1, K2, V> current; // current entry

		public HashIterator() {
			this.expectedModCount = HashDualMap.this.modCount;
			final Entry<K1, K2, V>[] t = HashDualMap.this.entries;
			int i = t.length;
			Entry<K1, K2, V> n = null;
			if (HashDualMap.this.size != 0)
				while (i > 0 && (n = t[--i]) == null) {
					// nothing TODO maybe refactor this so it's.. not so ugly
				}
			this.next = n;
			this.index = i;
		}

		public boolean hasNext() {
			return this.next != null;
		}

		protected Entry<K1, K2, V> nextEntry() {
			if (HashDualMap.this.modCount != this.expectedModCount)
				throw new ConcurrentModificationException();
			final Entry<K1, K2, V> e = this.next;
			if (e == null)
				throw new NoSuchElementException();

			Entry<K1, K2, V> n = e.getNextCollision();
			final Entry<K1, K2, V>[] t = HashDualMap.this.entries;
			int i = this.index;
			while (n == null && i > 0)
				n = t[--i];
			this.index = i;
			this.next = n;
			return this.current = e;
		}

		public void remove() {
			if (this.current == null)
				throw new IllegalStateException();
			if (HashDualMap.this.modCount != this.expectedModCount)
				throw new ConcurrentModificationException();
			final Pair<K1, K2> k = this.current.getKey();
			this.current = null;
			HashDualMap.this.removeEntryForKey(k.get1(), k.get2());
			this.expectedModCount = HashDualMap.this.modCount;
		}

	}

	private class ValueIterator extends HashIterator<V> {
		public V next() {
			return this.nextEntry().getValue();
		}
	}

	private class KeyIterator extends HashIterator<Pair<K1, K2>> {
		public Pair<K1, K2> next() {
			return this.nextEntry().getKey();
		}
	}

	private class EntryIterator extends HashIterator<Map.Entry<Pair<K1, K2>, V>> {
		public Map.Entry<Pair<K1, K2>, V> next() {
			return this.nextEntry();
		}
	}

	// Subclass overrides these to alter behavior of views' iterator() method
	Iterator<Pair<K1, K2>> newKeyIterator() {
		return new KeyIterator();
	}

	Iterator<V> newValueIterator() {
		return new ValueIterator();
	}

	Iterator<Map.Entry<Pair<K1, K2>, V>> newEntryIterator() {
		return new EntryIterator();
	}

	// Views
	/**
	 * Each of these fields are initialized to contain an instance of the
	 * appropriate view the first time this view is requested. The views are
	 * stateless, so there's no reason to create more than one of each.
	 */
	private transient volatile Set<Pair<K1, K2>> keySet = null;
	private transient volatile Collection<V> values = null;
	private transient volatile Set<Map.Entry<Pair<K1, K2>, V>> entrySet = null;

	/**
	 * Returns a set view of the keys contained in this map. The set is backed
	 * by the map, so changes to the map are reflected in the set, and
	 * vice-versa. The set supports element removal, which removes the
	 * corresponding mapping from this map, via the <tt>Iterator.remove</tt>,
	 * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt>, and
	 * <tt>clear</tt> operations. It does not support the <tt>add</tt> or
	 * <tt>addAll</tt> operations.
	 *
	 * @return a set view of the keys contained in this map.
	 */
	public Set<Pair<K1, K2>> keySet() {
		final Set<Pair<K1, K2>> ks = this.keySet;
		return (ks != null ? ks : (this.keySet = new KeySet()));
	}

	private class KeySet extends AbstractSet<Pair<K1, K2>> {
		@Override
		public Iterator<Pair<K1, K2>> iterator() {
			return HashDualMap.this.newKeyIterator();
		}

		@Override
		public int size() {
			return HashDualMap.this.size;
		}

		@Override
		public boolean contains(final Object o) {
			return HashDualMap.this.containsKey(o);
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(final Object o) {
			final Pair<K1, K2> other = (Pair<K1, K2>) o;
			return HashDualMap.this.removeEntryForKey(other.get1(), other.get2()) != null;
		}

		@Override
		public void clear() {
			HashDualMap.this.clear();
		}
	}

	/**
	 * Returns a collection view of the values contained in this map. The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa. The collection supports element removal,
	 * which removes the corresponding mapping from this map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>, <tt>removeAll</tt>,
	 * <tt>retainAll</tt>, and <tt>clear</tt> operations. It does not support
	 * the <tt>add</tt> or <tt>addAll</tt> operations.
	 *
	 * @return a collection view of the values contained in this map.
	 */
	public Collection<V> values() {
		final Collection<V> vs = this.values;
		return (vs != null ? vs : (this.values = new Values()));
	}

	private class Values extends AbstractCollection<V> {
		@Override
		public Iterator<V> iterator() {
			return HashDualMap.this.newValueIterator();
		}

		@Override
		public int size() {
			return HashDualMap.this.size;
		}

		@Override
		public boolean contains(final Object o) {
			return HashDualMap.this.containsValue(o);
		}

		@Override
		public void clear() {
			HashDualMap.this.clear();
		}
	}

	/**
	 * Returns a collection view of the mappings contained in this map. Each
	 * element in the returned collection is a <tt>Map.Entry</tt>. The
	 * collection is backed by the map, so changes to the map are reflected in
	 * the collection, and vice-versa. The collection supports element removal,
	 * which removes the corresponding mapping from the map, via the
	 * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>, <tt>removeAll</tt>,
	 * <tt>retainAll</tt>, and <tt>clear</tt> operations. It does not support
	 * the <tt>add</tt> or <tt>addAll</tt> operations.
	 *
	 * @return a collection view of the mappings contained in this map.
	 * @see Map.Entry
	 */
	public Set<Map.Entry<Pair<K1, K2>, V>> entrySet() {
		final Set<Map.Entry<Pair<K1, K2>, V>> es = this.entrySet;
		return (es != null ? es : (this.entrySet = new EntrySet()));
	}

	private Entry<K1, K2, V> getEntry(final K1 key1, final K2 key2) {
		for (Entry<K1, K2, V> entry = this.entries[HashDualMap.indexFor(HashDualMap.hash(key1, key2), this.entries.length)]; entry != null; entry = entry.getNextCollision())
			if (entry.matchesKeys(key1, key2))
				return entry;
		return null;
	}

	private class EntrySet extends AbstractSet<Map.Entry<Pair<K1, K2>, V>> {
		@Override
		public Iterator<Map.Entry<Pair<K1, K2>, V>> iterator() {
			return HashDualMap.this.newEntryIterator();
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean contains(final Object o) {
			if (!(o instanceof DualMap.Entry))
				return false;
			final DualMap.Entry<K1, K2, V> e = (DualMap.Entry<K1, K2, V>) o;
			final Entry<K1, K2, V> candidate = HashDualMap.this.getEntry(e.getKey1(), e.getKey2());
			return candidate != null && candidate.equals(e);
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean remove(final Object o) {
			return HashDualMap.this.removeMapping((Map.Entry<Pair<K1, K2>, V>) o) != null;
		}

		@Override
		public int size() {
			return HashDualMap.this.size;
		}

		@Override
		public void clear() {
			HashDualMap.this.clear();
		}
	}

	/**
	 * Save the state of the <tt>HashMap</tt> instance to a stream (i.e.,
	 * serialize it).
	 *
	 * @serialData The <i>capacity</i> of the HashMap (the length of the bucket
	 *             array) is emitted (int), followed by the <i>size</i> of the
	 *             HashMap (the number of key-value mappings), followed by the
	 *             key (Object) and value (Object) for each key-value mapping
	 *             represented by the HashMap The key-value mappings are emitted
	 *             in the order that they are returned by
	 *             <tt>entrySet().iterator()</tt>.
	 */
	private void writeObject(final java.io.ObjectOutputStream s) throws IOException {
		final Iterator<Map.Entry<Pair<K1, K2>, V>> i = this.entrySet().iterator();

		// Write out the threshold, loadfactor, and any hidden stuff
		s.defaultWriteObject();

		// Write out number of buckets
		s.writeInt(this.entries.length);

		// Write out size (number of Mappings)
		s.writeInt(this.size);

		// Write out keys and values (alternating)
		while (i.hasNext()) {
			final Map.Entry<Pair<K1, K2>, V> e = i.next();
			s.writeObject(e.getKey());
			s.writeObject(e.getValue());
		}
	}

	/**
	 * allow subclasses to do initialization after deserialization
	 */
	protected void init() {
		// do nothing;
	}

	private static final long serialVersionUID = 362498820763181265L;

	/**
	 * Reconstitute the <tt>HashMap</tt> instance from a stream (i.e.,
	 * deserialize it).
	 */
	@SuppressWarnings("unchecked")
	private void readObject(final java.io.ObjectInputStream s) throws IOException, ClassNotFoundException {
		// Read in the threshold, loadfactor, and any hidden stuff
		s.defaultReadObject();

		// Read in number of buckets and allocate the bucket array;
		final int numBuckets = s.readInt();
		this.entries = HashDualMap.createEntries(new Entry<K1, K2, V>(0, null, null, null, null).getClass(), numBuckets);

		this.init(); // Give subclass a chance to do its thing.

		// Read in size (number of Mappings)
		final int readSize = s.readInt();

		// Read the keys and values, and put the mappings in the HashMap
		for (int i = 0; i < readSize; i++) {
			final K1 key1 = (K1) s.readObject();
			final K2 key2 = (K2) s.readObject();
			final V value = (V) s.readObject();
			this.putForCreate(key1, key2, value);
		}
	}

	// These methods are used when serializing HashSets
	int capacity() {
		return this.entries.length;
	}

	float loadFactor() {
		return this.loadFactor;
	}

	/**
	 *
	 */
	public HashDualMap() {
		this.entries = HashDualMap.createEntries(new Entry<K1, K2, V>(0, null, null, null, null).getClass(), HashDualMap.DEFAULT_INITIAL_CAPACITY);
		this.loadFactor = HashDualMap.DEFAULT_LOAD_FACTOR;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#clear()
	 */
	@Override
	public void clear() {
		this.entries = HashDualMap.createEntries(this.entries, HashDualMap.DEFAULT_INITIAL_CAPACITY);
		this.k1Heads.clear();
		this.k2Heads.clear();
	}

	/**
	 * Special version of remove for EntrySet.
	 */
	private Entry<K1, K2, V> removeMapping(final Map.Entry<Pair<K1, K2>, V> o) {
		if (!(o instanceof DualMap.Entry))
			return null;

		final Map.Entry<Pair<K1, K2>, V> entry = o;
		final Pair<K1, K2> k = entry.getKey();
		final int hash = k.hashCode();
		final int i = HashDualMap.indexFor(hash, this.entries.length);
		Entry<K1, K2, V> prev = this.entries[i];
		Entry<K1, K2, V> e = prev;

		while (e != null) {
			final Entry<K1, K2, V> next = e.getNextCollision();
			if (e.hash == hash && e.equals(entry)) {
				this.modCount++;
				this.size--;
				if (prev == e)
					this.entries[i] = next;
				else
					prev.setNextCollision(next);
				e.removeSelf(this);
				return e;
			}
			prev = e;
			e = next;
		}

		return e;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#containsKey(java.lang.Object,
	 *      java.lang.Object)
	 * @param key1 COMMENT
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsKey(final K1 key1, final K2 key2) {
		return this.getEntry(key1, key2) != null;
	}

	/**
	 * This method is used instead of put by constructors and pseudoconstructors
	 * (clone, readObject). It does not resize the table, check for
	 * comodification, etc. It calls createEntry rather than addEntry.
	 */
	private void putForCreate(final K1 key1, final K2 key2, final V value) {
		final int hash = HashDualMap.hash(key1, key2);
		final int i = HashDualMap.indexFor(hash, this.entries.length);

		/**
		 * Look for preexisting entry for key. This will never happen for clone
		 * or deserialize. It will only happen for construction if the input Map
		 * is a sorted map whose ordering is inconsistent w/ equals.
		 */
		for (Entry<K1, K2, V> e = this.entries[i]; e != null; e = e.nextCollision)
			if (e.hash == hash && HashDualMap.objectsEqual(key1, e.get1()) && HashDualMap.objectsEqual(key2, e.get2())) {
				e.value = value;
				return;
			}

		this.createEntry(hash, key1, key2, value, i);
	}

	/**
	 * Like addEntry except that this version is used when creating entries as
	 * part of Map construction or "pseudo-construction" (cloning,
	 * deserialization). This version needn't worry about resizing the table.
	 * Subclass overrides this to alter the behavior of HashMap(Map), clone, and
	 * readObject.
	 */
	private void createEntry(final int hash, final K1 key1, final K2 key2, final V value, final int bucketIndex) {
		final Entry<K1, K2, V> e = this.entries[bucketIndex];
		this.entries[bucketIndex] = new Entry<K1, K2, V>(hash, key1, key2, value, e);
		this.size++;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#entrySetOnKey1(java.lang.Object)
	 * @param key1 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<DualMap.Entry<K1, K2, V>> entrySetOnKey1(final K1 key1) {
		return new SetView<Entry<K1, K2, V>, DualMap.Entry<K1, K2, V>>(this.k1Heads.get(key1), this.getBimapEntry);
	}

	private final L1<DualMap.Entry<K1, K2, V>, Entry<K1, K2, V>> getBimapEntry = new L1<DualMap.Entry<K1, K2, V>, Entry<K1, K2, V>>() {
		@Override
		public DualMap.Entry<K1, K2, V> op(final Entry<K1, K2, V> param) {
			return param;
		}
	};

	private final L1<K1, Entry<K1, K2, V>> getKey1 = new L1<K1, Entry<K1, K2, V>>() {
		@Override
		public K1 op(final Entry<K1, K2, V> param) {
			return param.getKey1();
		}
	};

	private final L1<K2, Entry<K1, K2, V>> getKey2 = new L1<K2, Entry<K1, K2, V>>() {
		@Override
		public K2 op(final Entry<K1, K2, V> param) {
			return param.getKey2();
		}
	};

	private final L1<V, Entry<K1, K2, V>> getValue = new L1<V, Entry<K1, K2, V>>() {
		@Override
		public V op(final Entry<K1, K2, V> param) {
			return param.getValue();
		}
	};

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#entrySetOnKey2(java.lang.Object)
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<DualMap.Entry<K1, K2, V>> entrySetOnKey2(final K2 key2) {
		return new SetView<Entry<K1, K2, V>, DualMap.Entry<K1, K2, V>>(this.k2Heads.get(key2), this.getBimapEntry);
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#get(java.lang.Object,
	 *      java.lang.Object)
	 * @param key1 COMMENT
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public V get(final K1 key1, final K2 key2) {
		if (this.containsKey(key1, key2))
			return this.getEntry(key1, key2).value;
		return null;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#isEmpty()
	 * @return COMMENT
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#keySetOnKey1(java.lang.Object)
	 * @param key1 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<K2> keySetOnKey1(final K1 key1) {
		if (!this.k1Heads.containsKey(key1))
			return null;
		return new SetView<Entry<K1, K2, V>, K2>(this.k1Heads.get(key1), this.getKey2);
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#keySetOnKey2(java.lang.Object)
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Set<K1> keySetOnKey2(final K2 key2) {
		if (!this.k2Heads.containsKey(key2))
			return null;
		return new SetView<Entry<K1, K2, V>, K1>(this.k2Heads.get(key2), this.getKey1);
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#put(java.lang.Object,
	 *      java.lang.Object, java.lang.Object)
	 * @param key1 COMMENT
	 * @param key2 COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	@Override
	public V put(final K1 key1, final K2 key2, final V value) {
		final Entry<K1, K2, V> entry = this.getEntry(key1, key2);
		if (entry != null)
			return entry.setValue(value);

		this.addEntry(key1, key2, value);
		return null;
	}

	private void addEntry(final K1 key1, final K2 key2, final V value) {
		if (this.size++ >= this.threshold)
			this.resize(this.entries.length << 1);
		final int hash = HashDualMap.hash(key1, key2);
		final int index = HashDualMap.indexFor(hash, this.entries.length);
		final Entry<K1, K2, V> e = this.entries[index];
		final Entry<K1, K2, V> newEntry = new Entry<K1, K2, V>(hash, key1, key2, value, e);
		this.entries[index] = newEntry;

		HeadSet1 key1Head = this.k1Heads.get(key1);
		if (key1Head == null) {
			key1Head = new HeadSet1();
			this.k1Heads.put(key1, key1Head);
		}

		HeadSet2 key2Head = this.k2Heads.get(key2);
		if (key2Head == null) {
			key2Head = new HeadSet2();
			this.k2Heads.put(key2, key2Head);
		}

		key1Head.insertNewHead(newEntry);
		key2Head.insertNewHead(newEntry);
	}

	private void resize(final int newSize) {
		final Entry<K1, K2, V>[] newTable = HashDualMap.createEntries(this.entries.getClass().getComponentType(), newSize);
		for (final Entry<K1, K2, V> entry : this.entries)
			if (entry != null)
				newTable[HashDualMap.indexFor(HashDualMap.hash(entry), newSize)] = entry;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#putAll(net.kezvh.collections.dualmaps.DualMap)
	 * @param m COMMENT
	 */
	@Override
	public void putAll(final DualMap<? extends K1, ? extends K2, ? extends V> m) {
		if (this.size + m.size() > this.threshold) {
			final int newSize = Integer.highestOneBit(this.size + m.size()) << 1;
			this.resize(newSize);
		}
		for (final Map.Entry<? extends Pair<? extends K1, ? extends K2>, ? extends V> o : m.entrySet())
			this.put(o.getKey().get1(), o.getKey().get2(), o.getValue());
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#remove(java.lang.Object,
	 *      java.lang.Object)
	 * @param key1 COMMENT
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public V remove(final K1 key1, final K2 key2) {
		final Entry<K1, K2, V> e = this.removeEntryForKey(key1, key2);
		return (e == null ? null : e.value);
	}

	private Entry<K1, K2, V> removeEntryForKey(final K1 key1, final K2 key2) {
		final int hash = HashDualMap.hash(key1, key2);
		final int index = HashDualMap.indexFor(hash, this.entries.length);

		Entry<K1, K2, V> prev = this.entries[index];
		Entry<K1, K2, V> e = prev;

		while (e != null) {
			final Entry<K1, K2, V> next = e.getNextCollision();
			if (e.hash == hash && e.matchesKeys(key1, key2)) {
				this.modCount++;
				this.size--;
				if (prev == e)
					this.entries[index] = next;
				else
					prev.setNextCollision(next);
				e.removeSelf(this);
				return e;
			}
			prev = e;
			e = next;
		}

		return e;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#size()
	 * @return COMMENT
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#valuesOnKey1(java.lang.Object)
	 * @param key1 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Collection<V> valuesOnKey1(final K1 key1) {
		return new SetView<Entry<K1, K2, V>, V>(this.k1Heads.get(key1), this.getValue);
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#valuesOnKey2(java.lang.Object)
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Collection<V> valuesOnKey2(final K2 key2) {
		return new SetView<Entry<K1, K2, V>, V>(this.k2Heads.get(key2), this.getValue);
	}

	/**
	 * @see java.util.Map#containsKey(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean containsKey(final Object key) {
		if (!(key instanceof Pair))
			return false;

		final Pair<K1, K2> keys = (Pair<K1, K2>) key;
		return this.containsKey(keys.get1(), keys.get2());
	}

	/**
	 * @see java.util.Map#containsValue(java.lang.Object)
	 * @param value COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean containsValue(final Object value) {
		for (final Entry<K1, K2, V> entry : this.entries)
			if (entry != null && HashDualMap.objectsEqual(entry.getValue(), value))
				return true;

		return false;
	}

	/**
	 * @see java.util.Map#get(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V get(final Object key) {
		if (!(key instanceof Pair))
			return null;

		final Pair<K1, K2> keys = (Pair<K1, K2>) key;
		return this.get(keys.get1(), keys.get2());
	}

	/**
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 * @param key COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	@Override
	public V put(final Pair<K1, K2> key, final V value) {
		return this.put(key.get1(), key.get2(), value);
	}

	/**
	 * @param entry COMMENT
	 * @return COMMENT
	 */
	public V put(final DualMap.Entry<K1, K2, V> entry) {
		return this.put(entry.getKey1(), entry.getKey2(), entry.getValue());
	}

	/**
	 * @see java.util.Map#putAll(java.util.Map)
	 * @param c COMMENT
	 * @return COMMENT
	 */
	public boolean putAll(final Collection<? extends DualMap.Entry<K1, K2, V>> c) {
		if (this.size + c.size() > this.threshold) {
			final int newSize = Integer.highestOneBit(this.size + c.size()) << 1;
			this.resize(newSize);
		}
		boolean changed = false;
		for (final DualMap.Entry<K1, K2, V> o : c)
			changed |= this.put(o.getKey().get1(), o.getKey().get2(), o.getValue()) != null;
		return changed;
	}

	/**
	 * @see java.util.Map#putAll(java.util.Map)
	 * @param t COMMENT
	 */
	@Override
	public void putAll(final Map<? extends Pair<K1, K2>, ? extends V> t) {
		if (this.size + t.size() > this.threshold) {
			final int newSize = Integer.highestOneBit(this.size + t.size()) << 1;
			this.resize(newSize);
		}
		for (final Map.Entry<? extends Pair<? extends K1, ? extends K2>, ? extends V> o : t.entrySet())
			this.put(o.getKey().get1(), o.getKey().get2(), o.getValue());
	}

	/**
	 * @see java.util.Map#remove(java.lang.Object)
	 * @param key COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V remove(final Object key) {
		if (!(key instanceof Pair))
			return null;

		final Pair<K1, K2> keys = (Pair<K1, K2>) key;
		return this.remove(keys.get1(), keys.get2());
	}

	private class EntryMap1 extends AbstractMap<K2, V> {
		private final K1 key1;

		private final Set<Map.Entry<K2, V>> entryMapSet;

		private final L1<Map.Entry<K2, V>, HashDualMap.Entry<K1, K2, V>> forwardMap = new L1<Map.Entry<K2, V>, HashDualMap.Entry<K1, K2, V>>() {
			/**
			 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
			 * @param param COMMENT
			 * @return COMMENT
			 */
			@Override
			public Map.Entry<K2, V> op(final HashDualMap.Entry<K1, K2, V> param) {
				return new Map.Entry<K2, V>() {
					/**
					 * @see java.util.Map.Entry#getKey()
					 * @return COMMENT
					 */
					@Override
					public K2 getKey() {
						return param.get2();
					}

					/**
					 * @see java.util.Map.Entry#getValue()
					 * @return COMMENT
					 */
					@Override
					public V getValue() {
						return param.getValue();
					}

					@Override
					public V setValue(final V value) {
						return param.setValue(value);
					}
				};
			}
		};

		/**
		 * @see java.util.AbstractMap#get(java.lang.Object)
		 * @param key COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public V get(final Object key) {
			return HashDualMap.this.get(this.key1, (K2) key);
		}

		@Override
		public V put(final K2 key, final V value) {
			return HashDualMap.this.put(this.key1, key, value);
		}

		/**
		 * @see java.util.AbstractMap#remove(java.lang.Object)
		 * @param key COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public V remove(final Object key) {
			return HashDualMap.this.remove(this.key1, (K2) key);
		}

		public EntryMap1(final K1 key1) {
			this.key1 = key1;
			this.entryMapSet = new SetView<HashDualMap.Entry<K1, K2, V>, Map.Entry<K2, V>>(HashDualMap.this.k1Heads.get(key1), this.forwardMap) {
				/**
				 * @see java.util.AbstractCollection#add(java.lang.Object)
				 * @param o COMMENT
				 * @return COMMENT
				 */
				@Override
				public boolean add(final java.util.Map.Entry<K2, V> o) {
					return HashDualMap.this.put(key1, o.getKey(), o.getValue()) != null;
				}

				/**
				 * @see java.util.AbstractCollection#clear()
				 */
				@Override
				public void clear() {
					HashDualMap.this.k1Heads.get(key1).clear();
				}

				/**
				 * @see java.util.AbstractCollection#contains(java.lang.Object)
				 * @param o COMMENT
				 * @return COMENT
				 */
				@SuppressWarnings("unchecked")
				@Override
				public boolean contains(final Object o) {
					if (!(o instanceof Map.Entry))
						return false;

					final Map.Entry<K2, V> other = (Map.Entry<K2, V>) o;
					return HashDualMap.this.containsKey(key1, other.getKey());
				}

				/**
				 * @see java.util.AbstractCollection#remove(java.lang.Object)
				 * @param o COMMENT
				 * @return COMMENT
				 */
				@SuppressWarnings("unchecked")
				@Override
				public boolean remove(final Object o) {
					final Map.Entry<K2, V> other = (Map.Entry<K2, V>) o;
					return HashDualMap.this.remove(key1, other.getKey()) != null;
				}
			};
		}

		/**
		 * @see java.util.AbstractMap#entrySet()
		 * @return COMMENT
		 */
		@Override
		public Set<java.util.Map.Entry<K2, V>> entrySet() {
			return this.entryMapSet;
		}
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#entryMapOnKey1(java.lang.Object)
	 * @param key1 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<K2, V> entryMapOnKey1(final K1 key1) {
		if (this.k1Heads.containsKey(key1))
			return new EntryMap1(key1);
		return null;
	}

	private class EntryMap2 extends AbstractMap<K1, V> {
		private final K2 key2;
		private final Set<Map.Entry<K1, V>> entryMapSet;

		private final L1<Map.Entry<K1, V>, HashDualMap.Entry<K1, K2, V>> forwardMap = new L1<Map.Entry<K1, V>, HashDualMap.Entry<K1, K2, V>>() {
			/**
			 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
			 * @param param COMMENT
			 * @return COMMENT
			 */
			@Override
			public Map.Entry<K1, V> op(final HashDualMap.Entry<K1, K2, V> param) {
				return new Map.Entry<K1, V>() {
					/**
					 * @see java.util.Map.Entry#getKey()
					 * @return COMMENT
					 */
					@Override
					public K1 getKey() {
						return param.get1();
					}

					/**
					 * @see java.util.Map.Entry#getValue()
					 * @return COMMENT
					 */
					@Override
					public V getValue() {
						return param.getValue();
					}

					@Override
					public V setValue(final V value) {
						return param.setValue(value);
					}
				};
			}
		};

		public EntryMap2(final K2 key2) {
			this.key2 = key2;
			this.entryMapSet = new SetView<HashDualMap.Entry<K1, K2, V>, Map.Entry<K1, V>>(HashDualMap.this.k2Heads.get(key2), this.forwardMap) {
				/**
				 * @see java.util.AbstractCollection#add(java.lang.Object)
				 * @param o COMMENT
				 * @return COMMENT
				 */
				@Override
				public boolean add(final java.util.Map.Entry<K1, V> o) {
					return HashDualMap.this.put(o.getKey(), key2, o.getValue()) != null;
				}

				/**
				 * @see java.util.AbstractCollection#clear()
				 */
				@Override
				public void clear() {
					HashDualMap.this.k2Heads.get(key2).clear();
				}

				/**
				 * @see java.util.AbstractCollection#contains(java.lang.Object)
				 * @param o COMMENT
				 * @return COMENT
				 */
				@SuppressWarnings("unchecked")
				@Override
				public boolean contains(final Object o) {
					if (!(o instanceof Map.Entry))
						return false;

					final Map.Entry<K1, V> other = (Map.Entry<K1, V>) o;
					return HashDualMap.this.containsKey(other.getKey(), key2);
				}

				/**
				 * @see java.util.AbstractCollection#remove(java.lang.Object)
				 * @param o COMMENT
				 * @return COMMENT
				 */
				@SuppressWarnings("unchecked")
				@Override
				public boolean remove(final Object o) {
					final Map.Entry<K1, V> other = (Map.Entry<K1, V>) o;
					return HashDualMap.this.remove(other.getKey(), key2) != null;
				}
			};
		}

		/**
		 * @see java.util.AbstractMap#entrySet()
		 * @return COMMENT
		 */
		@Override
		public Set<java.util.Map.Entry<K1, V>> entrySet() {
			return this.entryMapSet;
		}

		/**
		 * @see java.util.AbstractMap#get(java.lang.Object)
		 * @param key COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public V get(final Object key) {
			return HashDualMap.this.get((K1) key, this.key2);
		}

		@Override
		public V put(final K1 key, final V value) {
			return HashDualMap.this.put(key, this.key2, value);
		}

		/**
		 * @see java.util.AbstractMap#remove(java.lang.Object)
		 * @param key COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		@Override
		public V remove(final Object key) {
			return HashDualMap.this.remove((K1) key, this.key2);
		}
	}

	/**
	 * @see net.kezvh.collections.dualmaps.DualMap#entryMapOnKey2(java.lang.Object)
	 * @param key2 COMMENT
	 * @return COMMENT
	 */
	@Override
	public Map<K1, V> entryMapOnKey2(final K2 key2) {
		if (this.k2Heads.containsKey(key2))
			return new EntryMap2(key2);
		return null;
	}

	@Override
	public Collection<Map.Entry<K2, V>> get1(final K1 key1) {
		if (this.k1Heads.containsKey(key1))
			return this.entryMapOnKey1(key1).entrySet();
		return null;
	}

	@Override
	public Collection<Map.Entry<K1, V>> get2(final K2 key2) {
		if (this.k2Heads.containsKey(key2))
			return this.entryMapOnKey2(key2).entrySet();
		return null;
	}

	@Override
	public Collection<Map.Entry<K2, V>> remove1(final K1 key1) {
		if (!this.k1Heads.containsKey(key1))
			return null;
		final Collection<Map.Entry<K2, V>> removedEntries = new ArrayList<Map.Entry<K2, V>>(this.k1Heads.get(key1).size());

		for (final Entry<K1, K2, V> entry : this.k1Heads.get(key1))
			removedEntries.add(new MapEntryImpl<K2, V>(entry.get2(), entry.getValue()));

		for (final Map.Entry<K2, V> removedEntry : removedEntries)
			this.remove(key1, removedEntry.getKey());

		return removedEntries;
	}

	@Override
	public Collection<Map.Entry<K1, V>> remove2(final K2 key2) {
		if (!this.k2Heads.containsKey(key2))
			return null;

		final Collection<Map.Entry<K1, V>> removedEntries = new ArrayList<Map.Entry<K1, V>>(this.k2Heads.get(key2).size());

		for (final Entry<K1, K2, V> entry : this.k2Heads.get(key2))
			removedEntries.add(new MapEntryImpl<K1, V>(entry.get1(), entry.getValue()));

		for (final Map.Entry<K1, V> removedEntry : removedEntries)
			this.remove(removedEntry.getKey(), key2);

		return removedEntries;
	}

	public boolean containsKey1(final K1 key1) {
		return this.k1Heads.containsKey(key1);
	}

	public boolean containsKey2(final K2 key2) {
		return this.k2Heads.containsKey(key2);
	}
}
