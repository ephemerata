package net.kezvh.collections.arrays;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author mjacob
 *
 * @param <V> COMMENT
 */
public interface Matrix<V> extends Collection<V> {
	/**
	 * @return COMMENT
	 */
	long getSize();

	/**
	 * @return COMMENT
	 */
	int dimensions();

	/**
	 * @param dim COMMENT
	 * @return COMMENT
	 */
	int getDim(int dim);

	/**
	 * return a new dictionary-order iterator
	 *
	 * @return COMMENT
	 */
	@Override
	public Iterator<V> iterator();

	/**
	 * @param indices COMMENT
	 * @return COMMENT
	 */
	V get(int... indices);

	/**
	 * @param newvalue COMMENT
	 * @param indices COMMENT
	 * @return COMMENT
	 */
	V set(V newvalue, int... indices);

	/**
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	Matrix<V> sub(int... indicies);
}
