package net.kezvh.collections.arrays;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

import net.kezvh.development.UnimplementedException;

/**
 * @author mjacob
 *
 * @param <V> COMMENT
 */
public class ArrayMatrix<V> implements Matrix<V> {
	final Array array;
	final int[] dimensions;
	final long size;

	/**
	 * @param initialValues COMMENT
	 * @param dimensions COMMENT
	 */
	public ArrayMatrix(final V[] initialValues, final int... dimensions) {
		this.array = (Array) Array.newInstance(initialValues.getClass().getComponentType(), dimensions);
		this.dimensions = dimensions;
		throw new UnimplementedException();

		//		this.size = Something.product(dimensions);
	}

	@Override
	public int dimensions() {
		return this.dimensions.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(final int... indices) {
		Array x = this.array;
		for (int i = 0; i < indices.length - 1; i++)
			x = (Array) Array.get(x, indices[i]);
		return (V) Array.get(x, indices[indices.length - 1]);
	}

	@Override
	public int getDim(final int dim) {
		return this.dimensions[dim];
	}

	@Override
	public long getSize() {
		return this.size;
	}

	@Override
	public Iterator<V> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public V set(final V newvalue, final int... indices) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Matrix<V> sub(final int... indicies) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(final V e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(final Collection<? extends V> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean contains(final Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(final Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

}
