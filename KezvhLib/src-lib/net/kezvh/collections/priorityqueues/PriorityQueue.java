package net.kezvh.collections.priorityqueues;

/**
 * @author mjacob
 *
 * @param <T> collection type
 * @param <W> collection weight
 */
public interface PriorityQueue<T, W> extends Iterable<T> {
	/**
	 * @param t
	 * @param weight
	 * @return COMMENT
	 */
	W add(T t, W weight);

	/**
	 * @return COMMENT
	 */
	T peek();

	/**
	 * @return COMMENT
	 */
	T remove();
}
