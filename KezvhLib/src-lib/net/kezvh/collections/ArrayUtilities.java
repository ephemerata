package net.kezvh.collections;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import net.kezvh.lang.UtilityClassInstantiationException;
import net.kezvh.text.StringUtilities;

/**
 * @author mjacob
 *
 */
public final class ArrayUtilities {
	private ArrayUtilities() {
		throw new UtilityClassInstantiationException(ArrayUtilities.class);
	}

	private static final class ArrayIterator<T> extends AbstractListIterator<T> {
		private final T[] array;
		private int cursor = 0;
		private final int end;

		public ArrayIterator(final T[] array) {
			this(array, 0, 0, array.length);
		}

		public ArrayIterator(final T[] array, final int firstIndex, final int startIndex, final int lastIndex) {
			super(startIndex);
			this.array = Arrays.copyOf(array, array.length);
			this.cursor = firstIndex;
			this.end = lastIndex;
		}

		@Override
		protected void add0(final T e) {
			throw new UnsupportedOperationException();
		}

		@Override
		protected T findNext() throws NoSuchElementException {
			if (this.cursor >= this.end)
				throw new NoSuchElementException();
			return this.array[this.cursor++];
		}

		@Override
		protected T findPrevious() throws NoSuchElementException {
			if (this.cursor == 0)
				throw new NoSuchElementException();
			return this.array[--this.cursor];
		}

		@Override
		protected void remove(final boolean previous) {
			throw new UnsupportedOperationException();
		}

		@Override
		protected void set(final T e, final boolean previous) {
			if (previous)
				this.array[this.cursor] = e;
			else
				this.array[this.cursor - 1] = e;
		}
	}

	/**
	 * @param <T> COMMENT
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static <T> ListIterator<T> getIterator(final T[] array) {
		return new ArrayIterator<T>(array);
	}

	/**
	 * @param string COMMENT
	 * @return COMMENT
	 */
	public static int[] getInts(final String string) {
		final String[] numbers = StringUtilities.getAllGroups("([0-9]+)", string);
		final int[] ints = new int[numbers.length];
		for (int i = 0; i < numbers.length; i++)
			ints[i] = Integer.parseInt(numbers[i]);
		return ints;
	}

	/**
	 * @param <T>  COMMENT
	 * @param array COMMENT
	 */
	public static <T> void reverse(final T[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final T tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final long[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final long tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final int[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final int tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final short[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final short tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final byte[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final byte tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final boolean[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final boolean tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final char[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final char tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final float[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final float tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param array COMMENT
	 */
	public static void reverse(final double[] array) {
		final int mid = array.length / 2;
		for (int i = 0, j = array.length - 1; i < mid; i++, j--) {
			final double tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	/**
	 * @param <T> COMMENT
	 * @param n COMMENT
	 * @param o COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] nCopies(final int n, final T o) {
		final T[] copies = (T[]) Array.newInstance(o.getClass(), n);
		for (int i = 0; i < n; i++)
			copies[i] = o;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static byte[] nCopies(final int n, final byte b) {
		final byte[] copies = new byte[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static short[] nCopies(final int n, final short b) {
		final short[] copies = new short[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static int[] nCopies(final int n, final int b) {
		final int[] copies = new int[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static long[] nCopies(final int n, final long b) {
		final long[] copies = new long[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static float[] nCopies(final int n, final float b) {
		final float[] copies = new float[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static double[] nCopies(final int n, final double b) {
		final double[] copies = new double[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static boolean[] nCopies(final int n, final boolean b) {
		final boolean[] copies = new boolean[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param n COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static char[] nCopies(final int n, final char b) {
		final char[] copies = new char[n];
		for (int i = 0; i < n; i++)
			copies[i] = b;
		return copies;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	// ///////////////////////////////////////////////////////////
	public static int indexOf(final int[] array, final int element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final long[] array, final long element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final short[] array, final short element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final byte[] array, final byte element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final float[] array, final float element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final double[] array, final double element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final boolean[] array, final boolean element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final char[] array, final char element) {
		for (int i = 0; i < array.length; i++)
			if (array[i] == element)
				return i;
		return -1;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int indexOf(final Object[] array, final Object element) {
		for (int i = 0; i < array.length; i++)
			if (array[i].equals(element))
				return i;
		return -1;
	}

	// ///////////////////////////////////////////////////////////

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final int[] array, final int firstIndex, final int secondIndex) {
		final int tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final long[] array, final int firstIndex, final int secondIndex) {
		final long tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final short[] array, final int firstIndex, final int secondIndex) {
		final short tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final byte[] array, final int firstIndex, final int secondIndex) {
		final byte tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final float[] array, final int firstIndex, final int secondIndex) {
		final float tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final double[] array, final int firstIndex, final int secondIndex) {
		final double tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final boolean[] array, final int firstIndex, final int secondIndex) {
		final boolean tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final char[] array, final int firstIndex, final int secondIndex) {
		final char tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	/**
	 * @param array COMMENT
	 * @param firstIndex COMMENT
	 * @param secondIndex COMMENT
	 */
	public static void swap(final Object[] array, final int firstIndex, final int secondIndex) {
		final Object tmp = array[firstIndex];
		array[firstIndex] = array[secondIndex];
		array[secondIndex] = tmp;
	}

	// ///////////////////////////////////////////////////////////

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int[] insert(final int[] array, final int element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static int[] insert(final int[] array, final int index, final int element) {
		final int[] newArray = new int[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static long[] insert(final long[] array, final long element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static long[] insert(final long[] array, final int index, final long element) {
		final long[] newArray = new long[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static short[] insert(final short[] array, final short element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static short[] insert(final short[] array, final int index, final short element) {
		final short[] newArray = new short[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static byte[] insert(final byte[] array, final byte element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static byte[] insert(final byte[] array, final int index, final byte element) {
		final byte[] newArray = new byte[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static float[] insert(final float[] array, final float element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static float[] insert(final float[] array, final int index, final float element) {
		final float[] newArray = new float[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static double[] insert(final double[] array, final double element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static double[] insert(final double[] array, final int index, final double element) {
		final double[] newArray = new double[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static boolean[] insert(final boolean[] array, final boolean element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static boolean[] insert(final boolean[] array, final int index, final boolean element) {
		final boolean[] newArray = new boolean[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static char[] insert(final char[] array, final char element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static char[] insert(final char[] array, final int index, final char element) {
		final char[] newArray = new char[array.length + 1];
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param <T> COMMENT
	 * @param array COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	public static <T> T[] insert(final T[] array, final T element) {
		return ArrayUtilities.insert(array, 0, element);
	}

	/**
	 * @param <T> COMMENT
	 * @param array COMMENT
	 * @param index COMMENT
	 * @param element COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] insert(final T[] array, final int index, final T element) {
		final T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + 1);
		if (index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		newArray[index] = element;
		if (index != array.length)
			System.arraycopy(array, index, newArray, index + 1, array.length - index);
		return newArray;
	}

	/**
	 * @param array COMMENT
	 * @param object COMMENT
	 * @return COMMENT
	 */
	public static Object[] add(final Object[] array, final Object object) {
		Object concat[];
		if (array == null) {
			concat = (Object[]) Array.newInstance(object.getClass(), 1);
			concat[0] = object;
		} else {
			final int newSize = array.length + 1;
			concat = (Object[]) Array.newInstance(array.getClass().getComponentType(), newSize);

			System.arraycopy(array, 0, concat, 0, array.length);
			concat[array.length] = object;
		}
		return concat;
	}

	/**
	 * @param array COMMENT
	 * @param n COMMENT
	 * @return COMMENT
	 */
	public static int[] add(final int[] array, final int n) {
		if (array == null)
			return new int[] { n };
		final int[] concat = new int[array.length + 1];
		System.arraycopy(array, 0, concat, 0, array.length);
		concat[array.length] = n;
		return concat;
	}

	/**
	 * @param array COMMENT
	 * @param n COMMENT
	 * @return COMMENT
	 */
	public static int[] remove(final int[] array, final int n) {
		if (array == null)
			return null;
		final int[] concat = new int[array.length + 1];
		System.arraycopy(array, 0, concat, 0, array.length);
		concat[array.length] = n;
		return concat;
	}

	/**
	 * @param array COMMENT
	 * @param toAppend COMMENT
	 * @return COMMENT
	 */
	public static int[] addAll(final int[] array, final int[] toAppend) {
		final int[] concat = new int[array.length + toAppend.length];
		System.arraycopy(array, 0, concat, 0, array.length);
		System.arraycopy(toAppend, 0, concat, array.length, toAppend.length);
		return concat;
	}

	// //////////////////////////////////////

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final int values[], final int value) {
		for (final int element : values)
			if (element == value)
				return true;

		return false;
	}

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final long values[], final long value) {
		for (final long element : values)
			if (element == value)
				return true;

		return false;
	}

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final short values[], final short value) {
		for (final short element : values)
			if (element == value)
				return true;

		return false;
	}

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final byte values[], final byte value) {
		for (final byte element : values)
			if (element == value)
				return true;

		return false;
	}

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final float values[], final float value) {
		for (final float element : values)
			if (element == value)
				return true;

		return false;
	}

	/**
	 * @param values COMMENT
	 * @param value COMMENT
	 * @return COMMENT
	 */
	public static boolean contains(final double values[], final double value) {
		for (final double element : values)
			if (element == value)
				return true;

		return false;
	}

	// ///////////////////////////////////

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static int[] slice(final int[] array, final int[] indicies) {
		final int[] values = new int[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static long[] slice(final long[] array, final int[] indicies) {
		final long[] values = new long[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static float[] slice(final float[] array, final int[] indicies) {
		final float[] values = new float[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static double[] slice(final double[] array, final int[] indicies) {
		final double[] values = new double[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static byte[] slice(final byte[] array, final int[] indicies) {
		final byte[] values = new byte[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static short[] slice(final short[] array, final int[] indicies) {
		final short[] values = new short[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static char[] slice(final char[] array, final int[] indicies) {
		final char[] values = new char[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static boolean[] slice(final boolean[] array, final int[] indicies) {
		final boolean[] values = new boolean[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	/**
	 * @param array COMMENT
	 * @param indicies COMMENT
	 * @return COMMENT
	 */
	public static Object[] slice(final Object[] array, final int[] indicies) {
		final Object[] values = new Object[indicies.length];
		for (int i = 0; i < indicies.length; i++)
			values[i] = array[indicies[i]];
		return values;
	}

	// ///////////////////////////////

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Integer[] toObjects(final int[] array) {
		final Integer[] objects = new Integer[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Integer.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Long[] toObjects(final long[] array) {
		final Long[] objects = new Long[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Long.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Float[] toObjects(final float[] array) {
		final Float[] objects = new Float[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Float.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Double[] toObjects(final double[] array) {
		final Double[] objects = new Double[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Double.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Byte[] toObjects(final byte[] array) {
		final Byte[] objects = new Byte[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Byte.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Short[] toObjects(final short[] array) {
		final Short[] objects = new Short[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Short.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Boolean[] toObjects(final boolean[] array) {
		final Boolean[] objects = new Boolean[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Boolean.valueOf(array[i]);

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static Character[] toObjects(final char[] array) {
		final Character[] objects = new Character[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = Character.valueOf(array[i]);

		return objects;
	}

	// ////////////////////////////////

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static int[] toPrimatives(final Integer[] array) {
		final int[] objects = new int[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].intValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static long[] toPrimatives(final Long[] array) {
		final long[] objects = new long[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].longValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static short[] toPrimatives(final Short[] array) {
		final short[] objects = new short[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].shortValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static byte[] toPrimatives(final Byte[] array) {
		final byte[] objects = new byte[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].byteValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static char[] toPrimatives(final Character[] array) {
		final char[] objects = new char[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].charValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static boolean[] toPrimatives(final Boolean[] array) {
		final boolean[] objects = new boolean[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].booleanValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static float[] toPrimatives(final Float[] array) {
		final float[] objects = new float[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].floatValue();

		return objects;
	}

	/**
	 * @param array COMMENT
	 * @return COMMENT
	 */
	public static double[] toPrimatives(final Double[] array) {
		final double[] objects = new double[array.length];

		for (int i = 0; i < array.length; i++)
			objects[i] = array[i].doubleValue();

		return objects;
	}

}
