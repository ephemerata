package net.kezvh.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author afflux
 * @param <T> type to iterate over
 */
public abstract class AbstractIterator<T> implements Iterator<T> {
	private T nextObject;
	private boolean firstTime = true;
	private boolean noMoreElements = false;

	/**
	 * @return the next object
	 * @throws NoSuchElementException if there are no more elements
	 */
	protected abstract T findNext() throws NoSuchElementException;

	private T tryFindNext() {
		this.firstTime = false;
		try {
			return this.findNext();
		} catch (final NoSuchElementException e) {
			this.noMoreElements = true;
			return null;
		}
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 * @return x
	 */
	public final boolean hasNext() {
		if (this.firstTime)
			this.nextObject = this.tryFindNext();
		return !this.noMoreElements;
	}

	/**
	 * @see java.util.Iterator#next()
	 * @return x
	 */
	public final T next() {
		if (this.firstTime)
			this.nextObject = this.tryFindNext();
		if (!this.hasNext())
			throw new NoSuchElementException();
		try {
			return this.nextObject;
		} finally {
			this.nextObject = this.tryFindNext();
		}
	}

	/**
	 * By default, throws UnsupportedOperationException. This can be overridden.
	 *
	 * @see java.util.Iterator#remove()
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
