/**
 *
 */
package net.kezvh.collections.trees;

import java.util.NoSuchElementException;

import net.kezvh.collections.AbstractIterator;
import net.kezvh.development.UnimplementedException;

/**
 * @author afflux
 *
 * Same as BFS
 *
 * @param <T> type
 */
public class LevelOrderTreeIterator<T> extends AbstractIterator<T> {
	/**
	 * @see net.kezvh.collections.AbstractIterator#findNext()
	 */
	@Override
	protected T findNext() throws NoSuchElementException {
		throw new UnimplementedException();
	}
}
