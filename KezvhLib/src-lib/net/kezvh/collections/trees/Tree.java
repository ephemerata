/**
 *
 */
package net.kezvh.collections.trees;

import java.util.Collection;
import java.util.List;

/**
 * @author afflux
 *
 * @param <E> collection element type
 */
public interface Tree<E> {
	/**
	 * @return a value associated with the root node of this tree
	 */
	E value();

	/**
	 * @param value new value
	 * @return the previous value of the root node
	 */
	E setValue(E value);

	/**
	 * you can't add things to this list, though things may be removed
	 *
	 * @return a list of children of the tree, backed by the tree
	 */
	List<Tree<E>> children();

	/**
	 * backed by the tree
	 *
	 * @param index index of the child
	 * @return child at the index
	 */
	Tree<E> child(int index);

	/**
	 * @param e a value to associate with a new child of the tree
	 * @return the new child node
	 */
	Tree<E> addChild(E e);

	/**
	 * @param index index in the list of children to insert the child
	 * @param e a value to associate with a new child of the tree
	 * @return the new child node
	 */
	Tree<E> addChild(int index, E e);

	/**
	 * remove a child node from the tree
	 *
	 * @param index index of a child
	 * @return tree that exists with that child as the root
	 */
	Tree<E> pruneChild(int index);

	/**
	 * like "Collection.addAll", the original tree will not become *part* of this tree
	 * merely, its elements and structure will become part of this tree.
	 *
	 * @param tree
	 * @return the child tree node of the tree as spliced
	 */
	Tree<E> splice(Tree<E> tree);

	/**
	 * @param index index at which to insert the spliced tree
	 * @param tree
	 * @return the child tree node of the tree as spliced
	 */
	Tree<E> spliceAt(int index, Tree<E> tree);

	/**
	 * @param index in the list of children
	 * @return the value of the child at the specified index
	 */
	E childValue(int index);

	/**
	 * @return true if the node has no children
	 */
	boolean isLeaf();

	/**
	 * @return a collection of elements of the tree backed by the tree. iterates inorder.
	 */
	Collection<E> inorder();

	/**
	 * @return a collection of elements of the tree backed by the tree. iterates preorder (depth first).
	 */
	Collection<E> preorder();

	/**
	 * @return a collection of elements of the tree backed by the tree. iterates postorder.
	 */
	Collection<E> postorder();

	/**
	 * @return a collection of elements of the tree backed by the tree. iterates level order (breadth first).
	 */
	Collection<E> levelOrder();

	/**
	 * @return number of nodes in the tree
	 */
	int size();

	/**
	 * @return the tree to which this is a child, if such exists
	 */
	Tree<E> parent();
}
