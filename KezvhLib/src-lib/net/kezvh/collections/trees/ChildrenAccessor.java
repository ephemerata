package net.kezvh.collections.trees;

import java.util.Iterator;

/**
 * @author afflux
 *
 * @param <T> the type of the tree node
 */
public interface ChildrenAccessor<T> {
	/**
	 * @param node
	 * @return the children of the node
	 */
	Iterator<T> getChildren(T node);
}