/**
 *
 */
package net.kezvh.collections.trees;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author afflux
 *
 * @param <E> element type
 */
public class ListRefTree<E> implements Tree<E> {
	private int size;
	private E value;
	private ChildrenList _children = null;
	private int numChildren = 0;

	/**
	 * @param rootValue initial root value
	 */
	public ListRefTree(final E rootValue) {
		this.value = rootValue;
		this.size = 1;
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#addChild(java.lang.Object)
	 */
	@Override
	public Tree<E> addChild(final E e) {
		this.numChildren++;
		final Tree<E> newChild = new ChildTree(e);
		this.addChild(newChild);
		return newChild;
	}

	/**
	 * for internal usage
	 *
	 * @param newChild new child
	 */
	protected void addChild(final Tree<E> newChild) {
		this.children().addInternal(newChild);
		this.size++;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#addChild(int, java.lang.Object)
	 */
	@Override
	public Tree<E> addChild(final int index, final E e) {
		final Tree<E> newChild = new ListRefTree<E>(e);
		final List<Tree<E>> children = this.children();
		children.add(index, newChild);
		this.size++;
		return newChild;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#children()
	 */
	@Override
	public ChildrenList children() {
		if (this._children == null)
			this._children = new ChildrenList();
		return this._children;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#childValue(int)
	 */
	@Override
	public E childValue(final int index) {
		return this.children().get(index).value();
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#inorder()
	 */
	@Override
	public Collection<E> inorder() {
		return new TraverseOrderedCollection() {
			/**
			* @see java.util.AbstractCollection#iterator()
			*/
			@Override
			protected Iterator<Tree<E>> treeIterator() {
				return new InOrderTreeIterator<Tree<E>>();
			}
		};
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#isLeaf()
	 */
	@Override
	public boolean isLeaf() {
		return this.size() == 1;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#levelOrder()
	 */
	@Override
	public Collection<E> levelOrder() {
		return new TraverseOrderedCollection() {
			/**
			* @see java.util.AbstractCollection#iterator()
			*/
			@Override
			protected Iterator<Tree<E>> treeIterator() {
				return new LevelOrderTreeIterator<Tree<E>>();
			}
		};
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#postorder()
	 */
	@Override
	public Collection<E> postorder() {
		return new TraverseOrderedCollection() {
			/**
			* @see java.util.AbstractCollection#iterator()
			*/
			@Override
			protected Iterator<Tree<E>> treeIterator() {
				return new PostOrderTreeIterator<Tree<E>>();
			}
		};
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#preorder()
	 */
	@Override
	public Collection<E> preorder() {
		return new TraverseOrderedCollection() {
			/**
			* @see java.util.AbstractCollection#iterator()
			*/
			@Override
			protected Iterator<Tree<E>> treeIterator() {
				return new PreOrderTreeIterator<Tree<E>>(ListRefTree.this, new ChildrenAccessor<Tree<E>>() {
					/**
					 * @see net.kezvh.collections.trees.ChildrenAccessor#getChildren(java.lang.Object)
					 */
					@Override
					public Iterator<Tree<E>> getChildren(final Tree<E> node) {
						return node.children().iterator();
					}
				});
			}
		};
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#value()
	 */
	@Override
	public E value() {
		return this.value;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#setValue(Object)
	 */
	@Override
	public E setValue(final E value) {
		try {
			return value;
		} finally {
			this.value = value;
		}
	}

	/**
	 * called by child trees after they add a child
	 * @param count number of children added
	 */
	protected void childAdded(final int count) {
		this.size++;
	}

	/**
	 * called by child trees after they remove a child
	 * @param count number of children removed
	 */
	protected void childRemoved(final int count) {
		this.size--;
	}

	/**
	 * @author afflux
	 *
	 */
	private abstract class TraverseOrderedCollection extends AbstractCollection<E> {
		/**
		 * @see java.util.AbstractCollection#iterator()
		 */
		@Override
		public Iterator<E> iterator() {
			return null;
		}

		protected abstract Iterator<Tree<E>> treeIterator();

		/**
		 * @see java.util.AbstractCollection#size()
		 */
		@Override
		public int size() {
			return ListRefTree.this.size();
		}
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#parent()
	 */
	@Override
	public Tree<E> parent() {
		return null;
	}

	private class ChildTree extends ListRefTree<E> {
		public ChildTree(final Tree<E> toDuplicate) {
			super(toDuplicate.value());
			for (final Tree<E> child : toDuplicate.children())
				this.addChild(new ChildTree(child));
		}

		private void addChild(final ChildTree tree) {
			ListRefTree.this.childAdded(1);
			super.addChild(tree);
		}

		/**
		 * @see net.kezvh.collections.trees.ListRefTree#parent()
		 */
		@Override
		public Tree<E> parent() {
			return ListRefTree.this;
		}

		/**
		 * @see net.kezvh.collections.trees.ListRefTree#addChild(java.lang.Object)
		 */
		@Override
		public Tree<E> addChild(final E e) {
			ListRefTree.this.childAdded(1);
			return super.addChild(e);
		}

		/**
		 * @see net.kezvh.collections.trees.ListRefTree#addChild(int, java.lang.Object)
		 */
		@Override
		public Tree<E> addChild(final int index, final E e) {
			ListRefTree.this.childAdded(1);
			return super.addChild(index, e);
		}

		/**
		 * @param rootValue initial root value
		 */
		public ChildTree(final E rootValue) {
			super(rootValue);
		}

		/**
		 * @see net.kezvh.collections.trees.ListRefTree#childAdded(int)
		 * @param arg0 number added
		 */
		@Override
		protected void childAdded(final int arg0) {
			ListRefTree.this.childAdded(arg0);
			super.childAdded(arg0);
		}

		/**
		 * @see net.kezvh.collections.trees.ListRefTree#childRemoved(int)
		 * @param arg0 number removed
		 */
		@Override
		protected void childRemoved(final int arg0) {
			ListRefTree.this.childRemoved(arg0);
			super.childRemoved(arg0);
		}
	}

	private class ChildrenList extends LinkedList<Tree<E>> {
		/**
		 * @see java.util.LinkedList#addAll(int, java.util.Collection)
		 */
		@Override
		public boolean addAll(final int index, final Collection<? extends Tree<E>> c) {
			ListRefTree.this.childAdded(c.size());
			return super.addAll(index, c);
		}

		/**
		 * @see java.util.LinkedList#addFirst(java.lang.Object)
		 */
		@Override
		public void addFirst(final Tree<E> e) {
			ListRefTree.this.childAdded(1);
			super.addFirst(e);
		}

		/**
		 * @see java.util.LinkedList#addLast(java.lang.Object)
		 */
		@Override
		public void addLast(final Tree<E> e) {
			ListRefTree.this.childAdded(1);
			super.addLast(e);
		}

		/**
		 * @see java.util.LinkedList#remove(int)
		 */
		@Override
		public Tree<E> remove(final int index) {
			ListRefTree.this.childRemoved(1);
			return super.remove(index);
		}

		/**
		 * @see java.util.LinkedList#remove(java.lang.Object)
		 */
		@Override
		public boolean remove(final Object o) {
			final boolean removed = super.remove(o);
			if (removed)
				ListRefTree.this.childRemoved(1);
			return removed;
		}

		/**
		 * @see java.util.LinkedList#removeFirst()
		 */
		@Override
		public Tree<E> removeFirst() {
			ListRefTree.this.childRemoved(1);
			return super.removeFirst();
		}

		/**
		 * @see java.util.LinkedList#removeLast()
		 */
		@Override
		public Tree<E> removeLast() {
			ListRefTree.this.childRemoved(1);
			return super.removeLast();
		}

		/**
		 * @see java.util.LinkedList#removeLastOccurrence(java.lang.Object)
		 */
		@Override
		public boolean removeLastOccurrence(final Object o) {
			final boolean removed = super.removeLastOccurrence(o);
			if (removed)
				ListRefTree.this.childRemoved(1);
			return removed;
		}

		/**
		 * @see java.util.LinkedList#add(java.lang.Object)
		 */
		@Override
		public boolean add(final Tree<E> e) {
			throw new UnsupportedOperationException("cannot add bare trees to children list");
		}

		/**
		 * @see java.util.LinkedList#addAll(java.util.Collection)
		 */
		@Override
		public boolean addAll(final Collection<? extends Tree<E>> c) {
			throw new UnsupportedOperationException("cannot add bare trees to children list");
		}

		/**
		 * @see java.util.LinkedList#add(int, java.lang.Object)
		 */
		@Override
		public void add(final int index, final Tree<E> element) {
			throw new UnsupportedOperationException("cannot add bare trees to children list");
		}

		/**
		 * call this to add new nodes
		 *
		 * @param e new tree to add
		 * @return true if something
		 */
		protected boolean addInternal(final Tree<E> e) {
			return super.add(e);
		}
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#pruneChild(int)
	 */
	@Override
	public Tree<E> pruneChild(final int index) {
		final Tree<E> childTree = this.children().remove(index);
		this.size -= childTree.size();
		return childTree;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#child(int)
	 */
	@Override
	public Tree<E> child(final int index) {
		return this.children().get(index);
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#splice(net.kezvh.collections.trees.Tree)
	 */
	@Override
	public Tree<E> splice(final Tree<E> tree) {
		final Tree<E> treeAsChild = new ChildTree(tree);
		this.children().add(treeAsChild);
		return treeAsChild;
	}

	/**
	 * @see net.kezvh.collections.trees.Tree#spliceAt(int, net.kezvh.collections.trees.Tree)
	 */
	@Override
	public Tree<E> spliceAt(final int index, final Tree<E> tree) {
		final Tree<E> treeAsChild = new ChildTree(tree);
		this.children().add(index, treeAsChild);
		return treeAsChild;
	}
}
