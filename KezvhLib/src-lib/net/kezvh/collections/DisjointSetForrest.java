/**
 *
 */
package net.kezvh.collections;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import net.kezvh.development.UnimplementedException;

/**
 * @author afflux
 *
 * @param <T> COMMENT
 */
public class DisjointSetForrest<T> implements Forest<T> {
	private final class Node {
		private final T element;
		private Node parent;
		private int rank = 0;
		private int size = 1; // only has non-zero size if this is a root node

		/**
		 * @param element COMMENT
		 */
		public Node(final T element) {
			this.element = element;
			this.parent = this;
		}

		/**
		 * @return the size
		 */
		public int getSize() {
			return this.size;
		}

		/**
		 * @return the rank
		 */
		public int getRank() {
			return this.rank;
		}

		/**
		 * @param rank the rank to set
		 */
		public void setRank(final int rank) {
			this.rank = rank;
		}

		/**
		 * @return the element
		 */
		public T getElement() {
			return this.element;
		}

		/**
		 * @return the parent
		 */
		public Node getParent() {
			return this.parent;
		}

		/**
		 * @param parent the parent to set
		 */
		public void setParent(final Node parent) {
			if (parent != this) {
				parent.size += this.size; // if we're already not a root, our size is already 0
				this.size = 0;
			}
			this.parent = parent;

		}
	}

	private final class DisjointSet extends AbstractSet<T> {
		private final Node root;

		/**
		 * @param element COMMENT
		 */
		public DisjointSet(final T element) {
			final Node node = DisjointSetForrest.this.getNodeFor(element);
			this.root = DisjointSetForrest.this.findRoot(node);
		}

		/**
		 * @see java.util.AbstractCollection#iterator()
		 * @return iterator of the set
		 */
		@Override
		public Iterator<T> iterator() {
			throw new UnimplementedException("" + this.root); // TODO Unimplemented (may not be needed)
		}

		/**
		 * @see java.util.AbstractCollection#contains(java.lang.Object)
		 * @param o COMMENT
		 * @return true if the set contains the object
		 */
		@Override
		public boolean contains(final Object o) {
			final Node n = DisjointSetForrest.this.getNodeFor(o);
			if (n == null) // the object isn't even in the forest
				return false;
			return DisjointSetForrest.this.findRoot(n) == this.root;
		}

		/**
		 * @see java.util.AbstractCollection#size()
		 * @return number of elements in the set
		 */
		@Override
		public int size() {
			return this.root.size;
		}

		/**
		 * @see java.util.AbstractSet#equals(java.lang.Object)
		 * @param o COMMENT
		 * @return true if the sets have the same root
		 */
		@Override
		public boolean equals(final Object o) {
			if (o == null)
				return false;
			if (o == this)
				return true;
			if (this.getClass() != o.getClass())
				return false;

			final DisjointSet other = (DisjointSet) o;
			return this.root == other.root;
		}

		/**
		 * @see java.util.AbstractSet#hashCode()
		 * @return hash of root
		 */
		@Override
		public int hashCode() {
			return this.root.getElement().hashCode();
		}
	}

	private final HashMap<T, Node> elementNodes;
	private int setCount;

	/**
	 * @param elements COMMENT
	 */
	public DisjointSetForrest(final T[] elements) {
		this.setCount = elements.length;
		this.elementNodes = new HashMap<T, Node>(elements.length, 1.0f); // fixed size hash
		for (final T element : elements)
			this.elementNodes.put(element, new Node(element));
	}

	private final Node getNodeFor(final Object element) {
		return this.elementNodes.get(element);
	}

	private final Node findRoot(final T element) {
		return this.findRoot(this.getNodeFor(element));
	}

	private final Node findRoot(final Node node) {
		final LinkedList<Node> stack = new LinkedList<Node>();

		Node root = node;
		while (root.getParent() != root) {
			stack.add(root);
			root = root.getParent();
		}

		for (final Node n : stack)
			n.setParent(root);

		return root;
	}

	/**
	 * @see net.kezvh.collections.Forest#elements()
	 * @return the number of elements
	 */
	@Override
	public int elements() {
		return this.elementNodes.size();
	}

	/**
	 * TODO note that iterating the set is currently unimplemented
	 *
	 * @see net.kezvh.collections.Forest#find(java.lang.Object)
	 * @param element COMMENT
	 * @return the number of sets
	 */
	@Override
	public Set<T> find(final T element) {
		return new DisjointSet(element);
	}

	/**
	 * @see net.kezvh.collections.Forest#size()
	 * @return the number of sets
	 */
	@Override
	public int size() {
		return this.setCount;
	}

	/**
	 * @see net.kezvh.collections.Forest#union(java.lang.Object, java.lang.Object)
	 * @param a COMMENT
	 * @param b COMMENT
	 */
	@Override
	public void union(final T a, final T b) {
		final Node aRoot = this.findRoot(a);
		final Node bRoot = this.findRoot(b);
		if (aRoot.equals(bRoot))
			return;

		this.setCount--;
		if (aRoot.getRank() > bRoot.getRank())
			bRoot.setParent(aRoot);
		else if (bRoot.getRank() > aRoot.getRank())
			aRoot.setParent(bRoot);
		else {
			bRoot.setParent(aRoot);
			aRoot.setRank(aRoot.getRank() + 1);
		}
	}

}
