package net.kezvh.collections;

/**
 * @author mjacob
 *
 * @param <E1> first type
 * @param <E2> second type
 */
public class UnmodifiablePair<E1, E2> implements Pair<E1, E2> {
	private final E1 e1;
	private final E2 e2;

	/**
	 * @param e1 first element
	 * @param e2 second element
	 */
	public UnmodifiablePair(final E1 e1, final E2 e2) {
		super();
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public E1 get1() {
		return this.e1;
	}

	@Override
	public E2 get2() {
		return this.e2;
	}

}
