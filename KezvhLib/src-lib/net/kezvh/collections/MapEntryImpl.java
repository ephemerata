package net.kezvh.collections;

import com.google.common.collect.AbstractMapEntry;

/**
 * @author mjacob
 *
 * @param <K> COMMENT
 * @param <V> COMMENT
 */
public class MapEntryImpl<K, V> extends AbstractMapEntry<K, V> {
	private final K key;
	private V value;

	/**
	 * @param key COMMENT
	 * @param value COMMENT
	 */
	public MapEntryImpl(final K key, final V value) {
		super();
		this.key = key;
		this.value = value;
	}

	@Override
	public K getKey() {
		return this.key;
	}

	@Override
	public V getValue() {
		return this.value;
	}

	/**
	 * set the value stored inside this entry. Call this is you override setValue.
	 *
	 * @param value new value
	 * @return old value
	 */
	protected V setImplValue(final V value) {
		try {
			return this.value;
		} finally {
			this.value = value;
		}
	}

}
