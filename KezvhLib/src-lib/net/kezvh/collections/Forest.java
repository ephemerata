/**
 *
 */
package net.kezvh.collections;

import java.util.Set;

/**
 * @author afflux
 *
 * @param <T> FIXME comment
 */
public interface Forest<T> {
	/**
	 * @param element
	 * @return the set to which the element belongs
	 */
	Set<T> find(T element);

	/**
	 * unify the sets a and b
	 *
	 * @param a
	 * @param b
	 */
	void union(T a, T b);

	/**
	 * @return the number of SETS in the forest
	 */
	int size();

	/**
	 * @return the number of elements in the forest
	 */
	int elements();
}
