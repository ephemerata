package net.kezvh.collections.views;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import net.kezvh.collections.graphs.directed.DirectedGraph;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.graphs.directed.Edge;
import net.kezvh.collections.graphs.directed.EdgeWithEdgeValue;
import net.kezvh.functional.lambda.L1;
import net.kezvh.functional.lambda.L2;

/**
 * @author mjacob
 *
 * @param <Node> COMMENT
 * @param <EdgeValue> COMMENT
 */
public class DirectedGraphEdgesAsView<Node, EdgeValue> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {

	private final DirectedGraph<Node> innerGraph;
	private final L2<EdgeValue, Node, Node> getEdgeValue;
	private final L1<EdgeWithEdgeValue<Node, EdgeValue>, Edge<Node>> getEdgeWithValue = new L1<EdgeWithEdgeValue<Node, EdgeValue>, Edge<Node>>() {
		@Override
		public EdgeWithEdgeValue<Node, EdgeValue> op(final Edge<Node> param) {
			return new EdgeWithEdgeValue<Node, EdgeValue>() {
				@Override
				public Node getDestinationNode() {
					return param.getDestinationNode();
				}

				@Override
				public Node getSourceNode() {
					return param.getSourceNode();
				}

				@Override
				public EdgeValue getValue() {
					return DirectedGraphEdgesAsView.this.getEdgeValue.op(param.getSourceNode(), param.getDestinationNode());
				}

				public EdgeValue setValue(final EdgeValue newValue) {
					return DirectedGraphEdgesAsView.this.addEdge(param.getSourceNode(), param.getDestinationNode(), newValue);
				}
			};
		}
	};

	/**
	 * @param <Node> COMMENT
	 * @param <EdgeValue> COMMENT
	 * @param innerGraph COMMENT
	 * @param edgeValue COMMENT
	 * @return COMMENT
	 */
	public static <Node, EdgeValue> DirectedGraphWithEdgeValues<Node, EdgeValue> getGraphWithConstantEdgeValues(final DirectedGraph<Node> innerGraph, final EdgeValue edgeValue) {
		return new DirectedGraphEdgesAsView<Node, EdgeValue>(innerGraph, new L2<EdgeValue, Node, Node>() {
			public EdgeValue op(final Node param1, final Node param2) {
				return edgeValue;
			}
		});
	}

	/**
	 * @param innerGraph COMMENT
	 * @param getEdgeValue COMMENT
	 */
	public DirectedGraphEdgesAsView(final DirectedGraph<Node> innerGraph, final L2<EdgeValue, Node, Node> getEdgeValue) {
		super();
		this.innerGraph = innerGraph;
		this.getEdgeValue = getEdgeValue;
	}

	@Override
	public boolean add(final Node e) {
		return this.innerGraph.add(e);
	}

	@Override
	public boolean addAll(final Collection<? extends Node> c) {
		return this.innerGraph.addAll(c);
	}

	@Override
	public void clear() {
		this.innerGraph.clear();
	}

	@Override
	public boolean contains(final Object o) {
		return this.innerGraph.contains(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return this.innerGraph.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return this.innerGraph.isEmpty();
	}

	@Override
	public Iterator<Node> iterator() {
		return this.innerGraph.iterator();
	}

	@Override
	public boolean remove(final Object o) {
		return this.innerGraph.remove(o);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return this.innerGraph.removeAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return this.innerGraph.retainAll(c);
	}

	@Override
	public int size() {
		return this.innerGraph.size();
	}

	@Override
	public Object[] toArray() {
		return this.innerGraph.toArray();
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		return this.innerGraph.toArray(a);
	}

	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		return this.innerGraph.containsEdge(source, destination);
	}

	@Override
	public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.innerGraph.containsEdge(edge);
	}

	@Override
	public int edgeCount() {
		return this.innerGraph.edgeCount();
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
		return new SetView<Edge<Node>, EdgeWithEdgeValue<Node, EdgeValue>>(this.innerGraph.edges(), this.getEdgeWithValue);
	}

	@Override
	public int inDegree(final Node destination) {
		return this.innerGraph.inDegree(destination);
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
		return new SetView<Edge<Node>, EdgeWithEdgeValue<Node, EdgeValue>>(this.innerGraph.inEdges(destination), this.getEdgeWithValue);
	}

	@Override
	public int outDegree(final Node source) {
		return this.innerGraph.outDegree(source);
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
		return new SetView<Edge<Node>, EdgeWithEdgeValue<Node, EdgeValue>>(this.innerGraph.outEdges(source), this.getEdgeWithValue);
	}

	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		if (this.containsEdge(source, destination))
			return this.getEdgeValue(source, destination);

		this.innerGraph.addEdge(source, destination);
		return null;
	}

	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		if (this.containsEdge(source, destination))
			return this.getEdgeValue.op(source, destination);

		return null;
	}

	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		if (this.containsEdge(source, destination)) {
			this.innerGraph.removeEdge(source, destination);
			return this.getEdgeValue.op(source, destination);
		}
		return null;
	}

	@Override
	public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.removeEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	@Override
	public Set<Node> adjacentFrom(final Node source) {
		return this.innerGraph.adjacentFrom(source);
	}

	@Override
	public Set<Node> adjacentTo(final Node destination) {
		return this.innerGraph.adjacentTo(destination);
	}
}
