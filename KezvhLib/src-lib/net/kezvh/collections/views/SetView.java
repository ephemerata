/**
 *
 */
package net.kezvh.collections.views;

import java.util.Set;

import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <D> destination type
 * @param <S> source type
 */
public class SetView<S, D> extends CollectionView<S, D> implements Set<D> {
	/**
	 * @param innerCollection collection of objects on which to view
	 * @param mappingFunction function to map inner collection to destination
	 *            type
	 */
	public SetView(final Set<S> innerCollection, final L1<D, S> mappingFunction) {
		super(innerCollection, mappingFunction);
	}

	/**
	 * @param innerCollection collection of objects on which to view
	 * @param mappingFunction function to map inner collection to destination
	 *            type
	 * @param inverseFuction COMMENT
	 */
	public SetView(final Set<S> innerCollection, final L1<D, S> mappingFunction, final L1<S, D> inverseFuction) {
		super(innerCollection, mappingFunction, inverseFuction);
	}
}
