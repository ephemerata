/**
 *
 */
package net.kezvh.collections.views;

import java.util.Iterator;

import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <S> source type
 * @param <D> destination type
 */
public class IteratorView<S, D> implements Iterator<D> {
	private final L1<D, ? super S> map;
	private final Iterator<? extends S> iterator;

	/**
	 * @param map map from the source type to the destination type
	 * @param iterator a iterator of the source type
	 */
	public IteratorView(final L1<D, ? super S> map, final Iterator<? extends S> iterator) {
		super();
		this.map = map;
		this.iterator = iterator;
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 * @return x
	 */
	@Override
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	/**
	 * @see java.util.Iterator#next()
	 * @return x
	 */
	@Override
	public D next() {
		return this.map.op(this.iterator.next());
	}

	/**
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		this.iterator.remove();
	}

}
