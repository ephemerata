/**
 *
 */
package net.kezvh.collections.views;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

import net.kezvh.functional.Operations;
import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <D> the destination type
 * @param <S> the source type
 */
public class CollectionView<S, D> extends AbstractCollection<D> {

	private final Collection<S> innerCollection;
	private final L1<D, S> mappingFunction;
	private final L1<S, D> inverseFunction;

	/**
	 * @param innerCollection collection to map
	 * @param mappingFunction function to map collection
	 */
	@SuppressWarnings("unchecked")
	public CollectionView(final Collection<S> innerCollection, final L1<D, S> mappingFunction) {
		this(innerCollection, mappingFunction, (L1<S, D>) Operations.UNSUPPORTED);
	}

	/**
	 * @param innerCollection COMMENT
	 * @param mappingFunction COMMENT
	 * @param inverseFunction COMMENT
	 */
	public CollectionView(final Collection<S> innerCollection, final L1<D, S> mappingFunction, final L1<S, D> inverseFunction) {
		super();
		if (innerCollection == null)
			throw new IllegalArgumentException();
		this.innerCollection = innerCollection;
		this.mappingFunction = mappingFunction;
		this.inverseFunction = inverseFunction;
	}

	/**
	 * @see java.util.AbstractCollection#iterator()
	 * @return iterator
	 */
	@Override
	public Iterator<D> iterator() {
		return new IteratorView<S, D>(this.mappingFunction, this.innerCollection.iterator());
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 * @return size of the collecion
	 */
	@Override
	public int size() {
		return this.innerCollection.size();
	}

	/**
	 * @return the mappingFunction
	 */
	public L1<D, ? super S> getMappingFunction() {
		return this.mappingFunction;
	}

	@Override
	public boolean add(final D o) {
		return this.innerCollection.add(this.inverseFunction.op(o));
	}

	/**
	 * @see java.lang.Object#hashCode()
	 * @return COMMENT
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.innerCollection == null) ? 0 : this.innerCollection.hashCode());
		result = prime * result + ((this.inverseFunction == null) ? 0 : this.inverseFunction.hashCode());
		result = prime * result + ((this.mappingFunction == null) ? 0 : this.mappingFunction.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj COMMENT
	 * @return COMMENT
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final CollectionView<?, ?> other = (CollectionView<?, ?>) obj;
		if (this.innerCollection == null) {
			if (other.innerCollection != null)
				return false;
		} else if (!this.innerCollection.equals(other.innerCollection))
			return false;
		if (this.inverseFunction == null) {
			if (other.inverseFunction != null)
				return false;
		} else if (!this.inverseFunction.equals(other.inverseFunction))
			return false;
		if (this.mappingFunction == null) {
			if (other.mappingFunction != null)
				return false;
		} else if (!this.mappingFunction.equals(other.mappingFunction))
			return false;
		return true;
	}

	/**
	 * @see java.util.AbstractCollection#addAll(java.util.Collection)
	 * @param c COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(final Collection<? extends D> c) {
		final Collection<S> inverse = new CollectionView<D, S>((Collection<D>) c, this.inverseFunction, this.mappingFunction);
		return this.innerCollection.addAll(inverse);
	}

	/**
	 * @see java.util.AbstractCollection#clear()
	 */
	@Override
	public void clear() {
		this.innerCollection.clear();
	}

	/**
	 * @see java.util.AbstractCollection#contains(java.lang.Object)
	 * @param o COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean contains(final Object o) {
		if (this.inverseFunction.equals(Operations.UNSUPPORTED))
			return super.contains(o);
		return this.innerCollection.contains(this.inverseFunction.op((D) o));
	}

	/**
	 * @see java.util.AbstractCollection#containsAll(java.util.Collection)
	 * @param c COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean containsAll(final Collection<?> c) {
		if (this.inverseFunction.equals(Operations.UNSUPPORTED))
			return super.containsAll(c);

		final Collection<S> inverse = new CollectionView<D, S>((Collection<D>) c, this.inverseFunction, this.mappingFunction);
		return this.innerCollection.containsAll(inverse);
	}

	/**
	 * @see java.util.AbstractCollection#remove(java.lang.Object)
	 * @param o COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(final Object o) {
		if (this.inverseFunction.equals(Operations.UNSUPPORTED))
			super.remove(o);

		return this.innerCollection.remove(this.inverseFunction.op((D) o));
	}

	/**
	 * @see java.util.AbstractCollection#isEmpty()
	 * @return COMMENT
	 */
	@Override
	public boolean isEmpty() {
		return this.innerCollection.isEmpty();
	}

	/**
	 * @see java.util.AbstractCollection#removeAll(java.util.Collection)
	 * @param c COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean removeAll(final Collection<?> c) {
		if (this.inverseFunction.equals(Operations.UNSUPPORTED))
			return super.removeAll(c);

		final Collection<S> inverse = new CollectionView<D, S>((Collection<D>) c, this.inverseFunction, this.mappingFunction);
		return this.innerCollection.removeAll(inverse);
	}

	/**
	 * @see java.util.AbstractCollection#retainAll(java.util.Collection)
	 * @param c COMMENT
	 * @return COMMENT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean retainAll(final Collection<?> c) {
		if (this.inverseFunction.equals(Operations.UNSUPPORTED))
			return super.removeAll(c);

		final Collection<S> inverse = new CollectionView<D, S>((Collection<D>) c, this.inverseFunction, this.mappingFunction);
		return this.innerCollection.removeAll(inverse);
	}
}
