/**
 *
 */
package net.kezvh.collections.views;

import java.util.Collection;
import java.util.Set;

import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <E> set element type
 */
public class ConditionalSetView<E> extends ConditionalCollectionView<E> implements Set<E> {
	/**
	 * @param predicate predicate which elements of the inner collection must
	 *            sattisfy in order to belong to the condtional set
	 * @param innerCollection collection of elements which may or may not be seen by the view
	 */
	public ConditionalSetView(final L1<Boolean, ? super E> predicate, final Collection<? extends E> innerCollection) {
		super(predicate, innerCollection);
	}

}
