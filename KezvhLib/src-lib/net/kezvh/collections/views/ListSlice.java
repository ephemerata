/**
 */
package net.kezvh.collections.views;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;

/**
 * @author afflux
 * @param <T> FIXME comment
 */
public class ListSlice<T> extends AbstractList<T> {
	private final List<T> base;
	private final int[] indicies;

	/**
	 * @param base FIXME comment
	 * @param indicies FIXME comment
	 */
	public ListSlice(final List<T> base, final int... indicies) {
		this.base = base;
		this.indicies = indicies;
	}

	/**
	 * @param base FIXME comment
	 * @param indicies FIXME comment
	 */
	public ListSlice(final List<T> base, final Collection<Integer> indicies) {
		this.base = base;
		this.indicies = new int[indicies.size()];
		int index = 0;
		for (final int i : indicies)
			this.indicies[index++] = i;
	}

	/**
	 * @see java.util.AbstractList#get(int)
	 * @param arg0 FIXME comment
	 * @return x
	 */
	@Override
	public T get(final int arg0) {
		return this.base.get(this.indicies[arg0]);
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 * @return x
	 */
	@Override
	public int size() {
		return this.indicies.length;
	}

}
