/**
 */
package net.kezvh.collections.views;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author afflux
 * @param <K> FIXME comment
 * @param <V> FIXME comment
 */
public class MapSlice<K, V> extends AbstractMap<K, V> {
	private final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
		private final Map<K, V> base;
		private final Set<K> keys;

		EntrySet(final Map<K, V> base, final Set<K> keys) {
			this.base = base;
			this.keys = keys;
		}

		/**
		 * @see java.util.AbstractCollection#iterator()
		 * @return x
		 */
		@Override
		public Iterator<java.util.Map.Entry<K, V>> iterator() {
			return this.base.entrySet().iterator(); // FIXME this is garbage
		}

		/**
		 * @see java.util.AbstractCollection#size()
		 * @return x
		 */
		@Override
		public int size() {
			return this.keys.size();
		}

	}

	private final EntrySet entrySet;

	/**
	 * @param base FIXME comment
	 * @param keys FIXME comment
	 */
	public MapSlice(final Map<K, V> base, final Set<K> keys) {
		this.entrySet = new EntrySet(base, keys);
	}

	/**
	 * @see java.util.AbstractMap#entrySet()
	 * @return x
	 */
	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		return this.entrySet;
	}

}
