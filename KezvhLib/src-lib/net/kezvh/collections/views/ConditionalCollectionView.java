/**
 *
 */
package net.kezvh.collections.views;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

import net.kezvh.functional.Operations;
import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 *
 * @param <E> collection element type
 */
public class ConditionalCollectionView<E> extends AbstractCollection<E> {
	private final Collection<? extends E> innerCollection;
	private final L1<Boolean, ? super E> filter;

	/**
	 * @param predicate condition
	 * @param innerCollection original elements
	 */
	public ConditionalCollectionView(final L1<Boolean, ? super E> predicate, final Collection<? extends E> innerCollection) {
		super();
		this.filter = predicate;
		this.innerCollection = innerCollection;
	}

	/**
	 * @see java.util.AbstractCollection#iterator()
	 * @return iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return new ConditionalIteratorView<E>(this.filter, this.innerCollection.iterator());
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 * @return the size of the collection
	 */
	@Override
	public int size() {
		return Operations.ITERATOR_COUNT.op(this.iterator());
	}

}
