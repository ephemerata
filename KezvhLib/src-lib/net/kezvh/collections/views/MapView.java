/**
 *
 */
package net.kezvh.collections.views;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <K> COMMENT
 * @param <VD> Destination value type
 * @param <VS> Source value type
 */
public class MapView<K, VD, VS> extends AbstractMap<K, VD> {

	private final Map<K, VS> innerMap;
	private final L1<VD, VS> op;
	private final L1<VS, VD> inverseOp;
	private final Set<Entry<K, VD>> innerEntrySet;

	private final L1<Map.Entry<K, VD>, Map.Entry<K, VS>> entryOp = new L1<Entry<K, VD>, Entry<K, VS>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public java.util.Map.Entry<K, VD> op(final Map.Entry<K, VS> param) {
			return new Entry<K, VD>() {
				/**
				 * @see java.util.Map.Entry#getKey()
				 * @return COMMENT
				 */
				@Override
				public K getKey() {
					return param.getKey();
				}

				/**
				 * @see java.util.Map.Entry#getValue()
				 * @return COMMENT
				 */
				@Override
				public VD getValue() {
					return MapView.this.op.op(param.getValue());
				}

				public VD setValue(final VD arg0) {
					return MapView.this.op.op(param.setValue(MapView.this.inverseOp.op(arg0)));
				}
			};
		}
	};

	private final L1<Map.Entry<K, VS>, Map.Entry<K, VD>> inverseEntryOp = new L1<Entry<K, VS>, Entry<K, VD>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public java.util.Map.Entry<K, VS> op(final Map.Entry<K, VD> param) {
			return new Entry<K, VS>() {
				/**
				 * @see java.util.Map.Entry#getKey()
				 * @return COMMENT
				 */
				@Override
				public K getKey() {
					return param.getKey();
				}

				/**
				 * @see java.util.Map.Entry#getValue()
				 * @return COMMENT
				 */
				@Override
				public VS getValue() {
					return MapView.this.inverseOp.op(param.getValue());
				}

				public VS setValue(final VS arg0) {
					return MapView.this.inverseOp.op(param.setValue(MapView.this.op.op(arg0)));
				}
			};
		}
	};

	/**
	 * @param innerMap COMMENT
	 * @param op COMMENT
	 */
	public MapView(final Map<K, VS> innerMap, final L1<VD, VS> op) {
		super();
		this.innerMap = innerMap;
		this.op = op;
		this.inverseOp = new L1<VS, VD>() {
			public VS op(final VD param) {
				throw new UnsupportedOperationException();
			}
		};

		this.innerEntrySet = new SetView<Map.Entry<K, VS>, Map.Entry<K, VD>>(this.innerMap.entrySet(), this.entryOp, this.inverseEntryOp);
	}

	/**
	 * @see java.util.AbstractMap#entrySet()
	 * @return COMMENT
	 */
	@Override
	public Set<Map.Entry<K, VD>> entrySet() {
		return this.innerEntrySet;
	}

	/**
	 * @see java.util.AbstractMap#clear()
	 */
	@Override
	public void clear() {
		this.innerMap.clear();
	}
}
