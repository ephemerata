package net.kezvh.collections.views;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;

import net.kezvh.collections.CrossIterator;
import net.kezvh.collections.Pair;
import net.kezvh.collections.graphs.directed.DirectedGraphWithEdgeValues;
import net.kezvh.collections.graphs.directed.EdgeWithEdgeValue;
import net.kezvh.functional.lambda.L1;
import net.kezvh.functional.lambda.L2;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;

/**
 * create a graph where all edges between any two non-identical
 * elements of a set are considered to exist, with a map which
 * determines the value associated with an edge.
 *
 *
 * @author mjacob
 *
 * @param <Node> node type
 * @param <EdgeValue> edge value type
 */
public class DirectedGraphWithEdgeValuesAsView<Node, EdgeValue> extends AbstractSet<Node> implements DirectedGraphWithEdgeValues<Node, EdgeValue> {
	private final Set<Node> innerSet;
	private final L2<EdgeValue, Node, Node> edgeValues;

	/**
	 * @param innerSet set of nodes.
	 * @param edgeValues map of edges to values
	 */
	public DirectedGraphWithEdgeValuesAsView(final Set<Node> innerSet, final L2<EdgeValue, Node, Node> edgeValues) {
		super();
		this.innerSet = innerSet;
		this.edgeValues = edgeValues;
	}

	private static final <T> Predicate<T> notEquals(final T t) {
		return new Predicate<T>() {
			public boolean apply(final T input) {
				return !t.equals(input);
			}
		};
	}

	@Override
	public Set<Node> adjacentFrom(final Node source) {
		return Sets.filter(this.innerSet, DirectedGraphWithEdgeValuesAsView.notEquals(source));
	}

	@Override
	public Set<Node> adjacentTo(final Node destination) {
		return Sets.filter(this.innerSet, DirectedGraphWithEdgeValuesAsView.notEquals(destination));
	}

	@Override
	public boolean containsEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		return this.containsEdge(edge.getSourceNode(), edge.getDestinationNode());
	}

	@Override
	public boolean containsEdge(final Node source, final Node destination) {
		return !source.equals(destination);
	}

	@Override
	public int edgeCount() {
		final int size = this.innerSet.size();
		return size * (size - 1);
	}

	private final Predicate<Pair<Node, Node>> notEquals = new Predicate<Pair<Node, Node>>() {
		public boolean apply(final Pair<Node, Node> input) {
			return !input.get1().equals(input.get2());
		}
	};

	private final Set<EdgeWithEdgeValue<Node, EdgeValue>> edgeSet = new AbstractSet<EdgeWithEdgeValue<Node, EdgeValue>>() {

		@Override
		public Iterator<EdgeWithEdgeValue<Node, EdgeValue>> iterator() {
			return new IteratorView<Pair<Node, Node>, EdgeWithEdgeValue<Node, EdgeValue>>(new L1<EdgeWithEdgeValue<Node, EdgeValue>, Pair<Node, Node>>() {
				@Override
				public EdgeWithEdgeValue<Node, EdgeValue> op(final Pair<Node, Node> param) {
					return new EdgeWithEdgeValue<Node, EdgeValue>() {
						public Node getDestinationNode() {
							return param.get2();
						}

						public Node getSourceNode() {
							return param.get1();
						}

						public EdgeValue getValue() {
							return DirectedGraphWithEdgeValuesAsView.this.edgeValues.op(param.get1(), param.get2());
						}

						public EdgeValue setValue(final EdgeValue newValue) {
							throw new UnsupportedOperationException();
						}
					};
				}
			},

			Iterators.filter(new CrossIterator<Node, Node>(DirectedGraphWithEdgeValuesAsView.this.innerSet, DirectedGraphWithEdgeValuesAsView.this.innerSet), DirectedGraphWithEdgeValuesAsView.this.notEquals));
		}

		@Override
		public int size() {
			return DirectedGraphWithEdgeValuesAsView.this.edgeCount();
		}
	};

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> edges() {
		return this.edgeSet;
	}

	@Override
	public int inDegree(final Node destination) {
		return this.edgeSet.size() - 1;
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> inEdges(final Node destination) {
		return new AbstractSet<EdgeWithEdgeValue<Node, EdgeValue>>() {

			@Override
			public Iterator<EdgeWithEdgeValue<Node, EdgeValue>> iterator() {
				return new IteratorView<Node, EdgeWithEdgeValue<Node, EdgeValue>>(new L1<EdgeWithEdgeValue<Node, EdgeValue>, Node>() {
					public net.kezvh.collections.graphs.directed.EdgeWithEdgeValue<Node, EdgeValue> op(final Node param) {
						return new EdgeWithEdgeValue<Node, EdgeValue>() {
							@Override
							public Node getDestinationNode() {
								return destination;
							}

							@Override
							public Node getSourceNode() {
								return param;
							}

							@Override
							public EdgeValue getValue() {
								return DirectedGraphWithEdgeValuesAsView.this.edgeValues.op(param, destination);
							}

							public EdgeValue setValue(final EdgeValue newValue) {
								throw new UnsupportedOperationException();
							}
						};
					}
				}, DirectedGraphWithEdgeValuesAsView.this.adjacentTo(destination).iterator());
			}

			@Override
			public int size() {
				return DirectedGraphWithEdgeValuesAsView.this.inDegree(destination);
			}

		};
	}

	@Override
	public int outDegree(final Node source) {
		return this.size() - 1;
	}

	@Override
	public Set<EdgeWithEdgeValue<Node, EdgeValue>> outEdges(final Node source) {
		return new AbstractSet<EdgeWithEdgeValue<Node, EdgeValue>>() {

			@Override
			public Iterator<EdgeWithEdgeValue<Node, EdgeValue>> iterator() {
				return new IteratorView<Node, EdgeWithEdgeValue<Node, EdgeValue>>(new L1<EdgeWithEdgeValue<Node, EdgeValue>, Node>() {
					public net.kezvh.collections.graphs.directed.EdgeWithEdgeValue<Node, EdgeValue> op(final Node param) {
						return new EdgeWithEdgeValue<Node, EdgeValue>() {
							@Override
							public Node getDestinationNode() {
								return param;
							}

							@Override
							public Node getSourceNode() {
								return source;
							}

							@Override
							public EdgeValue getValue() {
								return DirectedGraphWithEdgeValuesAsView.this.edgeValues.op(source, param);
							}

							public EdgeValue setValue(final EdgeValue newValue) {
								throw new UnsupportedOperationException();
							}
						};
					}
				}, DirectedGraphWithEdgeValuesAsView.this.adjacentFrom(source).iterator());
			}

			@Override
			public int size() {
				return DirectedGraphWithEdgeValuesAsView.this.outDegree(source);
			}

		};
	}

	@Override
	public EdgeValue addEdge(final Node source, final Node destination, final EdgeValue edgeValue) {
		throw new UnsupportedOperationException();
	}

	@Override
	public EdgeValue getEdgeValue(final Node source, final Node destination) {
		return this.edgeValues.op(source, destination);
	}

	@Override
	public EdgeValue removeEdge(final EdgeWithEdgeValue<Node, EdgeValue> edge) {
		throw new UnsupportedOperationException();
	}

	@Override
	public EdgeValue removeEdge(final Node source, final Node destination) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<Node> iterator() {
		return this.innerSet.iterator();
	}

	@Override
	public int size() {
		return this.innerSet.size();
	}

}
