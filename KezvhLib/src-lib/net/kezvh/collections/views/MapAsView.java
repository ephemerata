/**
 *
 */
package net.kezvh.collections.views;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

import net.kezvh.collections.MapEntryImpl;
import net.kezvh.functional.Operations;
import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 * @param <S> COMMENT
 * @param <D> COMMENT
 */
public class MapAsView<S, D> extends AbstractMap<S, D> {

	private final Set<S> sourceSet;
	private final L1<D, S> mapOp;
	private final L1<S, D> inverseMapOp;

	private final L1<Entry<S, D>, S> entryOp = new L1<Entry<S, D>, S>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public Map.Entry<S, D> op(final S param) {
			return new MapEntryImpl<S, D>(param, MapAsView.this.mapOp.op(param));
		}
	};
	private final L1<S, Entry<S, D>> inverseEntryOp = new L1<S, Entry<S, D>>() {
		/**
		 * @see net.kezvh.functional.lambda.L1#op(java.lang.Object)
		 * @param param COMMENT
		 * @return COMMENT
		 */
		@Override
		public S op(final Map.Entry<S, D> param) {
			return param.getKey();
		}
	};
	private final Set<Entry<S, D>> entrySet;

	/**
	 * @param sourceSet COMMENT
	 * @param op COMMENT
	 * @param inverseOp COMMENT
	 */
	public MapAsView(final Set<S> sourceSet, final L1<D, S> op, final L1<S, D> inverseOp) {
		super();
		this.sourceSet = sourceSet;
		this.mapOp = op;
		this.inverseMapOp = inverseOp;
		this.entrySet = new SetView<S, Map.Entry<S, D>>(this.sourceSet, this.entryOp, this.inverseEntryOp);
	}

	/**
	 * @param sourceSet COMMENT
	 * @param op COMMENT
	 */
	@SuppressWarnings("unchecked")
	public MapAsView(final Set<S> sourceSet, final L1<D, S> op) {
		this(sourceSet, op, (L1<S, D>) Operations.UNSUPPORTED);
	}

	/**
	 * @see java.util.AbstractMap#entrySet()
	 * @return COMMENT
	 */
	@Override
	public Set<Map.Entry<S, D>> entrySet() {
		return this.entrySet;
	}

	@SuppressWarnings("unchecked")
	@Override
	public D get(final Object key) {
		return this.mapOp.op((S) key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean containsValue(final Object value) {
		if (Operations.UNSUPPORTED.equals(this.inverseEntryOp))
			return super.containsValue(value);

		return this.sourceSet.contains(this.inverseMapOp.op((D) value));
	}

	@Override
	public boolean containsKey(final Object key) {
		return this.sourceSet.contains(key);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.inverseMapOp == null) ? 0 : this.inverseMapOp.hashCode());
		result = prime * result + ((this.mapOp == null) ? 0 : this.mapOp.hashCode());
		result = prime * result + ((this.sourceSet == null) ? 0 : this.sourceSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final MapAsView<?, ?> other = (MapAsView<?, ?>) obj;
		if (this.inverseMapOp == null) {
			if (other.inverseMapOp != null)
				return false;
		} else if (!this.inverseMapOp.equals(other.inverseMapOp))
			return false;
		if (this.mapOp == null) {
			if (other.mapOp != null)
				return false;
		} else if (!this.mapOp.equals(other.mapOp))
			return false;
		if (this.sourceSet == null) {
			if (other.sourceSet != null)
				return false;
		} else if (!this.sourceSet.equals(other.sourceSet))
			return false;
		return true;
	}
}
