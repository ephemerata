/**
 *
 */
package net.kezvh.collections.views;

import java.util.Iterator;
import java.util.NoSuchElementException;

import net.kezvh.collections.AbstractIterator;
import net.kezvh.functional.lambda.L1;

/**
 * @author afflux
 *
 * @param <E> iterator element type
 */
public class ConditionalIteratorView<E> extends AbstractIterator<E> {
	private final Iterator<? extends E> innerIterator;
	private final L1<Boolean, ? super E> condition;

	/**
	 * @param predicate condition
	 * @param innerIterator elements which we may or many not iterate
	 */
	public ConditionalIteratorView(final L1<Boolean, ? super E> predicate, final Iterator<? extends E> innerIterator) {
		super();
		this.condition = predicate;
		this.innerIterator = innerIterator;
	}

	/**
	 * @see net.kezvh.collections.AbstractIterator#findNext()
	 * @return next element if such exists
	 * @throws NoSuchElementException if no more elments
	 */
	@Override
	protected E findNext() throws NoSuchElementException {
		while (this.innerIterator.hasNext()) {
			final E next = this.innerIterator.next();
			if (this.condition.op(next))
				return next;
		}
		throw new NoSuchElementException();
	}

}
