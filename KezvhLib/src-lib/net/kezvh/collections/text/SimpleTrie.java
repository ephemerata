/**
 *
 */
package net.kezvh.collections.text;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.SortedMap;

import net.kezvh.collections.AbstractIterator;
import net.kezvh.collections.SortedArrayMap;
import net.kezvh.development.UnimplementedException;

/**
 * @author afflux
 */
public final class SimpleTrie extends AbstractSet<String> {
	private final class SimpleTrieIterator extends AbstractIterator<String> {
		private final StringBuffer wurd = new StringBuffer("");
		//		private final LinkedList<Iterator<Map.Entry<Character, SimpleTrie>>> iteratorStack = new LinkedList<Iterator<Map.Entry<Character, SimpleTrie>>>() {
		//			{
		//				this.add(SimpleTrie.this.childrenByInitialChar.entrySet().iterator());
		//			}
		//		};
		private final LinkedList<Iterator<Character>> keyIteratorStack = new LinkedList<Iterator<Character>>() {
			{
				this.add(SimpleTrie.this.childrenByInitialChar.keySet().iterator());
			}
		};
		private final LinkedList<Iterator<SimpleTrie>> valueIteratorStack = new LinkedList<Iterator<SimpleTrie>>() {
			{
				this.add(SimpleTrie.this.childrenByInitialChar.values().iterator());
			}
		};

		@Override
		protected String findNext() throws NoSuchElementException {
			while (!this.keyIteratorStack.isEmpty() && !this.lastKey().hasNext()) {
				this.keyIteratorStack.removeLast();
				this.valueIteratorStack.removeLast();
				this.wurd.deleteCharAt(this.wurd.length() - 1);
			}
			if (this.keyIteratorStack.isEmpty())
				throw new NoSuchElementException();
			final Character keyNextNode = this.lastKey().next();
			final SimpleTrie valueNextNode = this.lastValue().next();

			if (this.wurd.length() < this.keyIteratorStack.size())
				this.wurd.append(keyNextNode);
			else
				this.wurd.setCharAt(this.wurd.length() - 1, keyNextNode);

			final Iterator<Character> keyChildren = valueNextNode.childrenByInitialChar.keySet().iterator();
			final Iterator<SimpleTrie> valueChildren = valueNextNode.childrenByInitialChar.values().iterator();

			if (keyChildren.hasNext()) {
				this.keyIteratorStack.addLast(keyChildren);
				this.valueIteratorStack.addLast(valueChildren);
			}

			return SimpleTrieIterator.this.wurd.toString();
		}

		private Iterator<Character> lastKey() {
			return this.keyIteratorStack.peekLast();
		}

		private Iterator<SimpleTrie> lastValue() {
			return this.valueIteratorStack.peekLast();
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			super.remove();
		}
	}

	private boolean complete = false;
	private final SortedMap<Character, SimpleTrie> childrenByInitialChar = new SortedArrayMap<Character, SimpleTrie>();

	private SimpleTrie() {
		// do nothing
	}

	/**
	 * @param dictionary FIXME COMMENT
	 * @throws IOException stuff
	 */
	public SimpleTrie(final LineNumberReader dictionary) throws IOException {
		this.complete = false;
		while (dictionary.ready())
			this.add((CharSequence) dictionary.readLine());
	}

	private void add(final CharSequence word) {
		if (word.length() == 0)
			this.complete = true;
		else {
			final char initial = word.charAt(0);
			if (!this.childrenByInitialChar.containsKey(initial))
				this.childrenByInitialChar.put(initial, new SimpleTrie());
			this.childrenByInitialChar.get(initial).add(word.subSequence(1, word.length()));
		}
	}

	/**
	 * @param c FIXME COMMENT
	 * @return FIXME COMMENT
	 */
	public boolean containsStartingWith(final char c) {
		return this.childrenByInitialChar.containsKey(c);
	}

	/**
	 * @param next FIXME COMMENT
	 * @return FIXME COMMENT
	 */
	public SimpleTrie subTrie(final char next) {
		return this.childrenByInitialChar.get(next);
	}

	/**
	 * @return FIXME COMMENT
	 */
	public boolean isComplete() {
		return this.complete;
	}

	/**
	 * @return an iterator of words in the trie
	 */
	@Override
	public Iterator<String> iterator() {
		return new SimpleTrieIterator();
	}

	@Override
	public int size() {
		throw new UnimplementedException();
	}
}