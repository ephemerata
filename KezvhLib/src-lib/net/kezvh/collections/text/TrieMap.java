package net.kezvh.collections.text;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;

import net.kezvh.collections.AbstractIterator;
import net.kezvh.collections.KezvhArrays;
import net.kezvh.development.UnimplementedException;

import com.google.common.collect.AbstractMapEntry;

/**
 * TODO refactor. this has to be one of the mose obtuse pieces of code i've ever
 * written. Pull salvageable logic outside this class to reduce its bulk. Law of
 * Demeter..
 *
 * @author afflux
 * @param <T> COMMENT
 */
public class TrieMap<T> extends AbstractMap<String, T> implements SortedMap<String, T> {

	/**
	 * because this trie is designed to hold wordlists up to a million words,
	 * space is very important. inserts are O(#letters in alphabet), but space
	 * isn't wasted.
	 *
	 * @author mjacob
	 * @param <V>
	 */
	private static final class TrieNode<V> extends AbstractMap<Character, TrieNode<V>> implements SortedMap<Character, TrieNode<V>> {
		private char[] keys = new char[] {};
		private TrieNode<V>[] suffices = this.createSuffices(0);
		private V value;

		@SuppressWarnings("unchecked")
		private TrieNode<V>[] createSuffices(final int size) {
			return (TrieNode<V>[]) Array.newInstance(this.getClass(), size);
		}

		/**
		 * @param index COMMENT
		 * @return x
		 */
		public char getSuffixKey(final int index) {
			return this.keys[index];
		}

		/**
		 * @param index COMMENT
		 * @return x
		 */
		public TrieNode<V> getSuffixNode(final int index) {
			return this.suffices[index];
		}

		/**
		 * @param key COMMENT
		 * @return x
		 */
		public TrieNode<V> find(final char key) {
			final int index = this.keyIndex(key);
			if (index < 0)
				return new TrieNode<V>();
			// throw new RuntimeException(key + "?" +
			// Arrays.toString(this.suffices));
			return this.getSuffixNode(index);
		}

		/**
		 * @param key COMMENT
		 * @return x
		 */
		public int keyIndex(final char key) {
			return Arrays.binarySearch(this.keys, key);
		}

		/**
		 * @param key COMMENT
		 * @param newValue COMMENT
		 * @param insertionPoint COMMENT
		 */
		public void insert(final char key, final TrieNode<V> newValue, final int insertionPoint) {
			final char[] newKeys = new char[this.keys.length + 1];
			final TrieNode<V>[] newSuffices = this.createSuffices(this.keys.length + 1);
			System.arraycopy(this.keys, 0, newKeys, 0, insertionPoint);
			System.arraycopy(this.suffices, 0, newSuffices, 0, insertionPoint);
			newKeys[insertionPoint] = key;
			newSuffices[insertionPoint] = newValue;
			System.arraycopy(this.keys, insertionPoint, newKeys, insertionPoint + 1, this.keys.length - insertionPoint);
			System.arraycopy(this.suffices, insertionPoint, newSuffices, insertionPoint + 1, this.keys.length - insertionPoint);
			this.keys = newKeys;
			this.suffices = newSuffices;
		}

		/**
		 * @return x
		 */
		public int getNumKeys() {
			return this.keys.length;
		}

		/**
		 * @return your mom
		 */
		public int getNumEntries() {
			int numEntries = 0;
			if (this.value != null)
				numEntries++;

			for (final TrieNode<V> t : this.suffices)
				numEntries += t.getNumEntries();
			return numEntries;
		}

		/**
		 * @param key COMMENT
		 * @param newValue COMMENT
		 * @return x
		 */
		public TrieNode<V> put(final char key, final TrieNode<V> newValue) {
			final int index = this.keyIndex(key);
			if (index >= 0)
				return this.suffices[index] = newValue;

			final int insertionPoint = -(index + 1);
			this.insert(key, newValue, insertionPoint);
			return null;
		}

		/**
		 * @return x
		 */
		public boolean hasValue() {
			return this.value != null;
		}

		/**
		 * @return x
		 */
		public V getValue() {
			return this.value;
		}

		/**
		 * @param value COMMENT
		 * @return x
		 */
		public V setValue(final V value) {
			try {
				return this.value;
			} finally {
				this.value = value;
			}
		}

		@Override
		public Set<java.util.Map.Entry<Character, TrieNode<V>>> entrySet() {
			return new AbstractSet<Entry<Character, TrieNode<V>>>() {

				@Override
				public Iterator<java.util.Map.Entry<Character, TrieNode<V>>> iterator() {
					return new AbstractIterator<Entry<Character, TrieNode<V>>>() {
						int index = 0;

						@Override
						protected java.util.Map.Entry<Character, TrieNode<V>> findNext() throws NoSuchElementException {
							if (this.index >= TrieNode.this.keys.length)
								throw new NoSuchElementException();
							try {
								return new AbstractMapEntry<Character, TrieNode<V>>() {
									private final int myIndex = index;

									@Override
									public Character getKey() {
										return TrieNode.this.keys[this.myIndex];
									}

									@Override
									public TrieNode<V> getValue() {
										return TrieNode.this.suffices[this.myIndex];
									}

									@Override
									public TrieNode<V> setValue(final TrieNode<V> value) {
										try {
											return TrieNode.this.suffices[this.myIndex];
										} finally {
											TrieNode.this.suffices[this.myIndex] = value;
										}
									}
								};
							} finally {
								this.index++;
							}
						}

						/**
						 * TODO this needs some checking to make sure it is used
						 * correctly
						 */
						@Override
						public void remove() {
							TrieNode.this.remove(TrieNode.this.keys[this.index - 1]);
						}
					};
				}

				@Override
				public int size() {
					return TrieNode.this.keys.length;
				}

				@Override
				public boolean remove(final Object o) {
					return TrieNode.this.remove(((Map.Entry<?, ?>) o).getKey()) != null;
				}

				@Override
				public boolean add(final java.util.Map.Entry<Character, TrieNode<V>> e) {
					return TrieNode.this.put(e.getKey(), e.getValue()) != null;
				}
			};
		}

		@Override
		public Comparator<? super Character> comparator() {
			return null;
		}

		@Override
		public Character firstKey() {
			if (this.keys.length > 0)
				return this.keys[0];
			return null;
		}

		@Override
		public SortedMap<Character, TrieNode<V>> headMap(final Character toKey) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Character lastKey() {
			if (this.keys.length > 0)
				return this.keys[this.keys.length - 1];
			return null;
		}

		@Override
		public SortedMap<Character, TrieNode<V>> subMap(final Character fromKey, final Character toKey) {
			throw new UnsupportedOperationException();
		}

		@Override
		public SortedMap<Character, TrieNode<V>> tailMap(final Character fromKey) {
			throw new UnsupportedOperationException();
		}
	}

	private final TrieNode<T> rootTrieNode = new TrieNode<T>();

	/**
	 * @see java.util.AbstractMap#put(java.lang.Object, java.lang.Object)
	 * @param key COMMENT
	 * @param value COMMENT
	 * @return x
	 */
	@Override
	public T put(final String key, final T value) {
		// final long[] longs = TrieMap.getLongs(key);
		final char[] chars = key.toCharArray();
		TrieNode<T> t = this.rootTrieNode;

		for (final char element : chars) {
			final int index = t.keyIndex(element);
			if (index >= 0)
				t = t.getSuffixNode(index);

			else {
				final int insertionPoint = -(index + 1);
				final TrieNode<T> tn = new TrieNode<T>();
				t.insert(element, tn, insertionPoint);
				t = tn;
			}
		}

		return t.setValue(value);
	}

	/**
	 * @see java.util.AbstractMap#containsKey(java.lang.Object)
	 * @param key COMMENT
	 * @return x
	 */
	@Override
	public boolean containsKey(final Object key) {
		if (key == null || String.class != key.getClass())
			return false;

		final String string = (String) key;
		// final long[] longs = TrieMap.getLongs(string);
		final char[] chars = string.toCharArray();
		TrieNode<T> t = this.rootTrieNode;
		for (final char element : chars) {
			final int index = t.keyIndex(element);
			if (index >= 0)
				t = t.getSuffixNode(index);
			else
				return false;
		}

		return t.hasValue();
	}

	/**
	 * @see java.util.AbstractMap#get(java.lang.Object)
	 * @param key COMMENT
	 * @return x
	 */
	@Override
	public T get(final Object key) {
		if (key == null || String.class != key.getClass())
			return null;

		final String string = (String) key;
		// final long[] longs = TrieMap.getLongs(string);
		final char[] chars = string.toCharArray();
		TrieNode<T> t = this.rootTrieNode;
		for (final char element : chars) {
			final int index = t.keyIndex(element);
			if (index >= 0)
				t = t.getSuffixNode(index);
			else
				return null;
		}

		return t.getValue();
	}

	/**
	 * TODO: this MUST implement SortedSet
	 *
	 * @author afflux
	 */
	private final class EntrySet extends AbstractSet<Map.Entry<String, T>> implements SortedSet<Map.Entry<String, T>> {

		private final class TrieNodeTreeIterator extends AbstractIterator<Entry<String, T>> {
			private final StringBuffer wurd = new StringBuffer("");
			private final LinkedList<Iterator<Map.Entry<Character, TrieNode<T>>>> iteratorStack = new LinkedList<Iterator<Map.Entry<Character, TrieNode<T>>>>() {
				{
					this.add(TrieMap.this.rootTrieNode.entrySet().iterator());
				}
			};

			@Override
			protected java.util.Map.Entry<String, T> findNext() throws NoSuchElementException {
				while (true) {
					while (this.last() != null && !this.last().hasNext()) {
						this.iteratorStack.removeLast(); // will throw a NSEE if hasNext() is false
						this.wurd.deleteCharAt(this.wurd.length() - 1);
					}
					if (this.last() == null)
						throw new NoSuchElementException();

					final Map.Entry<Character, TrieNode<T>> nextNode = this.last().next();

					if (this.wurd.length() < this.iteratorStack.size())
						this.wurd.append(nextNode.getKey());
					else
						this.wurd.setCharAt(this.wurd.length() - 1, nextNode.getKey());

					final Iterator<Map.Entry<Character, TrieNode<T>>> children = nextNode.getValue().entrySet().iterator();
					if (children.hasNext())
						this.iteratorStack.addLast(children);

					if (nextNode.getValue().hasValue())
						return new AbstractMapEntry<String, T>() {
							private final String string = TrieNodeTreeIterator.this.wurd.toString();

							@Override
							public String getKey() {
								return this.string;
							}

							@Override
							public T getValue() {
								return nextNode.getValue().getValue();
							}

						};
				}
			}

			private Iterator<Map.Entry<Character, TrieNode<T>>> last() {
				return this.iteratorStack.peekLast();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				super.remove();
			}
		}

		/**
		 * @see java.util.AbstractCollection#iterator()
		 * @return x
		 */
		@Override
		public Iterator<java.util.Map.Entry<String, T>> iterator() {
			return new TrieNodeTreeIterator();
		}

		/**
		 * @see java.util.AbstractCollection#size()
		 * @return x
		 */
		@Override
		public int size() {
			return TrieMap.this.size();
		}

		@Override
		public Comparator<? super java.util.Map.Entry<String, T>> comparator() {
			return null;
		}

		@Override
		public java.util.Map.Entry<String, T> first() {
			throw new UnimplementedException(); // FIXME
		}

		@Override
		public SortedSet<java.util.Map.Entry<String, T>> headSet(final java.util.Map.Entry<String, T> toElement) {
			throw new UnimplementedException(); // FIXME
		}

		@Override
		public java.util.Map.Entry<String, T> last() {
			throw new UnimplementedException(); // FIXME
		}

		@Override
		public SortedSet<java.util.Map.Entry<String, T>> subSet(final java.util.Map.Entry<String, T> fromElement, final java.util.Map.Entry<String, T> toElement) {
			throw new UnimplementedException(); // FIXME
		}

		@Override
		public SortedSet<java.util.Map.Entry<String, T>> tailSet(final java.util.Map.Entry<String, T> fromElement) {
			throw new UnimplementedException(); // FIXME
		}

	}

	private EntrySet entrySet = null;

	/**
	 * @see java.util.AbstractMap#entrySet()
	 * @return x
	 */
	@Override
	public Set<java.util.Map.Entry<String, T>> entrySet() {
		if (this.entrySet == null)
			this.entrySet = new EntrySet();
		return this.entrySet;
	}

	/**
	 * @see java.util.SortedMap#comparator()
	 * @return x
	 */
	public Comparator<? super String> comparator() {
		return new Comparator<String>() {
			/**
			 * @see java.util.Comparator#compare(java.lang.Object,
			 *      java.lang.Object)
			 * @param o1 COMMENT
			 * @param o2 COMMENT
			 * @return o1.compareTo(o2)
			 */
			@Override
			public int compare(final String o1, final String o2) {
				return o1.compareTo(o2);
			}
		};
	}

	/**
	 * @see java.util.SortedMap#firstKey()
	 * @return x
	 */
	public String firstKey() {
		if (this.isEmpty())
			throw new NoSuchElementException();

		return null;
	}

	/**
	 * @see java.util.SortedMap#headMap(java.lang.Object)
	 * @param toKey COMMENT
	 * @return x
	 */
	public SortedMap<String, T> headMap(final String toKey) {
		return null;
	}

	/**
	 * @see java.util.SortedMap#lastKey()
	 * @return x
	 */
	public String lastKey() {
		if (this.isEmpty())
			throw new NoSuchElementException();

		return null;
	}

	/**
	 * @see java.util.SortedMap#subMap(java.lang.Object, java.lang.Object)
	 * @param fromKey COMMENT
	 * @param toKey COMMENT
	 * @return x
	 */
	public SortedMap<String, T> subMap(final String fromKey, final String toKey) {
		return null;
	}

	/**
	 * @see java.util.SortedMap#tailMap(java.lang.Object)
	 * @param fromKey COMMENT
	 * @return x
	 */
	public SortedMap<String, T> tailMap(final String fromKey) {
		return null;
	}

	private String getString(final TrieNode<T> trieNode, final char[] chars, final int firstCharIndex) {
		TrieNode<T> currentNode = trieNode;
		for (int i = firstCharIndex; i < chars.length; i++) {
			final int keyIndex = currentNode.keyIndex(chars[i]);
			if (keyIndex >= 0)
				currentNode = currentNode.getSuffixNode(keyIndex);
			else
				return null;
		}

		final T t = currentNode.getValue();
		if (t == null)
			return null;
		final String s = t.toString();
		if (!(s.equals(new String(chars))))
			//		if (s.length() != chars.length)
			throw new RuntimeException("strings don't match: " + new String(chars) + " vs " + s);
		return s;
	}

	private TrieNode<T>[] getTrieNodes(final char[] chars) {
		TrieNode<T> currentTrieNode = this.rootTrieNode;

		final TrieNode<T>[] trieNodes = KezvhArrays.createArray(currentTrieNode.getClass(), chars.length + 1);
		trieNodes[0] = this.rootTrieNode;

		for (int i = 0; i < chars.length; i++) {
			final TrieNode<T> nextTrieNode;
			try {
				nextTrieNode = currentTrieNode.find(chars[i]);
			} catch (final RuntimeException e) {
				throw new RuntimeException("er, is this in dict: " + Arrays.toString(chars), e);
			}
			trieNodes[i + 1] = nextTrieNode;
			currentTrieNode = nextTrieNode;
		}
		return trieNodes;
	}

	private Collection<String> getSingleChars() {
		final Collection<String> similar = new LinkedList<String>();
		for (int i = 0; i < this.rootTrieNode.getNumKeys(); i++)
			if (this.rootTrieNode.getSuffixNode(i).hasValue())
				similar.add(String.valueOf(this.rootTrieNode.getSuffixKey(i)));
		return similar;
	}

	/**
	 *
	 */
	private void process(final String newS, final boolean recurseCondition, final char[] buf, final ConcurrentHashMap<String, Integer> minima, final int cost, final Map<String, Integer> similar, final int effectiveMaxDepth, final Set<String> visited, final List<Boolean> changedAt) {
		boolean doRecurse = true;
		if (newS != null) {
			final int cmp;
			final Integer previousMinimalCost = minima.putIfAbsent(newS, cost);
			boolean newConnection;
			if (previousMinimalCost != null) {
				newConnection = false;
				cmp = previousMinimalCost.compareTo(cost);
			} else {
				cmp = 1; // no path yet, so always minimal.
				newConnection = true;
			}

			if (cmp >= 0) {
				final Integer previousCost = similar.get(newS);
				if (previousCost == null || cost < previousCost)
					similar.put(newS, cost);

				if (!newConnection && cmp > 0) {
					Integer previousMinimum = minima.put(newS, cost);
					Integer costX = cost;
					while (previousMinimum < costX) {
						costX = previousMinimum;
						previousMinimum = minima.put(newS, costX);
					}
				}
			}
			if (cmp <= 0) // if the cost is too high, know this thing connects easier
				doRecurse = false;
		}
		if (recurseCondition && doRecurse && effectiveMaxDepth != 1 && !visited.contains(newS))
			this.getKeysSimilarN(buf, effectiveMaxDepth - 1, cost + 1, similar, visited, minima, changedAt);

	}

	private void doTailAdds(final char[] chars, final TrieNode<T> lastNode, final char[] add, final int cost, final int effectiveMaxDepth, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final Map<String, Integer> similar, final List<Boolean> changedAt) {
		final int len = chars.length;
		changedAt.add(true);
		for (int i = 0; i < lastNode.getNumKeys(); i++) {
			add[len] = lastNode.getSuffixKey(i);
			//			System.out.println(" tad: " + new String(add) + ", " + lastNode.getSuffixNode(i).hasValue() + ", " + true);

			final T t = lastNode.getSuffixNode(i).getValue();
			String s;
			if (t != null)
				s = t.toString();
			else
				s = null;
			this.process(s, true, add, minima, cost, similar, effectiveMaxDepth, visited, changedAt);
		}
		changedAt.remove(len);
	}

	private void doTranspose(final int k, final int j, final char[] chars, final TrieNode<T> cur, final char[] tra, final int cost, final int effectiveMaxDepth, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final Map<String, Integer> similar, final List<Boolean> changedAt) {
		final char c1 = chars[j];
		final char c2 = chars[k];
		if (c1 == c2)
			return;
		tra[k] = c1;
		tra[j] = c2;
		final boolean tmp = changedAt.get(k);
		changedAt.set(k, changedAt.get(j));
		changedAt.set(j, tmp);
		//		System.out.println(" trx: " + new String(tra) + ", " + this.hasSuffix(cur, c1, c2, chars, k + 2) + ", " + (cur.keyIndex(tra[k]) >= 0));
		this.process(this.getString(cur, tra, k), cur.keyIndex(tra[k]) >= 0, tra, minima, cost, similar, effectiveMaxDepth, visited, changedAt);
		changedAt.set(j, changedAt.get(k));
		changedAt.set(k, tmp);
		tra[k] = c2;
		tra[j] = c1;
	}

	private void doRemove(final int k, final char[] chars, final TrieNode<T> cur, final char[] rem, final int cost, final int effectiveMaxDepth, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final Map<String, Integer> similar, final List<Boolean> changedAt) {
		//		System.out.println(" rem: " + new String(rem) + ", " + this.hasSuffix(cur, chars, k + 1) + ", " + true);
		final boolean tmp = changedAt.remove(k);
		this.process(this.getString(cur, rem, k), true, rem, minima, cost, similar, effectiveMaxDepth, visited, changedAt);
		changedAt.add(k, tmp);
		if (k > 0) // TODO what is this doing?
			rem[k - 1] = chars[k];
	}

	private void doChange(final char ch, final TrieNode<T> node, final int k, final char[] chars, final char[] cha, final int cost, final int effectiveMaxDepth, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final Map<String, Integer> similar, final List<Boolean> changedAt) {
		if (changedAt.get(k))
			return;
		changedAt.set(k, true);
		cha[k] = ch;
		//		System.out.println(" cha: " + new String(cha) + ", " + this.hasSuffix(node, chars, k + 1) + ", " + (node.keyIndex(cha[k]) >= 0));
		this.process(this.getString(node, cha, k + 1), node.keyIndex(cha[k]) >= 0, cha, minima, cost, similar, effectiveMaxDepth, visited, changedAt);
		changedAt.set(k, false);
	}

	private void doAdd(final char ch, final TrieNode<T> node, final int k, final char[] chars, final char[] add, final int cost, final int effectiveMaxDepth, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final Map<String, Integer> similar, final List<Boolean> changedAt) {
		add[k] = ch;
		changedAt.add(k, true);
		//		System.out.println(" add: " + new String(add) + ", " + this.hasSuffix(node, chars, k) + ", " + (node.keyIndex(add[k]) >= 0));
		this.process(this.getString(node, add, k + 1), node.keyIndex(add[k]) >= 0, add, minima, cost, similar, effectiveMaxDepth, visited, changedAt);
		changedAt.remove(k);
	}

	/**
	 * This method is intended to return strings which are "similar" to the
	 * given key. They can vary by n "differences", where a difference is one of
	 * the following: letter added letter removed letter replaced adjacent
	 * letters transposed 94% of a 106k word list is connected with the others
	 * by chains where adjacent words in the chain differe by no more than 3 of
	 * these opperations. this method has some bugs in it, but it's working well
	 * enough for me now that i'm not going to care.
	 *
	 * optimizations:
	 * minima is kept around because we realize that if something is connected to
	 * the main body of words by a shorter path than what we've found so far,
	 * there is no reason to connect it here, or recurse into it.
	 * note that while we don't want to recurse deeper, if the path we've found is
	 * equal in depth to the minimum found so far, we need to mark it.
	 *
	 * also note, if maxdepth != 0,
	 * effective maxdepth = min(max(length - 2, 1), maxdepth)
	 *
	 * @param key original
	 * @param maxDepth depth
	 * @param cost intial cost
	 * @param similar map to store shit in
	 * @param minima a map of minimal distances so far to words we've connected.
	 */
	public void getKeysSimilarN(final CharSequence key, final int maxDepth, final int cost, final Map<String, Integer> similar, final ConcurrentHashMap<String, Integer> minima) {
		this.getKeysSimilarN(key.toString().toCharArray(), maxDepth, cost, similar, new HashSet<String>(), minima, new LinkedList<Boolean>(Collections.nCopies(key.length(), false)));
	}

	private char[] subSequence(final char[] s, final int start, final int fin) {
		final char[] n = new char[fin - start];
		System.arraycopy(s, start, n, 0, fin - start);
		return n;
	}

	private char[] append(final char[] s, final char c) {
		final char[] n = new char[s.length + 1];
		System.arraycopy(s, 0, n, 0, s.length);
		n[s.length] = c;
		return n;
	}

	private void getKeysSimilarN(final char[] key, final int maxDepth, final int cost, final Map<String, Integer> similar, final Set<String> visited, final ConcurrentHashMap<String, Integer> minima, final List<Boolean> changedAt) {
		// FIXME refactor this nasty motherfucker.
		final int len = key.length;

		//		System.out.println("looking at " + new String(key) + " with " + maxDepth + ", " + cost);
		if (len == 0)
			return;

		if (maxDepth == 0) {
			if (!this.containsKey(key)) // FIXME removewhen it's time
				throw new RuntimeException("not in trie, but we assume it has to be: " + new String(key));

			if (!similar.containsKey(key))
				similar.put(key.toString(), cost);
			return;
		}
		visited.add(key.toString());
		final int effectiveMaxDepth = Math.min(Math.max(len - 2, 1), maxDepth);

		final char[] add = this.append(key, '?'); // add
		final char[] rem = this.subSequence(key, 0, len - 1); // remove
		final char[] cha = new char[key.length]; // change, transpose
		System.arraycopy(key, 0, cha, 0, len);

		final char[] chars = key;
		final TrieNode<T>[] trieNodes = this.getTrieNodes(chars); // len + 1, elements, 0 is root

		{ // first iteration, last character, only care about adds
			this.doTailAdds(chars, trieNodes[len], add, cost, effectiveMaxDepth, visited, minima, similar, changedAt);
			add[len] = chars[len - 1]; // set last character of add
		}

		for (int k = len - 1; k >= 0; k--) {
			final TrieNode<T> cur = trieNodes[k];

			for (int j = k + 1; j < len; j++)
				//			if (k != len - 1) // i'd like to iterate over other possible transpositions too
				this.doTranspose(k, j, chars, cur, cha, cost, effectiveMaxDepth, visited, minima, similar, changedAt);// cha is unchanged after this is invoked

			if (len != 1)
				this.doRemove(k, chars, cur, rem, cost, effectiveMaxDepth, visited, minima, similar, changedAt);

			for (int i = 0; i < cur.getNumKeys(); i++) {
				final char ch = cur.getSuffixKey(i);

				final TrieNode<T> node = cur.getSuffixNode(i);

				if (chars[k] != ch)
					this.doChange(ch, node, k, chars, cha, cost, effectiveMaxDepth, visited, minima, similar, changedAt);

				this.doAdd(ch, node, k, chars, add, cost, effectiveMaxDepth, visited, minima, similar, changedAt);
			}

			// reset cha and add
			cha[k] = chars[k];
			if (k > 0)
				add[k] = chars[k - 1];
		}
	}
}
