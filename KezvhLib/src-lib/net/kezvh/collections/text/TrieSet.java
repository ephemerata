package net.kezvh.collections.text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author afflux
 */
public class TrieSet extends AbstractSet<String> implements SortedSet<String> {
	//	private static final Object CONTAINS = new Object();
	private final TrieMap<String> map = new TrieMap<String>();

	/**
	 *
	 */
	public TrieSet() {
		super(); // we're super!
	}

	/**
	 * @param filename name of a file containing a wordlist
	 * @throws IOException if reading is wrong
	 * @throws FileNotFoundException if file not found
	 */
	public TrieSet(final String filename) throws IOException {
		final BufferedReader x = new BufferedReader(new FileReader(filename));

		for (String word = x.readLine(); word != null; word = x.readLine())
			this.add(word);
	}

	/**
	 * @see java.util.AbstractCollection#add(java.lang.Object)
	 * @param e COMMENT
	 * @return x
	 */
	@Override
	public boolean add(final String e) {
		return this.map.put(e, e) != null;
	}

	/**
	 * @see java.util.AbstractCollection#contains(java.lang.Object)
	 * @param o COMMENT
	 * @return x
	 */
	@Override
	public boolean contains(final Object o) {
		return this.map.containsKey(o);
	}

	/**
	 * @see java.util.SortedSet#comparator()
	 * @return x
	 */
	public Comparator<? super String> comparator() {
		return null;
	}

	/**
	 * @see java.util.SortedSet#first()
	 * @return x
	 */
	public String first() {
		return this.map.firstKey();
	}

	/**
	 * @see java.util.SortedSet#headSet(java.lang.Object)
	 * @param toElement COMMENT
	 * @return x
	 */
	public SortedSet<String> headSet(final String toElement) {
		return (SortedSet<String>) this.map.headMap(toElement).keySet();
	}

	/**
	 * @see java.util.SortedSet#last()
	 * @return x
	 */
	public String last() {
		return this.map.lastKey();
	}

	/**
	 * @see java.util.SortedSet#subSet(java.lang.Object, java.lang.Object)
	 * @param fromElement COMMENT
	 * @param toElement COMMENT
	 * @return x
	 */
	public SortedSet<String> subSet(final String fromElement, final String toElement) {
		return (SortedSet<String>) this.map.subMap(fromElement, toElement).keySet();
	}

	/**
	 * @see java.util.SortedSet#tailSet(java.lang.Object)
	 * @param fromElement COMMENT
	 * @return x
	 */
	public SortedSet<String> tailSet(final String fromElement) {
		return (SortedSet<String>) this.map.tailMap(fromElement).keySet();
	}

	/**
	 * @see java.util.AbstractCollection#iterator()
	 * @return x
	 */
	@Override
	public Iterator<String> iterator() {
		return this.map.keySet().iterator();
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 * @return x
	 */
	@Override
	public int size() {
		return this.map.size();
	}

	/**
	 * This method is intended to return strings which are "similar" to the given key.
	 * They can vary by n "differences", where a difference is one of the following:
	 *
	 * letter added
	 * letter removed
	 * letter replaced
	 * adjacent letters transposed
	 *
	 * 94% of a 106k word list is connected with the others by chains where adjacent
	 * words in the chain differe by no more than 3 of these opperations.
	 *
	 * this method has some bugs in it, but it's working well enough for me now that
	 * i'm not going to care.
	 * @param key COMMENT
	 * @param n COMMENT
	 * @param c COMMENT
	 * @param similar COMMENT
	 * @param minima COMMENT
	 */
	public void getStringsSimilarN(final CharSequence key, final int n, final int c, final Map<String, Integer> similar, final ConcurrentHashMap<String, Integer> minima) {
		this.map.getKeysSimilarN(key, n, c, similar, minima);
	}
}
