package net.kezvh.collections;

/**
 * @author mjacob
 * @param <E1> first value type
 * @param <E2> second value type
 */
public interface Pair<E1, E2> {
	/**
	 * @return first value
	 */
	E1 get1();

	/**
	 * @return second value
	 */
	E2 get2();
}
