package net.kezvh.collections.wrappers;

import java.util.AbstractList;

/**
 * @author mjacob
 *
 * @param <E> type
 */
public class ArrayAsList<E> extends AbstractList<E> {
	private final int offset;
	private final int size;
	private final E[] array;

	/**
	 * @param array COMMENT
	 */
	public ArrayAsList(final E[] array) {
		this(array, 0, array.length);
	}

	/**
	 * @param array COMMENT
	 * @param offset COMMENT
	 */
	public ArrayAsList(final E[] array, final int offset) {
		this(array, offset, array.length - offset);
	}

	/**
	 * @param array COMMENT
	 * @param offset COMMENT
	 * @param size COMMENT
	 */
	public ArrayAsList(final E[] array, final int offset, final int size) {
		this.array = array;
		this.offset = offset;
		this.size = size;
	}

	@Override
	public E get(final int index) {
		return this.array[index + this.offset];
	}

	@Override
	public int size() {
		return this.size;
	}

}
