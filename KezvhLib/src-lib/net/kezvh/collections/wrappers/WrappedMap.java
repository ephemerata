/**
 */
package net.kezvh.collections.wrappers;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author afflux
 * @param <K> FIXME comment
 * @param <V> FIXME comment
 */
public abstract class WrappedMap<K, V> implements Map<K, V> {
	private final Map<K, V> base;

	/**
	 * @param base FIXME comment
	 */
	public WrappedMap(final Map<K, V> base) {
		this.base = base;
	}

	/**
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		this.base.clear();
	}

	/**
	 * @see java.util.Map#containsKey(java.lang.Object)
	 * @param arg0 FIXME comment
	 * @return x
	 */
	@Override
	public boolean containsKey(final Object arg0) {
		return this.base.containsKey(arg0);
	}

	/**
	 * @see java.util.Map#containsValue(java.lang.Object)
	 * @param arg0 FIXME comment
	 * @return x
	 */
	@Override
	public boolean containsValue(final Object arg0) {
		return this.base.containsValue(arg0);
	}

	/**
	 * @see java.util.Map#entrySet()
	 * @return x
	 */
	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		return this.base.entrySet();
	}

	/**
	 * @see java.util.Map#get(java.lang.Object)
	 * @param arg0 FIXME comment
	 * @return x
	 */
	@Override
	public V get(final Object arg0) {
		return this.base.get(arg0);
	}

	/**
	 * @see java.util.Map#isEmpty()
	 * @return x
	 */
	@Override
	public boolean isEmpty() {
		return this.base.isEmpty();
	}

	/**
	 * @see java.util.Map#keySet()
	 * @return x
	 */
	@Override
	public Set<K> keySet() {
		return this.base.keySet();
	}

	/**
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 * @param arg0 FIXME comment
	 * @param arg1 FIXME comment
	 * @return x
	 */
	@Override
	public V put(final K arg0, final V arg1) {
		return this.base.put(arg0, arg1);
	}

	/**
	 * @see java.util.Map#putAll(java.util.Map)
	 * @param arg0 FIXME comment
	 */
	@Override
	public void putAll(final Map<? extends K, ? extends V> arg0) {
		this.base.putAll(arg0);
	}

	/**
	 * @see java.util.Map#remove(java.lang.Object)
	 * @param arg0 FIXME comment
	 * @return x
	 */
	@Override
	public V remove(final Object arg0) {
		return this.base.remove(arg0);
	}

	/**
	 * @see java.util.Map#size()
	 * @return x
	 */
	@Override
	public int size() {
		return this.base.size();
	}

	/**
	 * @see java.util.Map#values()
	 * @return x
	 */
	@Override
	public Collection<V> values() {
		return this.base.values();
	}

}
