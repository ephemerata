package net.kezvh.collections.wrappers;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * @author mjacob
 *
 * @param <E> FIXME comment
 * @param <C> FIXME comment
 */
public class CollectionAsList<E, C extends Collection<E>> extends WrappedCollection<E, C> implements List<E> {

	/**
	 * @param innerCollection FIXME comment
	 */
	public CollectionAsList(final C innerCollection) {
		super(innerCollection);
	}

	private ListIterator<E> getAt(final int index) {
		final ListIterator<E> li = this.listIterator();
		for (int i = 0; i < index; i++)
			li.next();
		return li;
	}

	@Override
	public void add(final int index, final E element) {
		final ListIterator<E> li = this.getAt(index);
		li.add(element);
	}

	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		final ListIterator<E> li = this.getAt(index);
		for (final E e : c)
			li.add(e);
		return !c.isEmpty();
	}

	@Override
	public E get(final int index) {
		final ListIterator<E> li = this.getAt(index);
		return li.next();
	}

	@Override
	public int indexOf(final Object o) {
		int i = 0;
		for (final E e : this) {
			if ((e == null && o == null) || (e != null && e.equals(o)))
				return i;
			i++;
		}

		return -1;
	}

	@Override
	public int lastIndexOf(final Object o) {
		int i = 0;
		int x = -1;
		for (final E e : this) {
			if ((e == null && o == null) || (e != null && e.equals(o)))
				x = i;
			i++;
		}
		return x;
	}

	@Override
	public ListIterator<E> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<E> listIterator(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E remove(final int index) {
		final ListIterator<E> li = this.getAt(index);
		try {
			return li.next();
		} finally {
			li.remove();
		}
	}

	@Override
	public E set(final int index, final E element) {
		final ListIterator<E> li = this.getAt(index);
		try {
			return li.next();
		} finally {
			li.set(element);
		}

	}

	@Override
	public List<E> subList(final int fromIndex, final int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}
}
