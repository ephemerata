package net.kezvh.collections.wrappers;

import java.util.Iterator;

/**
 * @author mjacob
 *
 * @param <E> FIXME comment
 */
public abstract class WrappedIterator<E> implements Iterator<E> {
	private final Iterator<E> inner;

	/**
	 * @param inner FIXME comment
	 */
	public WrappedIterator(final Iterator<E> inner) {
		super();
		this.inner = inner;
	}

	@Override
	public boolean hasNext() {
		return this.inner.hasNext();
	}

	@Override
	public E next() {
		return this.inner.next();
	}

	@Override
	public void remove() {
		this.inner.remove();
	}

}
