/**
 *
 */
package net.kezvh.collections.wrappers;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author afflux
 * @param <E> value type
 * @param <C> collection type
 */
public abstract class WrappedCollection<E, C extends Collection<E>> implements Collection<E> {
	private final C inner;

	/**
	 * modify at your own risk
	 *
	 * @return inner collection
	 */
	protected final C getInner() {
		return this.inner;
	}

	/**
	 * @param innerCollection collection to wrap
	 */
	public WrappedCollection(final C innerCollection) {
		this.inner = innerCollection;
	}

	/**
	 * @see java.util.Collection#add(java.lang.Object)
	 * @param e element to add to collection
	 * @return true if element was already in the collection
	 */
	@Override
	public boolean add(final E e) {
		return this.inner.add(e);
	}

	/**
	 * @see java.util.Collection#addAll(java.util.Collection)
	 * @param c collection whose elements we add
	 * @return true if the collection was modified
	 */
	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return this.inner.addAll(c);
	}

	/**
	 * @see java.util.Collection#clear()
	 */
	@Override
	public void clear() {
		this.inner.clear();
	}

	/**
	 * @see java.util.Collection#contains(java.lang.Object)
	 * @param o object
	 * @return obvious?
	 */
	@Override
	public boolean contains(final Object o) {
		return this.inner.contains(o);
	}

	/**
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 * @param c collection
	 * @return true if collection modified
	 */
	@Override
	public boolean containsAll(final Collection<?> c) {
		return this.inner.containsAll(c);
	}

	/**
	 * @see java.util.Collection#isEmpty()
	 * @return ture if empty (yes, TURE. sometimes, the law of the excluded
	 *         middle doesn't apply.)
	 */
	@Override
	public boolean isEmpty() {
		return this.inner.isEmpty();
	}

	/**
	 * @see java.util.Collection#iterator()
	 * @return iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return this.inner.iterator();
	}

	/**
	 * @see java.util.Collection#remove(java.lang.Object)
	 * @param o your mom
	 * @return true if object was in collection
	 */
	@Override
	public boolean remove(final Object o) {
		return this.inner.remove(o);
	}

	/**
	 * @see java.util.Collection#removeAll(java.util.Collection)
	 * @param c collection
	 * @return true if collection was modified
	 */
	@Override
	public boolean removeAll(final Collection<?> c) {
		return this.inner.removeAll(c);
	}

	/**
	 * @see java.util.Collection#retainAll(java.util.Collection)
	 * @param c collection
	 * @return true if collection was modified
	 */
	@Override
	public boolean retainAll(final Collection<?> c) {
		return this.inner.retainAll(c);
	}

	/**
	 * @see java.util.Collection#size()
	 * @return size of collection
	 */
	@Override
	public int size() {
		return this.inner.size();
	}

	/**
	 * @see java.util.Collection#toArray()
	 * @return argh
	 */
	@Override
	public Object[] toArray() {
		return this.inner.toArray();
	}

	/**
	 * @see java.util.Collection#toArray(T[])
	 * @param <T> c
	 * @param a a
	 * @return b
	 */
	@Override
	public <T> T[] toArray(final T[] a) {
		return this.inner.toArray(a);
	}

}
