package net.kezvh.collections.wrappers;

import java.util.Collection;
import java.util.Set;

/**
 * @author mjacob
 *
 * @param <E> COMMENT
 * @param <C> COMMENT
 */
public class CollectionAsSet<E, C extends Collection<E>> extends WrappedCollection<E, C> implements Set<E> {
	/**
	 * @param innerCollection COMMENT
	 */
	public CollectionAsSet(final C innerCollection) {
		super(innerCollection);
	}
}
