/**
 *
 */
package net.kezvh.collections.wrappers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * @author afflux
 * @param <E> crunchy
 * @param <C> taco
 */
public abstract class WrappedList<E, C extends List<E>> extends WrappedCollection<E, C> implements List<E> {

	/**
	 * @param inner inner collection
	 */
	public WrappedList(final C inner) {
		super(inner);
	}

	/**
	 * @see java.util.List#add(int, java.lang.Object)
	 * @param index index
	 * @param element element to insert at index
	 */
	@Override
	public void add(final int index, final E element) {
		this.getInner().add(index, element);
	}

	/**
	 * @see java.util.List#addAll(int, java.util.Collection)
	 * @param index index
	 * @param c collectino to insert @ index
	 * @return true if .. see the see
	 */
	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		return this.getInner().addAll(index, c);
	}

	/**
	 * @see java.util.List#get(int)
	 * @param index index
	 * @return element @ index
	 */
	@Override
	public E get(final int index) {
		return this.getInner().get(index);
	}

	/**
	 * @see java.util.List#indexOf(java.lang.Object)
	 * @param o object
	 * @return index of object
	 */
	@Override
	public int indexOf(final Object o) {
		return this.getInner().indexOf(o);
	}

	/**
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 * @param o lastIndexOf
	 * @return blah blah
	 */
	@Override
	public int lastIndexOf(final Object o) {
		return this.getInner().lastIndexOf(o);
	}

	/**
	 * @see java.util.List#listIterator()
	 * @return list iterator!
	 */
	@Override
	public ListIterator<E> listIterator() {
		return this.getInner().listIterator();
	}

	/**
	 * @see java.util.List#listIterator(int)
	 * @param index start index
	 * @return list iterator
	 */
	@Override
	public ListIterator<E> listIterator(final int index) {
		return this.getInner().listIterator(index);
	}

	/**
	 * @see java.util.List#remove(int)
	 * @param index index
	 * @return element removed
	 */
	@Override
	public E remove(final int index) {
		return this.getInner().remove(index);
	}

	/**
	 * @see java.util.List#set(int, java.lang.Object)
	 * @param index index
	 * @param element element to set
	 * @return old value
	 */
	@Override
	public E set(final int index, final E element) {
		return this.getInner().set(index, element);
	}

	/**
	 * you probably want to override this because it will throw a runtime exception if it can't construct a wrapper for the
	 *
	 * @see java.util.List#subList(int, int)
	 * @param fromIndex from index
	 * @param toIndex to index
	 * @return sub list (backed by inner foo
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<E> subList(final int fromIndex, final int toIndex) {
		try {
			for (final Constructor<?> c : this.getClass().getConstructors())
				if (c.getParameterTypes().length == 1 && this.getInner().getClass().isAssignableFrom(c.getParameterTypes()[0]))
					return (List<E>) c.newInstance(this.getInner().subList(fromIndex, toIndex));
		} catch (final SecurityException e) {
			throw new RuntimeException(e);
		} catch (final IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (final InstantiationException e) {
			throw new RuntimeException(e);
		} catch (final IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (final InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		throw new RuntimeException();
	}

}
