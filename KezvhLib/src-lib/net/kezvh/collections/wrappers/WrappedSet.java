package net.kezvh.collections.wrappers;

import java.util.Set;

/**
 * @author mjacob
 *
 * @param <E> COMMENT
 * @param <C> COMMENT
 */
public class WrappedSet<E, C extends Set<E>> extends WrappedCollection<E, C> implements Set<E> {
	/**
	 * @param innerSet COMMENT
	 */
	public WrappedSet(final C innerSet) {
		super(innerSet);
	}

}
