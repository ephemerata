package net.kezvh.collections;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * @author mjacob
 *
 * @param <T> FIXME comment
 */
public abstract class AbstractListIterator<T> implements ListIterator<T> {
	private int nextIndex;
	private T nextElement = null;
	private T previousElement = null;
	private boolean atEnd;
	private boolean atBeginning;

	private boolean foundNext0 = false;
	private boolean foundPrevious0 = false;

	private boolean lastStepWasPrevious = false;
	private boolean lastStepWasNext = false;

	/**
	 * @param startingIndex FIXME comment
	 */
	protected AbstractListIterator(final int startingIndex) {
		this.nextIndex = startingIndex;
	}

	/**
	 * @return FIXME comment
	 * @throws NoSuchElementException FIXME comment
	 */
	protected abstract T findPrevious() throws NoSuchElementException;

	/**
	 * @return FIXME comment
	 * @throws NoSuchElementException FIXME comment
	 */
	protected abstract T findNext() throws NoSuchElementException;

	private void findPrevious0() {
		this.foundPrevious0 = true;
		try {
			this.previousElement = this.findPrevious();
		} catch (final NoSuchElementException e) {
			this.previousElement = null;
			this.atBeginning = true;
		}
	}

	private void findNext0() {
		this.foundNext0 = true;
		try {
			this.nextElement = this.findNext();
		} catch (final NoSuchElementException e) {
			this.nextElement = null;
			this.atEnd = true;
		}
	}

	@Override
	public boolean hasPrevious() {
		if (!this.foundPrevious0)
			this.findPrevious0();

		return !this.atBeginning;
	}

	@Override
	public int nextIndex() {
		if (!this.foundNext0)
			this.findNext0();

		return this.nextIndex;
	}

	@Override
	public T previous() {
		if (!this.foundPrevious0)
			this.findPrevious0();

		if (!this.atBeginning)
			try {
				return this.previousElement;
			} finally {
				this.nextIndex--;
				this.nextElement = this.previousElement;
				this.foundNext0 = true;
				try {
					this.previousElement = this.findPrevious();
					this.lastStepWasPrevious = true;
					this.lastStepWasNext = false;
				} catch (final NoSuchElementException e) {
					this.previousElement = null;
					this.atBeginning = true;
				}
			}
		throw new NoSuchElementException();
	}

	@Override
	public int previousIndex() {
		if (!this.foundPrevious0)
			this.findPrevious0();

		return this.nextIndex - 1;
	}

	@Override
	public boolean hasNext() {
		if (!this.foundNext0)
			this.findNext0();

		return !this.atEnd;
	}

	@Override
	public T next() {
		if (!this.foundNext0)
			this.findNext0();

		if (!this.atEnd)
			try {
				return this.nextElement;
			} finally {
				this.nextIndex++;
				this.previousElement = this.nextElement;
				this.foundPrevious0 = true;
				try {
					this.nextElement = this.findNext();
					this.lastStepWasNext = true;
					this.lastStepWasPrevious = true;
				} catch (final NoSuchElementException e) {
					this.nextElement = null;
					this.atEnd = true;
				}
			}
		throw new NoSuchElementException();
	}

	/**
	 * @param e FIXME comment
	 */
	protected abstract void add0(T e);

	public void add(final T e) {
		this.previousElement = e;
		this.foundPrevious0 = true;
		this.nextIndex++;
		this.add0(e);
	}

	/**
	 * @param previous FIXME comment
	 */
	protected abstract void remove(boolean previous);

	@Override
	public void remove() {
		if (this.lastStepWasNext) {
			this.previousElement = null;
			this.foundPrevious0 = false; // will need to find another element
			this.nextIndex--;
			this.lastStepWasNext = false;
			this.remove(false);
		} else if (this.lastStepWasPrevious) {
			this.nextElement = null;
			this.foundNext0 = false; // will need to find another element
			this.lastStepWasPrevious = false;
			this.remove(true);
		} else
			throw new IllegalStateException();
	}

	/**
	 * @param e  FIXME comment
	 * @param previous FIXME comment
	 */
	protected abstract void set(T e, boolean previous);

	public void set(final T e) {
		if (this.lastStepWasNext) {
			this.previousElement = e;
			this.foundPrevious0 = true;
			this.set(e, false);
		} else if (this.lastStepWasPrevious) {
			this.nextElement = e;
			this.foundNext0 = true;
			this.set(e, true);
		} else
			throw new IllegalStateException();
	}
}
