package net.kezvh.collections;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import net.kezvh.functional.lambda.EquivalenceRelation;
import net.kezvh.functional.lambda.L1;
import net.kezvh.functional.lambda.Predicate1;
import net.kezvh.math.KezvhMath;
import net.kezvh.patterns.AbstractFactory;
import net.kezvh.patterns.AbstractFactory.CreationFailedException;

/**
 * @author afflux
 */
public final class CollectionsUtilities {
	private static final String COULD_NOT_CONSTRUCT_DEFAULT_VALUE = "Could not construct default value";

	private static final class ConditionalIterator<T> implements Iterator<T> {
		private final Iterator<T> iterator;
		private final Predicate1<T> condition;
		private boolean notEmpty = false;
		private T current;

		public ConditionalIterator(final Iterator<T> iterator, final Predicate1<T> condition) {
			this.iterator = iterator;
			this.condition = condition;
			this.advance();
		}

		private void advance() {
			this.notEmpty = false;
			while (this.iterator.hasNext() && !this.notEmpty) {
				this.current = this.iterator.next();
				this.notEmpty = this.condition.op(this.current);
			}
		}

		public boolean hasNext() {
			return this.notEmpty;
		}

		public T next() {
			if (!this.hasNext())
				throw new NoSuchElementException();

			try {
				return this.current;
			} finally {
				this.advance();
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private static final class ConditionalSet<T> extends AbstractSet<T> {
		private final Set<T> set;
		private final Predicate1<T> condition;

		public ConditionalSet(final Set<T> set, final Predicate1<T> condition) {
			this.set = set;
			this.condition = condition;
		}

		@Override
		public int size() {
			return CollectionsUtilities.count(this.iterator());
		}

		@Override
		public Iterator<T> iterator() {
			return new ConditionalIterator<T>(this.set.iterator(), this.condition);
		}
	}

	private static class ConcatenatedIterator<T> implements Iterator<T> {
		private final Iterator<T>[] iterators;
		private int index = 0;

		public ConcatenatedIterator(final Iterator<T>[] iterators) {
			this.iterators = Arrays.copyOf(iterators, iterators.length);
		}

		public boolean hasNext() {
			return this.index != this.iterators.length && this.iterators[this.index].hasNext();
		}

		public T next() {
			while (!this.iterators[this.index].hasNext()) {
				this.index++;

				if (this.index == this.iterators.length)
					throw new NoSuchElementException();
			}

			return this.iterators[this.index].next();
		}

		public void remove() {
			this.iterators[this.index].remove();
		}
	}

	/**
	 * @param <T> COMMENT
	 * @param iterator COMMENT
	 * @return COMMENT
	 */
	public static <T> int count(final Iterator<T> iterator) {
		int count = 0;
		while (iterator.hasNext()) {
			iterator.next();
			count++;
		}
		return count;
	}

	/**
	 * @param <T> COMMENT
	 * @param start COMMENT
	 * @param end COMMENT
	 * @param incrementer COMMENT
	 * @return COMMENT
	 */
	public static <T extends Comparable<T>> List<T> range(final T start, final T end, final L1<T, T> incrementer) {
		final List<T> range = new ArrayList<T>();
		T current = start;
		while (current.compareTo(end) < 0) {
			range.add(current);
			current = incrementer.op(current);
		}
		return Collections.unmodifiableList(range);
	}

	/**
	 * @param <T> COMMENT
	 * @param iterators COMMENT
	 * @return COMMENT
	 */
	public static <T> Iterator<T> concatenate(final Iterator<T>[] iterators) {
		return new ConcatenatedIterator<T>(iterators);
	}

	/**
	 * @param <T> COMMENT
	 * @param iterators COMMENT
	 * @return COMMENT
	 */
	public static <T> Iterator<T> concatenate(final Collection<Iterator<T>> iterators) {
		final Iterator<T>[] array = (Iterator<T>[]) Array.newInstance(iterators.iterator().next().getClass(), iterators.size());
		return CollectionsUtilities.concatenate(array);
	}

	/**
	 * @param <T> COMMENT
	 * @param c COMMENT
	 * @param toAdd COMMENT
	 * @return COMMENT
	 */
	public static <T> boolean addAll(final Collection<T> c, final T[] toAdd) {
		return c.addAll(Arrays.asList(toAdd));
	}

	/**
	 * @param <T> COMMENT
	 * @param collection COMMENT
	 * @param condition COMMENT
	 * @return COMMENT
	 */
	// //////////////////////////////////////
	public static <T> List<T> match(final List<T> collection, final Predicate1<T> condition) {
		List<T> matches;
		try {
			matches = collection.getClass().newInstance();
		} catch (final Exception e) {
			matches = new ArrayList<T>();
		}
		for (final T o : collection)
			if (condition.op(o))
				matches.add(o);

		return matches;
	}

	/**
	 * @param <T> COMMENT
	 * @param iterator COMMENT
	 * @param condition COMMENT
	 * @return COMMENT
	 */
	public static <T> Iterator<T> matchView(final Iterator<T> iterator, final Predicate1<T> condition) {
		return new ConditionalIterator<T>(iterator, condition);
	}

	/**
	 * @param <T> COMMENT
	 * @param set COMMENT
	 * @param condition COMMENT
	 * @return COMMENT
	 */
	public static <T> Set<T> matchView(final Set<T> set, final Predicate1<T> condition) {
		return new ConditionalSet<T>(set, condition);
	}

	// //////////
	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param key COMMENT
	 * @param defaultValueClass COMMENT
	 * @return COMMENT
	 * @throws InstantiationException COMMENT
	 * @throws IllegalAccessException COMMENT
	 */
	public static <K, V> V ensureContainsKey(final Map<K, V> map, final K key, final Class<V> defaultValueClass) throws InstantiationException, IllegalAccessException {
		if (map.containsKey(key))
			return map.get(key);
		V defaultValue;

		defaultValue = defaultValueClass.newInstance();

		map.put(key, defaultValue);
		return defaultValue;
	}

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param key COMMENT
	 * @param clonableDefaultValue COMMENT
	 * @return COMMENT
	 * @throws NoSuchMethodException COMMENT
	 * @throws InvocationTargetException COMMENT
	 * @throws IllegalAccessException COMMENT
	 */
	public static <K, V> V ensureContainsKey(final Map<K, V> map, final K key, final Cloneable clonableDefaultValue) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		if (map.containsKey(key))
			return map.get(key);
		V defaultValue;

		final Method cloneMethod = clonableDefaultValue.getClass().getMethod("clone", null);
		defaultValue = (V) cloneMethod.invoke(clonableDefaultValue, null);

		map.put(key, defaultValue);
		return defaultValue;
	}

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param key COMMENT
	 * @param defaultValueConstructor COMMENT
	 * @return COMMENT
	 * @throws CreationFailedException  COMMENT
	 */
	public static <K, V> V ensureContainsKey(final Map<K, V> map, final K key, final AbstractFactory<V> defaultValueConstructor) throws CreationFailedException {
		if (map.containsKey(key))
			return map.get(key);
		final V defaultValue = defaultValueConstructor.create();

		map.put(key, defaultValue);
		return defaultValue;
	}

	// ///////////////////////////////////////

	/**
	 * @param <V> COMMENT
	 * @param c COMMENT
	 * @param o COMMENT
	 * @param e COMMENT
	 * @return COMMENT
	 */
	public static <V> boolean removeFirst(final Collection<V> c, final V o, final EquivalenceRelation<V> e) {
		for (final Iterator<V> i = c.iterator(); i.hasNext();)
			if (e.op(o, i.next())) {
				i.remove();
				return true;
			}
		return false;
	}

	/**
	 * @param <V> COMMENT
	 * @param c COMMENT
	 * @param o COMMENT
	 * @param e COMMENT
	 * @return COMMENT
	 */
	public static <V> boolean removeAll(final Collection<V> c, final V o, final EquivalenceRelation<V> e) {
		boolean removed = false;

		for (final Iterator<V> i = c.iterator(); i.hasNext();)
			if (e.op(o, i.next())) {
				i.remove();
				removed = true;
			}

		return removed;
	}

	/**
	 * @param <V> COMMENT
	 * @param l COMMENT
	 * @param o COMMENT
	 * @param e COMMENT
	 * @return COMMENT
	 */
	public static <V> boolean replaceFirst(final List<V> l, final V o, final EquivalenceRelation<V> e) {
		for (int i = 0; i < l.size(); i++)
			if (e.op(o, l.get(i))) {
				l.set(i, o);
				return true;
			}

		return false;
	}

	// ///////////////////////////////////////

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param keys COMMENT
	 * @return COMMENT
	 */
	public static <K, V> V[] slice(final Map<K, V> map, final K... keys) {
		final V[] values = (V[]) Array.newInstance(map.get(keys[0]).getClass(), keys.length);
		for (int i = 0; i < keys.length; i++)
			values[i] = map.get(keys[i]);
		return values;
	}

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param keys COMMENT
	 * @return COMMENT
	 */
	public static <K, V> List<V> slice(final Map<K, V> map, final List<K> keys) {
		List<V> values;
		try {
			values = keys.getClass().newInstance();
		} catch (final InstantiationException e) {
			values = new ArrayList<V>();
		} catch (final IllegalAccessException e) {
			values = new ArrayList<V>();
		}

		for (int i = 0; i < keys.size(); i++)
			values.add(map.get(keys.get(i)));
		return values;
	}

	// //////////////////////////////////////////////

	/**
	 * @param <V> COMMENT
	 * @param comparator COMMENT
	 * @return COMMENT
	 */
	public static <V> Comparator<V> reverse(final Comparator<V> comparator) {
		return new Comparator<V>() {
			public int compare(final V arg0, final V arg1) {
				return comparator.compare(arg1, arg0);
			}
		};
	}

	// ////////////////////////////////////////////////

	/**
	 * @param <V> COMMENT
	 * @param a COMMENT
	 * @param b COMMENT
	 * @return COMMENT
	 */
	public static <V> boolean sameContents(final Collection<V> a, final Collection<V> b) {
		final Multiset<V> x = new MappedMultiset<V>(a);
		final Multiset<V> y = new MappedMultiset<V>(b);
		return x.equals(y);
	}

	// ////////////////////////////////////////////////

	private static final class ComparableComarator<V extends Comparable<V>> implements Comparator<V> {
		public int compare(final V o1, final V o2) {
			return o1.compareTo(o2);
		}
	}

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @return COMMENT
	 */
	public static <K, V extends Comparable<V>> SortedSet<K> keySetByValues(final Map<K, V> map) {
		return CollectionsUtilities.keySetByValues(map, new ComparableComarator<V>());
	}

	@SuppressWarnings("unchecked")
	private static <T> int finagleComparison(final T arg0, final T arg1) {
		if (arg0 instanceof Comparable && arg1 instanceof Comparable)
			try {
				return ((Comparable<T>) arg0).compareTo(arg1);
			} catch (final ClassCastException e) {
				try {
					return -((Comparable<T>) arg1).compareTo(arg0);
				} catch (final ClassCastException f) {
					// fall through
				}
			}

		final int hashCompare = KezvhMath.compare(arg0.hashCode(), arg1.hashCode()); // usually different if objects are not
		// equal
		if (hashCompare != 0)
			return hashCompare;

		return KezvhMath.compare(System.identityHashCode(arg0), System.identityHashCode(arg1)); // kind of a last ditch effort,
		// which should *usually* return
		// different values for objects
		// which are not equal. otw, yur screwed
	}

	/**
	 * @param <K> COMMENT
	 * @param <V> COMMENT
	 * @param map COMMENT
	 * @param c COMMENT
	 * @return COMMENT
	 */
	public static <K, V> SortedSet<K> keySetByValues(final Map<K, V> map, final Comparator<V> c) {
		final Comparator<K> orderByValue = new Comparator<K>() {
			public int compare(final K arg0, final K arg1) {
				final boolean has0 = map.containsKey(arg0);
				final boolean has1 = map.containsKey(arg1);
				if (!has0 || !has1)
					return KezvhMath.compare(has0, has1);

				int cmp = c.compare(map.get(arg0), map.get(arg1));
				if (cmp == 0 && !arg0.equals(arg1)) { // distinct keys map to equal objects
					cmp = CollectionsUtilities.finagleComparison(arg0, arg1); // we do this to try to keep two separate keys from
					// mapping to the same index of the sorted set
					if (cmp == 0) // this is bad - no way to order things...
						throw new IllegalArgumentException("cannot figure out how to order two equal objects: " + arg0 + ", " + arg1);
				}
				return cmp;
			}
		};
		final SortedSet<K> keySet = new TreeSet<K>(orderByValue);
		keySet.addAll(map.keySet());
		return keySet;
	}

	// ///////////////////////////////////////////////

	/**
	 * @param <T>  COMMENT
	 * @param c COMMENT
	 * @return COMMENT
	 */
	public static <T> int hash(final Iterable<T> c) {
		if (c == null)
			return 0;

		int result = 1;

		for (final T element : c)
			result = 31 * result + (element == null ? 0 : element.hashCode());

		return result;
	}
}
