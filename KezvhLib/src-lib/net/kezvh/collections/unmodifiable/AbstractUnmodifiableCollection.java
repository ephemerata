package net.kezvh.collections.unmodifiable;

import java.util.AbstractCollection;
import java.util.Collection;

/**
 * @author afflux
 * @param <E> FIXME comment
 */
public abstract class AbstractUnmodifiableCollection<E> extends AbstractCollection<E> {

	/**
	 * @see java.util.Collection#clear()
	 */
	@Override
	public final void clear() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.Collection#remove(java.lang.Object)
	 * @param o FIXME comment
	 * @return b
	 */
	@Override
	public final boolean remove(final Object o) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.Collection#removeAll(java.util.Collection)
	 * @param c FIXME comment
	 * @return b
	 */
	@Override
	public final boolean removeAll(final Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.Collection#retainAll(java.util.Collection)
	 * @param c FIXME comment
	 * @return b
	 */
	@Override
	public final boolean retainAll(final Collection<?> c) {
		throw new UnsupportedOperationException();
	}
}
