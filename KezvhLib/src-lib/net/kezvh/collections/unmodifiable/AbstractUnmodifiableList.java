package net.kezvh.collections.unmodifiable;

import java.util.Collection;
import java.util.List;

/**
 * @author mjacob
 *
 * @param <E> COMMENT
 */
public abstract class AbstractUnmodifiableList<E> extends AbstractUnmodifiableCollection<E> implements List<E> {

	@Override
	public void add(final int index, final E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E remove(final int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E set(final int index, final E element) {
		throw new UnsupportedOperationException();
	}
}
