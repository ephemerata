package net.kezvh.collections.unmodifiable;

import java.util.SortedSet;

/**
 * @author mjacob
 *
 * @param <E> COMMENT
 */
public abstract class AbstractUnmodifiableSortedList<E> extends AbstractUnmodifiableList<E> implements SortedSet<E> {
	// joined
}
