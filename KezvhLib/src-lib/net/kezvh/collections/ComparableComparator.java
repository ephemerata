package net.kezvh.collections;

import java.util.Comparator;

/**
 * @author mjacob
 *
 * Useful when we can assume our parameter is comparable
 *
 * @param <T> comparable type
 */
public class ComparableComparator<T> implements Comparator<T> {

	@SuppressWarnings("unchecked")
	@Override
	public int compare(final T o1, final T o2) {
		return ((Comparable<T>) o1).compareTo(o2);
	}

}
