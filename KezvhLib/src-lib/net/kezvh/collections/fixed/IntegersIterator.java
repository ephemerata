package net.kezvh.collections.fixed;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author mjacob
 *
 */
public class IntegersIterator implements Iterator<Integer>, Cloneable {
	private int current;
	private final int increment;
	private final boolean allowOverflow;
	private boolean overflowed = false;

	/**
	 *
	 */
	public IntegersIterator() {
		this(0, true);
	}

	/**
	 * @param startingIndex startingIndex
	 */
	public IntegersIterator(final int startingIndex) {
		this(startingIndex, true);
	}

	/**
	 * @param startingIndex startingIndex
	 * @param canOverflow canOverflow
	 */
	public IntegersIterator(final int startingIndex, final boolean canOverflow) {
		this(startingIndex, canOverflow, 1);
	}

	/**
	 * @param startingIndex startingIndex
	 * @param canOverflow canOverflow
	 * @param increment increment
	 */
	public IntegersIterator(final int startingIndex, final boolean canOverflow, final int increment) {
		if (increment == 0)
			throw new IllegalArgumentException("cannot create an iterator which does not change ");
		this.current = startingIndex;
		this.allowOverflow = canOverflow;
		this.increment = increment;
	}

	public boolean hasNext() {
		return this.allowOverflow || !this.overflowed;
	}

	/**
	 * @return true if overflow is allowed
	 */
	public boolean canOverflow() {
		return this.allowOverflow;
	}

	public Integer next() {
		if (this.hasNext()) {
			final int nextValue = this.current += this.increment;
			if ((this.increment > 0 && (nextValue == Integer.MAX_VALUE || nextValue < this.current)) || (this.increment < 0 && (nextValue == Integer.MIN_VALUE || nextValue > this.current)))
				this.overflowed = true;
			return this.current += this.increment;
		}
		throw new NoSuchElementException(Integer.toString(this.current));
	}

	public void remove() {
		throw new UnsupportedOperationException("cannot remove from IntegerIterator.");
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
