package net.kezvh.collections.fixed;

import java.util.AbstractCollection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;

/**
 * @author mjacob
 *
 */
public class Integers extends AbstractCollection<Integer> implements NavigableSet<Integer> {
	@Override
	public Iterator<Integer> iterator() {
		return new IntegersIterator(Integer.MIN_VALUE, false);
	}

	@Override
	public int size() {
		return -1;
		//		throw new UnsupportedOperationException("cannot represent size of the integers as an integer");
	}

	@Override
	public Integer ceiling(final Integer e) {
		if (Integer.MAX_VALUE == e)
			return Integer.MAX_VALUE;
		return e + 1;
	}

	@Override
	public Iterator<Integer> descendingIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableSet<Integer> descendingSet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer floor(final Integer e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<Integer> headSet(final Integer toElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableSet<Integer> headSet(final Integer toElement, final boolean inclusive) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer higher(final Integer e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer lower(final Integer e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer pollFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer pollLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<Integer> subSet(final Integer fromElement, final Integer toElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableSet<Integer> subSet(final Integer fromElement, final boolean fromInclusive, final Integer toElement, final boolean toInclusive) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SortedSet<Integer> tailSet(final Integer fromElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableSet<Integer> tailSet(final Integer fromElement, final boolean inclusive) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Comparator<? super Integer> comparator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer first() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer last() {
		// TODO Auto-generated method stub
		return null;
	}

}
