package net.kezvh.collections;

import java.util.Comparator;

import net.kezvh.development.UnimplementedException;
import net.kezvh.lang.UtilityClass;

/**
 * TODO handle next(LAST) and previous(FIRST)
 *
 * @author mjacob
 *
 * @param <T> type to navigate
 */
public interface Navigator<T> extends Comparator<T> {
	/**
	 * @author mjacob
	 *
	 */
	public final class Utilities extends UtilityClass {
		/**
		 * @param <T> COMMENT
		 * @param clazz COMMENT
		 * @return COMMENT
		 */
		@SuppressWarnings("unchecked")
		public static <T> Navigator<T> getNumberNavigator(final Class<?> clazz) {
			if (clazz == Integer.class || clazz == int.class)
				return (Navigator<T>) Navigator.INTEGER_NAVIGATOR;
			return null;
		}
	}

	/**
	 * @author mjacob
	 *
	 * @param <T> type to navigate
	 */
	abstract class ComparableNavigator<T extends Comparable<T>> extends ComparableComparator<T> implements Navigator<T> {
		// no need for applause
	}

	/**
	 * @param current
	 * @return the element after the current element
	 */
	T next(T current);

	/**
	 * @param current
	 * @return the element before the current element
	 */
	T previous(T current);

	/**
	 * FIXME
	 * need a better understanding of IEEE floating point. Botched this the first time.
	 */
	Navigator<Double> DOUBLE_NAVIGATOR = new ComparableNavigator<Double>() {
		@Override
		public Double next(final Double arg0) {

			if (Double.isNaN(arg0))
				return Double.NaN;
			else if (Double.isInfinite(arg0)) {
				if (arg0 > 0)
					return Double.POSITIVE_INFINITY;
				return Double.NaN;
			} else if (arg0 > 0) // 0 xxxxxxxx yyyyyyyyyyyyyyyyyyyyyyy
				throw new UnimplementedException();
			else
				throw new UnimplementedException();

			// FIXME this can't be right
			//			return Double.longBitsToDouble(Double.doubleToLongBits(arg0) + 1);
		}

		@Override
		public Double previous(final Double arg0) {
			throw new UnimplementedException();
			// FIXME this can't be right
			//			return Double.longBitsToDouble(Double.doubleToLongBits(arg0) - 1);
		}
	};

	/**
	 *
	 */
	Navigator<Integer> INTEGER_NAVIGATOR = new ComparableNavigator<Integer>() {
		@Override
		public Integer next(final Integer arg0) {
			return arg0 + 1;
		}

		@Override
		public Integer previous(final Integer arg0) {
			return arg0 - 1;
		}
	};

	/**
	 *
	 */
	Navigator<Character> CHARACTER_NAVIGATOR = new ComparableNavigator<Character>() {
		@Override
		public Character next(final Character arg0) {
			return (char) (((int) arg0) + 1);
		}

		@Override
		public Character previous(final Character arg0) {
			return (char) (((int) arg0) - 1);
		}
	};
}
