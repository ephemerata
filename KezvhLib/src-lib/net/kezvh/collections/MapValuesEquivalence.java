package net.kezvh.collections;

import java.util.Map;
import java.util.Map.Entry;

import net.kezvh.functional.lambda.EquivalenceRelation;

/**
 * @author afflux
 * @param <K> FIXME comment
 * @param <V> FIXME comment
 */
public final class MapValuesEquivalence<K, V> implements EquivalenceRelation<Map<K, V>> {
	/**
	 *
	 */
	public static final MapValuesEquivalence<?, ?> VALUES_EQUAL_EQUIVALATOR = new MapValuesEquivalence<Object, Object>();

	private final EquivalenceRelation<V> valuesEquivalator;

	/**
	 *
	 */
	@SuppressWarnings("unchecked")
	public MapValuesEquivalence() {
		this((EquivalenceRelation<V>) EquivalenceRelation.EQUALS);
	}

	/**
	 * @param valuesEquivalator FIXME comment
	 */
	public MapValuesEquivalence(final EquivalenceRelation<V> valuesEquivalator) {
		this.valuesEquivalator = valuesEquivalator;
	}

	/**
	 * @param param1 FIXME comment
	 * @param param2 FIXME comment
	 * @return x
	 */
	public Boolean op(final Map<K, V> param1, final Map<K, V> param2) {
		if (param1 == null)
			return param2 == null;
		if (param2 == null)
			return false;

		if (!param1.keySet().equals(param2.keySet()))
			return false;

		return this.valuesEqual(param1, param2);
	}

	private boolean valuesEqual(final Map<K, V> m1, final Map<K, V> m2) {
		for (final Entry<K, V> entry : m1.entrySet()) {
			final K key = entry.getKey();
			final V value1 = entry.getValue();
			final V value2 = m2.get(key);

			if (!this.valuesEquivalator.op(value1, value2))
				return false;
		}

		return true;
	}
}
