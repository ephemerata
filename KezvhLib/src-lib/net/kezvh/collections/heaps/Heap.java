package net.kezvh.collections.heaps;

import java.util.Comparator;
import java.util.Map;

import net.kezvh.collections.priorityqueues.PriorityQueue;

/**
 * @author mjacob
 *
 * @param <E> element type
 * @param <W> element weight
 */
public interface Heap<E, W> extends PriorityQueue<E, W>, Map<E, W> {
	/**
	 * @param t
	 * @param newWeight
	 * @return the old weight
	 */
	W reweight(E t, W newWeight);

	/**
	 * This will generally destroy the other heap, unless the implementation specifies otherwise.
	 *
	 * @param other
	 */
	void merge(Heap<E, W> other);

	/**
	 * Returns the comparator used to order the elements in this set,
	 * or <tt>null</tt> if this set uses the {@linkplain Comparable
	 * natural ordering} of its elements.
	 *
	 * @return the comparator used to order the elements in this set,
	 *         or <tt>null</tt> if this set uses the natural ordering
	 *         of its elements
	 */
	Comparator<? super W> comparator();
}
