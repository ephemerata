package net.kezvh.collections.heaps;

import java.util.AbstractMap;

/**
 * @author mjacob
 *
 * @param <E> COMMENT
 * @param <W> COMMENT
 */
public abstract class AbstractHeap<E, W> extends AbstractMap<E, W> implements Heap<E, W> {
	//
}
