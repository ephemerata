package net.kezvh.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author afflux
 * @param <T> FIXME comment
 */
public class MappedMultiset<T> implements Multiset<T> {
	private final MapValuesEquivalence<T, Integer> MAP_VALUES_EQUIVALATOR = new MapValuesEquivalence<T, Integer>();

	private final Map<T, Integer> map = new HashMap<T, Integer>();

	/**
	 *
	 */
	public MappedMultiset() {
		super();
	}

	/**
	 * @param c FIXME comment
	 */
	public MappedMultiset(final Collection<T> c) {
		super();
		this.addAllForCreate(c);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @param obj FIXME comment
	 * @return x
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass()))
			return false;

		final MappedMultiset<T> other = (MappedMultiset<T>) obj;

		return this.MAP_VALUES_EQUIVALATOR.op(this.map, other.map);
	}

	@Override
	public int hashCode() {
		return this.map.hashCode();
	}

	@SuppressWarnings("unchecked")
	public boolean add(final Object element) {
		if (this.map.containsKey(element)) {
			final Integer count = this.map.get(element);

			final Integer newCount = Integer.valueOf(count.intValue() + 1);

			this.map.put((T) element, newCount);
			return true;
		}
		this.map.put((T) element, Integer.valueOf(1));
		return false;
	}

	@SuppressWarnings("unchecked")
	private final void addForCreate(final Object element) {
		if (this.map.containsKey(element)) {
			final Integer count = this.map.get(element);

			final Integer newCount = Integer.valueOf(count.intValue() + 1);

			this.map.put((T) element, newCount);
		} else
			this.map.put((T) element, Integer.valueOf(1));
	}

	@SuppressWarnings("unchecked")
	public boolean remove(final Object element) {
		if (this.map.containsKey(element)) {
			final int count = this.map.get(element);
			if (count == 1) {
				this.map.remove(element);
				return true;
			}

			final int newCount = count - 1;

			this.map.put((T) element, newCount);
			return true;
		}
		return false;
	}

	public boolean removeAllMembers(final Object element) {
		return this.map.remove(element) != null;
	}

	public Iterator<T> iterator() {
		return this.map.keySet().iterator();
	}

	/**
	 * @param arg0 FIXME comment
	 * @return FIXME comment
	 */
	public boolean addAll(final Collection<? extends T> arg0) {
		boolean modified = false;
		for (final T element : arg0)
			modified |= this.add(element);
		return modified;
	}

	private final void addAllForCreate(final Collection<? extends T> arg0) {
		for (final T element : arg0)
			this.addForCreate(element);
	}

	public void clear() {
		this.map.clear();
	}

	public boolean contains(final Object o) {
		return this.map.containsKey(o);
	}

	public boolean containsAll(final Collection<?> arg0) {
		boolean containsAll = true;
		for (final Object element : arg0)
			containsAll &= this.map.containsKey(element);
		return containsAll;
	}

	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	public boolean removeAll(final Collection<?> arg0) {
		boolean modified = false;
		for (final Object element : arg0)
			modified |= this.remove(element);
		return modified;
	}

	public boolean retainAll(final Collection<?> arg0) {
		boolean modified = false;

		for (final Object element : arg0)
			if (!arg0.contains(element))
				modified |= this.removeAllMembers(element);

		return modified;
	}

	public int size() {
		return this.map.size();
	}

	public Object[] toArray() {
		return this.map.keySet().toArray();
	}

	public <V> V[] toArray(final V[] arg0) {
		return this.map.keySet().toArray(arg0);
	}

	public int getMembership(final Object element) {
		if (this.map.containsKey(element))
			return (this.map.get(element)).intValue();
		return 0;
	}

	public int setMembership(final T element, final int multiplicity) {
		if (multiplicity < 0)
			throw new IllegalArgumentException("cannot have negative multiplicity (" + multiplicity + ") " + element);
		final int previousMultiplicity = this.getMembership(element);

		if (multiplicity == 0)
			this.map.remove(element);
		else
			this.map.put(element, Integer.valueOf(multiplicity));

		return previousMultiplicity;
	}

}
