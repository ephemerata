package net.kezvh.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.SortedSet;

import net.kezvh.collections.unmodifiable.AbstractUnmodifiableList;
import net.kezvh.development.UnimplementedException;

/**
 * @author afflux
 * @param <E> FIXME commment
 */
public class Range<E> extends AbstractUnmodifiableList<E> implements NavigableSet<E> {

	private final class RangeIterator implements ListIterator<E> {
		private int index;
		private E current;

		public RangeIterator() {
			this(0, Range.this.firstOpen ? Range.this.navigator.next(Range.this.first) : Range.this.first);
		}

		public RangeIterator(final int index, final E current) {
			super();
			this.index = index;
			this.current = current;
		}

		@Override
		public void add(final E e) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean hasNext() {
			return Range.this.contains(this.current);
		}

		@Override
		public boolean hasPrevious() {
			return Range.this.contains(Range.this.navigator.previous(this.current));
		}

		@Override
		public E next() {
			if (this.hasNext())
				try {
					return this.current;
				} finally {
					this.index++;
					this.current = Range.this.navigator.next(this.current);
				}
			throw new NoSuchElementException();
		}

		@Override
		public int nextIndex() {
			return this.index;
		}

		@Override
		public E previous() {
			if (this.hasPrevious())
				try {
					return Range.this.navigator.previous(this.current);
				} finally {
					this.index--;
					this.current = Range.this.navigator.previous(this.current);
				}
			throw new NoSuchElementException();
		}

		@Override
		public int previousIndex() {
			return this.index - 1;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void set(final E e) {
			throw new UnsupportedOperationException();
		}
	}

	private final Navigator<E> navigator;
	private final boolean directlyNavigable;

	private final E first;
	private final E last;

	private final boolean firstOpen;
	private final boolean lastOpen;

	/**
	 * @param first first element
	 * @param last last element
	 */
	public Range(final E first, final E last) {
		this(first, last, true, false, new Navigator<E>() {
			@SuppressWarnings("unchecked")
			public int compare(final E o1, final E o2) {
				return ((Comparable<E>) o1).compareTo(o2);
			}

			@SuppressWarnings("unchecked")
			public E next(final E current) {
				return ((Navigable<E>) current).next();
			}

			@SuppressWarnings("unchecked")
			public E previous(final E current) {
				return ((Navigable<E>) current).previous();
			}
		});
	}

	/**
	 * @param first first element
	 * @param last last element
	 * @param firstOpen exclude the first element. ignored if the first element is null.
	 * @param lastOpen exclude the last element. ignored if the first element is null.
	 * @param navigator navigator
	 */
	public Range(final E first, final E last, final boolean firstOpen, final boolean lastOpen, final Navigator<E> navigator) {
		throw new RuntimeException("too buggy to use");
		//		this.first = first;
		//		this.last = last;
		//
		//		if (navigator == null && first instanceof Number)
		//			this.navigator = Navigator.Utilities.getNumberNavigator(first.getClass());
		//		else
		//			this.navigator = navigator;
		//		this.firstOpen = firstOpen;
		//		this.lastOpen = lastOpen;
		//
		//		this.directlyNavigable = navigator != null;
		//
		//		if (first == null || (this.directlyNavigable && !(first instanceof Navigable) && !(first instanceof Number)))
		//			throw new IllegalArgumentException(first + " is not comparable");
		//
		//		if (last == null || (this.directlyNavigable && !(last instanceof Navigable) && !(first instanceof Number)))
		//			throw new IllegalArgumentException(last + " is not comparable");
	}

	/**
	 * @see java.util.SortedSet#comparator()
	 * @return compataror
	 */
	public Comparator<? super E> comparator() {
		return this.navigator;
	}

	/**
	 * @see java.util.Collection#contains(java.lang.Object)
	 * @param o objoct
	 * @return contains
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean contains(final Object o) {
		final int f = this.navigator.compare(this.first, (E) o);
		final int l = this.navigator.compare((E) o, this.last);

		if ((f > 0 && this.firstOpen) || f == 0) {
			if (this.lastOpen)
				return l < 0;
			return l <= 0;
		}
		return false;
	}

	/**
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 * @param c other collection
	 * @return containsAll
	 */
	@Override
	public boolean containsAll(final Collection<?> c) {
		for (final Object o : c)
			if (!this.contains(o))
				return false;
		return true;
	}

	/**
	 * @see java.util.SortedSet#first()
	 * @return first
	 */
	public E first() {
		return this.first;
	}

	/**
	 * @see java.util.SortedSet#headSet(java.lang.Object)
	 * @param toElement COMMENT
	 * @return headset
	 */
	public SortedSet<E> headSet(final E toElement) {
		return new Range<E>(this.first, toElement, this.firstOpen, this.lastOpen, this.navigator);
	}

	/**
	 * @see java.util.Collection#isEmpty()
	 * @return isempty
	 */
	@Override
	public boolean isEmpty() {
		if (this.firstOpen && this.lastOpen)
			return this.navigator.compare(this.navigator.next(this.first), this.last) > 0;
		if (this.firstOpen || this.lastOpen)
			return this.navigator.compare(this.first, this.last) > 0;
		return this.navigator.compare(this.first, this.last) >= 0;
	}

	/**
	 * @see java.util.Collection#iterator()
	 * @return iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return new RangeIterator();
	}

	/**
	 * @see java.util.SortedSet#last()
	 * @return last
	 */
	public E last() {
		return this.last;
	}

	/**
	 * @see java.util.Collection#size()
	 * @return size
	 */
	@Override
	public int size() {
		return CollectionsUtilities.count(this.iterator());
	}

	/**
	 * @see java.util.SortedSet#subSet(java.lang.Object, java.lang.Object)
	 * @param fromElement COMMENT
	 * @param toElement COMMENT
	 * @return subset
	 */
	public SortedSet<E> subSet(final E fromElement, final E toElement) {
		return new Range<E>(fromElement, toElement, this.firstOpen, this.lastOpen, this.navigator);
	}

	/**
	 * @see java.util.SortedSet#tailSet(java.lang.Object)
	 * @param fromElement COMMENT
	 * @return fromelement
	 */
	public SortedSet<E> tailSet(final E fromElement) {
		return new Range<E>(fromElement, this.last, this.firstOpen, this.lastOpen, this.navigator);
	}

	/**
	 * @see java.util.Collection#toArray()
	 * @return toarray
	 */
	@Override
	public Object[] toArray() {
		return new LinkedList<E>(this).toArray();
	}

	/**
	 * @see java.util.Collection#toArray(T[])
	 * @param <T> COMMENT
	 * @param a COMMENT
	 * @return toarray
	 */
	@Override
	public <T> T[] toArray(final T[] a) {
		return new LinkedList<E>(this).toArray(a);
	}

	@Override
	public E ceiling(final E e) {
		final E ceiling = this.higher(e);
		if (ceiling == null && this.contains(e))
			return e;
		return ceiling;
	}

	@Override
	public Iterator<E> descendingIterator() {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public NavigableSet<E> descendingSet() {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public E floor(final E e) {
		final E floor = this.lower(e);
		if (floor == null && this.contains(e))
			return e;
		return floor;
	}

	@Override
	public NavigableSet<E> headSet(final E toElement, final boolean inclusive) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public E higher(final E e) {
		final E higher = this.navigator.next(e);
		if (this.contains(higher))
			return higher;
		return null;
	}

	@Override
	public E lower(final E e) {
		final E lower = this.navigator.previous(e);
		if (this.contains(lower))
			return lower;
		return null;
	}

	@Override
	public E pollFirst() {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public E pollLast() {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public NavigableSet<E> subSet(final E fromElement, final boolean fromInclusive, final E toElement, final boolean toInclusive) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public NavigableSet<E> tailSet(final E fromElement, final boolean inclusive) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public E get(final int index) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public int indexOf(final Object o) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public int lastIndexOf(final Object o) {
		throw new UnimplementedException(); // FIXME
	}

	@Override
	public ListIterator<E> listIterator() {
		return new RangeIterator();
	}

	@Override
	public ListIterator<E> listIterator(final int index) {
		return new RangeIterator(index, this.get(index));
	}

	@Override
	public List<E> subList(final int fromIndex, final int toIndex) {
		return new Range<E>(this.get(fromIndex), this.get(toIndex), false, true, this.navigator);
	}

}
