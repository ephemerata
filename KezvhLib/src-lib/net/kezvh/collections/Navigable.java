package net.kezvh.collections;

/**
 * @author mjacob
 *
 * @param <T> type of
 */
public interface Navigable<T> extends Comparable<T> {
	/**
	 * @return the element after the current
	 */
	T next();

	/**
	 * @return the element before the current
	 */
	T previous();
}
