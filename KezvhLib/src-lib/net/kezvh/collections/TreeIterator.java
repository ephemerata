/**
 *
 */
package net.kezvh.collections;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author afflux
 *
 * @param <T> the type of the tree node
 */
public class TreeIterator<T> implements Iterator<T> {
	/**
	 * @author afflux
	 *
	 * @param <T> the type of the tree node
	 */
	public interface ChildrenAccessor<T> {
		/**
		 * @param node
		 * @return the children of the node
		 */
		Iterator<T> getChildren(T node);
	}

	private final ChildrenAccessor<T> childrenAccessor;

	private final LinkedList<Iterator<T>> iteratorStack = new LinkedList<Iterator<T>>();

	/**
	 * @param rootNode COMMENT
	 * @param childrenAccessor COMMENT
	 */
	public TreeIterator(final T rootNode, final ChildrenAccessor<T> childrenAccessor) {
		this.childrenAccessor = childrenAccessor;
		this.iteratorStack.add(Collections.singleton(rootNode).iterator());
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 * @return x
	 */
	@Override
	public boolean hasNext() {
		for (final Iterator<T> iterator : this.iteratorStack)
			if (iterator.hasNext())
				return true;
		return false;
	}

	/**
	 * @see java.util.Iterator#next()
	 * @return x
	 */
	@Override
	public T next() {
		while (!this.last().hasNext())
			this.iteratorStack.removeLast(); // will throw a NSEE if hasNext() is false
		final T nextNode = this.last().next();

		final Iterator<T> children = this.childrenAccessor.getChildren(nextNode);
		if (children.hasNext())
			this.iteratorStack.addLast(children);

		return nextNode;
	}

	private Iterator<T> last() {
		return this.iteratorStack.peekLast();
	}

	/**
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		this.iteratorStack.peekLast().remove();
	}

}
