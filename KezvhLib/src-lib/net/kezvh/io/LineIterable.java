/**
 *
 */
package net.kezvh.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.ListIterator;

import net.kezvh.collections.CollectionsUtilities;

/**
 * @author afflux
 *
 * Iterate the lines in a file as if he were a list
 */
public class LineIterable extends AbstractSequentialList<String> {

	private final LineNumberReader lineNumberReader;
	private final File file;

	/**
	 * @param filename COMMENT
	 * @throws FileNotFoundException COMMENT
	 */
	public LineIterable(final String filename) throws FileNotFoundException {
		this(new File(filename));
	}

	/**
	 * @param file COMMENT
	 * @throws FileNotFoundException COMMENT
	 */
	public LineIterable(final File file) throws FileNotFoundException {
		this.file = file;
		this.lineNumberReader = new LineNumberReader(new InputStreamReader(new FileInputStream(file)));
	}

	/**
	 * @param stream COMMENT
	 */
	public LineIterable(final InputStream stream) {
		this(new InputStreamReader(stream));
	}

	/**
	 * @param reader COMMENT
	 */
	public LineIterable(final Reader reader) {
		this(new LineNumberReader(reader));
	}

	/**
	 * @param lineNumberReader COMMENT
	 */
	public LineIterable(final LineNumberReader lineNumberReader) {
		this.lineNumberReader = lineNumberReader;
		this.file = null;
	}

	/**
	 * returns null if IO Exception
	 *
	 * @see java.util.AbstractList#get(int)
	 * @param index COMMENT
	 * @return x
	 */
	@Override
	public String get(final int index) {
		this.lineNumberReader.setLineNumber(index);
		try {
			return this.lineNumberReader.readLine();
		} catch (final IOException e) {
			return null;
		}
	}

	/**
	 * @see java.util.AbstractList#iterator()
	 * @return x
	 */
	@Override
	public Iterator<String> iterator() {
		if (this.file != null)
			try {
				return new LineIterator(this.file);
			} catch (final FileNotFoundException e) {
				throw new UnsupportedOperationException();
			}
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.AbstractCollection#size()
	 * @return x
	 */
	@Override
	public int size() {
		return CollectionsUtilities.count(this.iterator());
	}

	@Override
	public ListIterator<String> listIterator(final int index) {
		return new LineIterator(this.lineNumberReader);
	}

}
