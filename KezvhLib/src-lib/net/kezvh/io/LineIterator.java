/**
 *
 */
package net.kezvh.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ListIterator;

/**
 * @author afflux
 *
 */
public class LineIterator implements ListIterator<String> {
	private final LineNumberReader lineNumberReader;

	/**
	 * @param filename FIXME comment
	 * @throws FileNotFoundException FIXME comment
	 */
	public LineIterator(final String filename) throws FileNotFoundException {
		this(new File(filename));
	}

	/**
	 * @param file FIXME comment
	 * @throws FileNotFoundException FIXME comment
	 */
	public LineIterator(final File file) throws FileNotFoundException {
		this(new FileInputStream(file));
	}

	/**
	 * @param stream FIXME comment
	 */
	public LineIterator(final InputStream stream) {
		this(new InputStreamReader(stream));
	}

	/**
	 * @param reader FIXME comment
	 */
	public LineIterator(final Reader reader) {
		this(new LineNumberReader(reader));
	}

	/**
	 * @param lineNumberReader FIXME comment
	 */
	public LineIterator(final LineNumberReader lineNumberReader) {
		this.lineNumberReader = lineNumberReader;
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 * @return true if there are more lines
	 */
	@Override
	public boolean hasNext() {
		try {
			return this.lineNumberReader.ready();
		} catch (final IOException e) {
			return false;
		}
	}

	/**
	 * If there is an IO exception while reading the line, returns null.
	 * Possibly revise this behaviour in the future.
	 *
	 * @see java.util.Iterator#next()
	 * @return the next line
	 */
	@Override
	public String next() {
		try {
			return this.lineNumberReader.readLine();
		} catch (final IOException e) {
			return null;
		}
	}

	/**
	 * We don't support removing lines from a stream .. that'd just be weird.
	 *
	 * @see java.util.Iterator#remove()
	 * @throws UnsupportedOperationException whenever this is used
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(final String e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPrevious() {
		return this.lineNumberReader.getLineNumber() != 0;
	}

	@Override
	public int nextIndex() {
		return this.lineNumberReader.getLineNumber() + 1;
	}

	@Override
	public String previous() {
		this.lineNumberReader.setLineNumber(this.lineNumberReader.getLineNumber() - 2);
		try {
			return this.lineNumberReader.readLine();
		} catch (final IOException e) {
			return null;
		}
	}

	@Override
	public int previousIndex() {
		return this.lineNumberReader.getLineNumber() - 1;
	}

	@Override
	public void set(final String e) {
		throw new UnsupportedOperationException();
	}

}
