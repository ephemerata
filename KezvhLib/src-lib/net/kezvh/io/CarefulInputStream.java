package net.kezvh.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * instead of blocking on reads to an input stream, which may block at a lower level than java is capable of interpreting, sleep the thread until data is available reads (and skips) are synchronized on the parameter stream. currently this implementation is not very efficient for large reads.
 */
public class CarefulInputStream extends InputStream {
	private final InputStream inputStream;
	private final long carePeriod;

	/**
	 * @param inputStream FIXME comment
	 */
	public CarefulInputStream(final InputStream inputStream) {
		this(inputStream, 100);
	}

	/**
	 * @param inputStream stream to wrap
	 * @param carePeriod long to wait before polling to see if more data is available on input stream
	 */
	public CarefulInputStream(final InputStream inputStream, final long carePeriod) {
		this.inputStream = inputStream;
		this.carePeriod = carePeriod;
	}

	@Override
	public int available() throws IOException {
		return this.inputStream.available();
	}

	@Override
	public void close() throws IOException {
		this.inputStream.close();
	}

	@Override
	public synchronized void mark(final int readlimit) {
		this.inputStream.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return this.inputStream.markSupported();
	}

	@Override
	public int read() throws IOException {
		while (true)
			synchronized (this.inputStream) {
				if (this.available() != 0)
					return this.inputStream.read();
				try {
					this.inputStream.wait(this.carePeriod);
				} catch (final InterruptedException e) {
					return -1;
				}
			}
	}

	@Override
	public synchronized void reset() throws IOException {
		synchronized (this.inputStream) {
			this.inputStream.reset();
		}
	}

	@Override
	public long skip(final long n) throws IOException {
		synchronized (this.inputStream) {
			return this.inputStream.skip(this.available());
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass()))
			return false;

		return this.inputStream.equals(((CarefulInputStream) obj).inputStream);
	}

	@Override
	public int hashCode() {
		return this.inputStream.hashCode();
	}

	@Override
	public String toString() {
		return CarefulInputStream.class + "[" + this.inputStream.toString() + "]";
	}

}
