/**
 *
 */
package net.kezvh.algorithms.text;

import net.kezvh.io.LineIterable;

/**
 * @author afflux
 *
 */
public class EditDistanceLoad {
	/**
	 * @param args COMMENT
	 * @throws Exception COMMENT
	 */
	public static void main(final String... args) throws Exception {
		final LineIterable file = new LineIterable(args[0]);
		final String[] words = file.toArray(new String[file.size()]);
		long time1 = EditDistanceLoad.time(words, EditDistance.HAMMING_PLUS);
		System.out.println(time1 / 1000000000.0);
		time1 = EditDistanceLoad.time(words, EditDistance.HAMMING_OFFSET);
		System.out.println(time1 / 1000000000.0);
		//		final long time2 = EditDistanceLoad.time(words, EditDistance.DAMERAU_LEVENSHTEIN_NAIVE);
		//		System.out.println(time2 / 1000000000.0);
	}

	private static long time(final String[] words, final EditDistance ed) {
		final long startTime = System.nanoTime();
		for (final String word : words)
			for (final String word2 : words)
				ed.op(word, word2);
		return System.nanoTime() - startTime;
	}
}
