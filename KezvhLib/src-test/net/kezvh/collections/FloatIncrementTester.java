package net.kezvh.collections;

/**
 * @author mjacob
 *
 */
public class FloatIncrementTester {
	/**
	 * @param args nothing
	 */
	public static void main(final String... args) {
		int i = Integer.MIN_VALUE;
		float f = Float.intBitsToFloat(i);
		int j = i + 1;
		final float g = Float.intBitsToFloat(j);
		for (; j < Integer.MAX_VALUE; i = j, j++, f = g)
			if (f > g)
				System.out.println(i + ", " + j + ", " + f + ", " + g + ", " + (f - g));

	}
}
