package net.kezvh.math.algebra.fields;

/**
 * @author afflux
 */
public class Test {
	/**
	 * @param args description
	 */
	public static void main(final String... args) {
		Test.foo2();
	}

	/**
	 *
	 */
	public static void baz() {
		for (int i = 31; i < 64; i++)
			// }
			// }
			for (long j = 3; j < 1 << i; j += 2)
				try {
					// G2_N g = new G2_N(i, j);
					System.out.println(i + ": " + j);
					break;
				} catch (final Throwable t) {
					//
				}
	}

	/**
	 *
	 */
	public static void bar() {
		final G2_N g = new G2_N(16, 45);
		long t = System.currentTimeMillis();
		for (int i = 1; i < 65535; i++) {
			final long inverse = g.inverse(i);
			final long product = g.multiply(i, inverse);
			if (product != 1)
				System.out.println(i + " * " + inverse + " = " + product);
		}
		System.out.println(System.currentTimeMillis() - t);

		t = System.currentTimeMillis();
		for (double i = 1; i < 65535; i++) {
			final double inverse = 1 / i;
			final double product = 1 * inverse;
			if (product == 0)
				System.out.println(i + " * " + inverse + " = " + product);
		}
		System.out.println(System.currentTimeMillis() - t);
	}

	/**
	 *
	 */
	public static void foo() {
		Test.foo(3, 5, new long[] { 3, 1, 4, 1, 5, 9, 2 });
		Test.foo(3, 3, new long[] { 3, 1, 4, 1, 5, 9, 2 });
		Test.foo(4, 3, new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
		Test.foo(4, 9, new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
		Test.foo(4, 3, new long[] { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 });
		Test.foo(4, 9, new long[] { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 });
	}

	/**
	 *
	 */
	public static void foo2() {
		// foo2(3, 5, new long[] { 3,1,4,1,5,1,2 });
		// foo2(3, 3, new long[] { 3,1,4,1,5,1,2 });
		final long[] x = new long[7];

		final G2_N g3 = new G2_N(3, 3);
		final G2_N g5 = new G2_N(3, 5);
		for (int j = 1; j < 8; j++) {
			x[0] = j;
			for (int i = 1; i < 7; i++)
				x[i] = g3.multiply(x[i - 1], 2);
			Test.foo2(3, 3, x);
			for (int i = 1; i < 7; i++)
				x[i] = g5.multiply(x[i - 1], 2);
			Test.foo2(3, 5, x);
		}
		// foo2(4, 3, new long[] { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 });
		// foo2(4, 9, new long[] { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 });
		// foo2(4, 3, new long[] { 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1 });
		// foo2(4, 9, new long[] { 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1 });
	}

	/**
	 *
	 */
	public static void goo() {
		Test.goo(3, 5, 3, 3, new long[] { 1, 2, 3, 4, 5, 6, 7 });
		Test.goo(3, 5, 3, 3, new long[] { 1, 1, 1, 1, 1, 1, 1 });
		Test.goo(3, 5, 3, 3, new long[] { 0, 0, 0, 0, 0, 0, 0 });
		Test.goo(3, 5, 3, 3, new long[] { 2, 2, 2, 2, 2, 2, 2 });
		Test.goo(3, 5, 3, 3, new long[] { 1, 2, 3, 4, 5, 5, 2 });
	}

	/**
	 * @param n1 description
	 * @param q1 description
	 * @param n2 description
	 * @param q2 description
	 * @param numbers description
	 */
	public static void goo(final int n1, final long q1, final int n2, final long q2, final long[] numbers) {
		final G2_N g1 = new G2_N(n1, q1);
		final G2_N_DFT dft1 = new G2_N_DFT(g1);
		final G2_N g2 = new G2_N(n2, q2);
		final G2_N_DFT dft2 = new G2_N_DFT(g2);
		long[] result = dft1.dft(numbers);
		long[] result2 = dft2.idft(result);
		for (final long l : result2)
			System.out.print(l + ", ");
		System.out.println();
		result = dft2.dft(numbers);
		result2 = dft1.dft(result);
		for (final long l : result2)
			System.out.print(l + ", ");
		System.out.println();
		result = dft2.dft(result2);
		result2 = dft1.dft(result);
		for (final long l : result2)
			System.out.print(l + ", ");
		System.out.println();

	}

	/**
	 * @param n description
	 * @param q description
	 * @param numbers description
	 */
	public static void foo2(final int n, final long q, final long[] numbers) {
		final G2_N g = new G2_N(n, q);
		final G2_N_DFT dft = new G2_N_DFT(g);
		final long[] result = dft.dft(numbers);
		for (final long l : result)
			System.out.print(l + ", ");
		System.out.println();
		for (int i = 0; i < result.length; i++)
			result[i] = g.multiply(result[i], 2);
		final long[] result2 = dft.idft(result);
		for (final long l : result2)
			System.out.print(g.multiply(l, g.inverse(2)) + ", ");
		System.out.println();
	}

	/**
	 * @param n description
	 * @param q description
	 * @param numbers description
	 */
	public static void foo(final int n, final long q, final long[] numbers) {
		final G2_N g = new G2_N(n, q);
		final G2_N_DFT dft = new G2_N_DFT(g);
		final long[] result = dft.dft(numbers);
		for (final long l : result)
			System.out.print(l + ", ");
		System.out.println();
		final long[] result2 = dft.idft(result);
		for (int i = 0; i < numbers.length; i++)
			if (numbers[i] % (1 << n) != result2[i])
				throw new RuntimeException("poo, result isn't equal @ " + i + ": " + numbers[i] % (1 << n) + " vs " + result2[i]);
	}
}
