import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import net.kezvh.collections.text.TrieSet;

/**
 *
 */

/**
 * @author afflux
 *
 */
public class RocketGummer {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		final TrieSet wordlist = new TrieSet(args[0]);

		//		MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg;
		//
		//		try {
		//			final long start = System.nanoTime();
		//			mmg = LoadGraph.loadGraph(args[0] + ".graph", wordlist);
		//			System.out.println("loaded graph in " + ((System.nanoTime() - start) / 1000000000.0));
		//		} catch (final OutOfMemoryError e) {
		//			System.out.println("OUT OF MEMORY (this is a println)");
		//			e.printStackTrace();
		//			return;
		//		}

		final BufferedReader isr = new BufferedReader(new InputStreamReader(System.in));
		String line;
		int depth = 4;
		while ((line = isr.readLine()) != null) {
			System.out.println("read: '" + line + "'");
			if (line.matches("[0-9]+"))
				depth = Integer.parseInt(line);
			else {
				if (!wordlist.contains(line)) {
					System.out.println("not in dictionary");
					continue;
				}

				final ConcurrentHashMap<String, Integer> similar = new ConcurrentHashMap<String, Integer>();
				wordlist.getStringsSimilarN(line, depth, 1, similar, new ConcurrentHashMap<String, Integer>());
				System.out.println("found " + similar.size());

				final SortedSet<Entry<String, Integer>> sortedSimilar = new TreeSet<Entry<String, Integer>>(new Comparator<Entry<String, Integer>>() {
					/**
					 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
					 * @param o1
					 * @param o2
					 * @return x
					 */
					@Override
					public int compare(final Entry<String, Integer> o1, final Entry<String, Integer> o2) {
						if (o1.getValue() > o2.getValue())
							return -1;
						else if (o1.getValue() < o2.getValue())
							return 1;
						else
							return -o1.getKey().compareTo(o2.getKey());
					}
				});
				sortedSimilar.addAll(similar.entrySet());
				for (final Entry<String, Integer> word : sortedSimilar)
					System.out.println(word.getKey() + ": " + word.getValue());
			}
		}
	}
}
