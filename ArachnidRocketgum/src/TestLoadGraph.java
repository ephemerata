import java.util.concurrent.ConcurrentHashMap;

import net.kezvh.collections.graphs.directed.MapMapGraph;
import net.kezvh.collections.text.TrieSet;

/**
 * @author afflux
 *
 */
public class TestLoadGraph {
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String... args) throws Exception {
		final String dictionary = args[0];
		long start = System.nanoTime();
		final TrieSet wordlist = new TrieSet(dictionary);
		System.out.println("loaded dict in " + (System.nanoTime() - start) / 1000000000.0);
		start = System.nanoTime();
		final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg = LoadGraph.constructGraph(wordlist);
		System.out.println("constructed graph in " + (System.nanoTime() - start) / 1000000000.0);

		System.out.println(mmg.size() + " nodes");
		System.out.println(mmg.edgeCount() + " edges");
	}
}
