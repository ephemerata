import java.io.FileWriter;
import java.io.Writer;
import java.util.concurrent.ConcurrentHashMap;

import net.kezvh.algorithms.graph.Dijkstra;
import net.kezvh.collections.graphs.directed.MapMapGraph;
import net.kezvh.collections.text.TrieSet;

/**
 *
 */

/**
 * @author afflux
 *
 */
public class RocketDict {
	/**
	 * @param args we hates it, the precious
	 * @throws Exception
	 */
	public static void main(final String... args) throws Exception {
		MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg;
		final TrieSet trieSet = new TrieSet(args[0]);
		try {
			mmg = LoadGraph.loadGraph(args[0] + ".graph", trieSet);
		} catch (final OutOfMemoryError e) {
			System.out.println("OUT OF MEMORY (this is a println)");
			e.printStackTrace();
			return;
		}

		final Writer writer = new FileWriter(args[0] + ".sieve");
		System.out.println("invoking mr. dijkstra");
		final Dijkstra<String> sp = new Dijkstra<String>(mmg, "a");
		System.out.println("writing...");
		for (final String s : sp.getAllConnected())
			writer.write(s + "\n");
		writer.close();
	}
}
