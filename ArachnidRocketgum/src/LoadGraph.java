import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.kezvh.collections.graphs.directed.MapMapGraph;
import net.kezvh.collections.text.TrieSet;
import net.kezvh.collections.views.MapAsView;
import net.kezvh.collections.views.SetView;
import net.kezvh.functional.lambda.L1;
import net.kezvh.io.LineIterable;
import net.kezvh.patterns.AbstractFactory;

/**
 *
 */

/**
 * @author afflux
 *
 */
public class LoadGraph {

	/**
	 * @param wordlist
	 * @return stuff
	 * @throws Exception
	 */
	public static MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> constructGraph(final TrieSet wordlist) throws Exception {

		final Map<String, String> originals = new HashMap<String, String>(); // i heart java
		final Map<String, ConcurrentHashMap<String, Integer>> graph = new HashMap<String, ConcurrentHashMap<String, Integer>>();
		System.out.println("reading...");

		for (final String word : wordlist) {
			wordlist.add(word);
			graph.put(word, new ConcurrentHashMap<String, Integer>());
			originals.put(word, word);
		}

		final Map<Integer, Integer> weightFunction = new HashMap<Integer, Integer>() {
			{
				this.put(1, 1);
				this.put(2, 1 << 6);
				this.put(3, 1 << 12);
			}
		};

		final AtomicInteger nodes = new AtomicInteger(0);
		final AtomicInteger edges = new AtomicInteger(0);
		System.out.println("starting to create connection graph..");
		final long start = System.nanoTime();
		final AtomicLong round = new AtomicLong(System.nanoTime());
		final ConcurrentHashMap<String, Integer> minima = new ConcurrentHashMap<String, Integer>();

		final Iterator<String> i = wordlist.iterator();

		final Runnable r = new Runnable() {
			@Override
			public void run() {
				String t = "XXXX";
				final Map<String, Integer> similar = new HashMap<String, Integer>();
				final Set<String> originalStrings = new SetView<String, String>(similar.keySet(), new L1<String, String>() {
					@Override
					public String op(final String param) {
						if (!originals.containsKey(param))
							throw new NullPointerException(param + "! " + "\n" + similar);

						return originals.get(param);
					}
				});
				final Map<String, Integer> similarView = new MapAsView<String, Integer>(originalStrings, new L1<Integer, String>() {
					@Override
					public Integer op(final String param) {
						return weightFunction.get(similar.get(param));
					}
				});
				while (true) {
					synchronized (i) {
						if (i.hasNext())
							t = i.next();
						else
							return;
					}

					similar.clear();

					wordlist.getStringsSimilarN(t, 3, 1, similar, minima);

					graph.get(t).putAll(similarView);
					int edgesAdded = 0;
					for (final Map.Entry<String, Integer> entry : similarView.entrySet()) {
						if (!graph.containsKey(entry.getKey()))
							throw new NullPointerException(entry.getKey() + ", " + entry.getValue() + " :: " + t);

						final ConcurrentHashMap<String, Integer> row = graph.get(entry.getKey());
						Integer value = entry.getValue();
						final Integer otherValue = row.putIfAbsent(t, value);
						if (otherValue != null && otherValue > value) {
							Integer prevValue = row.put(t, value);
							while (prevValue < value) { // something changed, we need to put the minimum back in
								value = prevValue;
								prevValue = row.put(t, value);
							}
						} else if (otherValue == null)
							edgesAdded++;
					}
					final int n = nodes.incrementAndGet();
					final int e = edges.addAndGet(edgesAdded);
					if (n % 100 == 0) {
						System.out.println("loaded " + n + " and " + e + " edges in " + (System.nanoTime() - round.get()) / 1000000000.0);
						round.set(System.nanoTime());
					}
				}
			}
		};

		final Thread a = new Thread(r);
		final Thread b = new Thread(r);
		final Thread c = new Thread(r);

		a.start();
		b.start();
		c.start();

		a.join();
		b.join();
		c.join();

		System.out.println("constructed graph with " + nodes + " elements and " + edges + " edges in " + (System.nanoTime() - start) / 1000000000.0);

		return new MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>>(graph, new AbstractFactory<ConcurrentHashMap<String, Integer>>() {
			@Override
			public ConcurrentHashMap<String, Integer> create() throws net.kezvh.patterns.AbstractFactory.CreationFailedException {
				return new ConcurrentHashMap<String, Integer>();
			}
		});
	}

	/**
	 * @param mmg COMMENT
	 * @param filename COMMENT
	 * @param wordlist COMMENT
	 * @throws IOException COMMENT
	 */
	public static void saveGraph(final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg, final String filename, final TrieSet wordlist) throws IOException {
		final FileWriter fileWriter = new FileWriter(filename);
		for (final String source : mmg) {
			fileWriter.write(source + "\n");
			for (final String dest : mmg.adjacentFrom(source))
				fileWriter.write(" " + dest + " " + mmg.getEdgeValue(source, dest) + "\n");
		}
		fileWriter.close();
	}

	private static final Pattern edgePattern = Pattern.compile(" ([\\w']+) (\\d+)");
	private static final Pattern wordPattern = Pattern.compile("[\\w']+");

	/**
	 * @author afflux
	 *
	 */
	public static final class GraphFileFormatException extends RuntimeException {

		/**
		 *
		 */
		public GraphFileFormatException() {
			super();
		}

		/**
		 * @param message
		 * @param cause
		 */
		public GraphFileFormatException(final String message, final Throwable cause) {
			super(message, cause);
		}

		/**
		 * @param message
		 */
		public GraphFileFormatException(final String message) {
			super(message);
		}

		/**
		 * @param cause
		 */
		public GraphFileFormatException(final Throwable cause) {
			super(cause);
		}

	}

	/**
	 * This has practically no error checking.. all sort of stuff can break it
	 *
	 * @param filename
	 * @param wordlist
	 * @return COMMENT
	 * @throws IOException
	 */
	public static MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> loadGraph(final String filename, final TrieSet wordlist) throws IOException {
		//        final BufferedReader x = new BufferedReader(new FileReader(filename));
		final Map<String, ConcurrentHashMap<String, Integer>> graph = new HashMap<String, ConcurrentHashMap<String, Integer>>();
		final Map<String, String> originals = new HashMap<String, String>(); // i heart java

		for (final String word : wordlist) {
			graph.put(word, new ConcurrentHashMap<String, Integer>());
			originals.put(word, word);
		}

		boolean skip = false;
		String currentWord = null;
		//        int count = 0;
		for (final String line : new LineIterable(filename))
			//        for (String line = x.readLine(); line != null; line = x.readLine()) {
			//            count++;
			if (line.charAt(0) == ' ') {
				final Matcher matcher = LoadGraph.edgePattern.matcher(line);
				if (!matcher.matches())
					throw new RuntimeException(line);

				//				if (!graph.containsKey(currentWord))
				//					throw new GraphFileFormatException("you fucked up bub, the graph don't have word " + currentWord);
				if (!skip && graph.containsKey(currentWord))
					graph.get(currentWord).put(originals.get(matcher.group(1)), Integer.valueOf(matcher.group(2)));
			} else {
				final Matcher matcher = LoadGraph.wordPattern.matcher(line);
				if (!matcher.matches())
					throw new RuntimeException("not a word! '" + line + "'");
				currentWord = line;
				skip = !graph.containsKey(currentWord);
			}

		return new MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>>(graph, new AbstractFactory<ConcurrentHashMap<String, Integer>>() {
			@Override
			public ConcurrentHashMap<String, Integer> create() throws net.kezvh.patterns.AbstractFactory.CreationFailedException {
				return new ConcurrentHashMap<String, Integer>();
			}
		});
	}

	/**
	 * @param args COMMENT
	 * @throws Exception COMMENT
	 */
	public static void main(final String... args) throws Exception {
		final String dictionary = args[0];
		final String outputFile = dictionary + ".graph";

		long start = System.nanoTime();
		final TrieSet wordlist = new TrieSet(dictionary);
		System.out.println("loaded dict in " + (System.nanoTime() - start) / 1000000000.0);
		start = System.nanoTime();
		final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg = LoadGraph.constructGraph(wordlist);
		System.out.println("constructed graph in " + (System.nanoTime() - start) / 1000000000.0);
		start = System.nanoTime();
		LoadGraph.saveGraph(mmg, outputFile, wordlist);
		System.out.println("saved graph in " + (System.nanoTime() - start) / 1000000000.0);
		start = System.nanoTime();
		final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> forComparison = LoadGraph.loadGraph(outputFile, wordlist);
		System.out.println("reloaded graph in " + (System.nanoTime() - start) / 1000000000.0);

		System.out.println(forComparison.size());
		System.out.println(forComparison.edgeCount());
		start = System.nanoTime();
		final PrintStream diffFile = new PrintStream(new FileOutputStream(args[0] + ".diff"));
		System.out.println("graphs are equal: " + LoadGraph.diff(mmg, forComparison, diffFile));
		diffFile.close();

		System.out.println(mmg.equals(forComparison));
		System.out.println("compared graph in " + (System.nanoTime() - start) / 1000000000.0);
	}

	private static boolean diff(final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> a, final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> b, final PrintStream os) {
		boolean differencesFound = false;
		for (final String s : a)
			if (b.contains(a)) {
				final Set<String> na = a.adjacentFrom(s);
				final Set<String> nb = b.adjacentFrom(s);
				for (final String sn : na)
					if (!nb.contains(sn)) {
						os.println("b missing edge " + s + " => " + sn);
						differencesFound = true;
					}
				for (final String sn : nb)
					if (!na.contains(sn)) {
						os.println("a missing edge " + s + " => " + sn);
						differencesFound = true;
					}
			} else {
				os.println("b missing node " + s);
				differencesFound = true;
			}
		for (final String s : b)
			if (!a.contains(s)) {
				os.println("a missing node " + s);
				differencesFound = true;
			}
		return differencesFound;
	}
}
