import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentHashMap;

import net.kezvh.algorithms.graph.DijkstraPath;
import net.kezvh.algorithms.graph.Path;
import net.kezvh.collections.graphs.directed.MapMapGraph;
import net.kezvh.collections.text.TrieSet;
import net.kezvh.text.Joiner;
import net.kezvh.text.SimpleJoiner;

/**
 *
 */

/**
 * @author afflux
 *
 */
public class RocketLine {

	private static final Joiner<String> joiner = new SimpleJoiner<String>("', '");

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		final TrieSet wordlist = new TrieSet(args[0] + ".sieve");

		MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg;

		try {
			final long start = System.nanoTime();
			mmg = LoadGraph.loadGraph(args[0] + ".graph", wordlist);
			System.out.println("loaded graph in " + ((System.nanoTime() - start) / 1000000000.0));
		} catch (final OutOfMemoryError e) {
			System.out.println("OUT OF MEMORY (this is a println)");
			e.printStackTrace();
			return;
		}

		final BufferedReader isr = new BufferedReader(new InputStreamReader(System.in));
		String line;
		int depth = 4;
		while ((line = isr.readLine()) != null)
			if (line.matches("[0-9]+"))
				depth = Integer.parseInt(line);
			else {
				final String[] words = line.split(",");
				final String aw = words[0];
				final String bw = words[1];
				final Path<String> path = new DijkstraPath<String>(mmg, aw, bw).getShortestPath();

				if (path == null)
					System.out.println("no path");
				else
					System.out.println("  ['" + RocketLine.joiner.join(path) + "'],");
			}
	}

}
