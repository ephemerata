import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.kezvh.algorithms.graph.DijkstraPath;
import net.kezvh.algorithms.graph.Path;
import net.kezvh.collections.graphs.directed.MapMapGraph;
import net.kezvh.collections.text.TrieSet;
import net.kezvh.io.LineIterator;
import net.kezvh.lang.AlgorithmException;
import net.kezvh.text.Joiner;
import net.kezvh.text.SimpleJoiner;

/**
 *
 */

/**
 * @author afflux
 *
 */
public class Rocketgum {
	private static final String GUM_HEADER = "var sets = %s;\nvar setSize = %s;\nvar wordsLeft = %s;\nvar margins = %s;\n\nvar contents =\n [\n";

	private static final String WORD_REGEX = "([a-z']+)";
	private static final String PUNC_REGEX = "([^a-z']+)";
	private static final Pattern WORD_PATTERN = Pattern.compile(Rocketgum.WORD_REGEX);
	private static final Pattern PUNC_PATTERN = Pattern.compile(Rocketgum.PUNC_REGEX);

	private static final String PUNCUTATED_WORD_REGEX = "([^a-z']*)([a-z']+)([^a-z']*)";
	private static final Pattern PUNCUTATED_WORD_PATTERN = Pattern.compile(Rocketgum.PUNCUTATED_WORD_REGEX);

	private static final String POSSESSIVE_REGEX = "(^[a-z]+)'s";
	private static final String PLURAL_POSSESSIVE_REGEX = "(^[a-z]+)'";
	private static final Pattern POSSESSIVE_PATTERN = Pattern.compile(Rocketgum.POSSESSIVE_REGEX);
	private static final Pattern PLURAL_POSSESSIVE_PATTERN = Pattern.compile(Rocketgum.PLURAL_POSSESSIVE_REGEX);

	private static final class Parsed {
		public static final class Token {
			public static final Token LINE_BREAK = new Token("\n", false, false, false);
			public static final Token LEFT_NULL_PUNC = new Token("", false, true, false);
			public static final Token RIGHT_NULL_PUNC = new Token("", true, false, false);

			public String string;
			public boolean spaceToRight;
			public boolean spaceToLeft;
			public final boolean inDict;

			public Token(final String string, final boolean spaceToRight, final boolean spaceToLeft, final boolean inDict) {
				super();
				this.string = string;
				this.spaceToRight = spaceToRight;
				this.spaceToLeft = spaceToLeft;
				this.inDict = inDict;
			}

			public boolean isPunc() {
				return this.string.isEmpty() || this.string.matches("[^a-z']+");
			}

			@Override
			public String toString() {
				if (this.isPunc())
					return this.string.replaceAll("\n", "<br>");
				return this.string;
			}
		}

		private final List<Token> words = new ArrayList<Token>();
		private int numPuncs = 0;

		private void addAllWords(final String[] maybewords, final Collection<String> wordList) {
			if (maybewords.length == 1 && maybewords[0].equals("\n")) {
				final Token theLastWord = this.words.get(this.words.size() - 1);
				if (theLastWord.isPunc())
					theLastWord.string = theLastWord.string + "\n";
				else {
					theLastWord.spaceToRight = false;
					this.words.add(new Token(maybewords[0], false, false, false));
					this.numPuncs++;
				}
			} else

				for (final String maybeword : maybewords)
					if (wordList.contains(maybeword))
						this.words.add(new Token(maybeword, true, true, true));
					else {
						final Matcher punctuated = Rocketgum.PUNCUTATED_WORD_PATTERN.matcher(maybeword);
						if (!punctuated.matches())
							throw new AlgorithmException(maybewords.length + " no match found: '" + maybeword + "' (" + Rocketgum.PUNCUTATED_WORD_REGEX + ")");
						final String prePunc = punctuated.group(1);
						final String word = punctuated.group(2);
						final String postPunc = punctuated.group(3);

						if (!prePunc.isEmpty()) {
							this.words.add(new Token(prePunc, false, true, false));
							this.numPuncs++;
						}

						this.words.add(new Token(word, postPunc.isEmpty(), prePunc.isEmpty(), wordList.contains(word)));

						if (!postPunc.isEmpty()) {
							this.words.add(new Token(postPunc, true, false, false));
							this.numPuncs++;
						}
					}
		}

		public Parsed(final String filename, final Collection<String> wordList) throws FileNotFoundException {
			final LineIterator lineIterator = new LineIterator(filename);
			while (lineIterator.hasNext()) {
				final String line = lineIterator.next();
				final String[] maybewords = line.split(" ");
				if (lineIterator.hasNext())
					maybewords[maybewords.length - 1] = maybewords[maybewords.length - 1] + "\n";
				this.addAllWords(maybewords, wordList);
				//
				//					this.words.add(Token.LINE_BREAK);
				//					this.numPuncs++;
				//				}
			}
		}

		public List<Token> wordsAndPunc() {
			return this.words;
		}

		public Token getWord(final int wordIndex) {
			int wordCount = 0;
			for (final Token token : this.words)
				if (!token.isPunc()) {
					if (wordCount == wordIndex)
						return token;
					wordCount++;
				}
			throw new IndexOutOfBoundsException("word index too high: " + wordIndex);
		}

		public void insertNullPuncBeforeWord(final int wordIndex) {
			int wordCount = 0;
			int index = 0;

			Token word = null;
			for (final Token token : this.words) {
				if (!token.isPunc()) {
					if (wordCount == wordIndex) {
						word = token;
						break;
					}
					wordCount++;
				}
				index++;
			}

			if (word == null)
				throw new AlgorithmException();

			if (!word.spaceToLeft)
				return;

			this.insertLeftNullPunc(index);
		}

		private void insertLeftNullPunc(final int index) {
			this.words.get(index).spaceToLeft = false;
			this.words.add(index, Token.LEFT_NULL_PUNC);
			this.numPuncs++;
		}

		public void insertNullPuncAfterWord(final int wordIndex) {
			int wordCount = 0;
			int index = 0;

			Token word = null;
			for (final Token token : this.words) {
				if (!token.isPunc()) {
					if (wordCount == wordIndex) {
						word = token;
						break;
					}
					wordCount++;
				}
				index++;
			}

			if (word == null)
				throw new AlgorithmException();

			if (!word.spaceToRight)
				return;

			this.insertRightNullPunc(index);
		}

		private void insertRightNullPunc(final int index) {
			this.words.get(index).spaceToRight = false;
			this.words.add(index + 1, Token.RIGHT_NULL_PUNC);
			this.numPuncs++;
		}

		public int getNumWords() {
			return this.words.size() - this.numPuncs;
		}

		/**
		 * @return the numPuncs
		 */
		public int getNumPuncs() {
			return this.numPuncs;
		}

		@Override
		public String toString() {
			return this.words.toString();
		}
	}

	/**
	 * @author afflux
	 *
	 */
	public static final class GumFormatException extends Exception {
		/**
		 *
		 */
		public GumFormatException() {
			super();
		}

		/**
		 * @param message
		 * @param cause
		 */
		public GumFormatException(final String message, final Throwable cause) {
			super(message, cause);
		}

		/**
		 * @param message
		 */
		public GumFormatException(final String message) {
			super(message);
		}

		/**
		 * @param cause
		 */
		public GumFormatException(final Throwable cause) {
			super(cause);
		}
	}

	/**
	 * first argument: dictionary
	 * other arguments: stories
	 * output: transition maps for javascript on STDOUT
	 *
	 * @param args strings
	 * @throws Exception
	 */
	public static void main(final String... args) throws Exception {
		/**
		 * first we check to see that the words in the files actually will give us chains
		 */
		final TrieSet wordlist = new TrieSet(args[0] + ".sieve");

		final Parsed[] parsed = new Parsed[args.length - 1];
		int num = 0;
		for (int i = 0; i < parsed.length; i++) {
			parsed[i] = new Parsed(args[i + 1], wordlist); // if the words in the file aren't in the wordlist, exception
			if (num == 0)
				num = parsed[i].getNumWords();
			else if (num != parsed[i].getNumWords())
				throw new GumFormatException(args[i + 1] + " doesn't have the same number of words as " + args[1] + " (" + parsed[i].getNumWords() + " vs " + num + ")");
		}

		final int numWords = parsed[0].getNumWords();

		for (int i = 0; i < numWords; i++) {
			if (Rocketgum.hasPuncBefore(i, parsed))
				Rocketgum.insertPuncBefore(i, parsed); // put phantom punctuation in
			if (Rocketgum.hasPuncAfter(i, parsed))
				Rocketgum.insertPuncAfter(i, parsed); // put phantom punctuation in
		}

		MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg;

		try {
			final long start = System.nanoTime();
			mmg = LoadGraph.loadGraph(args[0] + ".graph", wordlist);
			System.out.println("loaded graph in " + ((System.nanoTime() - start) / 1000000000.0));
		} catch (final OutOfMemoryError e) {
			System.out.println("OUT OF MEMORY (this is a println)");
			e.printStackTrace();
			return;
		}

		System.out.format(Rocketgum.GUM_HEADER, parsed.length, parsed[0].wordsAndPunc().size(), Rocketgum.getLeft(parsed[0].wordsAndPunc().size()), Rocketgum.getMargins(parsed[0].wordsAndPunc()));

		for (int i = 0; i < parsed.length - 1; i++)
			Rocketgum.printTransitionsFor(mmg, parsed[i], parsed[i + 1]);
		Rocketgum.printTransitionsFor(mmg, parsed[parsed.length - 1], parsed[0]);

		System.out.println("];");
	}

	private static final String getLeft(final int count) {
		return Collections.nCopies(count, 0).toString();
	}

	private static final String getMargins(final List<Rocketgum.Parsed.Token> tokens) {
		final String[] margins = new String[tokens.size()];
		for (int i = 0; i < tokens.size(); i++)
			if (tokens.get(i).spaceToLeft) {
				if (tokens.get(i).spaceToRight)
					margins[i] = "'both'";
				else
					margins[i] = "'left'";
			} else if (tokens.get(i).spaceToRight)
				margins[i] = "'right'";
			else
				margins[i] = "'none'";
		return Arrays.toString(margins);
	}

	private static final Joiner<String> joiner = new SimpleJoiner<String>("', '");

	private static void printTransitionsFor(final MapMapGraph<String, Integer, ConcurrentHashMap<String, Integer>> mmg, final Parsed a, final Parsed b) {
		if (a.getNumPuncs() != b.getNumPuncs())
			throw new AlgorithmException("num puncs don't match \n" + a + "\n vs\n " + b);
		if (a.getNumWords() != b.getNumWords())
			throw new AlgorithmException("num words don't match");
		System.out.println(" [");
		for (int i = 0; i < a.wordsAndPunc().size(); i++) {
			final Rocketgum.Parsed.Token aw = a.wordsAndPunc().get(i);
			final Rocketgum.Parsed.Token bw = b.wordsAndPunc().get(i);
			if (aw.isPunc()) {
				if (!bw.isPunc())
					throw new AlgorithmException(aw + " is punc but " + bw + " is not");

				if (aw.string.equals(bw.string))
					System.out.println("  ['" + aw + "'],");
				else
					System.out.println("  ['" + aw + "', '" + bw + "'],");
				continue;
			} else if (bw.isPunc())
				throw new AlgorithmException(aw + " is not punc but " + bw + " is");

			if (aw.inDict && bw.inDict) {
				if (aw.string.equals(bw.string))
					System.out.println("  ['" + aw + "'],");
				else {

					final Path<String> path = new DijkstraPath<String>(mmg, aw.string, bw.string).getShortestPath();

					if (path == null)
						throw new NullPointerException("path from '" + aw + "' to '" + bw + "' is null");

					System.out.println("  ['" + Rocketgum.joiner.join(path) + "'],");
				}
			} else if (aw.string.equals(bw.string))
				System.out.println("  ['" + aw + "'],");
			else
				System.out.println("  ['" + aw.string + "', XXX, '" + bw.string + "'],");
		}
		System.out.println(" ],");
	}

	private static boolean hasPuncBefore(final int wordIndex, final Parsed[] parsed) {
		for (final Parsed parsed2 : parsed)
			if (!parsed2.getWord(wordIndex).spaceToLeft)
				return true;
		return false;
	}

	private static void insertPuncBefore(final int wordIndex, final Parsed[] parsed) {
		for (final Parsed parsed2 : parsed)
			parsed2.insertNullPuncBeforeWord(wordIndex);
	}

	private static boolean hasPuncAfter(final int wordIndex, final Parsed[] parsed) {
		for (final Parsed parsed2 : parsed)
			if (!parsed2.getWord(wordIndex).spaceToRight)
				return true;
		return false;
	}

	private static void insertPuncAfter(final int wordIndex, final Parsed[] parsed) {
		for (final Parsed parsed2 : parsed)
			parsed2.insertNullPuncAfterWord(wordIndex);
	}
}
