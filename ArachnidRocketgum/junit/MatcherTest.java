import java.util.regex.Pattern;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author afflux
 *
 */
public class MatcherTest extends TestCase {
	/**
	 *
	 */
	@Test
	public void testThingMatch() {
		final String pattern = " ([\\w']+) (\\d+)";
		Assert.assertTrue(Pattern.matches(pattern, " hello 1"));
		Assert.assertTrue(Pattern.matches(pattern, " h'ello 1"));
		Assert.assertFalse(Pattern.matches(pattern, "something bad"));
	}
}
