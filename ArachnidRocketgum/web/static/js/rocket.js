var initial = 15000;
var pause = 10000;
var delta = 100;

function faster() {
  if (delta > 1)
    delta >>= 1;
}

function slower() {
  if (delta < 10000000)
    delta <<= 1;
}

function longer() {
  if (pause < 1000000)
    pause <<= 1;
}

function shorter() {
  if (pause > 1)
    pause >>= 1;
}

function MWJ_findObj( oName, oFrame, oDoc ) {
  /* MWJ_findObj function got from
   * http://www.howtocreate.co.uk/tutorials/javascript/elementcontents
   *
   * if not working on a layer, document should be set to the
   * document of the working frame
   * if the working frame is not set, use the window object
   * of the current document
   * WARNING: - cross frame scripting will cause errors if
   * your page is in a frameset from a different domain */
  if( !oDoc ) { if( oFrame ) { oDoc = oFrame.document; } else { oDoc = window.document; } }

  //check for images, forms, layers
  if( oDoc[oName] ) { return oDoc[oName]; }

  //check for pDOM layers
  if( oDoc.all && oDoc.all[oName] ) { return oDoc.all[oName]; }

  //check for DOM layers
  if( oDoc.getElementById && oDoc.getElementById(oName) ) {
    return oDoc.getElementById(oName); }

  //check for form elements
  for( var x = 0; x < oDoc.forms.length; x++ ) {
    if( oDoc.forms[x][oName] ) { return oDoc.forms[x][oName]; } }

  //check for anchor elements
  //NOTE: only anchor properties will be available,
  //NOT link properties (in layers browsers)
  for( var x = 0; x < oDoc.anchors.length; x++ ) {
    if( oDoc.anchors[x].name == oName ) {
      return oDoc.anchors[x]; } }

  //check for any of the above within a layer in layers browsers
  for( var x = 0; document.layers && x < oDoc.layers.length; x++ ) {
    var theOb = MWJ_findObj( oName, null, oDoc.layers[x].document );
      if( theOb ) { return theOb; } }

  //check for frames, variables or functions
  if( !oFrame && window[oName] ) { return window[oName]; }
  if( oFrame && oFrame[oName] ) { return oFrame[oName]; }

  //if checking through frames, check for any of the above within
  //each child frame
  for( var x = 0; oFrame && oFrame.frames && x < oFrame.frames.length; x++ ) {
    var theOb = MWJ_findObj( oName, oFrame.frames[x], oFrame.frames[x].document ); if( theOb ) { return theOb; } }

  return null;
}

 var itersLeft = 0;
 var set = 0;

 function go() {
   var theOb = MWJ_findObj( 'your_mom' );
   for (i = 0; i < setSize; i++) {
     var newdiv = document.createElement('span');
     newdiv.setAttribute('id', 'thing_' + i);
     newdiv.setAttribute('class', margins[i]);
     newdiv.innerHTML = contents[0][i][0];
     theOb.appendChild(newdiv);
     wordsLeft[i] = contents[0][i].length - 1;
     itersLeft += wordsLeft[i];
   }
   setTimeout(iterate, initial);
 }

 function iterate() {
   if (itersLeft == 0) {
     set = (set + 1) % sets;
     for (i = 0; i < setSize; i++) {
       wordsLeft[i] = contents[set][i].length - 1;
       itersLeft += wordsLeft[i];
     }
     setTimeout(iterate, pause);
     return;
   }

   var ind = Math.floor(Math.random() * itersLeft);
   var i = findIndex(ind);
   var newdiv = MWJ_findObj('thing_' + i);

   var index = contents[set][i].length - wordsLeft[i];
   if (index <= 0) {
     alert("too small @" + i);
   }
   if (index >= contents[set][i].length) {
     alert("too big @" + i);
   }
   newdiv.innerHTML = contents[set][i][index];

   wordsLeft[i]--;
   itersLeft--;
   setTimeout(iterate, delta);
 }

 function findIndex(ind) {
   var count = wordsLeft[0];
   var i = 0;
   while (count <= ind) {
     i++;
     count += wordsLeft[i];
   }
   return i;
 }
